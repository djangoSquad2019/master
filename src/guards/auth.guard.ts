import { Injectable } from '@angular/core';
import {
    CanActivate,
    ActivatedRouteSnapshot,
    RouterStateSnapshot,
    Router
} from '@angular/router';
import { Observable } from 'rxjs';
import { SharedserviceService } from 'src/app/Services/sharedservice.service';

@Injectable({
    providedIn: 'root'
})
export class AuthGuard implements CanActivate {
    constructor(
        private router: Router,
        public sharedData: SharedserviceService
    ) {}
    canActivate(
        next: ActivatedRouteSnapshot,
        state: RouterStateSnapshot
    ): Observable<boolean> | Promise<boolean> | boolean {
        const EligibleCompetency = [
            'C7',
            'C8',
            'C9.1',
            'C9.2',
            'C10',
            'C11',
            'C12'
        ];
        // tslint:disable-next-line:max-line-length
            if (
                this.sharedData.roleIDOfLoggedInUser === '1' ||
                EligibleCompetency.includes(
                this.sharedData.competencyOfLoggedInUser
                )
                ) {
                return true;
                } else {
                this.router.navigate(['/dashboard']);
                return false;
                }
                }
        }


