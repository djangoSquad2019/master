import { Component } from '@angular/core';
import { AppComponent } from './app.component';
import { SharedserviceService } from './Services/sharedservice.service';
import { AdminServiceService } from './Modules/FetaureModules/admin/Services/admin-service.service';
import { UserDetailsModel } from './Models/UserDetailsModel';

@Component({
    selector: 'app-topbar',
    templateUrl: './app.topbar.component.html'
})
export class AppTopBarComponent {
    dialogValidMID = false;
    constructor(
        public app: AppComponent,
        public sharedData: SharedserviceService,
        public data: AdminServiceService
    ) {}

    DisplayFunnel = false; // Funnel and settings icon on the topbar
    nameOfUserTop = ''; // Name of user on the topbar
    midOfUserTop = ''; // Mid of user to get the image of user
    roleIDOfUser; // RoleID of user to get the role name
    competencyOfUser = ''; // Competency of user
    imgUrlHeader =
        'https://social.mindtree.com/User%20Photos/Profile%20Pictures/'; // Url for image of user
    imgUrlTail = '_LThumb.jpg';
    roleName: string; // To get the role name (User/Admin)
    Competency = ''; // To get the role name (User/Admin)
    GivenRole  = ''; // To get the role name (User/Admin)
    BillableStatus = ''; // To get the role name (User/Admin)
    Location = ''; // To get the role name (User/Admin)
    Vertical = ''; // To get the vertical name
    ProjectName = ''; // To get the name of project manager
    DeliveryManager = ''; // To get the name of delivery manager
    Title = ''; // To get the name of delivery manager

    // tslint:disable-next-line:use-life-cycle-interface
    ngOnInit() {
        const EligibleCompetency = [
            'C7',
            'C8',
            'C9.1',
            'C9.2',
            'C10',
            'C11',
            'C12'
        ];
       // Subscribing behavior Subject to get the project name of user. Created 'competency' as Behavior Subject in SharedserviceService.
       this.sharedData.ProjectName.subscribe(x => {
        this.ProjectName = x;
        this.sharedData.ProjectNameOfLoggedInUser = this.ProjectName;
    });
        // Subscribing behavior Subject to get the location of user. Created 'competency' as Behavior Subject in SharedserviceService.
        this.sharedData.Location.subscribe(x => {
            this.Location = x;
            this.sharedData.LocationOfLoggedInUser = this.Location;
        });

        // Subscribing behavior Subject to get the role of user. Created 'competency' as Behavior Subject in SharedserviceService.
        this.sharedData.GivenRole.subscribe(x => {
            this.GivenRole = x;
            this.sharedData.GivenRoleOfLoggedInUser = this.GivenRole;
        });

        // Subscribing behavior Subject to get the billable status of user.
        // Created 'competency' as Behavior Subject in SharedserviceService.
        this.sharedData.BillableStatus.subscribe(x => {
            this.BillableStatus = x;
            this.sharedData.BillableStatusOfLoggedInUser = this.BillableStatus;
        });
        // Subscribing behavior Subject to get the delivery manager of user.
        // Created 'competency' as Behavior Subject in SharedserviceService.
        this.sharedData.DeliveryManager.subscribe(x => {
            this.DeliveryManager = x;
            this.sharedData.DeliveryManagerOfLoggedInUser = this.DeliveryManager;
        });
        // Subscribing behavior Subject to get the vertical of user. Created 'competency' as Behavior Subject in SharedserviceService.
        this.sharedData.Vertical.subscribe(x => {
            this.Vertical = x;
            this.sharedData.VerticalOfLoggedInUser = this.Vertical;
        });
        // Subscribing behavior Subject to get the vertical of user. Created 'competency' as Behavior Subject in SharedserviceService.
        this.sharedData.Title.subscribe(x => {
            this.Title = x;
            this.sharedData.TitleOfLoggedInUser = this.Title;
        });
        // Subscribing behavior Subject to get the competency of user. Created 'competency' as Behavior Subject in SharedserviceService.
        this.sharedData.competency.subscribe(x => {
            this.competencyOfUser = x;
            this.sharedData.competencyOfLoggedInUser = this.competencyOfUser;
        });

        // Subscribing behavior Subject to get the name of user. Created 'name' as Behavior Subject in SharedserviceService.
        this.sharedData.name.subscribe(x => {
            this.nameOfUserTop = x;
            this.sharedData.nameOfLoggedInUser = this.nameOfUserTop;
            // this.competencyOfLoggedInUser=x.competency;
            // this.user=x.mid;
        });
         // Subscribing behavior Subject to get the name of user. Created 'name' as Behavior Subject in SharedserviceService.
         this.sharedData.name.subscribe(x => {
            this.nameOfUserTop = x;
            this.sharedData.nameOfLoggedInUser = this.nameOfUserTop;
                    });

        // Subscribing behavior Subject to get the mid of user. Created 'mid' as Behavior Subject in SharedserviceService.

        this.sharedData.mid.subscribe(x => {
            this.midOfUserTop = x;
            this.sharedData.midOfLoggedInUser = this.midOfUserTop;
        });

        // Subscribing behavior Subject to get the roleID of user. Created 'roleID' as Behavior Subject in SharedserviceService.

        this.sharedData.roleID.subscribe(x => {
            this.roleIDOfUser = x;
            this.sharedData.roleIDOfLoggedInUser = this.roleIDOfUser;
            if (
                this.roleIDOfUser === '1' ||
                EligibleCompetency.includes(this.competencyOfUser)
            ) {
                this.roleName = 'Admin';
            } else {
                this.roleName = 'User';
            }
            // if(this.roleIDOfUser!=null && this.competencyOfUser!=null)
            if (this.roleIDOfUser != null && this.competencyOfUser != null) {
                this.displayOfFunnel();
            }
        });

        // Subscribing behavior Subject to get the competency of user. Created 'competency' as Behavior Subject in SharedserviceService.
            this.sharedData.competency.subscribe(x => {
            this.competencyOfUser = x;
            this.sharedData.competencyOfLoggedInUser = this.competencyOfUser;
        });
    }

    test() {
        this.dialogValidMID = true;
    }
    // If roleIDOfUser and competencyOfUser is not null, then displayOfFunnel() is called.
    displayOfFunnel() {
        // if(this.roleIDOfLoggedInUser===1|| EligibleCompetency.includes(this.competencyOfLoggedInUser))

        // If roleID=1(Admin) and Competency >=C7, then Funnel icon(Select filters) and Settings icon(Save Preference) is visible.
        const EligibleCompetency = [
            'C7',
            'C8',
            'C9.1',
            'C9.2',
            'C10',
            'C11',
            'C12'
        ];
        if (
            this.roleIDOfUser === '1' ||
            EligibleCompetency.includes(this.competencyOfUser)
        ) {
            this.DisplayFunnel = true;
        } else {
            this.DisplayFunnel = false;
        }
    }
}
