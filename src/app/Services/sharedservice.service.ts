import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject } from 'rxjs';
import { JwtHelperService } from '@auth0/angular-jwt';
import * as CryptoJS from 'crypto-js';
import { UserDetailsModel } from '../Models/UserDetailsModel';


@Injectable({
    providedIn: 'root'
})
export class SharedserviceService {
    // Declaring Behavior Subject//
    name = new BehaviorSubject(null);
    competency = new BehaviorSubject(null);
    mid = new BehaviorSubject(null);
    roleID = new BehaviorSubject(null);
    DeliveryManager = new BehaviorSubject(null);
    ProjectName = new BehaviorSubject(null);
    GivenRole = new BehaviorSubject(null);
    Location = new BehaviorSubject(null);
    BillableStatus = new BehaviorSubject(null);
    Vertical = new BehaviorSubject(null);
    Title = new BehaviorSubject(null);
    token = new BehaviorSubject(null);
    secretKey = 'dsqwertuujexn4t34veem8ijtffd4vyn';
    parsedSecretKey = CryptoJS.enc.Utf8.parse(this.secretKey);
    IV = '45dm3pufjr2fdmwk';
    parsedIV = CryptoJS.enc.Utf8.parse(this.IV);

    // Declaring variables to be used in every component//

    nameOfLoggedInUser: string;
    midOfLoggedInUser: string;
    roleIDOfLoggedInUser: string;
    competencyOfLoggedInUser: string;
    tokenIDOfLoggedUser: string;
    GivenRoleOfLoggedInUser: string;
    LocationOfLoggedInUser: string;
    ProjectNameOfLoggedInUser: string;
    BillableStatusOfLoggedInUser: string;
    DeliveryManagerOfLoggedInUser: string;
    VerticalOfLoggedInUser: string;
    TitleOfLoggedInUser: string;

    constructor(private http: HttpClient) {}

    // Function to get the logged in user details
    showUserDetails(data) {
        this.competency.next(data[0]);
        this.mid.next(data[1]);
        this.name.next(data[2]);
        this.token.next(data[3]);
        this.roleID.next(data[4]);
    }

    // Function to get the logged in user details
    showProfileDetails(data) {
        this.ProjectName.next(data[0]);
        this.Location.next(data[1]);
        this.GivenRole.next(data[2]);
        this.BillableStatus.next(data[3]);
        this.DeliveryManager.next(data[4]);
        this.Vertical.next(data[5]);
        this.Title.next(data[6]);
    }

    // // Function to get the role ID of user
    // showRoleName(data) {
    //     this.roleID.next(data);
    // }
encryptingData(data) {
    const encryptedData = CryptoJS.AES.encrypt(data, this.parsedSecretKey, {
        keySize: 256 / 8,
        iv: this.parsedIV,
        mode: CryptoJS.mode.CBC,
        padding: CryptoJS.pad.Pkcs7
    }).toString();
        return encryptedData;
    }
decryptingData(data) {
    const decryptedData = CryptoJS.AES.decrypt(data, this.parsedSecretKey, {
        keySize: 256 / 8,
        iv: this.parsedIV,
        mode: CryptoJS.mode.CBC,
        padding: CryptoJS.pad.Pkcs7
    }).toString(CryptoJS.enc.Utf8);
    return decryptedData;
}
}
