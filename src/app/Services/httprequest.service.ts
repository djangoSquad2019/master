import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, timer } from 'rxjs';
import { environment } from '../../environments/environment';
import { Filter } from '../Models/filterModel';
import { CapacityData } from '../Models/CapacityDataModel';
import { PreferenceData } from '../Models/PreferenceModel';
import { SharedserviceService } from './sharedservice.service';


import {shareReplay, tap , switchMap } from 'rxjs/operators';

@Injectable({
    providedIn: 'root'
})
export class HTTPRequestService {
    readonly urlHost: string = environment.baseUrl;
    public  allCapacityData: CapacityData[ ];
    capacityDatacache$: Observable<any>;
    constructor(
        private http: HttpClient,
        public sharedData: SharedserviceService
    ) {}
    public storeCpacityData(data: CapacityData[]) {
        this.allCapacityData = data;
    }
    public GetCapacitydata() {
        return this.allCapacityData;
    }
    // Generic post Method to call external API
    public post<T>(url: string, data: string): Observable<T> {
        const token = this.sharedData.tokenIDOfLoggedUser;
        // tslint:disable-next-line:max-line-length
        const header: HttpHeaders = new HttpHeaders()
            .set('Content-Type', 'application/json')
            .set('Access-Control-Allow-Origin', '*')
            .set('Authorization', 'Bearer ' + token);
        const apiEndpoint = `${this.urlHost}${url}`;

        return this.http.post<T>(apiEndpoint, data, {
            headers: header
        });
    }

    // Generic post Method to call external API
    public postFile<T>(
        url: string,
        data: FormData,
        httpHeader: HttpHeaders
    ): Observable<T> {
        const apiEndpoint = `${this.urlHost}${url}`;
        return this.http.post<T>(apiEndpoint, data, { headers: httpHeader });
    }

    // Generic get Method to call external API
    public get<T>(url: string, httpHeader?: HttpHeaders): Observable<T> {
        return this.http.get<T>(`${this.urlHost}${url}`, {
            headers: httpHeader
        });
    }

    // Generic post Method to call external API
    public getAPI<T>(url: string): Observable<T> {
        const token = this.sharedData.tokenIDOfLoggedUser;
        // tslint:disable-next-line:max-line-length
        const header: HttpHeaders = new HttpHeaders()
            .set('Content-Type', 'application/json')
            .set('Access-Control-Allow-Origin', '*')
            .set('Authorization', 'Bearer ' + token);

        return this.http.get<T>(this.urlHost + url, { headers: header });
    }

    public put<T>(
        url: string,
        data: string,
        httpHeader: HttpHeaders
    ): Observable<T> {
        return this.http.put<T>(`${this.urlHost}${url}`, data, {
            headers: httpHeader
        });
    }
    public putFile(
        url: string,
        data: FormData,
        httpHeader: HttpHeaders
    ): Observable<any> {
        return this.http.put(`${this.urlHost}${url}`, data, {
            headers: httpHeader
        });
    }
    public delete(url: string, httpHeader?: HttpHeaders): Observable<any> {
        return this.http.delete(`${this.urlHost}${url}`, {
            headers: httpHeader
        });
    }
    // public getCapacityData<T>(url: string): Observable<T> {
    //     const token = this.sharedData.tokenIDOfLoggedUser;
    //     const header: HttpHeaders = new HttpHeaders().set(
    //         'Authorization',
    //         'Bearer ' + token
    //     );
    //     return this.http.get<T>(this.urlHost + url, { headers: header });
    // }
    public getCapacityData<T>(url: string): Observable<any> {
        if ( !this.capacityDatacache$) {
        const timer$ = timer(0, 900000);
        this.capacityDatacache$ = timer$.pipe(
            switchMap( _ => this.GetCapacityDataFromAPI(url)),
            shareReplay(1)
        );
        return this.capacityDatacache$;
        }
        return this.capacityDatacache$ ;

    }
   public GetCapacityDataFromAPI(url): Observable<any> {
    const token = this.sharedData.tokenIDOfLoggedUser;
    const header: HttpHeaders = new HttpHeaders().set(
        'Authorization',
        'Bearer ' + token
        );

        return this.http.get<CapacityData[]>(this.urlHost + url, { headers: header }).pipe(
            tap(
            )
        );

   }

    public getPreferenceData<T>(url: string): Observable<T> {
        const token = this.sharedData.tokenIDOfLoggedUser;
        const header: HttpHeaders = new HttpHeaders().set(
            'Authorization',
            'Bearer ' + token
        );
        return this.http.get<T>(this.urlHost + url, { headers: header });
    }

    // tslint:disable-next-line:no-shadowed-variable
    public getFilteredCapacityData<CapacityData>(
        url: string,
        filterData: Filter
    ): Observable<CapacityData> {
        const token = this.sharedData.tokenIDOfLoggedUser;
        // tslint:disable-next-line:max-line-length
        const header: HttpHeaders = new HttpHeaders()
            .set('Content-Type', 'application/json')
            .set('Access-Control-Allow-Origin', '*')
            .set('Authorization', 'Bearer ' + token);
        return this.http.post<CapacityData>(this.urlHost + url, filterData, {
            headers: header
        });
    }

    public getFilters<T>(url: string): Observable<T> {
        const token = this.sharedData.tokenIDOfLoggedUser;
        const header: HttpHeaders = new HttpHeaders().set(
            'Authorization',
            'Bearer ' + token
        );
        return this.http.get<T>(this.urlHost + url, { headers: header });
    }
    public getFiltersOnSelectedFilters<T>(
        url: string,
        filter: Filter
    ): Observable<T> {
        const token = this.sharedData.tokenIDOfLoggedUser;
        // tslint:disable-next-line:max-line-length
        const header: HttpHeaders = new HttpHeaders()
            .set('Content-Type', 'application/json')
            .set('Access-Control-Allow-Origin', '*')
            .set('Authorization', 'Bearer ' + token);
        return this.http.post<T>(this.urlHost + url, filter, {
            headers: header
        });
    }

    // tslint:disable-next-line:no-shadowed-variable
    public savePreference<CapacityData>(
        url: string,
        savePref: PreferenceData
    ): Observable<CapacityData> {
        const token = this.sharedData.tokenIDOfLoggedUser;
        // tslint:disable-next-line:max-line-length
        const header: HttpHeaders = new HttpHeaders()
            .set('Content-Type', 'application/json')
            .set('Access-Control-Allow-Origin', '*')
            .set('Authorization', 'Bearer ' + token);
        return this.http.post<CapacityData>(this.urlHost + url, savePref, {
            headers: header
        });
    }
}
