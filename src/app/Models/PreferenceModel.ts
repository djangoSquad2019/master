/* A Users stored preference will be retrieved through API
and binded to an object of this model */

import { Filter } from 'src/app/Models/FilterModel';
export class PreferenceData {
    mid: string;
    filterName?: string;
    preferences?: Filter;
    uploadedDate: Date;
    uploadedby: string;
    DefaultPreference: boolean;
    constructor(values: Object = {}) {
        Object.assign(this, values);
    }
}
