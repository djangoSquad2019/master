/*We mainly use the below 14 columns from capacity master table.
This model will allow us to extensievly work on the below 14 columns of capacity report */

export class CapacityData {
        EmployeeID: string;
        Name: string;
        Competency: string;
        ProjectName: string;
        GivenRole: string;
        Location: string;
        OnOffShore: string;
        BillableStatus: string;
        Vertical: string;
        SubVertical: string;
        Practice: string;
        SubPractice: string;
        ProfitCenter: string;
        Gender: string;
        ProjectManager: string;
        DeliveryManger: string ;
        EngagementType: string ;
        AccountName: string ;
      }
