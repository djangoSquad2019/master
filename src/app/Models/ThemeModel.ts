export class ThemeData {
    mid: string;
    theme: string;
    constructor(values: Object = {}) {
        Object.assign(this, values);
    }
}