export class SelectedItemsModel {
    label: any;
    value: any;

    constructor(values: Object = {}) {
        Object.assign(this, values);
    }
}
