/*The capacity data can be filtered based on any of the below 14 parameters.
Each individual parameter consists of multiple filter options. Hence they are considered as a string of array */

export class Filter {
     deliveryPartner: string[];
     billableStatus: string [];
     onOffshore: string[];
     gender: string[];
     sRStatus: string[];
     profitCenter: string[];
     location: string[];
     accountGroup: string[];
     deliveryManager: string[];
     competencyCode: string[];
     engagementCode: string[];
     projectName: string[];
     practice: string[];
    constructor(values: Object = {}) {
    Object.assign(this, values);
    }
    }
