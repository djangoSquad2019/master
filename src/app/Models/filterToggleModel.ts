/* To display a list of all avialble filter options */


export class FilterToggle {
    DeliveryPlatform: boolean;
    Competency: boolean;
    BillableStatus: boolean;
    Location: boolean;
    Gender: boolean;
    Practice: boolean;
    OnOffShore: boolean;
    SRStatus: boolean;
    AccountGroup: boolean;
    DeliveryManager: boolean;
    ProjectName: boolean;
    EngagementType: boolean;
    ProfitCenter: boolean;
    constructor(values: Object = {}) {
        Object.assign(this, values);
    }
}
