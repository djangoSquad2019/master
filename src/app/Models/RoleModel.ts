/* Model to store user role */

export class Role {
    public MID: string;
    public Role: string;
    public MidOfLoggedInUser: string;
}
