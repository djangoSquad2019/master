/* A model to bind user information.
This is a temporary one until we get the AAD setup */

export class UserDetailsModel {
    employeeID: string;
    name: string;
    competency: string;
    token: string;
    role: number;
}
