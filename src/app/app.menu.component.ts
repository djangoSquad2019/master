import {
    Component,
    Input,
    OnInit,
    AfterViewInit,
    ViewChild
} from '@angular/core';
import {
    trigger,
    state,
    style,
    transition,
    animate
} from '@angular/animations';
import { Location } from '@angular/common';
import { Router } from '@angular/router';
import { MenuItem, ScrollPanel } from 'primeng/primeng';
import { AppComponent } from './app.component';
import { ThemeData } from 'src/app/Models/ThemeModel';
import { DropdownModule } from 'primeng/dropdown';
import { SharedserviceService } from 'src/app/Services/sharedservice.service';
import { HTTPRequestService } from 'src/app/Services/httprequest.service';

@Component({
    selector: 'app-menu',
    templateUrl: './app.menu.component.html'
})
export class AppMenuComponent implements OnInit, AfterViewInit {
    @Input() reset: boolean;

    model: any[];
    theme: ThemeData;

    @ViewChild('layoutMenuScroller') layoutMenuScrollerViewChild: ScrollPanel;

    constructor(
        public app: AppComponent,
        private route: Router,
        public sharedData: SharedserviceService,
        private data: HTTPRequestService,
    ) {}
    roleIDOfUser;
    competencyOfUser;
    ngOnInit() {
        // Subscribing to get the roleID of User
        this.sharedData.roleID.subscribe(x => {
            this.roleIDOfUser = x;
            // if (this.roleIDOfUser != null && this.competencyOfUser != null) {
            //     this.displayTabs();
            // }
        });
        this.sharedData.competency.subscribe(x => {
            this.competencyOfUser = x;
        });
        if (this.roleIDOfUser != null && this.competencyOfUser != null) {
              this.displayTabs();
             }
             //getAPI to get the user saved theme
             this.data
        .getAPI('/api/Theme/GetTheme?MID='+ this.sharedData.midOfLoggedInUser)
        .subscribe(
                (resData: ThemeData) => {
                    this.theme = resData;
                    console.log(this.theme);
                      //this.GetPrefdata();
                      const layoutLink: HTMLLinkElement = <HTMLLinkElement>(
                        document.getElementById('layout-css')
                    );
                    layoutLink.href = 'assets/layout/css/layout-' + this.theme.theme + '.css';
                  }
               
            ,
            (error: any) => {
                if (error.status === 0) {
                    this.route.navigate(['/core/ServerDown']);
                } else if (error.status === 401) {
                    this.route.navigate(['/core/AccessDenied']);
            } else {
                    this.route.navigate(['/core/InternalServerError']);
                }
            }
        );
            
    }
    displayTabs() {
        const EligibleCompetency = [
            'C7',
            'C8',
            'C9.1',
            'C9.2',
            'C10',
            'C11',
            'C12'
        ];
        // If roleID=1 and competency>=C7  , then display 'CapacityReport' and 'Admin' tab else hide the tabs
        if (
            this.sharedData.roleIDOfLoggedInUser === '1' ||
            EligibleCompetency.includes(
                this.sharedData.competencyOfLoggedInUser
            )
        ) {
            this.model = [
                {
                    label: 'Capacity Report',
                    icon: 'fa fa-fw fa-home',
                    command: event => this.route.navigate(['/CapacityInsights'])
                },

                {
                    label: 'Dashboard',
                    icon: 'fa fa-fw fa-pie-chart',
                    command: event => this.route.navigate(['/dashboard'])
                },

                {
                    label: 'Admin',
                    icon: 'fa fa-fw fa-lock',
                    // items: [
                    //     {label: 'Add an admin', icon: 'fa fa-fw fa-user-plus',
                    command: event => this.route.navigate(['/admin'])
                    //     },
                    // ]
                },
                {
                    label: 'Layout Colors',
                    icon: 'fa fa-fw fa-magic',
                    items: [
                        {
                            label: 'Flat',
                            icon: 'fa fa-fw fa-circle',
                            badge: 3,
                            badgeStyleClass: 'blue-badge',
                            items: [
                                {
                                    label: 'Dark',
                                    icon: 'fa fa-fw fa-paint-brush',
                                    command: event => {
                                        this.changeLayout('dark');
                                    }
                                },
                                {
                                    label: 'Blue',
                                    icon: 'fa fa-fw fa-paint-brush',
                                    command: event => {
                                        this.changeLayout('blue');
                                    }
                                },
                                {
                                    label: 'Blue Grey',
                                    icon: 'fa fa-fw fa-paint-brush',
                                    command: event => {
                                        this.changeLayout('bluegrey');
                                    }
                                }
                            ]
                        },
                        {
                            label: 'Special',
                            icon: 'fa fa-fw fa-fire',
                            badge: 1,
                            badgeStyleClass: 'blue-badge',
                            items: [
                                {
                                    label: 'Cosmic',
                                    icon: 'fa fa-fw fa-paint-brush',
                                    command: event => {
                                        this.changeLayout('cosmic');
                                    }
                                }
                            ]
                        }
                    ]
                }
            ];
        } else {
            this.model = [
                {
                    label: 'Dashboard',
                    icon: 'fa fa-fw fa-pie-chart',
                    command: event => this.route.navigate(['/dashboard'])
                },

                {
                    label: 'Layout Colors',
                    icon: 'fa fa-fw fa-magic',
                    items: [
                        {
                            label: 'Flat',
                            icon: 'fa fa-fw fa-circle',
                            badge: 3,
                            badgeStyleClass: 'blue-badge',
                            items: [
                                {
                                    label: 'Dark',
                                    icon: 'fa fa-fw fa-paint-brush',
                                    command: event => {
                                        this.changeLayout('dark');
                                    }
                                },
                                {
                                    label: 'Blue',
                                    icon: 'fa fa-fw fa-paint-brush',
                                    command: event => {
                                        this.changeLayout('blue');
                                    }
                                },
                                {
                                    label: 'Blue Grey',
                                    icon: 'fa fa-fw fa-paint-brush',
                                    command: event => {
                                        this.changeLayout('bluegrey');
                                    }
                                }
                            ]
                        },
                        {
                            label: 'Special',
                            icon: 'fa fa-fw fa-fire',
                            badge: 1,
                            badgeStyleClass: 'blue-badge',
                            items: [
                                {
                                    label: 'Cosmic',
                                    icon: 'fa fa-fw fa-paint-brush',
                                    command: event => {
                                        this.changeLayout('cosmic');
                                    }
                                }
                            ]
                        }
                    ]
                }
            ];
        }
    }

    ngAfterViewInit() {
        setTimeout(() => {
            this.layoutMenuScrollerViewChild.moveBar();
        }, 100);
    }

    changeTheme(theme) {
        debugger;
        const themeLink: HTMLLinkElement = <HTMLLinkElement>(
            document.getElementById('theme-css')
        );
        themeLink.href = 'assets/theme/theme-' + theme + '.css';
    }
    changeLayout(layout) {
        //this.theme.MID='M1018494';
        //this.theme.Theme=layout;
        const saveTheme = new ThemeData({
            mid: this.sharedData.midOfLoggedInUser,
            theme:layout})
            console.log(saveTheme);
        const layoutLink: HTMLLinkElement = <HTMLLinkElement>(
            document.getElementById('layout-css')
        );
        layoutLink.href = 'assets/layout/css/layout-' + layout + '.css';

        this.data
        .post(
            '/api/Theme/SaveTheme',
            JSON.stringify(saveTheme)
        )
        .subscribe(
            success => {
               
            },
            (error: any) => {
                if (error.status === 0) {
                    this.route.navigate(['/core/ServerDown']);
                } else if (error.status === 401) {
                    this.route.navigate(['/core/AccessDenied']);
            } else {
                    this.route.navigate(['/core/InternalServerError']);
                }
            }
        );
    }

    onMenuClick() {
        if (!this.app.isHorizontal()) {
            setTimeout(() => {
                this.layoutMenuScrollerViewChild.moveBar();
            }, 450);
        }

        this.app.onMenuClick();
    }
}
@Component({
    /* tslint:disable:component-selector */
    selector: '[app-submenu]',
    /* tslint:enable:component-selector */
    template: `
        <ng-template
            ngFor
            let-child
            let-i="index"
            [ngForOf]="root ? item : item.items"
        >
            <li [ngClass]="{ 'active-menuitem': isActive(i) }">
                <a
                    [href]="child.url || '#'"
                    (click)="itemClick($event, child, i)"
                    *ngIf="!child.routerLink"
                    [attr.tabindex]="!visible ? '-1' : null"
                    [attr.target]="child.target"
                    (mouseenter)="onMouseEnter(i)"
                >
                    <i [ngClass]="child.icon"></i>
                    <span>{{ child.label }}</span>
                    <i
                        class="fa fa-fw fa-angle-down layout-submenu-toggler"
                        *ngIf="child.items"
                    ></i>
                    <span
                        class="menuitem-badge"
                        *ngIf="child.badge"
                        [ngClass]="child.badgeStyleClass"
                        >{{ child.badge }}</span
                    >
                </a>

                <a
                    (click)="itemClick($event, child, i)"
                    *ngIf="child.routerLink"
                    [routerLink]="child.routerLink"
                    routerLinkActive="active-menuitem-routerlink"
                    [routerLinkActiveOptions]="{ exact: true }"
                    [attr.tabindex]="!visible ? '-1' : null"
                    [attr.target]="child.target"
                    (mouseenter)="onMouseEnter(i)"
                >
                    <i [ngClass]="child.icon"></i>
                    <span>{{ child.label }}</span>
                    <i class="fa fa-fw fa-angle-down" *ngIf="child.items"></i>
                    <span
                        class="menuitem-badge"
                        *ngIf="child.badge"
                        [ngClass]="child.badgeStyleClass"
                        >{{ child.badge }}</span
                    >
                </a>
                <ul
                    app-submenu
                    [item]="child"
                    *ngIf="child.items"
                    [visible]="isActive(i)"
                    [reset]="reset"
                    [parentActive]="isActive(i)"
                    [@children]="isActive(i) ? 'visible' : 'hidden'"
                ></ul>
            </li>
        </ng-template>
    `,
    animations: [
        trigger('children', [
            state(
                'visible',
                style({
                    height: '*'
                })
            ),
            state(
                'hidden',
                style({
                    height: '0px'
                })
            ),
            transition(
                'visible => hidden',
                animate('400ms cubic-bezier(0.86, 0, 0.07, 1)')
            ),
            transition(
                'hidden => visible',
                animate('400ms cubic-bezier(0.86, 0, 0.07, 1)')
            )
        ])
    ]
})
export class AppSubMenuComponent {
    @Input() item: MenuItem;

    @Input() root: boolean;

    @Input() visible: boolean;

    _reset: boolean;

    _parentActive: boolean;

    activeIndex: number;

    hover: boolean;

    constructor(
        public app: AppComponent,
        public router: Router,
        public location: Location,
        public appMenu: AppMenuComponent
    ) {}

    itemClick(event: Event, item: MenuItem, index: number) {
        if (this.root) {
            this.app.menuHoverActive = !this.app.menuHoverActive;
        }

        // avoid processing disabled items
        if (item.disabled) {
            event.preventDefault();
            return true;
        }

        // activate current item and deactivate active sibling if any
        if (item.routerLink || item.items || item.command || item.url) {
            this.activeIndex = this.activeIndex === index ? null : index;
        }

        // execute command
        if (item.command) {
            item.command({ originalEvent: event, item: item });
        }

        // prevent hash change
        if (item.items || (!item.url && !item.routerLink)) {
            setTimeout(() => {
                this.appMenu.layoutMenuScrollerViewChild.moveBar();
            }, 450);
            event.preventDefault();
        }

        // hide menu
        if (!item.items) {
            if (this.app.menuMode === 'horizontal') {
                this.app.resetMenu = true;
            } else {
                this.app.resetMenu = false;
            }
            if (
                this.app.isMobile() ||
                this.app.menuMode === 'overlay' ||
                this.app.menuMode === 'popup'
            ) {
                this.app.menuActive = false;
            }

            this.app.menuHoverActive = false;
        }
    }

    onMouseEnter(index: number) {
        if (
            this.root &&
            this.app.menuHoverActive &&
            this.app.isHorizontal() &&
            !this.app.isMobile() &&
            !this.app.isTablet()
        ) {
            this.activeIndex = index;
        }
    }

    isActive(index: number): boolean {
        return this.activeIndex === index;
    }

    @Input() get reset(): boolean {
        return this._reset;
    }

    set reset(val: boolean) {
        this._reset = val;

        if (this._reset && this.app.menuMode === 'horizontal') {
            this.activeIndex = null;
        }
    }

    @Input() get parentActive(): boolean {
        return this._parentActive;
    }

    set parentActive(val: boolean) {
        this._parentActive = val;

        if (!this._parentActive) {
            this.activeIndex = null;
        }
    }
}
