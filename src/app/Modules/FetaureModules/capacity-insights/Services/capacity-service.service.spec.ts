import { TestBed } from '@angular/core/testing';

import { CapacityServiceService } from './capacity-service.service';

describe('CapacityServiceService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: CapacityServiceService = TestBed.get(CapacityServiceService);
    expect(service).toBeTruthy();
  });
});
