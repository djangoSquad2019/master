import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { FilterToggle } from '../../../../Models/filterToggleModel';
import { Filter } from 'src/app/Models/FilterModel';
import { BehaviorSubject } from 'rxjs';
import { PreferenceData } from 'src/app/Models/PreferenceModel';

@Injectable({
  providedIn: 'root'
})
export class CapacityServiceService {
  filtersapplied = new FilterToggle({
    DeliveryPlatform : true,
    Competency : true,
    BillableStatus : true,
    Location : true,
    Gender : true,
    Practice : true,
    OnOffShore : true,
    SRStatus : true,
    AccountGroup : true,
    DeliveryManager : true,
    ProjectName : true,
    EngagementType : true,
    ProfitCenter : true,
  });
  FiltersApplied = new BehaviorSubject(this.filtersapplied);
  PrefNameAndFilter = new BehaviorSubject(null);
  selectedPrefrence: Filter;
  SelectedPrefrence = new BehaviorSubject(null);
  SavedPreference = new BehaviorSubject(null);
  selectedPref: string;
  PrefFilter: PreferenceData = new PreferenceData();
  constructor(private http: HttpClient) { }

// Function to set the filters applied
setFiltersApplied(Filtersapplied: FilterToggle ) {
  this.filtersapplied = Filtersapplied;
  this.FiltersApplied.next(this.filtersapplied);
}

// Function to get the filters applied
getFiltersApplied() {
  return this.filtersapplied;
}

// Function to set the saved preference
// setSavedPrefrence(selectedValue) {
//    this.selectedPrefrence = selectedValue;
//    this.SavedPreference.next(selectedValue);
// }

// Function to get the saved preference
getSavedPrefrence() {
 return this.selectedPrefrence;
}

// Function to set the selected preference
setSelectedPref(selectedPref: string, filterObj: Filter) {
  this.PrefFilter.filterName = selectedPref;
  this.PrefFilter.preferences = filterObj;
  this.SelectedPrefrence.next(this.PrefFilter);
}

// Function to set the selected preference
clearSetSelectedPref(selectedPref: string, filterObj: Filter) {
  this.PrefFilter.filterName = selectedPref;
  this.PrefFilter.preferences = filterObj;
  this.SelectedPrefrence.next(this.PrefFilter);
}

// Function to get the selected preference
getSelectdPref() {
  return this.selectedPref;
}

}
