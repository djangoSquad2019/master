import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { FilterComponent } from './filter/filter.component';
import { PreferencesComponent } from './preferences/preferences.component';
import { CapacityReportComponent } from './capacity-report/capacity-report.component';
import { AuthGuard } from '../../../../guards/auth.guard';
// the routes array is feeded with routes within the components in Capacity module
const routes: Routes = [
    {
        path: '',
        component: CapacityReportComponent,
        children: [
            { path: 'filter', component: FilterComponent },
            { path: 'preference', component: PreferencesComponent }
        ]
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class CapacityInsightsRoutingModule {}
