import { Component, OnInit, OnDestroy, ViewChild } from '@angular/core';
import { CapacityServiceService } from '../Services/capacity-service.service';
import { CapacityData } from 'src/app/Models/CapacityDataModel';
import { SelectedItemsModel } from 'src/app/Models/SelectedItemsModel';
import { FilterToggle } from 'src/app/Models/filterToggleModel';
import { Filter } from 'src/app/Models/FilterModel';
import { HTTPRequestService } from 'src/app/Services/httprequest.service';
// import { Message } from 'primeng/primeng';
import { Message } from 'primeng/components/common/api';
import { MessageService } from 'primeng/components/common/messageservice';
import { TouchSequence } from 'selenium-webdriver';
import { LazyLoadEvent, SortEvent } from 'primeng/api';
import { SharedserviceService } from 'src/app/Services/sharedservice.service';
import {
    FormControl,
    FormGroup,
    FormBuilder,
    Validators
} from '@angular/forms';
import { PreferenceData } from 'src/app/Models/PreferenceModel';
import { element } from '@angular/core/src/render3/instructions';
import { HttpHeaders } from '@angular/common/http';
import { Table } from 'primeng/components/table/table';
import { Router } from '@angular/router';
import { THIS_EXPR } from '@angular/compiler/src/output/output_ast';

@Component({
    selector: 'app-capacity-report',
    templateUrl: './capacity-report.component.html',
    styleUrls: ['./capacity-report.component.css'],
    providers: [MessageService]
})
export class CapacityReportComponent implements OnInit {
    @ViewChild('mySelect') mySelect;
    @ViewChild('msg') messageBox;
    @ViewChild('dt') tableComponent: Table;
    SelectedColumns = new FormControl();
    msgsuccess: Message[] = [];
    show = false;
    preferenceName = ' ';
    displaydialog = false;
    display = false;
    displayDP = false;
    displayG = false;
    displayOnOff = false;
    displayBilable = false;
    displaySR = false;
    displayPC = false;
    displayL = false;
    displayAN = false;
    displayDM = false;
    displayC = false;
    displayET = false;
    displayPN = false;
    displayP = false;
    overlaybasicDeliveryP = false;
    showAllBillable = false;
    showAllpractice = false;
    showAllAccountName = false;
    showAllProjectName = false;
    showAllLocation = false;
    showAllSRStatus = false;
    showAllProfitCenter = false;
    showAllDeliveryManager = false;
    showAllEngagementType = false;
    totalRecords: number;
    selectedFilter: any;
    clearSelectedPreference: null;
    messagePreference = '';
    confirmationForm: FormGroup;
    submitted = false;
    prefName: string;
    isPrefSet = false;
    msgdialog: Boolean = false;
    BillableStatusOptions: SelectedItemsModel[];
    capacityData: CapacityData[];
    checked = false;
    columns: any[];
    SearchResult = false;
    // Declaration of filter arrays
    // tslint:disable-next-line:max-line-length
    DeliveryPartner = [];
    Billable = [];
    ProfitCenterName = [];
    Location = [];
    AccountName = [];
    DeliveryManager = [];
    Competency = [];
    ProjectName = [];
    Practice = [];
    DefaultColumns = [];
    DeliveryPartnerArray = [];
    BillableArray = [];
    ProfitCenterNameArray = [];
    LocationArray = [];
    AccountNameArray = [];
    DeliveryManagerArray = [];
    CompetencyArray = [];
    ProjectNameArray = [];
    PracticeArray = [];
    filterapplied: FilterToggle;
    // Declaration of filter arrays for selected option storing
    // tslint:disable-next-line:max-line-length
    DeliveryPartnerS = [];
    DeliveryManagerS  = [];
    AccountNameS = [];
    ProfitCenterNameS = [];
    ProjectNameS = [];
    PracticeS = [];
    BillableS = [];
    LocationS = [];
    SRStatusS = [];
    EngagementTypeS = [];
    selectedCompetencies = [];
    selectedGender = [] ;
    selectedOnOffshore = [] ;
    selectedSRStatus = [];
    selectedEngagementType = [];
    genderArray = [] ;
    onOffshoreArray = [] ;
    srStatusArray = [];
    engagementTypeArray = [];
    ColumnSelections = [];
    testbool = false;
    step = 0;
    loading: boolean;
    filterLoading = true;
    isDefaultPref = false;
    competency: SelectedItemsModel[];
    selectedColumns = [];
    pageLoading = false;
    haveRows = true;
    isPrefName = true;
    isEmpty = false;
    defaultPref: PreferenceData;
    mid: string;
    defaultvalue = ' ';
    isSavePrefOn = true;
    isFilter = true;
    isFetch: boolean;
    loadProfitCenterCard = false;
    loadDeliveryManagerCard = false;
    loadDeliveryPartnerCard = false;
    loadAccountNamesCard = false;
    loadProjectsCard = false;
    loadPracticesCard = false;
    loadBillableStatusCard = false;
    loadSRStatusCard = false;
    loadEngagementTypeCard = false;
    loadLocationCard = false;
    SearchKey: string;
    selPref: string;
    filters: Filter;
    MaleFilterButtonClick = false;
    FemaleFilterButtonClick = false;
    OnShoreFilterButtonClick = false;
    OffShoreFilterButtonClick = false;
    allCapacityData: any [];
    tableObject: any ;
    prefFlag = false;
    dataPreferences: Filter;
    ViewOptions() {
        this.mySelect.open();
    }
    // this method is used to reset data table
    reset() {

        this.selectedCompetencies = [];
        this.columns = this.DefaultColumns;
        this.ResetFilterselections( );
        this.SearchKey = ' ';
        this.FetchFilteredCapacityData();
    }
    // on click of enter in glbal filter this method will fetch results from db
    FetchResultFromDB(datatable) {
        this.loading = true;
        // Start of Reset all the table settings
        // End of Reset all the table settings
        datatable.reset();
        this.tableObject = datatable ;
        datatable.value = null;
        const token = this.sharedData.tokenIDOfLoggedUser;
        // tslint:disable-next-line:max-line-length
        const header: HttpHeaders = new HttpHeaders()
            .set('Content-Type', 'application/json')
            .set('Access-Control-Allow-Origin', '*')
            .set('Authorization', 'Bearer ' + token);
        this.data
            .get(
                '/api/capacity/GetGlobalSearchResult?key=' + this.SearchKey,
                header
            )
            .subscribe(
                (SearchResultdata: CapacityData[]) => {
                    this.capacityData = SearchResultdata;
                    this.totalRecords = SearchResultdata.length;
                    this.isDefaultPref = false;
                    this.loading = false;
                    if (SearchResultdata.length === 0) {
                        this.SearchResult = true;
                    } else {
                        this.SearchResult = false;
                    }
                    this.ResetFilterselections();
                    this.getFilterOptions(SearchResultdata);
                },
                (error: any) => {
                    if (error.status === 0) {
                        this.route.navigate(['/core/ServerDown']);
                    } else if (error.status === 401) {
                        this.route.navigate(['/core/AccessDenied']);
                } else {
                        this.route.navigate(['/core/InternalServerError']);
                    }
                }
            );
    }

    // this is used to reset all filter array's
    ResetFilterselections() {
        // tslint:disable-next-line:max-line-length
        this.DeliveryPartnerArray = [];
        this.genderArray = [];
        this.BillableArray = [];
        this.srStatusArray = [];
        this.ProfitCenterNameArray = [];
        this.LocationArray = [];
        this.AccountNameArray = [];
        this.DeliveryManagerArray = [];
        this.CompetencyArray = [];
        this.ProjectNameArray = [];
        this.PracticeArray = [];
        this.selectedCompetencies = [] ;
        this.selectedEngagementType = [] ;
        this.selectedGender = [] ;
        this.selectedSRStatus = [];
        this.selectedOnOffshore = [ ];
        this.isSavePrefOn = true;
       this.haveRows = true;
    }
    // this is after removing default preference applied
    resetDefaultPref() {
        this.isDefaultPref = false;
        this.loading = true;
        this.ResetFilterselections();
        this.FetchFilteredCapacityData();
    }

    // tslint:disable-next-line:max-line-length
    constructor(
        private formBuilder: FormBuilder,
        private data: HTTPRequestService,
        private service: CapacityServiceService,
        public sharedData: SharedserviceService,
        private route: Router,
        private messageService: MessageService
    ) {
        this.pageLoading = true;
        this.DefaultColumns = [
            { field: 'employeeID', header: 'MID' },
            { field: 'name', header: 'Employee Name' },
            { field: 'projectName', header: 'Project' },
            { field: 'givenRole', header: 'TM Role' },
            { field: 'location', header: 'Location' },
          {  field: 'billableStatus',
            header: 'BillableStatus'},
            { field: 'vertical', header: 'Vertical' },
            { field: 'practice', header: 'Practice' }
        ];
        this.columns = this.DefaultColumns;
        this.SelectedColumns.setValue(this.DefaultColumns);
        // the Column selections array is feed with values to enable dynamic selection of columns
        this.ColumnSelections = [
            {
                label: 'MID',
                value: { field: 'employeeID', header: 'MID' }
            },
            {
                label: 'Employee Name',
                value: { field: 'name', header: 'Employee Name' }
            },
            {
                label: 'Project',
                value: { field: 'projectName', header: 'Project' }
            },
            {
                label: 'TM Role',
                value: { field: 'givenRole', header: 'TM Role' }
            },
            {
                label: 'Location',
                value: { field: 'location', header: 'Location' }
            },
            {
                label: 'Billability Status',
                value: {
                    field: 'billableStatus',
                    header: 'BillableStatus'
                }
            },
            {
                label: 'Vertical',
                value: { field: 'vertical', header: 'Vertical' }
            },
            {
                label: 'Practice',
                value: { field: 'practice', header: 'Practice' }
            },
            {
                label: 'Competency',
                value: { field: 'competency', header: 'Competency' }
            },
            {
                label: 'OnOffShore',
                value: { field: 'onOffShore', header: 'OnOffshore' }
            },
            {
                label: 'Gender',
                value: { field: 'gender', header: 'Gender' }
            },
            {
                label: 'Sub Practice',
                value: { field: 'subPractice', header: 'Sub Practice' }
            },
            {
                label: 'Profit Centre',
                value: { field: 'profitCenter', header: 'ProfitCenter' }
            }
        ];

        this.service.FiltersApplied.subscribe((respdata: FilterToggle) => {
            this.filterapplied = respdata;
            if (respdata.AccountGroup === false) {
                this.AccountNameArray = [];
            }
            if (respdata.BillableStatus === false) {
                this.BillableArray = [];
            }
            if (respdata.DeliveryManager === false) {
                this.DeliveryManagerArray = [];
            }
            if (respdata.DeliveryPlatform === false) {
                this.DeliveryPartnerArray = [];
            }
            if (respdata.Location === false) {
                this.Location = [];
            }
            if (respdata.Practice === false) {
                this.Practice = [];
            }
            if (respdata.ProjectName === false) {
                this.ProjectNameArray = [];
            }
            if (respdata.SRStatus === false) {
                this.srStatusArray = [];
            }
            if (respdata.EngagementType === false) {
                this.engagementTypeArray = [];
            }
            if (respdata.ProfitCenter === false) {
                this.ProfitCenterNameArray = [];
            }
        });
        this.data
        .getCapacityData('/api/capacity/GetCapacity?MID=' + this.sharedData.midOfLoggedInUser)
        .subscribe(
            (data: CapacityData[]) => {
              this.allCapacityData = data;
                this.GetPrefdata();
            },
            (error: any) => {
                if (error.status === 0) {
                    this.route.navigate(['/core/ServerDown']);
                } else if (error.status === 401) {
                    this.route.navigate(['/core/AccessDenied']);
            } else {
                    this.route.navigate(['/core/InternalServerError']);
                }
            }
        );
        // tslint:disable-next-line:max-line-length
        this.mid = this.sharedData.midOfLoggedInUser;
        this.confirmationForm = this.formBuilder.group({
            prefName: ['', Validators.required],
            def: ['', Validators.required]
        });
        this.service.SelectedPrefrence.subscribe((Response: PreferenceData) => {
            if (Response != null) {
                this.isDefaultPref = true;
                this.isPrefSet = true;
                this.selPref = Response.filterName;
                this.selectedFilter = Response.preferences;
                this.SearchKey = '';
                if (typeof this.tableComponent !== 'undefined') {
                    this.tableComponent.reset();
                    this.reset();
                }
                this.loading = true;

                if(Response.filterName!=null)
                {
                this.isDefaultPref = true;
                this.DeliveryPartnerArray = this.selectedFilter.deliveryPartner;
                this.BillableArray = this.selectedFilter.billableStatus;
                this.ProfitCenterNameArray = this.selectedFilter.profitCenter;
                this.LocationArray = this.selectedFilter.location;
                this.AccountNameArray = this.selectedFilter.accountGroup;
                this.DeliveryManagerArray = this.selectedFilter.deliveryManager;
                this.selectedEngagementType = this.selectedFilter.engagementCode;
                this.selectedCompetencies = this.selectedFilter.competencyCode;
                this.selectedGender = this.selectedFilter.gender;
                this.selectedOnOffshore = this.selectedFilter.onOffshore;
                this.ProjectNameArray = this.selectedFilter.projectName;
                this.PracticeArray = this.selectedFilter.practice;
                this.selectedSRStatus = this.selectedFilter.srStatus;
                this.prefFlag = true;
                this.UpdateFilterOptions();
                this.FetchFilteredCapacityData();
                }
                else
                {
                    this.FetchFilteredCapacityData();
                }
            } else {
            }
        });
    }
    GetPrefdata() {
          this.data.getAPI('/api/preferences/getDefaultPreference?MID=' +  this.sharedData.midOfLoggedInUser)
        // tslint:disable-next-line:no-shadowed-variable
        .subscribe((data: any) => {
            if (data == null) {
                this.isDefaultPref = false;
                this.defaultPref = null;
                this.FetchFilteredCapacityData();
            } else {
                this.isDefaultPref = true;
                if (!this.isPrefSet) {
                    this.dataPreferences = data.preferences;
                    this.dataPreferences.sRStatus = data.preferences.srStatus;
                    this.defaultPref = data;
                    this.selPref = data.filterName;
                    this.DeliveryPartnerArray =
                        data.preferences.deliveryPartner;
                    this.BillableArray = this.dataPreferences.billableStatus;
                    this.ProfitCenterNameArray =
                    this.dataPreferences.profitCenter;
                    this.LocationArray = this.dataPreferences.location;
                    this.AccountNameArray = this.dataPreferences.accountGroup;
                    this.DeliveryManagerArray =
                    this.dataPreferences.deliveryManager;
                    this.engagementTypeArray =
                    this.dataPreferences.engagementCode;
                    this.ProjectNameArray = this.dataPreferences.projectName;
                    this.PracticeArray = this.dataPreferences.practice;
                    this.selectedEngagementType = this.dataPreferences.engagementCode;
                    this.selectedCompetencies = this.dataPreferences.competencyCode;
                    this.selectedGender = this.dataPreferences.gender;
                    this.selectedOnOffshore = this.dataPreferences.onOffshore;
                    this.selectedSRStatus = this.dataPreferences.sRStatus;
                    this.UpdateFilterOptions();
                    this.prefFlag = true;
                    this.FetchFilteredCapacityData( );
                }
            }
        });

    }

    UpdateFilterOptions() {
        this.DeliveryManagerS = [] ;
        this.DeliveryManagerArray.forEach((item, index) => {
            this.DeliveryManagerS.push({
                label: item,
                value: item
            });
        });
this.BillableS = [ ]
this.BillableArray.forEach((item, index) => {
            this.BillableS.push({
                label: item,
                value: item
            });
        });
        // this.SRStatusS = [ ];
// this.srStatusArray .forEach((item, index) => {
//             this.SRStatusS.push({
//                 label: item,
//                 value: item
//             });
//         });
this.ProfitCenterNameS = [ ];
this.ProfitCenterNameArray.forEach((item, index) => {
        this.ProfitCenterNameS.push({
            label: item,
            value: item
        });
    });
this.LocationS = [ ] ;
this.LocationArray.forEach((item, index) => {
            this.LocationS.push({
                label: item,
                value: item
            });
        });

        this.ProjectNameS = [ ];
this.ProjectNameArray.forEach((item, index) => {
            this.ProjectNameS.push({
                label: item,
                value: item
            });
        });

this.PracticeS = [ ];
this.PracticeArray.forEach((item, index) => {
            this.PracticeS.push({
                label: item,
                value: item
            });
        });
this.DeliveryPartnerS = [ ];
this.DeliveryPartnerArray.forEach((item, index) => {
                this.DeliveryPartnerS.push({
                    label: item,
                    value: item
                });
            });
this.AccountNameS = [ ];
this.AccountNameArray.forEach((item, index) => {
                    this.AccountNameS.push({
                        label: item,
                        value: item
                    });
                });
                this.CompetencyArray = [ ];
                this.selectedCompetencies.forEach((item, index) => {
                    this.CompetencyArray.push({
                        label: item,
                        value: item
                    });
                });
                this.onOffshoreArray = [ ];
                this.selectedOnOffshore.forEach((item, index) => {
                    this.onOffshoreArray.push({
                        label: item,
                        value: item
                    });
                });
                this.engagementTypeArray = [ ];
                this.selectedEngagementType.forEach((item, index) => {
                    this.engagementTypeArray.push({
                        label: item,
                        value: item
                    });
                });
                this.srStatusArray = [ ] ;
                this.selectedSRStatus.forEach((item, index) => {
                    this.srStatusArray.push({
                        label: item,
                        value: item
                    });
                });
                this.genderArray = [ ];
                this.selectedGender.forEach((item, index) => {
                    this.genderArray.push({
                        label: item,
                        value: item
                    });
                });
            }
    UncheckDP(event) {
        const idb = event.currentTarget.parentElement.attributes.id.nodeValue;
        for (const entry of this.DeliveryPartnerArray) {
            if (entry === idb) {
                this.DeliveryPartnerArray.splice(
                    this.DeliveryPartnerArray.indexOf(entry),
                    1
                );
                this.FetchFilteredCapacityData();
            }
        }
    }
    /// on uncheck delete from the array 2-DM
    UncheckDM(event) {
        const idb = event.currentTarget.parentElement.attributes.id.nodeValue;
        for (const entry of this.DeliveryManagerArray) {
            if (entry === idb) {
                this.DeliveryManagerArray.splice(
                    this.DeliveryManagerArray.indexOf(entry),
                    1
                );
                this.FetchFilteredCapacityData();
            }
        }
    }
    /// on uncheck delete from the array 3-Pc
    UncheckPC(event) {
        const idb = event.currentTarget.parentElement.attributes.id.nodeValue;
        for (const entry of this.ProfitCenterNameArray) {
            if (entry === idb) {
                this.ProfitCenterNameArray.splice(
                    this.ProfitCenterNameArray.indexOf(entry),
                    1
                );
                this.FetchFilteredCapacityData();
            }
        }
    }
    /// on uncheck delete from the array 4-PN
    UncheckPN(event) {
        const idb = event.currentTarget.parentElement.attributes.id.nodeValue;
        for (const entry of this.ProjectNameArray) {
            if (entry === idb) {
                this.ProjectNameArray.splice(
                    this.ProjectNameArray.indexOf(entry),
                    1
                );
                this.FetchFilteredCapacityData();
            }
        }
    }
    /// on uncheck delete from the array 5-AN
    UncheckAN(event) {
        const idb = event.currentTarget.parentElement.attributes.id.nodeValue;
        for (const entry of this.AccountNameArray) {
            if (entry === idb) {
                this.AccountNameArray.splice(
                    this.AccountNameArray.indexOf(entry),
                    1
                );
                this.FetchFilteredCapacityData();
            }
        }
    }
    /// on uncheck delete from the array 6-P
    UncheckP(event) {
        const idb = event.currentTarget.parentElement.attributes.id.nodeValue;
        for (const entry of this.PracticeArray) {
            if (entry === idb) {
                this.PracticeArray.splice(this.PracticeArray.indexOf(entry), 1);
                this.FetchFilteredCapacityData();
            }
        }
    }
    /// on uncheck delete from the array 7-B
    UncheckB(event) {
        const idb = event.currentTarget.parentElement.attributes.id.nodeValue;
        for (const entry of this.BillableArray) {
            if (entry === idb) {
                this.BillableArray.splice(this.BillableArray.indexOf(entry), 1);
                this.FetchFilteredCapacityData();
            }
        }
    }
    /// on uncheck delete from the array 8-L
    UncheckL(event) {
        const idb = event.currentTarget.parentElement.attributes.id.nodeValue;
        for (const entry of this.LocationArray) {
            if (entry === idb) {
                this.LocationArray.splice(this.LocationArray.indexOf(entry), 1);
                this.FetchFilteredCapacityData();
            }
        }
    }
    ///  Show selected filters for Delivery partner
    showDeliveryP() {
        this.displayDP = true;
    }
    ///  Show selected filters for Billable status
    showBillableS() {
        this.displayBilable = true;
    }
    ///  Show selected filters for practices
    showPractise() {
        this.displayP = true;
    }
    ///  Show selected filters for Account name
    showAccountN() {
        this.displayAN = true;
    }
    ///  Show selected filters for Project Name
    showProjectN() {
        this.displayPN = true;
    }
    ///  Show selected filters for Location
    showLocation() {
        this.displayL = true;
    }
    ///  Show selected filters for SR status
    showSRS() {
        this.displaySR = true;
    }
    ///  Show selected filters for Profit center
    showPC() {
        this.displayPC = true;
    }
    ///  Show selected filters for Delivery manager
    showDM() {
        this.displayDM = true;
    }
    ///  Show selected filters for Engagement type
    showET() {
        this.displayET = true;
    }
    showAllDP() {
        this.overlaybasicDeliveryP = true;
    }
    /// show All BS
    showAllBS() {
        this.showAllBillable = true;
    }
    /// show All P
    showAllP() {
        this.showAllpractice = true;
    }
    /// show All AN
    showAllAN() {
        this.showAllAccountName = true;
    }
    /// show All PN
    showAllPN() {
        this.showAllProjectName = true;
    }
    /// show All L
    showAllL() {
        this.showAllLocation = true;
    }
    /// show All SR
    showAllSR() {
        this.showAllSRStatus = true;
    }
    /// show All PC
    showAllPC() {
        this.showAllProfitCenter = true;
    }
    /// show All DM
    showAllDM() {
        this.showAllDeliveryManager = true;
    }
    /// show All ET
    showAllET() {
        this.showAllEngagementType = true;
    }
    // Page load function
    ngOnInit() {

    }

    // this method update columns display based on columns selected by user to display
    UpdateColumns() {
        this.columns = [];
        if (this.SelectedColumns.value.length === 0) {
            this.columns = this.DefaultColumns;
        } else {
            // tslint:disable-next-line:no-shadowed-variable
            this.SelectedColumns.value.forEach(element => {
                this.columns.push(element);
            });
        }
    }
    compareFn(v1, v2): boolean {
        return compareFn(v1, v2);
    }

    // to start loading icon on all filters
     StartFilterLoading() {
            this.loadProfitCenterCard = true;
            this.loadDeliveryPartnerCard = true ;
            this.loadDeliveryManagerCard = true;
            this.loadAccountNamesCard = true;
            this.loadProjectsCard = true;
            this.loadPracticesCard = true;
            this.loadBillableStatusCard = true;
            this.loadLocationCard = true;
            this.loadSRStatusCard = true;
            this.loadEngagementTypeCard = true;
        }
        // to stop loading icon on all filters
        StopFilterLoading() {
            this.loadProfitCenterCard = false;
            this.loadDeliveryPartnerCard = false ;
            this.loadDeliveryManagerCard = false;
            this.loadAccountNamesCard = false;
            this.loadProjectsCard = false;
            this.loadPracticesCard = false;
            this.loadBillableStatusCard = false;
            this.loadLocationCard = false;
            this.loadSRStatusCard = false;
            this.loadEngagementTypeCard = false;
        }
    // to apply filters to capacity data
    FetchFilteredCapacityData() {
   const data = this .allCapacityData.filter( x => {
                     if ((this.ProfitCenterNameArray.length === 0 ||
                        ( x.profitCenter != null && this.ProfitCenterNameArray.includes(x.profitCenter) ) ) &&
                  ( this.PracticeArray.length === 0 ||
                   ( x.practice != null && this.PracticeArray.includes(x.practice) )) &&
                 ( this.BillableArray.length === 0 ||
                   ( x.billableStatus != null && this.BillableArray.includes(x.billableStatus) )) &&
                  ( this.LocationArray.length === 0 ||
                   ( x.location != null && this.LocationArray.includes(x.location) )) &&
                  ( this.AccountNameArray.length === 0 ||
                   ( x.accountName != null && this.AccountNameArray.includes(x.accountName) )) &&
                  ( this.ProjectNameArray.length === 0 ||
                   ( x.projectName != null && this.ProjectNameArray.includes(x.projectName) ) ) &&
                  ( this.selectedEngagementType.length === 0 ||
                   (x.engagementType != null && this.selectedEngagementType.includes(x.engagementType) ) ) &&
                   (  this.selectedGender.length === 0 ||
                    ( x.gender != null && this.selectedGender.includes(x.gender) )) &&
                    (  this.selectedCompetencies.length === 0 ||
                        ( x.competency != null && this.selectedCompetencies.includes(x.competency) )) &&
                    (  this.selectedSRStatus.length === 0 ||
                        ( x.srStatus != null && this.selectedSRStatus.includes(x.srStatus) )) &&
                    (  this.selectedOnOffshore.length === 0 ||
                        ( x.onOffShore != null && this.selectedOnOffshore.includes(x.onOffShore) )) &&
                  (  this.DeliveryManagerArray.length === 0 ||
                   ( x.deliveryManager != null && this.DeliveryManagerArray.includes(x.deliveryManager) )) &&
                    (this.DeliveryPartnerArray.length === 0 ||
                        ( x.deliveryPartner != null && this.DeliveryPartnerArray.includes(x.deliveryPartner) )) )  {
                return true ;
                }
                   }) ;
                    this.totalRecords = data.length;
                    this.capacityData = data;
                    this.isFetch = false;

                    if (data.length === 0) {
                        this.haveRows = false;
                    } else {
                        this.haveRows = true;
                    }

                    this.SearchKey = ' ';
                    this.getFilterOptions(data);
    }

    // to update filter options
    getFilterOptions(data) {
        this.isSavePrefOn = false;
        this.StartFilterLoading();
        if (this.DeliveryManagerArray.length === 0) {
        const deliveryManagerOptions = data.map(x => {
            return  x.deliveryManager ;
        } );
        this.DeliveryManagerS = [ ];
        Array.from(new Set(deliveryManagerOptions)).forEach((item, index) => {
           if (item != null) {
                this.DeliveryManagerS.push({
                label: item,
                value: item
            });
        }
        });
    }
    if (this.BillableArray.length === 0) {
        const billablestatusOptions = data.map(x => {
            return  x.billableStatus ;
        } );
        this.BillableS =  [];
        Array.from(new Set(billablestatusOptions)).forEach((item, index) => {
            if (item != null) {
            this.BillableS.push({
                label: item,
                value: item
            });
        }
        });
    }
    if (this.selectedSRStatus.length === 0) {
        const srStatusOptions = data.map(x => {
            return  x.srStatus ;
        } );
        this.srStatusArray = [];
        Array.from(new Set(srStatusOptions)).forEach((item, index) => {
            if (item != null) {
                this.srStatusArray.push({
                    label: item,
                    value: item
                });
            }
        });
    }
    if (this.ProfitCenterNameArray.length === 0) {
        const profitCentreOptions = data.map(x => {
            return  x.profitCenter ;
        } );
        this.ProfitCenterNameS = [ ];

    Array.from(new Set(profitCentreOptions)).forEach((item, index) => {
        if (item != null) {
        this.ProfitCenterNameS.push({
            label: item,
            value: item
        });
    }
    });
    }
    if (this.LocationArray.length === 0) {
        const locationOptions = data.map(x => {
            return  x.location ;
        } );
        this.LocationS = [];
        Array.from(new Set(locationOptions)).forEach((item, index) => {
            if (item != null) {
            this.LocationS.push({
                label: item,
                value: item
            });
        }
        });
    }
    if (this.AccountNameArray .length === 0) {
    const accountNameOptions = data.map(x => {
            return  x.accountName ;
        } );
        this.AccountNameS = [];
        Array.from(new Set(accountNameOptions)).forEach((item, index) => {
            if (item != null) {
            this.AccountNameS.push({
                label: item,
                value: item
            });
        }
        });
    }
    if (this.selectedEngagementType.length === 0) {
    const engagementTypeOptions = data.map(x => {
            return  x.engagementType ;
        } );
        this.engagementTypeArray = [];
        Array.from(new Set(engagementTypeOptions)).forEach((item, index) => {
            if (item != null) {
            this.engagementTypeArray.push({
                label: item,
                value: item
            });
        }
        });
    }
    if (this.ProjectNameArray.length === 0) {
        const projectNameOptions = data.map(x => {
            return  x.projectName ;
        } );
        this.ProjectNameS = [];
        Array.from(new Set(projectNameOptions)).forEach((item, index) => {
            if (item != null) {
            this.ProjectNameS.push({
                label: item,
                value: item
            });
        }
        });
    }
    if (this.PracticeArray .length === 0) {
        const practiceOptions = data.map(x => {
            return  x.practice ;
        } );
        this.PracticeS = [];
        Array.from(new Set(practiceOptions)).forEach((item, index) => {
            if (item != null) {
            this.PracticeS.push({
                label: item,
                value: item
            });
        }
        });
    }
        if (this.DeliveryPartnerArray .length === 0) {
            const deliveryPartnerOptions = data.map(x => {
                return  x.deliveryPartner ;
            } );
            this.DeliveryPartnerS = [];
            Array.from(new Set(deliveryPartnerOptions)).forEach((item, index) => {
                if (item != null) {
                this.DeliveryPartnerS.push({
                    label: item,
                    value: item
                });
            }
            });
        }
            if (this.AccountNameArray .length === 0) {
                const accountNameArrayOptions = data.map(x => {
                    return  x.accountName ;
                } );
                this.AccountNameS = [];
                Array.from(new Set(accountNameArrayOptions)).forEach((item, index) => {
                    if ( item != null) {
                        this.AccountNameS.push({
                            label: item,
                            value: item
                        });
                    }
                });
            }

                if (this.selectedCompetencies.length === 0) {
                    const competencyArrayOptions = data.map(x => {
                        return  x.competency ;
                    } );
                    this.CompetencyArray = [];
                    Array.from(new Set(competencyArrayOptions)).forEach((item, index) => {
                        if ( item != null) {
                            this.CompetencyArray.push({
                                label: item,
                                value: item
                            });
                        }
                    });
                }
                      if (this.selectedOnOffshore.length === 0) {
                        const onOffshoreArrayOptions = data.map(x => {
                            return  x.onOffShore ;
                        } );
                        this.onOffshoreArray = [];
                        Array.from(new Set(onOffshoreArrayOptions)).forEach((item, index) => {
                            if ( item != null) {
                                this.onOffshoreArray.push({
                                    label: item,
                                    value: item
                                });
                            }
                        });
                    }

                        if (this.selectedGender.length === 0) {
                            const genderArrayOptions = data.map(x => {
                                return  x.gender ;
                            } );
                            this.genderArray = [];
                            Array.from(new Set(genderArrayOptions)).forEach((item, index) => {
                                if ( item != null) {
                                    this.genderArray.push({
                                        label: item,
                                        value: item
                                    });
                                }
                            }); }
  this.StopFilterLoading() ;
        this.pageLoading = false ;
        if (this.prefFlag !== true) {
          this.isDefaultPref = false;
        }
        this.prefFlag = false;
        this.loading = false ;
 }

    /// Show dialog box
    showDialog() {
        this.displaydialog = true;
    }
    /// close dialog box
    closeDialog() {
        this.displaydialog = false;
    }
    /// on search change function
    onSearchChange(searchValue: string) {
        if (searchValue.length === 0) {
            this.isPrefName = true;
            this.isEmpty = true;
        } else {
            this.isPrefName = false;
        }
    }

    // Function to save preference information
    savePref() {
        this.preferenceName = this.confirmationForm.controls.prefName.value;
        this.defaultvalue = this.confirmationForm.controls.def.value;
        const filterModelObj = new Filter({
            deliveryPartner: this.DeliveryPartnerArray,
            billableStatus: this.BillableArray,
            onOffshore: this.selectedOnOffshore,
            gender: this.selectedGender,
            sRStatus: this.selectedSRStatus,
            profitCenter: this.ProfitCenterNameArray,
            location: this.LocationArray,
            accountGroup: this.AccountNameArray,
            deliveryManager: this.DeliveryManagerArray,
            competencyCode: this.selectedCompetencies,
            engagementCode: this.selectedEngagementType,
            projectName: this.ProjectNameArray,
            practice: this.PracticeArray ,
        });
        const date = new Date();
        let defa: boolean;
        if (this.defaultvalue[0] === 'Yes') {
            defa = true;
        } else {
            defa = false;
        }
        const savePref = new PreferenceData({
            mid: this.mid,

            // tslint:disable-next-line:max-line-length
            filterName: this.preferenceName,
            preferences: filterModelObj,
            uploadedDate: date,
            uploadedby: this.mid,
            DefaultPreference: defa
        });
        this.show = false;
        // API call to save preference data
        this.data
            .savePreference('/api/Preferences/SavePreference', savePref)
            .subscribe(
                (data: any) => {
                    // this.capacityData = data;
                    // this.totalRecords = data.length;
                    this.isDefaultPref = true;
                    this.selPref = this.preferenceName;
                    // Hide the dialog and display success message
                    this.displaydialog = false;
                    this.msgdialog = true;
                    this.show = true;
                    this.msgsuccess = [];
                    this.submitted = false;
                    this.msgsuccess.push({
                        severity: 'success',
                        summary: 'You have successfully saved your preference!!'
                    });
                    this.confirmationForm.reset();
                },
                (error: any) => {
                    this.confirmationForm.reset();
                    this.displaydialog = false;
                    this.msgdialog = true;
                    this.show = true;
                    this.msgsuccess = [];
                    this.submitted = false;
                    // tslint:disable-next-line:max-line-length
                    this.msgsuccess.push({
                        severity: 'error',
                        summary:
                            this.preferenceName +
                            ' is already used, give a different name to your preference!!'
                    });
                }
            );
    }
}
// this is used to update column selections
function compareFn(v1, v2): boolean {
    return v1 && v2 ? v1.field === v2.field : v1 === v2;
}
