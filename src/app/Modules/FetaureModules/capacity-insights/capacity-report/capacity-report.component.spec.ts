import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CapacityReportComponent } from './capacity-report.component';

describe('CapacityReportComponent', () => {
  let component: CapacityReportComponent;
  let fixture: ComponentFixture<CapacityReportComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CapacityReportComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CapacityReportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
