import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { FormGroup, FormBuilder } from '@angular/forms';
import { CapacityServiceService } from '../Services/capacity-service.service';
import { FilterToggle } from 'src/app/Models/filterToggleModel';

@Component({
    selector: 'app-filter',
    templateUrl: './filter.component.html',
    styleUrls: ['./filter.component.css']
})
export class FilterComponent implements OnInit {
    display = true;
    cities = [];
    _filtersApplied: FilterToggle;
    filterForm: FormGroup;
    filterapplied: any;
    filter: FilterToggle;
    selectedCities: string[];

    constructor(
        private route: Router,
        private formBuilder: FormBuilder,
        private data: CapacityServiceService
    ) {
        this.filterForm = this.formBuilder.group({
            DeliveryPlatform: [''],
            Competency: [''],
            BillableStatus: [''],
            Location: [''],
            Gender: [''],
            Practice: [''],
            OnOffShore: [''],
            SRStatus: [''],
            AccountGroup: [''],
            DeliveryManager: [''],
            ProjectName: [''],
            EngagementType: [''],
            ProfitCenter: ['']
        });
        this.filterapplied = this.filterForm.value;
        this.cities = [];
        this.cities.push({ label: 'Male', value: 'Male' });
        this.cities.push({ label: 'Female', value: 'Female' });
    }

    // Page load function
    ngOnInit() {
        this._filtersApplied = this.data.getFiltersApplied();
        if (this._filtersApplied) {
            this.filterForm.patchValue(this._filtersApplied);
        }
    }

    //  Function to close side panel
    close() {
        this.filterapplied = this.filterForm.value;
        this.data.setFiltersApplied(this.filterapplied);
        this.route.navigate(['/CapacityInsights']);
    }

    // select all filter on click of this btn//assign true will select all 13 filters
    selectAllFilter() {
        this.filterForm = this.formBuilder.group({
            DeliveryPlatform: true,
            Competency: true,
            BillableStatus: true,
            Location: true,
            Gender: true,
            Practice: true,
            OnOffShore: true,
            SRStatus: true,
            AccountGroup: true,
            DeliveryManager: true,
            ProjectName: true,
            EngagementType: true,
            ProfitCenter: true
        });
    }

    // function to  select all filter on click of this btn
    // assign false will select all 13 filters
    // Reset all filters
    resetAllFilter() {
        this.filterForm = this.formBuilder.group({
            DeliveryPlatform: false,
            Competency: false,
            BillableStatus: false,
            Location: false,
            Gender: false,
            Practice: false,
            OnOffShore: false,
            SRStatus: false,
            AccountGroup: false,
            DeliveryManager: false,
            ProjectName: false,
            EngagementType: false,
            ProfitCenter: false
        });
    }
}
