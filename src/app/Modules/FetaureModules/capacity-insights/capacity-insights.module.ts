import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CapacityInsightsRoutingModule } from './capacity-insights-routing.module';
import { FilterComponent } from './filter/filter.component';
import { PreferencesComponent } from './preferences/preferences.component';
import { SharedModule } from '../../shared/shared.module';
import { CapacityReportComponent } from './capacity-report/capacity-report.component';
import { HTTPRequestService } from 'src/app/Services/httprequest.service';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
// import { ActiveDisplayService } from 'src/app/Services/active-display.service';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';

@NgModule({
    declarations: [
        FilterComponent,
        PreferencesComponent,
        CapacityReportComponent
    ],
    imports: [
        CommonModule,
        CapacityInsightsRoutingModule,
        SharedModule,
        ReactiveFormsModule,
        FormsModule
    ],
    providers: [HTTPRequestService]
})
export class CapacityInsightsModule {}
