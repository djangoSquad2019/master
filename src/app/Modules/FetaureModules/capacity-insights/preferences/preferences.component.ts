import { Component, OnInit } from '@angular/core';
import { SelectItem } from 'primeng/api';
import { CapacityServiceService } from '../Services/capacity-service.service';
import { CapacityData } from 'src/app/Models/CapacityDataModel';
import { FormGroup, FormControl, ReactiveFormsModule } from '@angular/forms';
import { SelectedItemsModel } from 'src/app/Models/SelectedItemsModel';
import { FilterToggle } from 'src/app/Models/filterToggleModel';
import { Filter } from 'src/app/Models/FilterModel';
import { HTTPRequestService } from 'src/app/Services/httprequest.service';
import { Router } from '@angular/router';
import { PreferenceData } from 'src/app/Models/PreferenceModel';
import { DropdownModule } from 'primeng/dropdown';
import { SharedserviceService } from 'src/app/Services/sharedservice.service';
import { HttpHeaders } from '@angular/common/http';

@Component({
    selector: 'app-preferences',
    templateUrl: './preferences.component.html',
    styleUrls: ['./preferences.component.css']
})
export class PreferencesComponent implements OnInit {
    preference: SelectItem[];
    DeliveryPartnerArray = [];
    MakePrefrenceDefault: boolean;
    OnOffshoreArray = [];
    GenderArray = [];
    BillableArray = [];
    SRStatusArray = [];
    ProfitCenterNameArray = [];
    LocationArray = [];
    AccountNameArray = [];
    DeliveryManagerArray = [];
    CompetencyArray = [];
    EngagementTypeArray = [];
    ProjectNameArray = [];
    PracticeArray = [];
    capacityData: CapacityData[];
    preferenceDetails: PreferenceData[];
    defaultData: PreferenceData[];
    isDefaultPrefClass: Boolean = false;
    defPrefname = new PreferenceData();
    hideshowdef: Boolean = false;
    // Fetch button is enabled by default
    isNoPreferenceSet = true;
    defaultPrefrence: PreferenceData;
    allPreference: string[];
    selectedPreference: PreferenceData;
    display = true;
    mid: string;
    ShowPref = false;
    SavedConfirmationDisplay: boolean;
    ShowMessage = false;
    loading = false;
    // tslint:disable-next-line:max-line-length
    constructor(
        private route: Router,
        private data: HTTPRequestService,
        private service: CapacityServiceService,
        public sharedData: SharedserviceService
    ) {
        this.mid = this.sharedData.midOfLoggedInUser;
        this.loading = true;
        // API call to get prefernces saved by a particular user
        // tslint:disable-next-line:max-line-length
        // tslint:disable-next-line:no-shadowed-variable
        this.data
            .getPreferenceData(
                '/api/Preferences/GetProfilePreference?MID=' + this.mid
            )
            .subscribe(
                // tslint:disable-next-line:no-shadowed-variable
                (data: PreferenceData[]) => {
                    // Check if there are any prefernces set by the user. If yes display preference.
                    if (data.length > 0) {
                        this.loading = false;
                        this.ShowPref = true;
                        this.preferenceDetails = data;
                    } else {
                        this.loading = false;
                        this.ShowMessage = true;
                    }
                },
                (error: any) => {
                    this.route.navigate(['/core/InternalServerError']);
                    if (error.status === 0) {
                        this.route.navigate(['/core/ServerDown']);
                    } else if (error.status === 401) {
                        this.route.navigate(['/core/AccessDenied']);
                } else {
                        this.route.navigate(['/core/InternalServerError']);
                    }
                }
            );

        // Function to fetch default preference to show in side bar

        this.data
            .getAPI(
                '/api/preferences/GetDefaultPreferenceForDisplay?MID=' +
                    this.mid
            )
            .subscribe(
                (resp: any) => {
                    if (resp == null) {
                        this.defPrefname.filterName = null;
                        this.defPrefname.preferences = null;
                        this.hideshowdef = false;
                    } else {
                        this.defPrefname.filterName = resp.filterName;
                        this.defPrefname.preferences = resp.preferences;
                        this.hideshowdef = true; // This is used to hide and show the default preference
                    }
                },
                (error: any) => {
                    this.route.navigate(['/core/InternalServerError']);
                    if (error.status === 0) {
                        this.route.navigate(['/core/ServerDown']);
                    } else if (error.status === 401) {
                        this.route.navigate(['/core/AccessDenied']);
                } else {
                        this.route.navigate(['/core/InternalServerError']);
                    }
                }
            );
    }
    ConfirmationDisplay = false;
    showDialog() {
        this.ConfirmationDisplay = true;
    }
    showSavedConfirmationDialog() {
        this.SavedConfirmationDisplay = true;
    }
    // Page load function
    ngOnInit() {}

    //Reset selected preference and select all data
    FetchAllCapacityData()
    {
        const selectedPref = null;
        const filterModelObj = null;
        this.service.clearSetSelectedPref(selectedPref, filterModelObj);
        this.close();
    }
    // Fetch button click function to capture details of selected preference
    FetchFilteredCapacityDataAndUpdateDefault() {
        const selectedPref = this.selectedPreference.filterName;
      this.service.setSelectedPref(selectedPref, this.selectedPreference.preferences);
        // this.service.setSavedPrefrence();
        const token = this.sharedData.tokenIDOfLoggedUser;
        // tslint:disable-next-line:max-line-length
        const header: HttpHeaders = new HttpHeaders()
            .set('Content-Type', 'application/json')
            .set('Access-Control-Allow-Origin', '*')
            .set('Authorization', 'Bearer ' + token);
        this.data
            .post(
                '/api/Preferences/ChangeDefaultPreference',
                JSON.stringify(this.selectedPreference)
            )
            .subscribe(
                success => {
                    this.showDialog();
                    setTimeout(() => {
                        this.route.navigate([
                            '/CapacityInsights'
                        ]);
                    }, 3000);
                },
                (error: any) => {
                    if (error.status === 0) {
                        this.route.navigate(['/core/ServerDown']);
                    } else if (error.status === 401) {
                        this.route.navigate(['/core/AccessDenied']);
                } else {
                        this.route.navigate(['/core/InternalServerError']);
                    }
                }
            );
    }
    // this method is used to fetch filtered capacity data on preference selection if checkbox is checked it will make it default preference
    FetchFilteredCapacityData() {
        if (this.MakePrefrenceDefault === true) {
            this.FetchFilteredCapacityDataAndUpdateDefault();
        } else {
            const selectedPref = this.selectedPreference.filterName;
            const filterModelObj = this.selectedPreference.preferences;
            this.service.setSelectedPref(selectedPref, filterModelObj);
            this.close();
        }
    }
    // Close preference side tab
    close() {
        this.route.navigate(['/CapacityInsights']);
    }
    Validate() {
        if (this.selectedPreference != null) {
            this.isNoPreferenceSet = false;
        }
    }
    SaveDefaultPreference() {
        this.data
            .post(
                '/api/Preferences/ChangeDefaultPreference',
                JSON.stringify(this.selectedPreference)
            )
            .subscribe(
                success => {
                    this.showSavedConfirmationDialog();
                },
                (error: any) => {
                    this.route.navigate(['/core/InternalServerError']);
                    if (error.status === 0) {
                        this.route.navigate(['/core/ServerDown']);
                    } else if (error.status === 401) {
                        this.route.navigate(['/core/AccessDenied']);
                } else {
                        this.route.navigate(['/core/InternalServerError']);
                    }
                }
            );
    }
    // this method refereshes preference after saving a default preference.
    RefreshPreferences() {
        this.selectedPreference = null;
        this.MakePrefrenceDefault = false;
        this.isNoPreferenceSet = true;
        this.data
            .getAPI(
                '/api/preferences/GetDefaultPreferenceForDisplay?MID=' +
                    this.mid
            )
            .subscribe(
                (resp: any) => {
                    if (resp == null) {
                        this.defPrefname.filterName = null;
                        this.defPrefname.preferences = null;
                        this.hideshowdef = false;
                    } else {
                        this.defPrefname.filterName = resp.filterName;
                        this.defPrefname.preferences = resp.preferences;
                        this.hideshowdef = true; // This is used to hide and show the default preference
                    }
                },
                (error: any) => {
                    this.route.navigate(['/core/InternalServerError']);
                    if (error.status === 0) {
                        this.route.navigate(['/core/ServerDown']);
                    } else if (error.status === 401) {
                        this.route.navigate(['/core/AccessDenied']);
                } else {
                        this.route.navigate(['/core/InternalServerError']);
                    }
                }
            );
    }
}
