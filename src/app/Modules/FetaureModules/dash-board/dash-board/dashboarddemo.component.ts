import { Component, OnInit } from '@angular/core';
import { SelectItem } from 'primeng/primeng';
import { MenuItem } from 'primeng/primeng';
import { Router } from '@angular/router';
import { HTTPRequestService } from '../../../../Services/httprequest.service';

@Component({
    templateUrl: './dashboard.component.html'
    // styleUrls:['./dashboard.component.css']
})
export class DashboardDemoComponent implements OnInit {
    MindtreemindsCount: number;
    UnBilledMindsCount: number;
    BilledMindsCount: number;
    DeployableCount: number;
    CompetencyBuildingCount: number;
    offshore: number;
    onsite: number;
    not_listed: number;
    male: number;
    female: number;
    notListed: number;
    HYB: number;
    TNM: number;
    FPC: number;
    FMC: number;
    DisplayPieChartOnOffShore = true;
    DisplayPieChartGender = true;
    DisplayPieChartEngagement = true;
    DisplayBoards = true;

    data: any;
    pieChartData: any;
    pieChartDataCompetency: any;
    pieChartMaleFemale: any;
    pieChartEngagementType: any;

    constructor(private resp: HTTPRequestService, public router: Router) {}

    // Page load function
    ngOnInit() {
        // API call to get total employee count
        this.resp.getAPI('/api/DashBoard/GetMindtreemindsCount').subscribe(
            (resp: number) => {
                this.DisplayBoards = false;
                this.MindtreemindsCount = resp;
            },
            (error: any) => {
                if (error.status === 0) {
                    this.router.navigate(['/core/ServerDown']);
                } else if (error.status === 401) {
                    this.router.navigate(['/core/AccessDenied']);
            } else {
                    this.router.navigate(['/core/InternalServerError']);
                }
            }
        );

        // API call to get total blocked employee count
        this.resp.getAPI('/api/DashBoard/GetUnBilledMindsCount').subscribe(
            (resp: number) => {
                this.DisplayBoards = false;
                this.UnBilledMindsCount = resp;
            },
            (error: any) => {
                if (error.status === 0) {
                    this.router.navigate(['/core/ServerDown']);
                } else if (error.status === 401) {
                    this.router.navigate(['/core/AccessDenied']);
            } else {
                    this.router.navigate(['/core/InternalServerError']);
                }
            }
        );

        // API call to get total deployable employee count
        this.resp.getAPI('/api/DashBoard/GetDeployableCount').subscribe(
            (resp: number) => {
                this.DisplayBoards = false;
                this.DeployableCount = resp;
            },
            (error: any) => {
                if (error.status === 0) {
                    this.router.navigate(['/core/ServerDown']);
                } else if (error.status === 401) {
                    this.router.navigate(['/core/AccessDenied']);
            } else {
                    this.router.navigate(['/core/InternalServerError']);
                }
            }
        );

        // API call to get total Competency building employee count
        this.resp.getAPI('/api/DashBoard/GetBilledMindsCount').subscribe(
            (resp: number) => {
                this.DisplayBoards = false;
                this.BilledMindsCount = resp;
            },
            (error: any) => {
                if (error.status === 0) {
                    this.router.navigate(['/core/ServerDown']);
                } else if (error.status === 401) {
                    this.router.navigate(['/core/AccessDenied']);
            } else {
                    this.router.navigate(['/core/InternalServerError']);
                }
            }
        );

        // API call to get total Onsite Offshore ratio
        this.resp.getAPI('/api/DashBoard/GetOnOffShoreCount').subscribe(
            data => {
                // this.DisplayPieChart = false;
                this.assignValue(data);
            },
            (error: any) => {
                if (error.status === 0) {
                    this.router.navigate(['/core/ServerDown']);
                } else if (error.status === 401) {
                    this.router.navigate(['/core/AccessDenied']);
            } else {
                    this.router.navigate(['/core/InternalServerError']);
                }
            }
        );
        this.resp.getAPI('/api/DashBoard/GetMaleFemaleCount').subscribe(
            data => {
                // this.DisplayPieChart = false;
                this.assignMaleFemaleValue(data);
            },
            (error: any) => {
                if (error.status === 0) {
                    this.router.navigate(['/core/ServerDown']);
                } else if (error.status === 401) {
                    this.router.navigate(['/core/AccessDenied']);
            } else {
                    this.router.navigate(['/core/InternalServerError']);
                }
            }
        );

        this.resp.getAPI('/api/DashBoard/GetEngagementTypeCount').subscribe(
            data => {
                // this.DisplayPieChart = false;
                this.assignEngagementTypeValue(data);
            },
            (error: any) => {
                if (error.status === 0) {
                    this.router.navigate(['/core/ServerDown']);
                } else if (error.status === 401) {
                    this.router.navigate(['/core/AccessDenied']);
            } else {
                    this.router.navigate(['/core/InternalServerError']);
                }
            }
        );
    }

    // Pie chart to represent onsite offshore employee ratio
    assignValue(data) {
        this.onsite = data.onsite;
        this.offshore = data.offshore;
        this.not_listed = data.not_listed;
        // hardcoded value for demo
        this.DisplayPieChartOnOffShore = false;
        this.pieChartDataCompetency = {
            labels: ['Offshore', 'Onsite', 'Not listed'],
            datasets: [
                {
                    data: [this.offshore, this.onsite, this.not_listed],
                    backgroundColor: ['#FF6384', '#36A2EB', '#FFCE56'],
                    hoverBackgroundColor: ['#FF6384', '#36A2EB', '#FFCE56']
                }
            ]
        };
    }

    assignMaleFemaleValue(data) {
        this.male = data.male;
        this.female = data.female;

        // hardcoded value for demo
        this.DisplayPieChartGender = false;
        this.pieChartMaleFemale = {
            labels: ['Male', 'Female'],
            datasets: [
                {
                    data: [this.male, this.female],
                    backgroundColor: [
                        '#FF6384',
                        '#36A2EB'
                        // "#FFCE56",
                    ],
                    hoverBackgroundColor: [
                        '#FF6384',
                        '#36A2EB'
                        // "#FFCE56",
                    ]
                }
            ]
        };
    }
    assignEngagementTypeValue(data) {
        this.notListed = data.notListed;
        this.HYB = data.hyb;
        this.TNM = data.tnm;
        this.FPC = data.fpc;
        this.FMC = data.fmc;

        // hardcoded value for demo
        this.DisplayPieChartEngagement = false;
        this.pieChartEngagementType = {
            labels: ['Not Listed', 'HYB', 'TNM', 'FPC', 'FMC'],
            datasets: [
                {
                    data: [
                        this.notListed,
                        this.HYB,
                        this.TNM,
                        this.FPC,
                        this.FMC
                    ],
                    backgroundColor: [
                        '#FF6384',
                        '#36A2EB',
                        '#FFCE56',
                        '#1E77A3',
                        '#E01D32'
                    ],
                    hoverBackgroundColor: [
                        '#FF6384',
                        '#36A2EB',
                        '#FFCE56',
                        '#1E77A3',
                        '#E01D32'
                    ]
                }
            ]
        };
    }
}
