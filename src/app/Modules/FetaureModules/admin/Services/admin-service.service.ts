import { Injectable, Inject } from '@angular/core';
import {
    HttpClient,
    HttpParams,
    HttpHeaders,
    HttpResponse
} from '@angular/common/http';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';
// tslint:disable-next-line:import-blacklist
import 'rxjs/Rx';
import { THROW_IF_NOT_FOUND } from '@angular/core/src/di/injector';
import { Role } from 'src/app/Models/RoleModel';
import { UserDetailsModel } from 'src/app/Models/UserDetailsModel';
import { CapacityData } from 'src/app/Models/CapacityDataModel';
import { map } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
import { SharedserviceService } from '../../../../Services/sharedservice.service';
import { debug } from 'util';
import * as CryptoJS from 'crypto-js';

@Injectable({
    providedIn: 'root'
})
export class AdminServiceService {
    baseURL: string;
    public userlogdata: UserDetailsModel;
    mid: string;
    bytes: string;
    constructor(
        private http: HttpClient,
        public sharedData: SharedserviceService
    ) {
        // Get base URL based on the environment of the run
        this.baseURL = environment.baseUrl;
    }

    // function to view all admins.
    getUsers(): Observable<any> {
        const token = this.sharedData.tokenIDOfLoggedUser;
        // tslint:disable-next-line:max-line-length
        const header: HttpHeaders = new HttpHeaders()
            .set('Content-Type', 'application/json')
            .set('Access-Control-Allow-Origin', '*')
            .set('Authorization', 'Bearer ' + token);
        return this.http.get(this.baseURL + '/api/Role/GetAllAdmins', {
            headers: header
        });
    }

    // Funtion to get the roleID after getting the mid of logged in user
    // getRole(Mid: string): Observable<any> {
    //     const token = this.sharedData.tokenIDOfLoggedUser;
    //     const encrypted = this.sharedData.encryptingData(Mid);
    //     const header: HttpHeaders = new HttpHeaders()
    //     .set('Authorization', 'Bearer ' + token)
    //     .set('Access-Control-Allow-Origin', '*')
    //     .set('Strict-Transport-Security', 'max-age=31536000; includeSubDomains; preload')
    //     .set('X-Frame-Options', 'DENY');
    //     return this.http.get(
    //         this.baseURL + '/api/Role/GetUserRole?EncryptedMID=' +  btoa(encrypted),
    //         { headers: header }
    //     );
    // }

    // Function to get the details of user after getting mid from textbox
    getUserDetails(midOfUser): Observable<any> {
        const header: HttpHeaders = new HttpHeaders()
        .set('Strict-Transport-Security', 'max-age=31536000; includeSubDomains; preload')
        .set('Access-Control-Allow-Origin', '*')
        .set('X-Frame-Options', 'DENY');
        const encrypted = this.sharedData.encryptingData(midOfUser);
        return this.http.get(
            this.baseURL +
                '/api/MicrosoftGraphAPI/GetUserDetails?EncryptedMID=' +
                btoa(encrypted)
                // user: CapacityData
        );
    }

    // Function to create add a Mindtree mind as admin
    saveRole(value1, value2) {
        const obj = new Role();
        const encryptedMid = this.sharedData.encryptingData(this.sharedData.midOfLoggedInUser);
        const encryptedMIDOfNewAdmin = this.sharedData.encryptingData(value1);
        const encryptedRoleOfNewAdmin = this.sharedData.encryptingData(value2);
        obj.MID = encryptedMIDOfNewAdmin;
        obj.Role = encryptedRoleOfNewAdmin;
        obj.MidOfLoggedInUser = encryptedMid;
        const jsonObj = JSON.stringify(obj);
        const token = this.sharedData.tokenIDOfLoggedUser;
        // tslint:disable-next-line:max-line-length
        const header: HttpHeaders = new HttpHeaders()
            .set('Content-Type', 'application/json')
            .set('Access-Control-Allow-Origin', '*')
            .set('Authorization', 'Bearer ' + token);
        return this.http.post<Role>(
            this.baseURL + '/api/Role/CreateRole',
            jsonObj,
            { headers: header }
        );
    }
    DeleteRole(user: UserDetailsModel) {
        const jsonObj = JSON.stringify(user);
        const token = this.sharedData.tokenIDOfLoggedUser;
        // tslint:disable-next-line:max-line-length
        const header: HttpHeaders = new HttpHeaders()
            .set('Content-Type', 'application/json')
            .set('Access-Control-Allow-Origin', '*')
            .set('Authorization', 'Bearer ' + token);
        return this.http.post<Role>(
            this.baseURL + '/api/Role/DeleteAdmin',
            jsonObj,
            { headers: header }
        );
    }
    GetProfileDetails(midOfUser) {
        const header: HttpHeaders = new HttpHeaders()
        .set('Strict-Transport-Security', 'max-age=31536000; includeSubDomains; preload')
        .set('Access-Control-Allow-Origin', '*')
        .set('X-Frame-Options', 'DENY');
        const encrypted = this.sharedData.encryptingData(midOfUser);
        return this.http.get(
            this.baseURL +
                '/api/MicrosoftGraphAPI/GetProfileDetails?EncryptedMID=' +
                btoa(encrypted)
        );
    }
}
