import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';



import { AdminRoutingModule } from './admin-routing.module';
import { AdminComponent } from './admin/admin.component';


import { FormsModule } from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from '../../shared/shared.module';

import { AddAdminComponent } from './add-admin/add-admin.component';

import { AdminServiceService } from './Services/admin-service.service';

@NgModule({
  declarations: [AdminComponent, AddAdminComponent],
  imports: [
    CommonModule,
    AdminRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    SharedModule
  ],
  providers: [AdminServiceService]
})
export class AdminModule { }
