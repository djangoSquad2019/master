import { Component, OnInit, EventEmitter, Output } from '@angular/core';
import {
    FormBuilder,
    FormGroup,
    Validators,
    FormControl
} from '@angular/forms';
import { Router } from '@angular/router';
import { AdminServiceService } from '../Services/admin-service.service';
import { Message } from 'primeng/primeng';

@Component({
    selector: 'app-add-admin',
    templateUrl: './add-admin.component.html',
    styleUrls: ['./add-admin.component.css']
})
export class AddAdminComponent implements OnInit {
    // @Output() submitForm = new EventEmitter<AddAdminComponent>();
    @Output() cancelForm = new EventEmitter<void>();
    roles: string[] = ['Admin'];
    default = 'Admin'; // default value for Role dropdown
    adminForm: FormGroup;
    submitted = false;
    success = false;
    errorMessage: any;
    MID: string;
    Role: string;
    show = true;
    roleadded = [];
    msg: string = null;
    msgs: Message[] = [];
    msgDialog = false;
    empnamePattern = '^[Mm]{1}\\d{7}';
    msgWarningDialog = false;
    errorMsgs: Message[] = [];
    displayMessage = ' ';

    constructor(
        private formBuilder: FormBuilder,
        private route: Router,
        private data: AdminServiceService
    ) {}

    ngOnInit() {
        // check the values of form controls and validations
        this.adminForm = this.formBuilder.group({
            MID: [
                '',
                [Validators.required, Validators.pattern(this.empnamePattern)]
            ], // MID input validation
            Role: ['', Validators.required],
            roles: new FormControl(null)
        });
        // set the values Role dropdown
        this.adminForm.controls['Role'].setValue(this.default, {
            onlySelf: true
        });
    }
    // function for submitting the form.
    submit() {
        this.submitted = true;
        if (!this.adminForm.valid) {
            return;
        }
        this.data
            .saveRole(
                this.adminForm.controls.MID.value,
                this.adminForm.controls.Role.value
            )
            .subscribe(
                data => {
                   this.msgDialog = true;
                    this.msgs = [];
                    this.show = true;
                },
                (error: any) => {
                    if (error.status === 400) {
                        this.submitted = true;
                        this.msgWarningDialog = true;
                        this.errorMsgs = [];
                        this.show = true;
                        this.displayMessage = 'This MID is already registered';
                    } else if (error.status === 501) {
                        // Status code 501 (Not Implemented)
                        this.submitted = true;
                        this.msgWarningDialog = true;
                        this.errorMsgs = [];
                        this.show = true;
                        this.displayMessage = 'This MID does not exist';
                    } else if (error.status === 401) {
                        this.route.navigate(['/core/AccessDenied']);
                } else if (error.status === 0) {
                        this.route.navigate(['/core/ServerDown']);
                    } else {
                        this.route.navigate(['/core/InternalServerError']);
                    }
                }
            );
    }
    // function to cancel the form.Navigate to admin page
    cancel() {
        this.submitted = false;
        this.adminForm.patchValue({'MID': ' ' } );
        this.msgDialog = false;
    }
}
