import { Component, OnInit } from '@angular/core';
import { MenuItem } from 'primeng/primeng';
import { Router } from '@angular/router';
import { AdminServiceService } from '../Services/admin-service.service';
import { UserDetailsModel } from 'src/app/Models/UserDetailsModel';
import { Message } from 'primeng/primeng';


@Component({
    selector: 'app-admin',
    templateUrl: './admin.component.html',
    styleUrls: ['./admin.component.css']
})
export class AdminComponent implements OnInit {
    items: MenuItem[];
    users: UserDetailsModel[];
    saveDeleteData: UserDetailsModel;
    show = false;
    roleadded = [];
    msg: string = null;
    msgs: Message[] = [];
    submitted = false;
    msgDialog = false;
    imgUrlHeader =
        'https://social.mindtree.com/User%20Photos/Profile%20Pictures/';
    imgUrlTail = '_LThumb.jpg';
    isViewAdmin = true;
    displayConfirm = false;


    constructor(private route: Router, private data: AdminServiceService) {}

    // tab menu function
    ngOnInit() {
        this.items = [
            {
                label: 'Add Admin',
                icon: 'fa fa-user',
                command: event => this.route.navigate(['/admin/addadmin'])
            }
        ];
        this.GetAdmins();
    }

     // function to fetch all admins
    GetAdmins() {
        this.data.getUsers().subscribe(
            (data: UserDetailsModel[]) => {
                this.isViewAdmin = false;
                this.users = data;
            },
            (error: any) => {
                if (error.status === 0) {
                    this.route.navigate(['/core/ServerDown']);
                } else if (error.status === 401) {
                    this.route.navigate(['/core/AccessDenied']);
            }  else {
                    this.route.navigate(['/core/InternalServerError']);
                }
            }
        );
    }
    deleteadmin() {
        this.displayConfirm = false;
        this.data.DeleteRole(this.saveDeleteData).subscribe(
            data => {
                this.submitted = true;
                this.msgDialog = true;
                this.msgs = [];
                this.show = true;
                this.GetAdmins();
            });
    }
    showConfirmDialog(user: UserDetailsModel) {
        this.saveDeleteData = user;
       this.displayConfirm = true;
        }
}
