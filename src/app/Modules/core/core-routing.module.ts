import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { UnauthorizedComponent } from './unauthorized/unauthorized.component';
import { AccessDeniedComponent } from './access-denied/access-denied.component';
import { InternalServerErrorComponent } from './internal-server-error/internal-server-error.component';
import { ServerDownComponent } from './server-down/server-down.component';
// the routes array is feeded with routes within the components in core module
const routes: Routes = [
  {path: 'unauthorized', component: UnauthorizedComponent},
  {path: 'InternalServerError', component: InternalServerErrorComponent},
  {path: 'ServerDown', component: ServerDownComponent},
  {path: 'AccessDenied', component: AccessDeniedComponent}
// {path:'addadmin',component:AddAdminComponent},
// {path:'viewadmin',component:ViewAdminsComponent}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CoreRoutingModule { }
