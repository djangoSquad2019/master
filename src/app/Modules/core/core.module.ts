import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CoreRoutingModule } from './core-routing.module';
import { AccessDeniedComponent } from './access-denied/access-denied.component';
// import { UnAuthorizedPageComponent } from './un-authorized-page/un-authorized-page.component';
import { UnauthorizedComponent } from './unauthorized/unauthorized.component';
import { UnderconstructionComponent } from './underconstruction/underconstruction.component';
import { InternalServerErrorComponent } from './internal-server-error/internal-server-error.component';
import { ServerDownComponent } from './server-down/server-down.component';

@NgModule({
  declarations: [AccessDeniedComponent, UnauthorizedComponent, UnderconstructionComponent, 
    InternalServerErrorComponent, ServerDownComponent ],
  imports: [
    CommonModule,
    CoreRoutingModule
  ]
})
export class CoreModule { }
