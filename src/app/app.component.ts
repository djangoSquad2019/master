import { Component } from '@angular/core';
import { AdminServiceService } from './Modules/FetaureModules/admin/Services/admin-service.service';
import { Router } from '@angular/router';
import { UserDetailsModel } from './Models/UserDetailsModel';
import { load } from '@angular/core/src/render3/instructions';
import { debug } from 'util';
import { NgModel } from '@angular/forms';
import { Params, ActivatedRoute } from '@angular/router';
// import { AdalService } from 'adal-angular4';
import { OnInit } from '@angular/core';
import { SharedserviceService } from './Services/sharedservice.service';
import { SlimLoadingBarService } from 'ng2-slim-loading-bar';
import { JwtHelperService } from '@auth0/angular-jwt';
import * as CryptoJS from 'crypto-js';
import { CapacityData } from './Models/CapacityDataModel';
import { HTTPRequestService } from './Services/httprequest.service';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html'
})
export class AppComponent {
    ShowCapacityInsights = true;

    public menuMode = 'horizontal';

    public menuActive = true;

    public topbarMenuActive = false;

    activeTopbarItem: Element;

    menuClick: boolean;

    menuButtonClick: boolean;

    secretKey: string;

    topbarMenuButtonClick: boolean;
    resetMenu: boolean;
    menuHoverActive: boolean;
    success: string;
    error: string;
    userDetails: string;
    CI = false;
    UnderConstruction = false;
    result: boolean;
    message: string;
    showMessage = false;
    displaydialog = false;
    dialogEnterMID = false;
    dialogValidMID = false;
    capsMID: string;
    splitString: string[];

    constructor(
        public data: AdminServiceService,
        public router: Router,
        private slimLoadingBarService: SlimLoadingBarService,
        public sharedData: SharedserviceService,
        private _http:  HTTPRequestService
    ) {}

    // To get mid from textbox
    midOfUser: string;

    // Function called when Launch button is clicked
    ShowCI() {
        if (this.midOfUser != null && this.midOfUser !== '') {
            this.capsMID = this.midOfUser;
            this.capsMID = this.capsMID.toUpperCase();
            const startsWithDigit = new RegExp('^\\d{7}');
            const startsWithM = new RegExp('^[Mm]{1}\\d{7}');
            this.result =
                startsWithDigit.test(this.capsMID) ||
                startsWithM.test(this.capsMID);
            if (this.result) {
                this.slimLoadingBarService.start();
                // API call to get user details
                this.data.getUserDetails(this.capsMID).subscribe(
                    (data: UserDetailsModel) => {
                        this.ShowCapacityInsights = false;
                        this.CI = true;
                        this.sharedData.token.subscribe(x => {
                            this.sharedData.tokenIDOfLoggedUser = x;
                        });
                        this.sharedData.mid.subscribe(x => {
                            this.sharedData.midOfLoggedInUser = x;
                        });
                        this.sharedData.roleID.subscribe(x => {
                            this.sharedData.roleIDOfLoggedInUser = x;
                        });
                        const decryptedData = this.sharedData.decryptingData(data);
                        this.splitString = decryptedData.split(',', 5);
                        // Saving user details in Sharedservcie
                        this.sharedData.showUserDetails(this.splitString);

                        // Getting profile details of the user
                        this.data.GetProfileDetails(this.capsMID).subscribe(
                            (profiledata: CapacityData) => {
                                // decrypting the data received from the api
                                const decryptedprofileData = this.sharedData.decryptingData(profiledata);
                                this.splitString = decryptedprofileData.split('@', 7);
                                // Saving user profile details in Sharedservcie
                        this.sharedData.showProfileDetails(this.splitString);
                            });

                        // tslint:disable-next-line:no-shadowed-variable
                        // this.data.getRole(this.sharedData.midOfLoggedInUser).subscribe(
                        //     // tslint:disable-next-line:no-shadowed-variable
                        //     (data: any) => {
                        //         const decryptedRole = this.sharedData.decryptingData(data);
                        //         this.sharedData.showRoleName(decryptedRole); // Saving roleID in SharedService
                                // If roleID is 1( Admin) and competency>=C7, redirect to CapacityInsights page, else Dashboard page
                                const EligibleCompetency = [
                                    'C7',
                                    'C8',
                                    'C9.1',
                                    'C9.2',
                                    'C10',
                                    'C11',
                                    'C12'
                                ];
                                // Redirect to Capacity report page
                                if (
                                    this.sharedData.roleIDOfLoggedInUser ===
                                        '1' ||
                                        EligibleCompetency.includes(
                                            this.sharedData.competencyOfLoggedInUser)
                                ) {
                                    this.slimLoadingBarService.complete();
                                    this.slimLoadingBarService.stop();
                                    this._http
                                    .getCapacityData('/api/capacity/GetCapacity?MID=' + this.sharedData.midOfLoggedInUser)
                                    .subscribe(
                                        // tslint:disable-next-line:no-shadowed-variable
                                        (data: CapacityData[]) => {
                                            this._http.storeCpacityData(data) ;
                                            this.router.navigate(['/CapacityInsights']);
                                        },
                                        (error: any) => {
                                            this.ShowCapacityInsights = false;
                                            this.CI = true;
                                            if (error.status === 0) {
                                                this.router.navigate(['/core/ServerDown']);
                                            } else if (error.status === 401) {
                                                this.router.navigate(['/core/AccessDenied']);
                                        } else {
                                                this.router.navigate(['/core/InternalServerError']);
                                            }
                                        }
                                    );

                                } else {
                                    this.slimLoadingBarService.complete();
                                    this.slimLoadingBarService.stop();
                                    this.router.navigate(['/dashboard']);
                                }
                            }
                             ,
                            (error: any) => {
                                if (error.status === 0) {
                                    this.ShowCapacityInsights = false;
                                    this.CI = true;
                                    this.router.navigate(['/core/ServerDown']);
                                } else if (error.status === 401) {
                                    this.ShowCapacityInsights = false;
                                    this.CI = true;
                                    this.router.navigate(['/core/AccessDenied']);
                            } else if (error.status === 404) {
                                    this.ShowCapacityInsights = false;
                                    this.CI = true;
                                    this.router.navigate(['/core/InternalServerError']);
                                } else if (error.status === 501) {
                                        this.dialogValidMID = true;
                                    } else {
                                        this.ShowCapacityInsights = false;
                                        this.CI = true;
                                    this.router.navigate([
                                        '/core/InternalServerError'
                                    ]);
                                }
                            }
                        );
            } else {

                this.displaydialog = true;
            }
        } else {

            this.dialogEnterMID = true;
        }
    }
    /// Show dialog box
    showDialog() {
        this.displaydialog = true;
    }
    ShowUnderConstruction() {
        this.ShowCapacityInsights = false;
        this.UnderConstruction = true;
    }
    // tslint:disable-next-line:use-life-cycle-interface
    ngOnInit() {}
    onMenuButtonClick(event: Event) {
        this.menuButtonClick = true;
        this.menuActive = !this.menuActive;
        event.preventDefault();
    }

    onTopbarMenuButtonClick(event: Event) {
        this.topbarMenuButtonClick = true;
        this.topbarMenuActive = !this.topbarMenuActive;
        event.preventDefault();
    }

    onTopbarItemClick(event: Event, item: Element) {
        this.topbarMenuButtonClick = true;

        if (this.activeTopbarItem === item) {
            this.activeTopbarItem = null;
        } else {
            this.activeTopbarItem = item;
        }
        event.preventDefault();
    }

    onTopbarSubItemClick(event) {
        event.preventDefault();
    }

    onLayoutClick() {
        if (!this.menuButtonClick && !this.menuClick) {
            if (this.menuMode === 'horizontal') {
                this.resetMenu = true;
            }

            if (
                this.isMobile() ||
                this.menuMode === 'overlay' ||
                this.menuMode === 'popup'
            ) {
                this.menuActive = false;
            }

            this.menuHoverActive = false;
        }

        if (!this.topbarMenuButtonClick) {
            this.activeTopbarItem = null;
            this.topbarMenuActive = false;
        }

        this.menuButtonClick = false;
        this.menuClick = false;
        this.topbarMenuButtonClick = false;
    }

    onMenuClick() {
        this.menuClick = true;
        this.resetMenu = false;
    }

    isMobile() {
        return window.innerWidth < 1025;
    }

    isHorizontal() {
        return this.menuMode === 'horizontal';
    }

    isTablet() {
        const width = window.innerWidth;
        return width <= 1024 && width > 640;
    }
}
