import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DashboardDemoComponent } from './Modules/FetaureModules/dash-board/dash-board/dashboarddemo.component';
import { AuthGuard } from '../guards/auth.guard';

// In this array we register all the routes for the application
const routes: Routes = [
    // {path:'',canActivate:[AuthGuard],component:DashboardDemoComponent},
    {path: 'dashboard', component: DashboardDemoComponent},
    // tslint:disable-next-line:max-line-length
    {
        path: 'CapacityInsights',
        canActivate: [AuthGuard],
        loadChildren:
            './Modules/FetaureModules/capacity-insights/capacity-insights.module#CapacityInsightsModule'
    },
    {
        path: 'admin',
        canActivate: [AuthGuard],
        loadChildren: './Modules/FetaureModules/admin/admin.module#AdminModule'
    },
    { path: 'core', loadChildren: './Modules/core/core.module#CoreModule' }
    // ,  {path:'http://localhost:4200/:id_token', component:AppComponent}
];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})
export class AppRoutingModule {}
