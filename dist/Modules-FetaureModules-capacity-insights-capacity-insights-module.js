(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["Modules-FetaureModules-capacity-insights-capacity-insights-module"],{

/***/ "./src/app/Models/FilterModel.ts":
/*!***************************************!*\
  !*** ./src/app/Models/FilterModel.ts ***!
  \***************************************/
/*! exports provided: Filter */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Filter", function() { return Filter; });
/*The capacity data can be filtered based on any of the below 14 parameters.
Each individual parameter consists of multiple filter options. Hence they are considered as a string of array */
var Filter = /** @class */ (function () {
    function Filter(values) {
        if (values === void 0) { values = {}; }
        Object.assign(this, values);
    }
    return Filter;
}());



/***/ }),

/***/ "./src/app/Models/PreferenceModel.ts":
/*!*******************************************!*\
  !*** ./src/app/Models/PreferenceModel.ts ***!
  \*******************************************/
/*! exports provided: PreferenceData */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PreferenceData", function() { return PreferenceData; });
/* A Users stored preference will be retrieved through API
and binded to an object of this model */
var PreferenceData = /** @class */ (function () {
    function PreferenceData(values) {
        if (values === void 0) { values = {}; }
        Object.assign(this, values);
    }
    return PreferenceData;
}());



/***/ }),

/***/ "./src/app/Models/filterToggleModel.ts":
/*!*********************************************!*\
  !*** ./src/app/Models/filterToggleModel.ts ***!
  \*********************************************/
/*! exports provided: FilterToggle */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FilterToggle", function() { return FilterToggle; });
/* To display a list of all avialble filter options */
var FilterToggle = /** @class */ (function () {
    function FilterToggle(values) {
        if (values === void 0) { values = {}; }
        Object.assign(this, values);
    }
    return FilterToggle;
}());



/***/ }),

/***/ "./src/app/Modules/FetaureModules/capacity-insights/Services/capacity-service.service.ts":
/*!***********************************************************************************************!*\
  !*** ./src/app/Modules/FetaureModules/capacity-insights/Services/capacity-service.service.ts ***!
  \***********************************************************************************************/
/*! exports provided: CapacityServiceService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CapacityServiceService", function() { return CapacityServiceService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _Models_filterToggleModel__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../Models/filterToggleModel */ "./src/app/Models/filterToggleModel.ts");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm5/index.js");
/* harmony import */ var src_app_Models_PreferenceModel__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! src/app/Models/PreferenceModel */ "./src/app/Models/PreferenceModel.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var CapacityServiceService = /** @class */ (function () {
    function CapacityServiceService(http) {
        this.http = http;
        this.filtersapplied = new _Models_filterToggleModel__WEBPACK_IMPORTED_MODULE_2__["FilterToggle"]({
            DeliveryPlatform: true,
            Competency: true,
            BillableStatus: true,
            Location: true,
            Gender: true,
            Practice: true,
            OnOffShore: true,
            SRStatus: true,
            AccountGroup: true,
            DeliveryManager: true,
            ProjectName: true,
            EngagementType: true,
            ProfitCenter: true,
        });
        this.FiltersApplied = new rxjs__WEBPACK_IMPORTED_MODULE_3__["BehaviorSubject"](this.filtersapplied);
        this.PrefNameAndFilter = new rxjs__WEBPACK_IMPORTED_MODULE_3__["BehaviorSubject"](null);
        this.SelectedPrefrence = new rxjs__WEBPACK_IMPORTED_MODULE_3__["BehaviorSubject"](null);
        this.SavedPreference = new rxjs__WEBPACK_IMPORTED_MODULE_3__["BehaviorSubject"](null);
        this.PrefFilter = new src_app_Models_PreferenceModel__WEBPACK_IMPORTED_MODULE_4__["PreferenceData"]();
    }
    // Function to set the filters applied
    CapacityServiceService.prototype.setFiltersApplied = function (Filtersapplied) {
        this.filtersapplied = Filtersapplied;
        this.FiltersApplied.next(this.filtersapplied);
    };
    // Function to get the filters applied
    CapacityServiceService.prototype.getFiltersApplied = function () {
        return this.filtersapplied;
    };
    // Function to set the saved preference
    // setSavedPrefrence(selectedValue) {
    //    this.selectedPrefrence = selectedValue;
    //    this.SavedPreference.next(selectedValue);
    // }
    // Function to get the saved preference
    CapacityServiceService.prototype.getSavedPrefrence = function () {
        return this.selectedPrefrence;
    };
    // Function to set the selected preference
    CapacityServiceService.prototype.setSelectedPref = function (selectedPref, filterObj) {
        this.PrefFilter.filterName = selectedPref;
        this.PrefFilter.preferences = filterObj;
        this.SelectedPrefrence.next(this.PrefFilter);
    };
    // Function to set the selected preference
    CapacityServiceService.prototype.clearSetSelectedPref = function (selectedPref, filterObj) {
        debugger;
        this.PrefFilter.filterName = selectedPref;
        this.PrefFilter.preferences = filterObj;
        this.SelectedPrefrence.next(this.PrefFilter);
    };
    // Function to get the selected preference
    CapacityServiceService.prototype.getSelectdPref = function () {
        return this.selectedPref;
    };
    CapacityServiceService = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])({
            providedIn: 'root'
        }),
        __metadata("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"]])
    ], CapacityServiceService);
    return CapacityServiceService;
}());



/***/ }),

/***/ "./src/app/Modules/FetaureModules/capacity-insights/capacity-insights-routing.module.ts":
/*!**********************************************************************************************!*\
  !*** ./src/app/Modules/FetaureModules/capacity-insights/capacity-insights-routing.module.ts ***!
  \**********************************************************************************************/
/*! exports provided: CapacityInsightsRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CapacityInsightsRoutingModule", function() { return CapacityInsightsRoutingModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _filter_filter_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./filter/filter.component */ "./src/app/Modules/FetaureModules/capacity-insights/filter/filter.component.ts");
/* harmony import */ var _preferences_preferences_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./preferences/preferences.component */ "./src/app/Modules/FetaureModules/capacity-insights/preferences/preferences.component.ts");
/* harmony import */ var _capacity_report_capacity_report_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./capacity-report/capacity-report.component */ "./src/app/Modules/FetaureModules/capacity-insights/capacity-report/capacity-report.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};





// the routes array is feeded with routes within the components in Capacity module
var routes = [
    {
        path: '',
        component: _capacity_report_capacity_report_component__WEBPACK_IMPORTED_MODULE_4__["CapacityReportComponent"],
        children: [
            { path: 'filter', component: _filter_filter_component__WEBPACK_IMPORTED_MODULE_2__["FilterComponent"] },
            { path: 'preference', component: _preferences_preferences_component__WEBPACK_IMPORTED_MODULE_3__["PreferencesComponent"] }
        ]
    }
];
var CapacityInsightsRoutingModule = /** @class */ (function () {
    function CapacityInsightsRoutingModule() {
    }
    CapacityInsightsRoutingModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"].forChild(routes)],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"]]
        })
    ], CapacityInsightsRoutingModule);
    return CapacityInsightsRoutingModule;
}());



/***/ }),

/***/ "./src/app/Modules/FetaureModules/capacity-insights/capacity-insights.module.ts":
/*!**************************************************************************************!*\
  !*** ./src/app/Modules/FetaureModules/capacity-insights/capacity-insights.module.ts ***!
  \**************************************************************************************/
/*! exports provided: CapacityInsightsModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CapacityInsightsModule", function() { return CapacityInsightsModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _capacity_insights_routing_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./capacity-insights-routing.module */ "./src/app/Modules/FetaureModules/capacity-insights/capacity-insights-routing.module.ts");
/* harmony import */ var _filter_filter_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./filter/filter.component */ "./src/app/Modules/FetaureModules/capacity-insights/filter/filter.component.ts");
/* harmony import */ var _preferences_preferences_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./preferences/preferences.component */ "./src/app/Modules/FetaureModules/capacity-insights/preferences/preferences.component.ts");
/* harmony import */ var _shared_shared_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../shared/shared.module */ "./src/app/Modules/shared/shared.module.ts");
/* harmony import */ var _capacity_report_capacity_report_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./capacity-report/capacity-report.component */ "./src/app/Modules/FetaureModules/capacity-insights/capacity-report/capacity-report.component.ts");
/* harmony import */ var src_app_Services_httprequest_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! src/app/Services/httprequest.service */ "./src/app/Services/httprequest.service.ts");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};









var CapacityInsightsModule = /** @class */ (function () {
    function CapacityInsightsModule() {
    }
    CapacityInsightsModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            declarations: [
                _filter_filter_component__WEBPACK_IMPORTED_MODULE_3__["FilterComponent"],
                _preferences_preferences_component__WEBPACK_IMPORTED_MODULE_4__["PreferencesComponent"],
                _capacity_report_capacity_report_component__WEBPACK_IMPORTED_MODULE_6__["CapacityReportComponent"]
            ],
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"],
                _capacity_insights_routing_module__WEBPACK_IMPORTED_MODULE_2__["CapacityInsightsRoutingModule"],
                _shared_shared_module__WEBPACK_IMPORTED_MODULE_5__["SharedModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_8__["ReactiveFormsModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_8__["FormsModule"]
            ],
            providers: [src_app_Services_httprequest_service__WEBPACK_IMPORTED_MODULE_7__["HTTPRequestService"]]
        })
    ], CapacityInsightsModule);
    return CapacityInsightsModule;
}());



/***/ }),

/***/ "./src/app/Modules/FetaureModules/capacity-insights/capacity-report/capacity-report.component.css":
/*!********************************************************************************************************!*\
  !*** ./src/app/Modules/FetaureModules/capacity-insights/capacity-report/capacity-report.component.css ***!
  \********************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".padding-LR{\r\n    padding-left: 10px;\r\n    padding-right: 15px;\r\n}\r\ndiv.cardHeight{\r\n    height: 300px !important;\r\n}\r\n.gridPadding{\r\n    padding:2px;\r\n    \r\n}\r\n.justify > ::ng-deep figure {\r\n    justify-content: left !important;\r\n}\r\n.zeroPadding{\r\n    padding: 0px;\r\n    border: none;\r\n}\r\n.padding{\r\n    padding-top:2px;\r\n    padding-left: 2px;\r\n}\r\n.columnOne{\r\n    display: flex;\r\n    flex-flow: column;\r\n    padding: 15px;\r\n    height: 175px;  \r\n    overflow: scroll;\r\n    /* box-shadow: 0px 0px 4px; */\r\n   \r\n}\r\n.noWrap{\r\n    white-space: nowrap;\r\n}\r\n.arrowIcon\r\n{\r\n    float: left;\r\n    font-size: 18px;\r\n    cursor: pointer;\r\n    margin-left: 10px;\r\n    color: #00796b;\r\n    padding: 5px;\r\n}\r\n.plusIcon\r\n{\r\n    float: right;\r\n    font-size: 18px;\r\n    cursor: pointer;\r\n    margin-right: 10px;\r\n    color: #f57f17;\r\n    padding: 5px;\r\n}\r\n.fa-spin-hover:hover {\r\n    -webkit-animation: fa-spin 2s infinite linear;\r\n            animation: fa-spin 2s infinite linear;\r\n}\r\n.btn{\r\n    font-weight: 500;\r\n    text-align: center;\r\n    white-space: nowrap;\r\n    vertical-align: middle;    \r\n    border: 1px solid transparent;\r\n    padding: 0.375rem 0.75rem;\r\n    font-size: 13px;\r\n    line-height: 1;\r\n    border-radius:2px;\r\n    text-transform: capitalize;\r\n    height: 30px;\r\n}\r\n.marginRight{\r\n    margin-right: 20px;\r\n}\r\n.buttonPadding{\r\n    padding-left: 10px;\r\n  \r\n}\r\n.btn-primary{\r\n    float:left;\r\n    margin-bottom:3px !important;\r\n}\r\n.textHeading{\r\n    border-left: 5px solid #d81b60;\r\n    color: #e65100;\r\n    height: 30px;\r\n    font-size: 13px;\r\n    margin-top: 2;\r\n    border-radius: 2px;\r\n    padding-top: 7px;\r\n    font-weight: 500;\r\n    background-color: #f5f5f5;\r\n}\r\nbody .ui-table .ui-table-thead > tr > th, body .ui-table .ui-table-tfoot > tr > td {\r\n    background-color: #d0d6e2;\r\n    color: #2d353c;\r\n    border: 1px solid #dbe1e6;\r\n}\r\n.Cardfooter\r\n{\r\n border-top: 1px solid #eee;\r\n}\r\nlabel.mat-checkbox-layout\r\n{\r\n    white-space: normal;\r\n}\r\n::ng-deep body .ui-listbox .listbox-header {\r\n    padding: 0px;\r\n    border: 0 none;\r\n    border-bottom: 1px solid #dce2e7;\r\n    margin: 0;\r\n    border-radius: 0;\r\n}\r\n::ng-deep .ui-listbox{\r\n\tpadding: .25em;\r\n    width: 100%;\r\n}\r\n.ui-multiselect .ui-multiselect-label-container {\r\n    overflow: hidden;\r\n    text-align: center;\r\n    background-color: orangered;\r\n}\r\nth\r\n{\r\n    white-space: nowrap;\r\n}\r\n.divHeader {\r\n    font-size: 18px;\r\n    margin-bottom: 10px;\r\n}\r\n.PaddingRight5{\r\n    padding-right:5px\r\n}\r\n::ng-deep.mat-select-value {\r\n    display: table-cell;\r\n    max-width: 0;\r\n    width: 100%;\r\n    overflow: hidden;\r\n    text-overflow: ellipsis;\r\n    white-space: nowrap;\r\n    color: transparent;\r\n}\r\n::ng-deep .mat-form-field-appearance-legacy .mat-form-field-infix {\r\n    padding: .4375em 0;\r\n    width: 0px;\r\n}\r\n::ng-deep.mat-select-arrow {\r\n    color:transparent;\r\n}\r\n::ng-deep table td {\r\n    font-size: 12px;\r\n    font-weight: 300;\r\n}\r\n.width\r\n{\r\n    width:300px;\r\n}\r\n::ng-deep .splash-screen {\r\n    position: absolute;\r\n    top: 50%;\r\n    bottom: 50%;\r\n    left: 50%;\r\n}\r\n.selectheader\r\n{\r\n   border-top:2px solid red;\r\n}\r\nmat-select\r\n{\r\n    float: right;\r\n    margin-right: 15px;\r\n    margin-top: 9px;\r\n}\r\n.TextAligncentre\r\n{\r\n    text-align: center;\r\n}\r\n.active\r\n{\r\n    height: 30px;\r\n    border-radius: 2px;\r\n    font-size: 13px;\r\n    background-color:white ;\r\n    color: #00695c;\r\n}\r\n.activeg\r\n{\r\n    height: 30px;\r\n    border-radius: 2px;\r\n    font-size: 13px;\r\n    background-color: white;\r\n    color: #c2185b;\r\n}\r\n.remove-preference{\r\n    padding-left: 8px;\r\n    font-size: 15px;\r\n    cursor: pointer\r\n}\r\n.p-list-width{\r\n    width: 168px;\r\n}\r\n.mat-expansion-panel-header-title {\r\n    flex-direction: column;\r\n}\r\n.filter-icon{\r\n    color: white;\r\n    font-size: 30px;\r\n    margin-top: -6px;\r\n    padding-top: 2px;\r\n}\r\n.mat-expansion-panel-body {\r\n    padding: 0 24px 0px !important;\r\n}\r\n.height-table{\r\n    height: 55px;\r\n}\r\n.pannel-filter{\r\n    background: white;\r\n    padding: 10px 15px;\r\n    margin-top: 15px;\r\n    box-shadow: 0 3px 1px -2px rgba(0,0,0,.2), 0 2px 2px 0 rgba(0,0,0,.14), 0 1px 5px 0 rgba(0,0,0,.12);\r\n    margin-left: 2px;\r\n    margin-right: 2px;\r\n    border-top: 5px solid #3F729B;\r\n    padding-top: 15px;\r\n}\r\n::ng-deep.ui-multiselect .ui-multiselect-trigger .ui-multiselect-trigger-icon {\r\n    top: 55%;\r\n    left: 60%;\r\n}\r\n::ng-deep label {\r\n    display: inline-block;\r\n     margin-bottom: 0; \r\n}\r\n::ng-deep .filter-plist ul * {\r\n    margin: 0;\r\n    border-radius: 0;\r\n    font-size: 11px !important;\r\n    max-width: 12em;\r\n}\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvTW9kdWxlcy9GZXRhdXJlTW9kdWxlcy9jYXBhY2l0eS1pbnNpZ2h0cy9jYXBhY2l0eS1yZXBvcnQvY2FwYWNpdHktcmVwb3J0LmNvbXBvbmVudC5jc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7SUFDSSxtQkFBbUI7SUFDbkIsb0JBQW9CO0NBQ3ZCO0FBQ0Q7SUFDSSx5QkFBeUI7Q0FDNUI7QUFFRDtJQUNJLFlBQVk7O0NBRWY7QUFDRDtJQUNJLGlDQUFpQztDQUNwQztBQUNEO0lBQ0ksYUFBYTtJQUNiLGFBQWE7Q0FDaEI7QUFDRDtJQUNJLGdCQUFnQjtJQUNoQixrQkFBa0I7Q0FDckI7QUFFRDtJQUNJLGNBQWM7SUFDZCxrQkFBa0I7SUFDbEIsY0FBYztJQUNkLGNBQWM7SUFDZCxpQkFBaUI7SUFDakIsOEJBQThCOztDQUVqQztBQUVEO0lBQ0ksb0JBQW9CO0NBQ3ZCO0FBQ0E7O0lBRUcsWUFBWTtJQUNaLGdCQUFnQjtJQUNoQixnQkFBZ0I7SUFDaEIsa0JBQWtCO0lBQ2xCLGVBQWU7SUFDZixhQUFhO0NBQ2hCO0FBQ0Q7O0lBRUksYUFBYTtJQUNiLGdCQUFnQjtJQUNoQixnQkFBZ0I7SUFDaEIsbUJBQW1CO0lBQ25CLGVBQWU7SUFDZixhQUFhO0NBQ2hCO0FBRUQ7SUFDSSw4Q0FBc0M7WUFBdEMsc0NBQXNDO0NBQ3pDO0FBQ0Q7SUFDSSxpQkFBaUI7SUFDakIsbUJBQW1CO0lBQ25CLG9CQUFvQjtJQUNwQix1QkFBdUI7SUFDdkIsOEJBQThCO0lBQzlCLDBCQUEwQjtJQUMxQixnQkFBZ0I7SUFDaEIsZUFBZTtJQUNmLGtCQUFrQjtJQUNsQiwyQkFBMkI7SUFDM0IsYUFBYTtDQUNoQjtBQUNEO0lBQ0ksbUJBQW1CO0NBQ3RCO0FBQ0Q7SUFDSSxtQkFBbUI7O0NBRXRCO0FBQ0Q7SUFDSSxXQUFXO0lBQ1gsNkJBQTZCO0NBQ2hDO0FBRUQ7SUFDSSwrQkFBK0I7SUFDL0IsZUFBZTtJQUNmLGFBQWE7SUFDYixnQkFBZ0I7SUFDaEIsY0FBYztJQUNkLG1CQUFtQjtJQUNuQixpQkFBaUI7SUFDakIsaUJBQWlCO0lBQ2pCLDBCQUEwQjtDQUM3QjtBQUNEO0lBQ0ksMEJBQTBCO0lBQzFCLGVBQWU7SUFDZiwwQkFBMEI7Q0FDN0I7QUFDRDs7Q0FFQywyQkFBMkI7Q0FDM0I7QUFDRDs7SUFFSSxvQkFBb0I7Q0FDdkI7QUFFRDtJQUNJLGFBQWE7SUFDYixlQUFlO0lBQ2YsaUNBQWlDO0lBQ2pDLFVBQVU7SUFHVixpQkFBaUI7Q0FDcEI7QUFFRDtDQUNDLGVBQWU7SUFDWixZQUFZO0NBQ2Y7QUFDRDtJQUNJLGlCQUFpQjtJQUNqQixtQkFBbUI7SUFDbkIsNEJBQTRCO0NBQy9CO0FBRUQ7O0lBRUksb0JBQW9CO0NBQ3ZCO0FBTUQ7SUFDSSxnQkFBZ0I7SUFDaEIsb0JBQW9CO0NBQ3ZCO0FBRUQ7SUFDSSxpQkFBaUI7Q0FDcEI7QUFFRDtJQUNJLG9CQUFvQjtJQUNwQixhQUFhO0lBQ2IsWUFBWTtJQUNaLGlCQUFpQjtJQUNqQix3QkFBd0I7SUFDeEIsb0JBQW9CO0lBQ3BCLG1CQUFtQjtDQUN0QjtBQUVEO0lBQ0ksbUJBQW1CO0lBQ25CLFdBQVc7Q0FDZDtBQUNEO0lBQ0ksa0JBQWtCO0NBQ3JCO0FBQ0Q7SUFDSSxnQkFBZ0I7SUFDaEIsaUJBQWlCO0NBQ3BCO0FBQ0Q7O0lBRUksWUFBWTtDQUNmO0FBR0Q7SUFDSSxtQkFBbUI7SUFDbkIsU0FBUztJQUNULFlBQVk7SUFDWixVQUFVO0NBQ2I7QUFDRDs7R0FFRyx5QkFBeUI7Q0FDM0I7QUFDRDs7SUFFSSxhQUFhO0lBQ2IsbUJBQW1CO0lBQ25CLGdCQUFnQjtDQUNuQjtBQUNEOztJQUVJLG1CQUFtQjtDQUN0QjtBQUNEOztJQUVJLGFBQWE7SUFDYixtQkFBbUI7SUFDbkIsZ0JBQWdCO0lBQ2hCLHdCQUF3QjtJQUN4QixlQUFlO0NBQ2xCO0FBQ0Q7O0lBRUksYUFBYTtJQUNiLG1CQUFtQjtJQUNuQixnQkFBZ0I7SUFDaEIsd0JBQXdCO0lBQ3hCLGVBQWU7Q0FDbEI7QUFDRDtJQUNJLGtCQUFrQjtJQUNsQixnQkFBZ0I7SUFDaEIsZUFBZTtDQUNsQjtBQUNEO0lBQ0ksYUFBYTtDQUNoQjtBQUNEO0lBQ0ksdUJBQXVCO0NBQzFCO0FBQ0Q7SUFDSSxhQUFhO0lBQ2IsZ0JBQWdCO0lBQ2hCLGlCQUFpQjtJQUNqQixpQkFBaUI7Q0FDcEI7QUFDRDtJQUNJLCtCQUErQjtDQUNsQztBQUNEO0lBQ0ksYUFBYTtDQUNoQjtBQUNEO0lBQ0ksa0JBQWtCO0lBQ2xCLG1CQUFtQjtJQUNuQixpQkFBaUI7SUFDakIsb0dBQW9HO0lBQ3BHLGlCQUFpQjtJQUNqQixrQkFBa0I7SUFDbEIsOEJBQThCO0lBQzlCLGtCQUFrQjtDQUNyQjtBQUNEO0lBQ0ksU0FBUztJQUNULFVBQVU7Q0FDYjtBQUNEO0lBQ0ksc0JBQXNCO0tBQ3JCLGlCQUFpQjtDQUNyQjtBQUNEO0lBQ0ksVUFBVTtJQUdWLGlCQUFpQjtJQUNqQiwyQkFBMkI7SUFDM0IsZ0JBQWdCO0NBQ25CIiwiZmlsZSI6InNyYy9hcHAvTW9kdWxlcy9GZXRhdXJlTW9kdWxlcy9jYXBhY2l0eS1pbnNpZ2h0cy9jYXBhY2l0eS1yZXBvcnQvY2FwYWNpdHktcmVwb3J0LmNvbXBvbmVudC5jc3MiLCJzb3VyY2VzQ29udGVudCI6WyIucGFkZGluZy1MUntcclxuICAgIHBhZGRpbmctbGVmdDogMTBweDtcclxuICAgIHBhZGRpbmctcmlnaHQ6IDE1cHg7XHJcbn1cclxuZGl2LmNhcmRIZWlnaHR7XHJcbiAgICBoZWlnaHQ6IDMwMHB4ICFpbXBvcnRhbnQ7XHJcbn1cclxuXHJcbi5ncmlkUGFkZGluZ3tcclxuICAgIHBhZGRpbmc6MnB4O1xyXG4gICAgXHJcbn1cclxuLmp1c3RpZnkgPiA6Om5nLWRlZXAgZmlndXJlIHtcclxuICAgIGp1c3RpZnktY29udGVudDogbGVmdCAhaW1wb3J0YW50O1xyXG59XHJcbi56ZXJvUGFkZGluZ3tcclxuICAgIHBhZGRpbmc6IDBweDtcclxuICAgIGJvcmRlcjogbm9uZTtcclxufVxyXG4ucGFkZGluZ3tcclxuICAgIHBhZGRpbmctdG9wOjJweDtcclxuICAgIHBhZGRpbmctbGVmdDogMnB4O1xyXG59XHJcblxyXG4uY29sdW1uT25le1xyXG4gICAgZGlzcGxheTogZmxleDtcclxuICAgIGZsZXgtZmxvdzogY29sdW1uO1xyXG4gICAgcGFkZGluZzogMTVweDtcclxuICAgIGhlaWdodDogMTc1cHg7ICBcclxuICAgIG92ZXJmbG93OiBzY3JvbGw7XHJcbiAgICAvKiBib3gtc2hhZG93OiAwcHggMHB4IDRweDsgKi9cclxuICAgXHJcbn1cclxuXHJcbi5ub1dyYXB7XHJcbiAgICB3aGl0ZS1zcGFjZTogbm93cmFwO1xyXG59XHJcbiAuYXJyb3dJY29uXHJcbntcclxuICAgIGZsb2F0OiBsZWZ0O1xyXG4gICAgZm9udC1zaXplOiAxOHB4O1xyXG4gICAgY3Vyc29yOiBwb2ludGVyO1xyXG4gICAgbWFyZ2luLWxlZnQ6IDEwcHg7XHJcbiAgICBjb2xvcjogIzAwNzk2YjtcclxuICAgIHBhZGRpbmc6IDVweDtcclxufVxyXG4ucGx1c0ljb25cclxue1xyXG4gICAgZmxvYXQ6IHJpZ2h0O1xyXG4gICAgZm9udC1zaXplOiAxOHB4O1xyXG4gICAgY3Vyc29yOiBwb2ludGVyO1xyXG4gICAgbWFyZ2luLXJpZ2h0OiAxMHB4O1xyXG4gICAgY29sb3I6ICNmNTdmMTc7XHJcbiAgICBwYWRkaW5nOiA1cHg7XHJcbn0gXHJcblxyXG4uZmEtc3Bpbi1ob3Zlcjpob3ZlciB7XHJcbiAgICBhbmltYXRpb246IGZhLXNwaW4gMnMgaW5maW5pdGUgbGluZWFyO1xyXG59XHJcbi5idG57XHJcbiAgICBmb250LXdlaWdodDogNTAwO1xyXG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gICAgd2hpdGUtc3BhY2U6IG5vd3JhcDtcclxuICAgIHZlcnRpY2FsLWFsaWduOiBtaWRkbGU7ICAgIFxyXG4gICAgYm9yZGVyOiAxcHggc29saWQgdHJhbnNwYXJlbnQ7XHJcbiAgICBwYWRkaW5nOiAwLjM3NXJlbSAwLjc1cmVtO1xyXG4gICAgZm9udC1zaXplOiAxM3B4O1xyXG4gICAgbGluZS1oZWlnaHQ6IDE7XHJcbiAgICBib3JkZXItcmFkaXVzOjJweDtcclxuICAgIHRleHQtdHJhbnNmb3JtOiBjYXBpdGFsaXplO1xyXG4gICAgaGVpZ2h0OiAzMHB4O1xyXG59XHJcbi5tYXJnaW5SaWdodHtcclxuICAgIG1hcmdpbi1yaWdodDogMjBweDtcclxufVxyXG4uYnV0dG9uUGFkZGluZ3tcclxuICAgIHBhZGRpbmctbGVmdDogMTBweDtcclxuICBcclxufVxyXG4uYnRuLXByaW1hcnl7XHJcbiAgICBmbG9hdDpsZWZ0O1xyXG4gICAgbWFyZ2luLWJvdHRvbTozcHggIWltcG9ydGFudDtcclxufVxyXG5cclxuLnRleHRIZWFkaW5ne1xyXG4gICAgYm9yZGVyLWxlZnQ6IDVweCBzb2xpZCAjZDgxYjYwO1xyXG4gICAgY29sb3I6ICNlNjUxMDA7XHJcbiAgICBoZWlnaHQ6IDMwcHg7XHJcbiAgICBmb250LXNpemU6IDEzcHg7XHJcbiAgICBtYXJnaW4tdG9wOiAyO1xyXG4gICAgYm9yZGVyLXJhZGl1czogMnB4O1xyXG4gICAgcGFkZGluZy10b3A6IDdweDtcclxuICAgIGZvbnQtd2VpZ2h0OiA1MDA7XHJcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjZjVmNWY1O1xyXG59XHJcbmJvZHkgLnVpLXRhYmxlIC51aS10YWJsZS10aGVhZCA+IHRyID4gdGgsIGJvZHkgLnVpLXRhYmxlIC51aS10YWJsZS10Zm9vdCA+IHRyID4gdGQge1xyXG4gICAgYmFja2dyb3VuZC1jb2xvcjogI2QwZDZlMjtcclxuICAgIGNvbG9yOiAjMmQzNTNjO1xyXG4gICAgYm9yZGVyOiAxcHggc29saWQgI2RiZTFlNjtcclxufVxyXG4uQ2FyZGZvb3RlclxyXG57XHJcbiBib3JkZXItdG9wOiAxcHggc29saWQgI2VlZTtcclxufVxyXG5sYWJlbC5tYXQtY2hlY2tib3gtbGF5b3V0XHJcbntcclxuICAgIHdoaXRlLXNwYWNlOiBub3JtYWw7XHJcbn1cclxuXHJcbjo6bmctZGVlcCBib2R5IC51aS1saXN0Ym94IC5saXN0Ym94LWhlYWRlciB7XHJcbiAgICBwYWRkaW5nOiAwcHg7XHJcbiAgICBib3JkZXI6IDAgbm9uZTtcclxuICAgIGJvcmRlci1ib3R0b206IDFweCBzb2xpZCAjZGNlMmU3O1xyXG4gICAgbWFyZ2luOiAwO1xyXG4gICAgLW1vei1ib3JkZXItcmFkaXVzOiAwO1xyXG4gICAgLXdlYmtpdC1ib3JkZXItcmFkaXVzOiAwO1xyXG4gICAgYm9yZGVyLXJhZGl1czogMDtcclxufVxyXG5cclxuOjpuZy1kZWVwIC51aS1saXN0Ym94e1xyXG5cdHBhZGRpbmc6IC4yNWVtO1xyXG4gICAgd2lkdGg6IDEwMCU7XHJcbn1cclxuLnVpLW11bHRpc2VsZWN0IC51aS1tdWx0aXNlbGVjdC1sYWJlbC1jb250YWluZXIge1xyXG4gICAgb3ZlcmZsb3c6IGhpZGRlbjtcclxuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICAgIGJhY2tncm91bmQtY29sb3I6IG9yYW5nZXJlZDtcclxufVxyXG5cclxudGhcclxue1xyXG4gICAgd2hpdGUtc3BhY2U6IG5vd3JhcDtcclxufVxyXG5cclxuXHJcblxyXG5cclxuXHJcbi5kaXZIZWFkZXIge1xyXG4gICAgZm9udC1zaXplOiAxOHB4O1xyXG4gICAgbWFyZ2luLWJvdHRvbTogMTBweDtcclxufVxyXG5cclxuLlBhZGRpbmdSaWdodDV7XHJcbiAgICBwYWRkaW5nLXJpZ2h0OjVweFxyXG59XHJcblxyXG46Om5nLWRlZXAubWF0LXNlbGVjdC12YWx1ZSB7XHJcbiAgICBkaXNwbGF5OiB0YWJsZS1jZWxsO1xyXG4gICAgbWF4LXdpZHRoOiAwO1xyXG4gICAgd2lkdGg6IDEwMCU7XHJcbiAgICBvdmVyZmxvdzogaGlkZGVuO1xyXG4gICAgdGV4dC1vdmVyZmxvdzogZWxsaXBzaXM7XHJcbiAgICB3aGl0ZS1zcGFjZTogbm93cmFwO1xyXG4gICAgY29sb3I6IHRyYW5zcGFyZW50O1xyXG59XHJcblxyXG46Om5nLWRlZXAgLm1hdC1mb3JtLWZpZWxkLWFwcGVhcmFuY2UtbGVnYWN5IC5tYXQtZm9ybS1maWVsZC1pbmZpeCB7XHJcbiAgICBwYWRkaW5nOiAuNDM3NWVtIDA7XHJcbiAgICB3aWR0aDogMHB4O1xyXG59XHJcbjo6bmctZGVlcC5tYXQtc2VsZWN0LWFycm93IHtcclxuICAgIGNvbG9yOnRyYW5zcGFyZW50O1xyXG59XHJcbjo6bmctZGVlcCB0YWJsZSB0ZCB7XHJcbiAgICBmb250LXNpemU6IDEycHg7XHJcbiAgICBmb250LXdlaWdodDogMzAwO1xyXG59XHJcbi53aWR0aFxyXG57XHJcbiAgICB3aWR0aDozMDBweDtcclxufVxyXG5cclxuXHJcbjo6bmctZGVlcCAuc3BsYXNoLXNjcmVlbsKge1xyXG4gICAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gICAgdG9wOiA1MCU7XHJcbiAgICBib3R0b206IDUwJTtcclxuICAgIGxlZnQ6IDUwJTtcclxufVxyXG4uc2VsZWN0aGVhZGVyXHJcbntcclxuICAgYm9yZGVyLXRvcDoycHggc29saWQgcmVkO1xyXG59XHJcbm1hdC1zZWxlY3Rcclxue1xyXG4gICAgZmxvYXQ6IHJpZ2h0O1xyXG4gICAgbWFyZ2luLXJpZ2h0OiAxNXB4O1xyXG4gICAgbWFyZ2luLXRvcDogOXB4O1xyXG59XHJcbi5UZXh0QWxpZ25jZW50cmVcclxue1xyXG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG59XHJcbi5hY3RpdmVcclxue1xyXG4gICAgaGVpZ2h0OiAzMHB4O1xyXG4gICAgYm9yZGVyLXJhZGl1czogMnB4O1xyXG4gICAgZm9udC1zaXplOiAxM3B4O1xyXG4gICAgYmFja2dyb3VuZC1jb2xvcjp3aGl0ZSA7XHJcbiAgICBjb2xvcjogIzAwNjk1YztcclxufSBcclxuLmFjdGl2ZWdcclxue1xyXG4gICAgaGVpZ2h0OiAzMHB4O1xyXG4gICAgYm9yZGVyLXJhZGl1czogMnB4O1xyXG4gICAgZm9udC1zaXplOiAxM3B4O1xyXG4gICAgYmFja2dyb3VuZC1jb2xvcjogd2hpdGU7XHJcbiAgICBjb2xvcjogI2MyMTg1YjtcclxufSBcclxuLnJlbW92ZS1wcmVmZXJlbmNle1xyXG4gICAgcGFkZGluZy1sZWZ0OiA4cHg7XHJcbiAgICBmb250LXNpemU6IDE1cHg7XHJcbiAgICBjdXJzb3I6IHBvaW50ZXJcclxufVxyXG4ucC1saXN0LXdpZHRoe1xyXG4gICAgd2lkdGg6IDE2OHB4O1xyXG59XHJcbi5tYXQtZXhwYW5zaW9uLXBhbmVsLWhlYWRlci10aXRsZSB7XHJcbiAgICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xyXG59XHJcbi5maWx0ZXItaWNvbntcclxuICAgIGNvbG9yOiB3aGl0ZTtcclxuICAgIGZvbnQtc2l6ZTogMzBweDtcclxuICAgIG1hcmdpbi10b3A6IC02cHg7XHJcbiAgICBwYWRkaW5nLXRvcDogMnB4O1xyXG59XHJcbi5tYXQtZXhwYW5zaW9uLXBhbmVsLWJvZHkge1xyXG4gICAgcGFkZGluZzogMCAyNHB4IDBweCAhaW1wb3J0YW50O1xyXG59XHJcbi5oZWlnaHQtdGFibGV7XHJcbiAgICBoZWlnaHQ6IDU1cHg7XHJcbn1cclxuLnBhbm5lbC1maWx0ZXJ7XHJcbiAgICBiYWNrZ3JvdW5kOiB3aGl0ZTtcclxuICAgIHBhZGRpbmc6IDEwcHggMTVweDtcclxuICAgIG1hcmdpbi10b3A6IDE1cHg7XHJcbiAgICBib3gtc2hhZG93OiAwIDNweCAxcHggLTJweCByZ2JhKDAsMCwwLC4yKSwgMCAycHggMnB4IDAgcmdiYSgwLDAsMCwuMTQpLCAwIDFweCA1cHggMCByZ2JhKDAsMCwwLC4xMik7XHJcbiAgICBtYXJnaW4tbGVmdDogMnB4O1xyXG4gICAgbWFyZ2luLXJpZ2h0OiAycHg7XHJcbiAgICBib3JkZXItdG9wOiA1cHggc29saWQgIzNGNzI5QjtcclxuICAgIHBhZGRpbmctdG9wOiAxNXB4O1xyXG59XHJcbjo6bmctZGVlcC51aS1tdWx0aXNlbGVjdCAudWktbXVsdGlzZWxlY3QtdHJpZ2dlciAudWktbXVsdGlzZWxlY3QtdHJpZ2dlci1pY29uIHtcclxuICAgIHRvcDogNTUlO1xyXG4gICAgbGVmdDogNjAlO1xyXG59XHJcbjo6bmctZGVlcCBsYWJlbCB7XHJcbiAgICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XHJcbiAgICAgbWFyZ2luLWJvdHRvbTogMDsgXHJcbn1cclxuOjpuZy1kZWVwIC5maWx0ZXItcGxpc3QgdWwgKiB7XHJcbiAgICBtYXJnaW46IDA7XHJcbiAgICAtbW96LWJvcmRlci1yYWRpdXM6IDA7XHJcbiAgICAtd2Via2l0LWJvcmRlci1yYWRpdXM6IDA7XHJcbiAgICBib3JkZXItcmFkaXVzOiAwO1xyXG4gICAgZm9udC1zaXplOiAxMXB4ICFpbXBvcnRhbnQ7XHJcbiAgICBtYXgtd2lkdGg6IDEyZW07XHJcbn0iXX0= */"

/***/ }),

/***/ "./src/app/Modules/FetaureModules/capacity-insights/capacity-report/capacity-report.component.html":
/*!*********************************************************************************************************!*\
  !*** ./src/app/Modules/FetaureModules/capacity-insights/capacity-report/capacity-report.component.html ***!
  \*********************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div *ngIf=\"pageLoading; else page\">\n  <div class=\"splash-screen\">\n    <div class=\"splash-loader\">\n      <div class=\"splash-cube1\"></div>\n      <div class=\"splash-cube2\"></div>\n      <div class=\"splash-cube4\"></div>\n      <div class=\"splash-cube3\"></div>\n    </div>\n  </div>\n</div>\n<ng-template #page>\n  <p-dialog header=\"Confirmation\" [(visible)]=\"msgdialog\" [modal]=\"true\" [responsive]=\"true\" [width]=\"700\"\n    [minY]=\"70\" [baseZIndex]=\"10000\">\n    <p-messages [value]=\"msgsuccess\" *ngIf=\"show\" [closable]=\"false\"></p-messages>\n  </p-dialog>\n  <div class=\"row pannel-filter\">\n      <div class=\"p-grid p-justify-around\">\n          <div class=\"p-col\"  *ngIf=\"filterapplied.ProfitCenter\"> \n            <p-listbox [listStyle]=\"{'max-height':'200px','height':'140px'}\" [options]=\"ProfitCenterNameS\" [(ngModel)]=\"ProfitCenterNameArray\"\n            multiple=\"multiple\" checkbox=\"checkbox\" filter=\"filter\" (onChange)=\"FetchFilteredCapacityData()\" class=\"filter-plist\">\n            <p-header>\n              <div class=\"textHeading heading\">\n                <p class=\"text-center\">Profit Center <span *ngIf=\"loadProfitCenterCard\"  class=\"fa fa-spinner fa-2x fa-spin filter-loader\"></span></p>\n                \n              </div>\n            </p-header>\n            <p-footer>\n              <div class=\"Cardfooter\">\n                <i class=\"fa fa-search-plus fa-1x plusIcon\" pTooltip=\"View More\" aria-hidden=\"true\" (click)=\"showAllPC()\"></i>\n                <i *ngIf=\"!(ProfitCenterNameArray.length==0)\" class=\"fa fa-eye arrowIcon fa-1x\"  pTooltip=\"View Selections\"aria-hidden=\"true\" (click)=\"showPC()\"></i>\n              </div>\n            </p-footer>\n          </p-listbox>\n          </div>\n          <div class=\"p-col\" *ngIf=\"filterapplied.DeliveryPlatform\">\n              <p-listbox [listStyle]=\"{'max-height':'200px','height':'140px'}\" [options]=\"DeliveryPartnerS\" id=\"DeliveryPartnerS\"\n              [(ngModel)]=\"DeliveryPartnerArray\" multiple=\"multiple\" checkbox=\"checkbox\" filter=\"filter\" (onChange)=\"FetchFilteredCapacityData()\" class=\"filter-plist\">\n              <p-header>\n                <div class=\"textHeading\">\n                  <p class=\"text-center\">Delivery Partner <span *ngIf=\"loadDeliveryPartnerCard\"  class=\"fa fa-spinner fa-2x fa-spin filter-loader\"></span></p>\n                </div>\n              </p-header>\n              <p-footer>\n                <div class=\"Cardfooter\">\n                  <i class=\"fa fa-search-plus fa-1x plusIcon\" pTooltip=\"View More\"aria-hidden=\"true\" (click)=\"showAllDP()\"></i>\n                  <i *ngIf=\"!(DeliveryPartnerArray.length==0)\" class=\"fa fa-eye arrowIcon fa-1x\" pTooltip=\"View Selections\" aria-hidden=\"true\" (click)=\"showDeliveryP()\"></i>\n                </div>\n              </p-footer>\n            </p-listbox>\n          </div>\n          <div class=\"p-col\" *ngIf=\"filterapplied.DeliveryManager\">\n              <p-listbox [listStyle]=\"{'max-height':'200px','height':'140px'}\" [options]=\"DeliveryManagerS\" [(ngModel)]=\"DeliveryManagerArray\"\n              multiple=\"multiple\" checkbox=\"checkbox\" filter=\"filter\" (onChange)=\"FetchFilteredCapacityData()\" class=\"filter-plist\">\n              <p-header>\n                <div class=\"textHeading\">\n                  <p class=\"text-center \">Delivery Manager <span *ngIf=\"loadDeliveryManagerCard\"  class=\"fa fa-spinner fa-2x fa-spin filter-loader\"></span></p>\n                </div>\n              </p-header>\n              <p-footer>\n                <div class=\"Cardfooter\">\n                  <i class=\"fa fa-search-plus fa-1x plusIcon\" pTooltip=\"View More\" aria-hidden=\"true\" (click)=\"showAllDM()\"></i>\n                  <i *ngIf=\"!(DeliveryManagerArray.length==0)\" class=\"fa fa-eye arrowIcon fa-1x\" pTooltip=\"View Selections\" aria-hidden=\"true\" (click)=\"showDM()\"></i>\n                </div>\n              </p-footer>\n            </p-listbox>\n          </div>\n          <div class=\"p-col\"  *ngIf=\"filterapplied.AccountGroup\">\n              <p-listbox [listStyle]=\"{'max-height':'200px','height':'140px'}\" [options]=\"AccountNameS\" [(ngModel)]=\"AccountNameArray\"\n              multiple=\"multiple\" checkbox=\"checkbox\" filter=\"filter\" (onChange)=\"FetchFilteredCapacityData()\" class=\"filter-plist\">\n              <p-header>\n                <div class=\"textHeading\">\n                  <p class=\"text-center\">Account Names <span *ngIf=\"loadAccountNamesCard\"  class=\"fa fa-spinner fa-2x fa-spin filter-loader\"></span></p>\n                </div>\n              </p-header>\n              <p-footer>\n                <div class=\"Cardfooter\">\n                  <i class=\"fa fa-search-plus fa-1x plusIcon\" pTooltip=\"View More\" aria-hidden=\"true\" (click)=\"showAllAN()\"></i>\n                  <i *ngIf=\"!(AccountNameArray.length==0)\" class=\"fa fa-eye arrowIcon fa-1x\"  pTooltip=\"View Selections\" aria-hidden=\"true\" (click)=\"showAccountN()\"></i>\n                </div>\n              </p-footer>\n            </p-listbox>\n          </div>\n          <div class=\"p-col\" *ngIf=\"filterapplied.ProjectName\">\n              <p-listbox [listStyle]=\"{'max-height':'200px','height':'140px'}\" [options]=\"ProjectNameS\" [(ngModel)]=\"ProjectNameArray\"\n              multiple=\"multiple\" checkbox=\"checkbox\" filter=\"filter\" (onChange)=\"FetchFilteredCapacityData()\" class=\"filter-plist\" >\n              <p-header>\n                <div class=\"textHeading\">\n                  <p class=\"text-center\">Projects <span *ngIf=\"loadProjectsCard\"  class=\"fa fa-spinner fa-2x fa-spin filter-loader\"></span></p>\n                </div>\n              </p-header>\n              <p-footer>\n                <div class=\"Cardfooter\">\n                  <i class=\"fa fa-search-plus fa-1x plusIcon\" pTooltip=\"View More\" aria-hidden=\"true\" (click)=\"showAllPN()\"></i>\n                  <i *ngIf=\"!(ProjectNameArray.length==0)\" class=\"fa fa-eye arrowIcon fa-1x\" pTooltip=\"View Selections\"aria-hidden=\"true\" (click)=\"showProjectN()\"></i>\n                </div>\n              </p-footer>\n            </p-listbox>\n          </div>\n          <div class=\"p-col\" *ngIf=\"filterapplied.Practice\">\n              <p-listbox [listStyle]=\"{'max-height':'200px','height':'140px'}\" [options]=\"PracticeS\" [(ngModel)]=\"PracticeArray\"\n              multiple=\"multiple\" checkbox=\"checkbox\" filter=\"filter\" (onChange)=\"FetchFilteredCapacityData()\" class=\"filter-plist\">\n              <p-header>\n                <div class=\"textHeading\">\n                  <p class=\"text-center\">Practices <span *ngIf=\"loadPracticesCard\"  class=\"fa fa-spinner fa-2x fa-spin filter-loader\"></span></p>\n                </div>\n              </p-header>\n              <p-footer>\n                <div class=\"Cardfooter\">\n                  <i class=\"fa fa-search-plus fa-1x plusIcon\" pTooltip=\"View More\"  aria-hidden=\"true\" (click)=\"showAllP()\"></i>\n                  <i *ngIf=\"!(PracticeArray.length==0)\" class=\"fa fa-eye arrowIcon fa-1x\" pTooltip=\"View Selections\" aria-hidden=\"true\" (click)=\"showPractise()\"></i>\n                </div>\n              </p-footer>\n            </p-listbox>\n          </div>\n          <div class=\"p-col\" *ngIf=\"filterapplied.BillableStatus\">\n              <p-listbox [listStyle]=\"{'max-height':'200px','height':'140px'}\" [options]=\"BillableS\" [(ngModel)]=\"BillableArray\"\n              multiple=\"multiple\" checkbox=\"checkbox\" filter=\"filter\" (onChange)=\"FetchFilteredCapacityData()\" class=\"filter-plist\" >\n              <p-header>\n                <div class=\"textHeading\">\n                  <p class=\"text-center\">Billable Status <span *ngIf=\"loadBillableStatusCard\"  class=\"fa fa-spinner fa-2x fa-spin filter-loader\"></span></p>\n                </div>\n              </p-header>\n              <p-footer>\n                <div class=\"Cardfooter\">\n                  <i class=\"fa fa-search-plus fa-1x plusIcon\" pTooltip=\"View More\"   aria-hidden=\"true\" (click)=\"showAllBS()\"></i>\n                  <i *ngIf=\"!(BillableArray.length==0)\" class=\"fa fa-eye arrowIcon fa-1x\" pTooltip=\"View Selections\" aria-hidden=\"true\" (click)=\"showBillableS()\"></i>\n                </div>\n              </p-footer>\n            </p-listbox>\n          </div>\n          <div class=\"p-col\" *ngIf=\"filterapplied.Location\">\n              <p-listbox [listStyle]=\"{'max-height':'200px','height':'140px'}\" [options]=\"LocationS\" [(ngModel)]=\"LocationArray\"\n              multiple=\"multiple\" checkbox=\"checkbox\" filter=\"filter\" (onChange)=\"FetchFilteredCapacityData()\" class=\"filter-plist\" >\n              <p-header>\n                <div class=\"textHeading\">\n                  <p class=\"text-center\">Location <span *ngIf=\"loadLocationCard\"  class=\"fa fa-spinner fa-2x fa-spin filter-loader\"></span></p>\n                </div>\n              </p-header>\n              <p-footer>\n                <div class=\"Cardfooter\">\n                  <i class=\"fa fa-search-plus fa-1x plusIcon\" pTooltip=\"View More\" aria-hidden=\"true\" (click)=\"showAllL()\"></i>\n                  <i *ngIf=\"!(LocationArray.length==0)\" class=\"fa fa-eye arrowIcon fa-1x\" pTooltip=\"View Selections\" aria-hidden=\"true\" (click)=\"showLocation()\"></i>\n                </div>\n              </p-footer>\n            </p-listbox>\n          </div>\n      </div>\n  </div>\n\n  <!-- <mat-accordion>\n    <mat-expansion-panel [expanded]=\"step === 0\" (opened)=\"setStep(0)\" hideToggle=\"true\">\n      <mat-expansion-panel-header class=\"panel-header\">\n        <mat-panel-title>\n          <p class=\"panel-title\">Select Filter</p>\n        </mat-panel-title>\n        \n      </mat-expansion-panel-header>\n      <div *ngIf=\"filterapplied\"> \n        <div class=\"ui-g\">\n          <div class=\"ui-g-12 ui-md-6 ui-lg-2 p-list-width\" *ngIf=\"filterapplied.ProfitCenter\">\n            <p-listbox [listStyle]=\"{'max-height':'200px','height':'150px'}\" [options]=\"ProfitCenterNameS\" [(ngModel)]=\"ProfitCenterNameArray\"\n              multiple=\"multiple\" checkbox=\"checkbox\" filter=\"filter\" (onChange)=\"FetchFilteredCapacityData()\">\n              <p-header>\n                <div class=\"textHeading heading\">\n                  <p class=\"text-center\">Profit Center <span *ngIf=\"loadProfitCenterCard\"  class=\"fa fa-spinner fa-2x fa-spin filter-loader\"></span></p>\n                  \n                </div>\n              </p-header>\n              <p-footer>\n                <div class=\"Cardfooter\">\n                  <i class=\"fa fa-search-plus fa-1x plusIcon\" pTooltip=\"View More\" aria-hidden=\"true\" (click)=\"showAllPC()\"></i>\n                  <i *ngIf=\"!(ProfitCenterNameArray.length==0)\" class=\"fa fa-eye arrowIcon fa-1x\"  pTooltip=\"View Selections\"aria-hidden=\"true\" (click)=\"showPC()\"></i>\n                </div>\n              </p-footer>\n            </p-listbox>\n          </div>\n          <div class=\"ui-g-12 ui-md-6 ui-lg-2 p-list-width\" *ngIf=\"filterapplied.DeliveryPlatform\">\n            <p-listbox [listStyle]=\"{'max-height':'200px','height':'150px'}\" [options]=\"DeliveryPartnerS\" id=\"DeliveryPartnerS\"\n              [(ngModel)]=\"DeliveryPartnerArray\" multiple=\"multiple\" checkbox=\"checkbox\" filter=\"filter\" (onChange)=\"FetchFilteredCapacityData()\">\n              <p-header>\n                <div class=\"textHeading\">\n                  <p class=\"text-center\">Delivery Partner <span *ngIf=\"loadDeliveryPartnerCard\"  class=\"fa fa-spinner fa-2x fa-spin filter-loader\"></span></p>\n                </div>\n              </p-header>\n              <p-footer>\n                <div class=\"Cardfooter\">\n                  <i class=\"fa fa-search-plus fa-1x plusIcon\" pTooltip=\"View More\"aria-hidden=\"true\" (click)=\"showAllDP()\"></i>\n                  <i *ngIf=\"!(DeliveryPartnerArray.length==0)\" class=\"fa fa-eye arrowIcon fa-1x\" pTooltip=\"View Selections\" aria-hidden=\"true\" (click)=\"showDeliveryP()\"></i>\n                </div>\n              </p-footer>\n            </p-listbox>\n          </div>\n          <div class=\"ui-g-12 ui-md-6 ui-lg-2 p-list-width\" *ngIf=\"filterapplied.DeliveryManager\">\n            <p-listbox [listStyle]=\"{'max-height':'200px','height':'150px'}\" [options]=\"DeliveryManagerS\" [(ngModel)]=\"DeliveryManagerArray\"\n              multiple=\"multiple\" checkbox=\"checkbox\" filter=\"filter\" (onChange)=\"FetchFilteredCapacityData()\">\n              <p-header>\n                <div class=\"textHeading\">\n                  <p class=\"text-center \">Delivery Manager <span *ngIf=\"loadDeliveryManagerCard\"  class=\"fa fa-spinner fa-2x fa-spin filter-loader\"></span></p>\n                </div>\n              </p-header>\n              <p-footer>\n                <div class=\"Cardfooter\">\n                  <i class=\"fa fa-search-plus fa-1x plusIcon\" pTooltip=\"View More\" aria-hidden=\"true\" (click)=\"showAllDM()\"></i>\n                  <i *ngIf=\"!(DeliveryManagerArray.length==0)\" class=\"fa fa-eye arrowIcon fa-1x\" pTooltip=\"View Selections\" aria-hidden=\"true\" (click)=\"showDM()\"></i>\n                </div>\n              </p-footer>\n            </p-listbox>\n          </div>\n          <div class=\"ui-g-12 ui-md-6 ui-lg-2 p-list-width\" *ngIf=\"filterapplied.AccountGroup\">\n            <p-listbox [listStyle]=\"{'max-height':'200px','height':'150px'}\" [options]=\"AccountNameS\" [(ngModel)]=\"AccountNameArray\"\n              multiple=\"multiple\" checkbox=\"checkbox\" filter=\"filter\" (onChange)=\"FetchFilteredCapacityData()\">\n              <p-header>\n                <div class=\"textHeading\">\n                  <p class=\"text-center\">Account Names <span *ngIf=\"loadAccountNamesCard\"  class=\"fa fa-spinner fa-2x fa-spin filter-loader\"></span></p>\n                </div>\n              </p-header>\n              <p-footer>\n                <div class=\"Cardfooter\">\n                  <i class=\"fa fa-search-plus fa-1x plusIcon\" pTooltip=\"View More\" aria-hidden=\"true\" (click)=\"showAllAN()\"></i>\n                  <i *ngIf=\"!(AccountNameArray.length==0)\" class=\"fa fa-eye arrowIcon fa-1x\"  pTooltip=\"View Selections\" aria-hidden=\"true\" (click)=\"showAccountN()\"></i>\n                </div>\n              </p-footer>\n            </p-listbox>\n          </div>\n          <div class=\"ui-g-12 ui-md-6 ui-lg-2 p-list-width\" *ngIf=\"filterapplied.ProjectName\">\n            <p-listbox [listStyle]=\"{'max-height':'200px','height':'150px'}\" [options]=\"ProjectNameS\" [(ngModel)]=\"ProjectNameArray\"\n              multiple=\"multiple\" checkbox=\"checkbox\" filter=\"filter\" (onChange)=\"FetchFilteredCapacityData()\">\n              <p-header>\n                <div class=\"textHeading\">\n                  <p class=\"text-center\">Projects <span *ngIf=\"loadProjectsCard\"  class=\"fa fa-spinner fa-2x fa-spin filter-loader\"></span></p>\n                </div>\n              </p-header>\n              <p-footer>\n                <div class=\"Cardfooter\">\n                  <i class=\"fa fa-search-plus fa-1x plusIcon\" pTooltip=\"View More\" aria-hidden=\"true\" (click)=\"showAllPN()\"></i>\n                  <i *ngIf=\"!(ProjectNameArray.length==0)\" class=\"fa fa-eye arrowIcon fa-1x\" pTooltip=\"View Selections\"aria-hidden=\"true\" (click)=\"showProjectN()\"></i>\n                </div>\n              </p-footer>\n            </p-listbox>\n          </div>\n          <div class=\"ui-g-12 ui-md-6 ui-lg-2 p-list-width\" *ngIf=\"filterapplied.Practice\">\n            <p-listbox [listStyle]=\"{'max-height':'200px','height':'150px'}\" [options]=\"PracticeS\" [(ngModel)]=\"PracticeArray\"\n              multiple=\"multiple\" checkbox=\"checkbox\" filter=\"filter\" (onChange)=\"FetchFilteredCapacityData()\">\n              <p-header>\n                <div class=\"textHeading\">\n                  <p class=\"text-center\">Practices <span *ngIf=\"loadPracticesCard\"  class=\"fa fa-spinner fa-2x fa-spin filter-loader\"></span></p>\n                </div>\n              </p-header>\n              <p-footer>\n                <div class=\"Cardfooter\">\n                  <i class=\"fa fa-search-plus fa-1x plusIcon\" pTooltip=\"View More\"  aria-hidden=\"true\" (click)=\"showAllP()\"></i>\n                  <i *ngIf=\"!(PracticeArray.length==0)\" class=\"fa fa-eye arrowIcon fa-1x\" pTooltip=\"View Selections\" aria-hidden=\"true\" (click)=\"showPractise()\"></i>\n                </div>\n              </p-footer>\n            </p-listbox>\n          </div>\n          <div class=\"ui-g-12 ui-md-6 ui-lg-2 p-list-width\" *ngIf=\"filterapplied.BillableStatus\">\n            <p-listbox [listStyle]=\"{'max-height':'200px','height':'150px'}\" [options]=\"BillableS\" [(ngModel)]=\"BillableArray\"\n              multiple=\"multiple\" checkbox=\"checkbox\" filter=\"filter\" (onChange)=\"FetchFilteredCapacityData()\">\n              <p-header>\n                <div class=\"textHeading\">\n                  <p class=\"text-center\">Billable Status <span *ngIf=\"loadBillableStatusCard\"  class=\"fa fa-spinner fa-2x fa-spin filter-loader\"></span></p>\n                </div>\n              </p-header>\n              <p-footer>\n                <div class=\"Cardfooter\">\n                  <i class=\"fa fa-search-plus fa-1x plusIcon\" pTooltip=\"View More\"   aria-hidden=\"true\" (click)=\"showAllBS()\"></i>\n                  <i *ngIf=\"!(BillableArray.length==0)\" class=\"fa fa-eye arrowIcon fa-1x\" pTooltip=\"View Selections\" aria-hidden=\"true\" (click)=\"showBillableS()\"></i>\n                </div>\n              </p-footer>\n            </p-listbox>\n          </div>\n          <div class=\"ui-g-12 ui-md-6 ui-lg-2 p-list-width\" *ngIf=\"filterapplied.Location\">\n            <p-listbox [listStyle]=\"{'max-height':'200px','height':'150px'}\" [options]=\"LocationS\" [(ngModel)]=\"LocationArray\"\n              multiple=\"multiple\" checkbox=\"checkbox\" filter=\"filter\" (onChange)=\"FetchFilteredCapacityData()\">\n              <p-header>\n                <div class=\"textHeading\">\n                  <p class=\"text-center\">Location <span *ngIf=\"loadLocationCard\"  class=\"fa fa-spinner fa-2x fa-spin filter-loader\"></span></p>\n                </div>\n              </p-header>\n              <p-footer>\n                <div class=\"Cardfooter\">\n                  <i class=\"fa fa-search-plus fa-1x plusIcon\" pTooltip=\"View More\" aria-hidden=\"true\" (click)=\"showAllL()\"></i>\n                  <i *ngIf=\"!(LocationArray.length==0)\" class=\"fa fa-eye arrowIcon fa-1x\" pTooltip=\"View Selections\" aria-hidden=\"true\" (click)=\"showLocation()\"></i>\n                </div>\n              </p-footer>\n            </p-listbox>\n          </div>\n\n          <div class=\"ui-g-12 ui-md-6 ui-lg-2 p-list-width\" *ngIf=\"filterapplied.SRStatus\">\n            <p-listbox [listStyle]=\"{'max-height':'200px','height':'150px'}\" [options]=\"SRStatusS\" [(ngModel)]=\"SRStatusArray\"\n              multiple=\"multiple\" checkbox=\"checkbox\" filter=\"filter\" (onChange)=\"FetchFilteredCapacityData()\">\n              <p-header>\n                <div class=\"textHeading\">\n                  <p class=\"text-center\">SR Status <span *ngIf=\"loadSRStatusCard\"  class=\"fa fa-spinner fa-2x fa-spin filter-loader\"></span></p>\n                </div>\n              </p-header>\n              <p-footer>\n                <div class=\"Cardfooter\">\n                  <i class=\"fa fa-search-plus fa-1x plusIcon\" pTooltip=\"View More\" aria-hidden=\"true\" (click)=\"showAllSR()\"></i>\n                  <i *ngIf=\"!(SRStatusArray.length==0)\" class=\"fa fa-eye arrowIcon fa-1x\" pTooltip=\"View Selections\" aria-hidden=\"true\" (click)=\"showSRS()\"></i>\n                </div>\n              </p-footer>\n            </p-listbox>\n          </div>\n\n          <div class=\"ui-g-12 ui-md-6 ui-lg-2 p-list-width\" *ngIf=\"filterapplied.EngagementType\">\n            <p-listbox [listStyle]=\"{'max-height':'200px','height':'150px'}\" [options]=\"EngagementTypeS\" [(ngModel)]=\"EngagementTypeArray\"\n              multiple=\"multiple\" checkbox=\"checkbox\" filter=\"filter\" (onChange)=\"FetchFilteredCapacityData()\">\n              <p-header>\n                <div class=\"textHeading\">\n                  <p class=\"text-center\">Engagement Type <span *ngIf=\"loadEngagementTypeCard\"  class=\"fa fa-spinner fa-2x fa-spin filter-loader\"></span></p>\n                </div>\n              </p-header>\n              <p-footer>\n                <div class=\"Cardfooter\">\n                  <i class=\"fa fa-search-plus fa-1x plusIcon\" pTooltip=\"View More\"  aria-hidden=\"true\" (click)=\"showAllET()\"></i>\n                  <i *ngIf=\"!(EngagementTypeArray.length==0)\" class=\"fa fa-eye arrowIcon fa-1x\" pTooltip=\"View Selections\" aria-hidden=\"true\" (click)=\"showET()\"></i>\n                </div>\n              </p-footer>\n            </p-listbox>\n          </div>\n        </div>\n      </div>\n\n      <div class=\"ui-g-12 ui-md-6 ui-lg-11\">\n        <button class=\"btn button-cancel\" (click)=\"nextStep()\">Cancel</button>\n        <button class=\"btn button-save\" (click)=\"showDialog()\" [disabled]=\"isSavePrefOn\">Save Filter</button>\n      </div>\n    </mat-expansion-panel>\n  </mat-accordion> -->\n<div class=\"row\">\n  <div class=\"col-sm-12\">\n    <p-table #dt *ngIf=\"haveRows; else message\" [columns]=\"columns\" [value]=\"capacityData\" [paginator]=\"true\" [rows]=\"10\"\n    [totalRecords]=\"totalRecords\" [loading]=\"loading\" [responsive]=\"true\">\n    <ng-template pTemplate=\"caption\">\n      <div class=\"height-table\">\n        <span>\n            <div class=\"btn-group margin-left margin-top-xs float-left margin-left-mobile\">\n                <p-multiSelect [options]=\"genderArray\" [(ngModel)]=\"selectedGender\" (onChange)=\"FetchFilteredCapacityData()\"\n                  defaultLabel=\"Gender\" class=\"btn-gender\"></p-multiSelect>\n              </div>\n          <!-- <div class=\"btn-group margin-top-xs float-left margin-left-neg\"  role=\"group\" aria-label=\"Basic example\" (click)=\"onButtonGroupClick2($event)\">\n            <button id=\"btngender1\" class=\"btn btn-gender\" (click)=\"MaleFilter(dt,$event)\">Male</button>\n            <button id=\"btngender2\" class=\"btn btn-gender margin-left-xs\" (click)=\"FemaleFilter(dt,$event)\">Female</button>\n          </div> -->\n          <div class=\"btn-group margin-left margin-top-xs float-left margin-left-mobile\">\n              <p-multiSelect [options]=\"onOffshoreArray\" [(ngModel)]=\"selectedOnOffshore\" (onChange)=\"FetchFilteredCapacityData()\"\n                defaultLabel=\"On/offshore\" class=\"btn-onoffshore\"></p-multiSelect>\n            </div>\n          <!-- <div class=\"btn-group  margin-left margin-top-xs float-left\" role=\"group\" aria-label=\"Basic example\" (click)=\"onButtonGroupClick($event)\">\n            <button id=\"btnonoffshore1\" type=\"button\" class=\"btn  btn-onoffshore\" (click)=\" OnShoreFilter(dt,$event) \">Onshore</button>   <!--filter is a predefine pipe--> \n            <!-- <button id=\"btnonoffshore2\" type=\"button\" class=\"btn   btn-onoffshore margin-left-xs\" (click)=\" OffShoreFilter(dt, $event) \">Offshore</button>\n\n          </div> -->\n          <div class=\"btn-group margin-left margin-top-xs float-left margin-left-mobile\">\n              <p-multiSelect [options]=\"srStatusArray\" [(ngModel)]=\"selectedSRStatus\" (onChange)=\"FetchFilteredCapacityData()\"\n                defaultLabel=\"SR Status\" class=\"button-reset\"></p-multiSelect>\n            </div>\n            <div class=\"btn-group margin-left margin-top-xs float-left margin-left-mobile\">\n                <p-multiSelect [options]=\"engagementTypeArray\" [(ngModel)]=\"selectedEngagementType\" (onChange)=\"FetchFilteredCapacityData()\"\n                  defaultLabel=\"Engagement Type\" class=\"button-engtype\"></p-multiSelect>\n              </div>\n          <div class=\"btn-group margin-left margin-top-xs float-left margin-left-mobile\">\n            <p-multiSelect [options]=\"CompetencyArray\" [(ngModel)]=\"selectedCompetencies\" (onChange)=\"FetchFilteredCapacityData()\"\n              defaultLabel=\"Competency\" class=\"button-compentency\"></p-multiSelect>\n          </div>\n          <div class=\"btn-group margin-left margin-top-xs float-left\">\n            <button class=\"btn fa fa-repeat button-reset-table\" pTooltip=\"Remove filters applied\"  tooltipPosition=\"bottom\" type=\"button\"\n              (click)=\"reset()\" (click)=\"dt.reset()\"></button>\n\n          </div>\n          <div class=\"btn-group margin-left margin-top-xs float-left\">\n              <button class=\"btn button-save\" (click)=\"showDialog()\" [disabled]=\"isSavePrefOn\">Save Filter</button>\n            </div>\n          \n          <div class=\"search-block float-left margin-left-mobile\">\n            <i class=\"fa fa-search seacrh-icon\"></i>\n            <input type=\"text\" [(ngModel)]=\"SearchKey\" pInputText size=\"50\"  pTooltip=\"Press enter to refresh data\" placeholder=\"Global Filter\" (keyup.enter)=\"FetchResultFromDB(dt)\"   (input)=\"dt.filterGlobal(this.SearchKey, 'contains')\"\n              class=\"search-text\">\n          </div>\n        </span>\n        <div class=\"display-flex\">\n          <span class=\"count-table float-right\"><span class=\"count-bubble\" pTooltip=\"Total records found\"  tooltipPosition=\"bottom\">{{dt.totalRecords}}</span></span>\n        </div>\n        <!-- <p-multiSelect class=\"\" [options]=\"ColumnSelections\" (onChange)=\"UpdateColumns()\" [(ngModel)]=\"selectedColumns\" defaultLabel=\"Select Columns to view\" ></p-multiSelect> -->\n\n        <mat-form-field>\n          <mat-select  #mySelect placeholder=\"select columns to display\" [formControl]=\"SelectedColumns\"\n\n            (selectionChange)=\"UpdateColumns()\"  multiple  [compareWith]=\"compareFn\">\n\n            <div class=\"textHeading\">\n            <p class=\"TextAligncentre\">Edit Columns</p>\n            </div>\n            <mat-option *ngFor=\"let col of ColumnSelections \" [value]=\"col.value\">{{col.label}}</mat-option>\n          </mat-select>\n        </mat-form-field>\n        <i class=\"fa fa-cog fa-2x setting-table\" (click)=\"ViewOptions()\" aria-hidden=\"true\" pTooltip=\"Select columns to display\"  tooltipPosition=\"bottom\"></i>\n\n        <img src=\"../assets/layout/images/excel_logo.png\" (click)=\"dt.exportCSV()\" class=\"img-excel\" pTooltip=\"Export table to excel\"  tooltipPosition=\"bottom\">\n        <div class=\"display-flex float-right\" *ngIf=\"isDefaultPref\">\n          <span class=\"count-table\"><span class=\"count-bubblePref\" pTooltip=\"Applied Preference\"  tooltipPosition=\"bottom\" >{{selPref}}  <i class=\"fa fa-close remove-preference\" (click)=\"resetDefaultPref()\"></i></span></span>\n        </div>\n\n      </div>\n    </ng-template>\n    <ng-template pTemplate=\"header\" let-columns>\n      <tr>\n        <th *ngFor=\"let col of columns\" [pSortableColumn]=\"col.field\" [ngSwitch]=\"col.field\" >\n          {{col.header}}\n         \n        \n          <p-sortIcon [field]=\"col.field\" ariaLabel=\"Activate to sort\" ariaLabelDesc=\"Activate to sort in descending order\"\n            ariaLabelAsc=\"Activate to sort in ascending order\"></p-sortIcon>\n        </th>\n      </tr>\n    </ng-template>\n    <ng-template pTemplate=\"body\" let-rowData let-columns=\"columns\">\n      <tr>\n\n        <td *ngFor=\"let col of columns\">\n            <span class=\"ui-column-title\">{{col.header}}</span>\n          <span pTooltip=\" {{rowData[col.field]}}\" tooltipPosition=\"top\">{{rowData[col.field]}}</span>  \n         \n        </td>\n\n     \n      </tr>\n\n    </ng-template>\n     <div *ngIf=\"dt.totalRecords == 0\" class=\"alert alert-info\">\n      <strong>Info!!</strong> No rows found\n    </div>\n  </p-table>\n  \n   <div *ngIf=\"SearchResult\" class=\"alert alert-info\">\n      <strong>Info!!</strong> No rows found\n    </div>\n  </div>\n</div>\n  \n  <ng-template #message>\n    <div class=\"alert alert-info\">\n      <strong>Info!!</strong> No rows found\n    </div>\n  </ng-template>\n\n  <!-- View Selected Filter buttons -->\n  <p-dialog header=\"Delivery Partner\" (onHide)=\"cancel()\" [(visible)]=\"displayDP\" width=\"700px\" modal=\"modal\" \n    responsive=\"true\">\n      \n    <span *ngFor=\"let filter of DeliveryPartnerArray\">    \n      <button class=\"btn button-select\" id={{filter}}>{{filter}} <i class=\"fa fa-close\" (click)=\"UncheckDP($event)\"></i></button>\n    </span>\n  <p *ngIf=\"(DeliveryPartnerArray.length==0)\">No selection found</p>\n  </p-dialog>\n  <p-dialog header=\"Delivery Manager\" (onHide)=\"cancel()\" [(visible)]=\"displayDM\" modal=\"modal\"  responsive=\"true\">\n    <span *ngFor=\"let filter of DeliveryManagerArray\">\n      <button class=\"btn button-select\" id={{filter}}>{{filter}} <i class=\"fa fa-close\" (click)=\"UncheckDM($event)\"></i></button>\n    </span>\n   <p *ngIf=\"(DeliveryManagerArray.length==0)\">No selection found</p> \n  </p-dialog>\n  <p-dialog header=\"Profit Center\" (onHide)=\"cancel()\" [(visible)]=\"displayPC\" modal=\"modal\"  responsive=\"true\">\n    <span *ngFor=\"let filter of ProfitCenterNameArray\">\n      <button class=\"btn button-select\" id={{filter}}>{{filter}} <i class=\"fa fa-close\" (click)=\"UncheckPC($event)\"></i></button>\n      </span>\n <p *ngIf=\"(ProfitCenterNameArray.length==0)\">No selection found</p>  \n \n  </p-dialog>\n  <p-dialog header=\"Project Name\" (onHide)=\"cancel()\" [(visible)]=\"displayPN\" modal=\"modal\"  responsive=\"true\">\n    <span *ngFor=\"let filter of ProjectNameArray\">\n      <button class=\"btn button-select\" id={{filter}}>{{filter}} <i class=\"fa fa-close\" (click)=\"UncheckPN($event)\"></i></button>\n    </span>\n  <p *ngIf=\"(ProjectNameArray.length==0)\">No selection found</p>\n  </p-dialog>\n  <p-dialog header=\"Account Name\" (onHide)=\"cancel()\" [(visible)]=\"displayAN\" modal=\"modal\"  responsive=\"true\">\n    <span *ngFor=\"let filter of AccountNameArray\">\n      <button class=\"btn button-select\" id={{filter}}>{{filter}} <i class=\"fa fa-close\" (click)=\"UncheckAN($event)\"></i></button>\n    </span>\n <p *ngIf=\"(AccountNameArray.length==0)\">No selection found</p>\n  </p-dialog>\n  <p-dialog header=\"Practise\" (onHide)=\"cancel()\" [(visible)]=\"displayP\" modal=\"modal\"  responsive=\"true\">\n    <span *ngFor=\"let filter of PracticeArray\">\n      <button class=\"btn button-select\" id={{filter}}>{{filter}} <i class=\"fa fa-close\" (click)=\"UncheckP($event)\"></i></button>\n    </span>\n  <p *ngIf=\"(PracticeArray.length==0)\">No selection found</p>\n  </p-dialog>\n  <p-dialog header=\"Billable Status\" (onHide)=\"cancel()\" [(visible)]=\"displayBilable\" modal=\"modal\" \n    responsive=\"true\">\n    <span *ngFor=\"let filter of BillableArray\">\n      <button class=\"btn button-select\" id={{filter}}>{{filter}} <i class=\"fa fa-close\" (click)=\"UncheckB($event)\"></i></button>\n   </span>\n     <p *ngIf=\"(BillableArray.length==0)\">No selection found</p>\n  </p-dialog>\n  <p-dialog header=\"Location\" (onHide)=\"cancel()\" [(visible)]=\"displayL\" modal=\"modal\"  responsive=\"true\">\n    <span *ngFor=\"let filter of LocationArray\">\n      <button class=\"btn button-select\" id={{filter}}>{{filter}} <i class=\"fa fa-close\" (click)=\"UncheckL($event)\"></i></button>\n    </span>\n    <p *ngIf=\"(LocationArray.length==0)\">No selection found</p>\n  </p-dialog>\n\n\n\n  <!-- View All Filter Option button -->\n  <p-dialog header=\"Delivery partner\" (onHide)=\"cancel()\" [(visible)]=\"overlaybasicDeliveryP\" modal=\"modal\" \n    responsive=\"true\">\n   \n    <p-listbox [options]=\"DeliveryPartnerS\" [(ngModel)]=\"DeliveryPartnerArray\" multiple=\"multiple\" checkbox=\"checkbox\"\n      filter=\"filter\" (onChange)=\"FetchFilteredCapacityData()\"></p-listbox>\n  </p-dialog>\n  <p-dialog header=\"Delivery Manager\" (onHide)=\"cancel()\" [(visible)]=\"showAllDeliveryManager\" modal=\"modal\" \n    responsive=\"true\">\n    <p-listbox [options]=\"DeliveryManagerS\" [(ngModel)]=\"DeliveryManagerArray\" multiple=\"multiple\" checkbox=\"checkbox\"\n      filter=\"filter\" (onChange)=\"FetchFilteredCapacityData()\"></p-listbox>\n  </p-dialog>\n  <p-dialog header=\"Account Name\" (onHide)=\"cancel()\" [(visible)]=\"showAllAccountName\" modal=\"modal\" \n    responsive=\"true\" >\n    <p-listbox [options]=\"AccountNameS\" [(ngModel)]=\"AccountNameArray\" multiple=\"multiple\" checkbox=\"checkbox\" filter=\"filter\" (onChange)=\"FetchFilteredCapacityData()\"></p-listbox>\n  </p-dialog>\n  <p-dialog header=\"Profit Center\" (onHide)=\"cancel()\" [(visible)]=\"showAllProfitCenter\" modal=\"modal\"\n    responsive=\"true\" (onChange)=\"onChangeDP()\">\n    <p-listbox [options]=\"ProfitCenterNameS\" [(ngModel)]=\"ProfitCenterNameArray\" multiple=\"multiple\" checkbox=\"checkbox\"\n      filter=\"filter\" (onChange)=\"FetchFilteredCapacityData()\"></p-listbox>\n  </p-dialog>\n  <p-dialog header=\"Project Names\" (onHide)=\"cancel()\" [(visible)]=\"showAllProjectName\" modal=\"modal\" \n    responsive=\"true\" (onChange)=\"FetchFilteredCapacityData()\">\n    <p-listbox [options]=\"ProjectNameS\" [(ngModel)]=\"ProjectNameArray\" multiple=\"multiple\" checkbox=\"checkbox\" filter=\"filter\" (onChange)=\"FetchFilteredCapacityData()\"></p-listbox>\n  </p-dialog>\n  <p-dialog header=\"Practice\" (onHide)=\"cancel()\" [(visible)]=\"showAllpractice\" modal=\"modal\"  responsive=\"true\">\n    <p-listbox [options]=\"PracticeS\" [(ngModel)]=\"PracticeArray\" multiple=\"multiple\" checkbox=\"checkbox\" filter=\"filter\" (onChange)=\"FetchFilteredCapacityData()\"></p-listbox>\n  </p-dialog>\n  <p-dialog header=\"Billable Status\" (onHide)=\"cancel()\" [(visible)]=\"showAllBillable\" modal=\"modal\" \n    responsive=\"true\">\n    <p-listbox [options]=\"BillableS\" [(ngModel)]=\"BillableArray\" multiple=\"multiple\" checkbox=\"checkbox\" filter=\"filter\" (onChange)=\"FetchFilteredCapacityData()\"></p-listbox>\n  </p-dialog>\n  <p-dialog header=\"Location\" (onHide)=\"cancel()\" [(visible)]=\"showAllLocation\" modal=\"modal\"  responsive=\"true\">\n    <p-listbox [options]=\"LocationS\" [(ngModel)]=\"LocationArray\" multiple=\"multiple\" checkbox=\"checkbox\" filter=\"filter\" (onChange)=\"FetchFilteredCapacityData()\"></p-listbox>\n  </p-dialog>\n\n  <!-- Modal to save preference as default -->\n\n\n  <p-dialog header=\"Confirmation\" [(visible)]=\"displaydialog\" [modal]=\"true\" [responsive]=\"true\" \n     [minY]=\"70\" [maximizable]=\"true\" [baseZIndex]=\"10000\">\n    <form [formGroup]=\"confirmationForm\" (ngSubmit)=\"savePref()\" class=\"p-3 bg-faded\">\n      <div class=\"form-group ui-g\">\n        <div class=\"ui-g-12 ui-md-6 ui-lg-12\">\n          <span class=\"text-modal\">Preference Name</span>\n        </div>\n        <div class=\"ui-g-12 ui-md-6 ui-lg-12\">\n          <input id=\"prefName\" type=\"text\" class=\"form-control\" formControlName=\"prefName\" (input)=\"onSearchChange($event.target.value)\" />\n          <div *ngIf=\"isEmpty\" class=\"error\">\n            <div>A Preference name is required</div>\n            </div>\n        </div>\n      </div>\n      <div class=\"form-group ui-g ui-g-12 ui-md-6 ui-lg-11\">\n        <div>\n          <span class=\"text-modal\">Save this as your default preference?</span>\n        </div>\n        <p-checkbox name=\"group1\" value=\"Yes\" label=\"\"  formControlName=\"def\" class=\"prefrence-check\"></p-checkbox>\n      </div>\n      <div class=\"ui-g-12 ui-md-6 ui-lg-12\">\n        <button type=\"submit\" pButton icon=\"pi pi-close\"  label=\"Save and Apply\" class=\"btn button-fetch float-right\" [disabled]=\"isPrefName\"></button>\n        <button type=\"button\" pButton icon=\"pi pi-close\" (click)=\"closeDialog()\" label=\"Cancel\" class=\"btn button-cancel float-right\"></button>\n      </div>\n    </form>\n  </p-dialog>\n\n</ng-template>\n<router-outlet></router-outlet>\n"

/***/ }),

/***/ "./src/app/Modules/FetaureModules/capacity-insights/capacity-report/capacity-report.component.ts":
/*!*******************************************************************************************************!*\
  !*** ./src/app/Modules/FetaureModules/capacity-insights/capacity-report/capacity-report.component.ts ***!
  \*******************************************************************************************************/
/*! exports provided: CapacityReportComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CapacityReportComponent", function() { return CapacityReportComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _Services_capacity_service_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../Services/capacity-service.service */ "./src/app/Modules/FetaureModules/capacity-insights/Services/capacity-service.service.ts");
/* harmony import */ var src_app_Models_FilterModel__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/Models/FilterModel */ "./src/app/Models/FilterModel.ts");
/* harmony import */ var src_app_Services_httprequest_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/app/Services/httprequest.service */ "./src/app/Services/httprequest.service.ts");
/* harmony import */ var primeng_components_common_messageservice__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! primeng/components/common/messageservice */ "./node_modules/primeng/components/common/messageservice.js");
/* harmony import */ var primeng_components_common_messageservice__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(primeng_components_common_messageservice__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var src_app_Services_sharedservice_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! src/app/Services/sharedservice.service */ "./src/app/Services/sharedservice.service.ts");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var src_app_Models_PreferenceModel__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! src/app/Models/PreferenceModel */ "./src/app/Models/PreferenceModel.ts");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var primeng_components_table_table__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! primeng/components/table/table */ "./node_modules/primeng/components/table/table.js");
/* harmony import */ var primeng_components_table_table__WEBPACK_IMPORTED_MODULE_9___default = /*#__PURE__*/__webpack_require__.n(primeng_components_table_table__WEBPACK_IMPORTED_MODULE_9__);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};











var CapacityReportComponent = /** @class */ (function () {
    // tslint:disable-next-line:max-line-length
    function CapacityReportComponent(formBuilder, data, service, sharedData, route, messageService) {
        var _this = this;
        this.formBuilder = formBuilder;
        this.data = data;
        this.service = service;
        this.sharedData = sharedData;
        this.route = route;
        this.messageService = messageService;
        this.SelectedColumns = new _angular_forms__WEBPACK_IMPORTED_MODULE_6__["FormControl"]();
        this.msgsuccess = [];
        this.show = false;
        this.preferenceName = ' ';
        this.displaydialog = false;
        this.display = false;
        this.displayDP = false;
        this.displayG = false;
        this.displayOnOff = false;
        this.displayBilable = false;
        this.displaySR = false;
        this.displayPC = false;
        this.displayL = false;
        this.displayAN = false;
        this.displayDM = false;
        this.displayC = false;
        this.displayET = false;
        this.displayPN = false;
        this.displayP = false;
        this.overlaybasicDeliveryP = false;
        this.showAllBillable = false;
        this.showAllpractice = false;
        this.showAllAccountName = false;
        this.showAllProjectName = false;
        this.showAllLocation = false;
        this.showAllSRStatus = false;
        this.showAllProfitCenter = false;
        this.showAllDeliveryManager = false;
        this.showAllEngagementType = false;
        this.messagePreference = '';
        this.submitted = false;
        this.isPrefSet = false;
        this.msgdialog = false;
        this.checked = false;
        this.SearchResult = false;
        // Declaration of filter arrays
        // tslint:disable-next-line:max-line-length
        this.DeliveryPartner = [];
        this.Billable = [];
        this.ProfitCenterName = [];
        this.Location = [];
        this.AccountName = [];
        this.DeliveryManager = [];
        this.Competency = [];
        this.ProjectName = [];
        this.Practice = [];
        this.DefaultColumns = [];
        this.DeliveryPartnerArray = [];
        this.BillableArray = [];
        this.ProfitCenterNameArray = [];
        this.LocationArray = [];
        this.AccountNameArray = [];
        this.DeliveryManagerArray = [];
        this.CompetencyArray = [];
        this.ProjectNameArray = [];
        this.PracticeArray = [];
        // Declaration of filter arrays for selected option storing
        // tslint:disable-next-line:max-line-length
        this.DeliveryPartnerS = [];
        this.DeliveryManagerS = [];
        this.AccountNameS = [];
        this.ProfitCenterNameS = [];
        this.ProjectNameS = [];
        this.PracticeS = [];
        this.BillableS = [];
        this.LocationS = [];
        this.SRStatusS = [];
        this.EngagementTypeS = [];
        this.selectedCompetencies = [];
        this.selectedGender = [];
        this.selectedOnOffshore = [];
        this.selectedSRStatus = [];
        this.selectedEngagementType = [];
        this.genderArray = [];
        this.onOffshoreArray = [];
        this.srStatusArray = [];
        this.engagementTypeArray = [];
        this.ColumnSelections = [];
        this.testbool = false;
        this.step = 0;
        this.filterLoading = true;
        this.isDefaultPref = false;
        this.selectedColumns = [];
        this.pageLoading = false;
        this.haveRows = true;
        this.isPrefName = true;
        this.isEmpty = false;
        this.defaultvalue = ' ';
        this.isSavePrefOn = true;
        this.isFilter = true;
        this.loadProfitCenterCard = false;
        this.loadDeliveryManagerCard = false;
        this.loadDeliveryPartnerCard = false;
        this.loadAccountNamesCard = false;
        this.loadProjectsCard = false;
        this.loadPracticesCard = false;
        this.loadBillableStatusCard = false;
        this.loadSRStatusCard = false;
        this.loadEngagementTypeCard = false;
        this.loadLocationCard = false;
        this.MaleFilterButtonClick = false;
        this.FemaleFilterButtonClick = false;
        this.OnShoreFilterButtonClick = false;
        this.OffShoreFilterButtonClick = false;
        this.prefFlag = false;
        debugger;
        this.pageLoading = true;
        this.DefaultColumns = [
            { field: 'employeeID', header: 'MID' },
            { field: 'name', header: 'Employee Name' },
            { field: 'projectName', header: 'Project' },
            { field: 'givenRole', header: 'TM Role' },
            { field: 'location', header: 'Location' },
            { field: 'billableStatus',
                header: 'BillableStatus' },
            { field: 'vertical', header: 'Vertical' },
            { field: 'practice', header: 'Practice' }
        ];
        this.columns = this.DefaultColumns;
        this.SelectedColumns.setValue(this.DefaultColumns);
        // the Column selections array is feed with values to enable dynamic selection of columns
        this.ColumnSelections = [
            {
                label: 'MID',
                value: { field: 'employeeID', header: 'MID' }
            },
            {
                label: 'Employee Name',
                value: { field: 'name', header: 'Employee Name' }
            },
            {
                label: 'Project',
                value: { field: 'projectName', header: 'Project' }
            },
            {
                label: 'TM Role',
                value: { field: 'givenRole', header: 'TM Role' }
            },
            {
                label: 'Location',
                value: { field: 'location', header: 'Location' }
            },
            {
                label: 'Billability Status',
                value: {
                    field: 'billableStatus',
                    header: 'BillableStatus'
                }
            },
            {
                label: 'Vertical',
                value: { field: 'vertical', header: 'Vertical' }
            },
            {
                label: 'Practice',
                value: { field: 'practice', header: 'Practice' }
            },
            {
                label: 'Competency',
                value: { field: 'competency', header: 'Competency' }
            },
            {
                label: 'OnOffShore',
                value: { field: 'onOffShore', header: 'OnOffshore' }
            },
            {
                label: 'Gender',
                value: { field: 'gender', header: 'Gender' }
            },
            {
                label: 'Sub Practice',
                value: { field: 'subPractice', header: 'Sub Practice' }
            },
            {
                label: 'Profit Centre',
                value: { field: 'profitCenter', header: 'ProfitCenter' }
            }
        ];
        this.service.FiltersApplied.subscribe(function (respdata) {
            _this.filterapplied = respdata;
            if (respdata.AccountGroup === false) {
                _this.AccountNameArray = [];
            }
            if (respdata.BillableStatus === false) {
                _this.BillableArray = [];
            }
            if (respdata.DeliveryManager === false) {
                _this.DeliveryManagerArray = [];
            }
            if (respdata.DeliveryPlatform === false) {
                _this.DeliveryPartnerArray = [];
            }
            if (respdata.Location === false) {
                _this.Location = [];
            }
            if (respdata.Practice === false) {
                _this.Practice = [];
            }
            if (respdata.ProjectName === false) {
                _this.ProjectNameArray = [];
            }
            if (respdata.SRStatus === false) {
                _this.srStatusArray = [];
            }
            if (respdata.EngagementType === false) {
                _this.engagementTypeArray = [];
            }
            if (respdata.ProfitCenter === false) {
                _this.ProfitCenterNameArray = [];
            }
        });
        this.data
            .getCapacityData('/api/capacity/GetCapacity?MID=' + this.sharedData.midOfLoggedInUser)
            .subscribe(function (data) {
            _this.allCapacityData = data;
            _this.GetPrefdata();
        }, function (error) {
            if (error.status === 0) {
                _this.route.navigate(['/core/ServerDown']);
            }
            else if (error.status === 401) {
                _this.route.navigate(['/core/AccessDenied']);
            }
            else {
                _this.route.navigate(['/core/InternalServerError']);
            }
        });
        // tslint:disable-next-line:max-line-length
        this.mid = this.sharedData.midOfLoggedInUser;
        this.confirmationForm = this.formBuilder.group({
            prefName: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_6__["Validators"].required],
            def: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_6__["Validators"].required]
        });
        debugger;
        this.service.SelectedPrefrence.subscribe(function (Response) {
            debugger;
            if (Response != null) {
                _this.isDefaultPref = true;
                _this.isPrefSet = true;
                _this.selPref = Response.filterName;
                _this.selectedFilter = Response.preferences;
                _this.SearchKey = '';
                if (typeof _this.tableComponent !== 'undefined') {
                    _this.tableComponent.reset();
                    _this.reset();
                }
                _this.loading = true;
                if (Response.filterName != null) {
                    _this.isDefaultPref = true;
                    _this.DeliveryPartnerArray = _this.selectedFilter.deliveryPartner;
                    _this.BillableArray = _this.selectedFilter.billableStatus;
                    _this.ProfitCenterNameArray = _this.selectedFilter.profitCenter;
                    _this.LocationArray = _this.selectedFilter.location;
                    _this.AccountNameArray = _this.selectedFilter.accountGroup;
                    _this.DeliveryManagerArray = _this.selectedFilter.deliveryManager;
                    _this.selectedEngagementType = _this.selectedFilter.engagementCode;
                    _this.selectedCompetencies = _this.selectedFilter.competencyCode;
                    _this.selectedGender = _this.selectedFilter.gender;
                    _this.selectedOnOffshore = _this.selectedFilter.onOffshore;
                    _this.ProjectNameArray = _this.selectedFilter.projectName;
                    _this.PracticeArray = _this.selectedFilter.practice;
                    _this.selectedSRStatus = _this.selectedFilter.srStatus;
                    _this.prefFlag = true;
                    _this.UpdateFilterOptions();
                    _this.FetchFilteredCapacityData();
                }
                else {
                    _this.FetchFilteredCapacityData();
                }
            }
            else {
            }
        });
    }
    CapacityReportComponent.prototype.ViewOptions = function () {
        this.mySelect.open();
    };
    // this method is used to reset data table
    CapacityReportComponent.prototype.reset = function () {
        this.selectedCompetencies = [];
        this.columns = this.DefaultColumns;
        this.ResetFilterselections();
        this.SearchKey = ' ';
        this.FetchFilteredCapacityData();
    };
    // on click of enter in glbal filter this method will fetch results from db
    CapacityReportComponent.prototype.FetchResultFromDB = function (datatable) {
        var _this = this;
        this.loading = true;
        // Start of Reset all the table settings
        // End of Reset all the table settings
        datatable.reset();
        this.tableObject = datatable;
        datatable.value = null;
        var token = this.sharedData.tokenIDOfLoggedUser;
        // tslint:disable-next-line:max-line-length
        var header = new _angular_common_http__WEBPACK_IMPORTED_MODULE_8__["HttpHeaders"]()
            .set('Content-Type', 'application/json')
            .set('Access-Control-Allow-Origin', '*')
            .set('Authorization', 'Bearer ' + token);
        this.data
            .get('/api/capacity/GetGlobalSearchResult?key=' + this.SearchKey, header)
            .subscribe(function (SearchResultdata) {
            _this.capacityData = SearchResultdata;
            _this.totalRecords = SearchResultdata.length;
            _this.isDefaultPref = false;
            _this.loading = false;
            if (SearchResultdata.length === 0) {
                _this.SearchResult = true;
            }
            else {
                _this.SearchResult = false;
            }
            _this.ResetFilterselections();
            _this.getFilterOptions(SearchResultdata);
        }, function (error) {
            if (error.status === 0) {
                _this.route.navigate(['/core/ServerDown']);
            }
            else if (error.status === 401) {
                _this.route.navigate(['/core/AccessDenied']);
            }
            else {
                _this.route.navigate(['/core/InternalServerError']);
            }
        });
    };
    // this is used to reset all filter array's
    CapacityReportComponent.prototype.ResetFilterselections = function () {
        // tslint:disable-next-line:max-line-length
        this.DeliveryPartnerArray = [];
        this.genderArray = [];
        this.BillableArray = [];
        this.srStatusArray = [];
        this.ProfitCenterNameArray = [];
        this.LocationArray = [];
        this.AccountNameArray = [];
        this.DeliveryManagerArray = [];
        this.CompetencyArray = [];
        this.ProjectNameArray = [];
        this.PracticeArray = [];
        this.selectedCompetencies = [];
        this.selectedEngagementType = [];
        this.selectedGender = [];
        this.selectedSRStatus = [];
        this.selectedOnOffshore = [];
        this.isSavePrefOn = true;
        this.haveRows = true;
    };
    // this is after removing default preference applied
    CapacityReportComponent.prototype.resetDefaultPref = function () {
        this.isDefaultPref = false;
        this.loading = true;
        this.ResetFilterselections();
        this.FetchFilteredCapacityData();
    };
    CapacityReportComponent.prototype.GetPrefdata = function () {
        var _this = this;
        this.data.getAPI('/api/preferences/getDefaultPreference?MID=' + this.sharedData.midOfLoggedInUser)
            .subscribe(function (data) {
            if (data == null) {
                _this.isDefaultPref = false;
                _this.defaultPref = null;
                _this.FetchFilteredCapacityData();
            }
            else {
                _this.isDefaultPref = true;
                if (!_this.isPrefSet) {
                    _this.dataPreferences = data.preferences;
                    _this.dataPreferences.sRStatus = data.preferences.srStatus;
                    _this.defaultPref = data;
                    _this.selPref = data.filterName;
                    _this.DeliveryPartnerArray =
                        data.preferences.deliveryPartner;
                    _this.BillableArray = _this.dataPreferences.billableStatus;
                    _this.ProfitCenterNameArray =
                        _this.dataPreferences.profitCenter;
                    _this.LocationArray = _this.dataPreferences.location;
                    _this.AccountNameArray = _this.dataPreferences.accountGroup;
                    _this.DeliveryManagerArray =
                        _this.dataPreferences.deliveryManager;
                    _this.engagementTypeArray =
                        _this.dataPreferences.engagementCode;
                    _this.ProjectNameArray = _this.dataPreferences.projectName;
                    _this.PracticeArray = _this.dataPreferences.practice;
                    _this.selectedEngagementType = _this.dataPreferences.engagementCode;
                    _this.selectedCompetencies = _this.dataPreferences.competencyCode;
                    _this.selectedGender = _this.dataPreferences.gender;
                    _this.selectedOnOffshore = _this.dataPreferences.onOffshore;
                    _this.selectedSRStatus = _this.dataPreferences.sRStatus;
                    _this.UpdateFilterOptions();
                    _this.prefFlag = true;
                    _this.FetchFilteredCapacityData();
                }
            }
        });
    };
    CapacityReportComponent.prototype.UpdateFilterOptions = function () {
        var _this = this;
        this.DeliveryManagerS = [];
        this.DeliveryManagerArray.forEach(function (item, index) {
            _this.DeliveryManagerS.push({
                label: item,
                value: item
            });
        });
        this.BillableS = [];
        this.BillableArray.forEach(function (item, index) {
            _this.BillableS.push({
                label: item,
                value: item
            });
        });
        // this.SRStatusS = [ ];
        // this.srStatusArray .forEach((item, index) => {
        //             this.SRStatusS.push({
        //                 label: item,
        //                 value: item
        //             });
        //         });
        this.ProfitCenterNameS = [];
        this.ProfitCenterNameArray.forEach(function (item, index) {
            _this.ProfitCenterNameS.push({
                label: item,
                value: item
            });
        });
        this.LocationS = [];
        this.LocationArray.forEach(function (item, index) {
            _this.LocationS.push({
                label: item,
                value: item
            });
        });
        this.ProjectNameS = [];
        this.ProjectNameArray.forEach(function (item, index) {
            _this.ProjectNameS.push({
                label: item,
                value: item
            });
        });
        this.PracticeS = [];
        this.PracticeArray.forEach(function (item, index) {
            _this.PracticeS.push({
                label: item,
                value: item
            });
        });
        this.DeliveryPartnerS = [];
        this.DeliveryPartnerArray.forEach(function (item, index) {
            _this.DeliveryPartnerS.push({
                label: item,
                value: item
            });
        });
        this.AccountNameS = [];
        this.AccountNameArray.forEach(function (item, index) {
            _this.AccountNameS.push({
                label: item,
                value: item
            });
        });
        this.CompetencyArray = [];
        this.selectedCompetencies.forEach(function (item, index) {
            _this.CompetencyArray.push({
                label: item,
                value: item
            });
        });
        this.onOffshoreArray = [];
        this.selectedOnOffshore.forEach(function (item, index) {
            _this.onOffshoreArray.push({
                label: item,
                value: item
            });
        });
        this.engagementTypeArray = [];
        this.selectedEngagementType.forEach(function (item, index) {
            _this.engagementTypeArray.push({
                label: item,
                value: item
            });
        });
        this.srStatusArray = [];
        this.selectedSRStatus.forEach(function (item, index) {
            _this.srStatusArray.push({
                label: item,
                value: item
            });
        });
        this.genderArray = [];
        this.selectedGender.forEach(function (item, index) {
            _this.genderArray.push({
                label: item,
                value: item
            });
        });
    };
    CapacityReportComponent.prototype.UncheckDP = function (event) {
        var idb = event.currentTarget.parentElement.attributes.id.nodeValue;
        for (var _i = 0, _a = this.DeliveryPartnerArray; _i < _a.length; _i++) {
            var entry = _a[_i];
            if (entry === idb) {
                this.DeliveryPartnerArray.splice(this.DeliveryPartnerArray.indexOf(entry), 1);
                this.FetchFilteredCapacityData();
            }
        }
    };
    /// on uncheck delete from the array 2-DM
    CapacityReportComponent.prototype.UncheckDM = function (event) {
        var idb = event.currentTarget.parentElement.attributes.id.nodeValue;
        for (var _i = 0, _a = this.DeliveryManagerArray; _i < _a.length; _i++) {
            var entry = _a[_i];
            if (entry === idb) {
                this.DeliveryManagerArray.splice(this.DeliveryManagerArray.indexOf(entry), 1);
                this.FetchFilteredCapacityData();
            }
        }
    };
    /// on uncheck delete from the array 3-Pc
    CapacityReportComponent.prototype.UncheckPC = function (event) {
        var idb = event.currentTarget.parentElement.attributes.id.nodeValue;
        for (var _i = 0, _a = this.ProfitCenterNameArray; _i < _a.length; _i++) {
            var entry = _a[_i];
            if (entry === idb) {
                this.ProfitCenterNameArray.splice(this.ProfitCenterNameArray.indexOf(entry), 1);
                this.FetchFilteredCapacityData();
            }
        }
    };
    /// on uncheck delete from the array 4-PN
    CapacityReportComponent.prototype.UncheckPN = function (event) {
        var idb = event.currentTarget.parentElement.attributes.id.nodeValue;
        for (var _i = 0, _a = this.ProjectNameArray; _i < _a.length; _i++) {
            var entry = _a[_i];
            if (entry === idb) {
                this.ProjectNameArray.splice(this.ProjectNameArray.indexOf(entry), 1);
                this.FetchFilteredCapacityData();
            }
        }
    };
    /// on uncheck delete from the array 5-AN
    CapacityReportComponent.prototype.UncheckAN = function (event) {
        var idb = event.currentTarget.parentElement.attributes.id.nodeValue;
        for (var _i = 0, _a = this.AccountNameArray; _i < _a.length; _i++) {
            var entry = _a[_i];
            if (entry === idb) {
                this.AccountNameArray.splice(this.AccountNameArray.indexOf(entry), 1);
                this.FetchFilteredCapacityData();
            }
        }
    };
    /// on uncheck delete from the array 6-P
    CapacityReportComponent.prototype.UncheckP = function (event) {
        var idb = event.currentTarget.parentElement.attributes.id.nodeValue;
        for (var _i = 0, _a = this.PracticeArray; _i < _a.length; _i++) {
            var entry = _a[_i];
            if (entry === idb) {
                this.PracticeArray.splice(this.PracticeArray.indexOf(entry), 1);
                this.FetchFilteredCapacityData();
            }
        }
    };
    /// on uncheck delete from the array 7-B
    CapacityReportComponent.prototype.UncheckB = function (event) {
        var idb = event.currentTarget.parentElement.attributes.id.nodeValue;
        for (var _i = 0, _a = this.BillableArray; _i < _a.length; _i++) {
            var entry = _a[_i];
            if (entry === idb) {
                this.BillableArray.splice(this.BillableArray.indexOf(entry), 1);
                this.FetchFilteredCapacityData();
            }
        }
    };
    /// on uncheck delete from the array 8-L
    CapacityReportComponent.prototype.UncheckL = function (event) {
        var idb = event.currentTarget.parentElement.attributes.id.nodeValue;
        for (var _i = 0, _a = this.LocationArray; _i < _a.length; _i++) {
            var entry = _a[_i];
            if (entry === idb) {
                this.LocationArray.splice(this.LocationArray.indexOf(entry), 1);
                this.FetchFilteredCapacityData();
            }
        }
    };
    ///  Show selected filters for Delivery partner
    CapacityReportComponent.prototype.showDeliveryP = function () {
        this.displayDP = true;
    };
    ///  Show selected filters for Billable status
    CapacityReportComponent.prototype.showBillableS = function () {
        this.displayBilable = true;
    };
    ///  Show selected filters for practices
    CapacityReportComponent.prototype.showPractise = function () {
        this.displayP = true;
    };
    ///  Show selected filters for Account name
    CapacityReportComponent.prototype.showAccountN = function () {
        this.displayAN = true;
    };
    ///  Show selected filters for Project Name
    CapacityReportComponent.prototype.showProjectN = function () {
        this.displayPN = true;
    };
    ///  Show selected filters for Location
    CapacityReportComponent.prototype.showLocation = function () {
        this.displayL = true;
    };
    ///  Show selected filters for SR status
    CapacityReportComponent.prototype.showSRS = function () {
        this.displaySR = true;
    };
    ///  Show selected filters for Profit center
    CapacityReportComponent.prototype.showPC = function () {
        this.displayPC = true;
    };
    ///  Show selected filters for Delivery manager
    CapacityReportComponent.prototype.showDM = function () {
        this.displayDM = true;
    };
    ///  Show selected filters for Engagement type
    CapacityReportComponent.prototype.showET = function () {
        this.displayET = true;
    };
    CapacityReportComponent.prototype.showAllDP = function () {
        this.overlaybasicDeliveryP = true;
    };
    /// show All BS
    CapacityReportComponent.prototype.showAllBS = function () {
        this.showAllBillable = true;
    };
    /// show All P
    CapacityReportComponent.prototype.showAllP = function () {
        this.showAllpractice = true;
    };
    /// show All AN
    CapacityReportComponent.prototype.showAllAN = function () {
        this.showAllAccountName = true;
    };
    /// show All PN
    CapacityReportComponent.prototype.showAllPN = function () {
        this.showAllProjectName = true;
    };
    /// show All L
    CapacityReportComponent.prototype.showAllL = function () {
        this.showAllLocation = true;
    };
    /// show All SR
    CapacityReportComponent.prototype.showAllSR = function () {
        this.showAllSRStatus = true;
    };
    /// show All PC
    CapacityReportComponent.prototype.showAllPC = function () {
        this.showAllProfitCenter = true;
    };
    /// show All DM
    CapacityReportComponent.prototype.showAllDM = function () {
        this.showAllDeliveryManager = true;
    };
    /// show All ET
    CapacityReportComponent.prototype.showAllET = function () {
        this.showAllEngagementType = true;
    };
    // Page load function
    CapacityReportComponent.prototype.ngOnInit = function () {
    };
    // this method update columns display based on columns selected by user to display
    CapacityReportComponent.prototype.UpdateColumns = function () {
        var _this = this;
        this.columns = [];
        if (this.SelectedColumns.value.length === 0) {
            this.columns = this.DefaultColumns;
        }
        else {
            // tslint:disable-next-line:no-shadowed-variable
            this.SelectedColumns.value.forEach(function (element) {
                _this.columns.push(element);
            });
        }
    };
    CapacityReportComponent.prototype.compareFn = function (v1, v2) {
        return compareFn(v1, v2);
    };
    // to start loading icon on all filters
    CapacityReportComponent.prototype.StartFilterLoading = function () {
        this.loadProfitCenterCard = true;
        this.loadDeliveryPartnerCard = true;
        this.loadDeliveryManagerCard = true;
        this.loadAccountNamesCard = true;
        this.loadProjectsCard = true;
        this.loadPracticesCard = true;
        this.loadBillableStatusCard = true;
        this.loadLocationCard = true;
        this.loadSRStatusCard = true;
        this.loadEngagementTypeCard = true;
    };
    // to stop loading icon on all filters
    CapacityReportComponent.prototype.StopFilterLoading = function () {
        this.loadProfitCenterCard = false;
        this.loadDeliveryPartnerCard = false;
        this.loadDeliveryManagerCard = false;
        this.loadAccountNamesCard = false;
        this.loadProjectsCard = false;
        this.loadPracticesCard = false;
        this.loadBillableStatusCard = false;
        this.loadLocationCard = false;
        this.loadSRStatusCard = false;
        this.loadEngagementTypeCard = false;
    };
    // to apply filters to capacity data
    CapacityReportComponent.prototype.FetchFilteredCapacityData = function () {
        var _this = this;
        var data = this.allCapacityData.filter(function (x) {
            if ((_this.ProfitCenterNameArray.length === 0 ||
                (x.profitCenter != null && _this.ProfitCenterNameArray.includes(x.profitCenter))) &&
                (_this.PracticeArray.length === 0 ||
                    (x.practice != null && _this.PracticeArray.includes(x.practice))) &&
                (_this.BillableArray.length === 0 ||
                    (x.billableStatus != null && _this.BillableArray.includes(x.billableStatus))) &&
                (_this.LocationArray.length === 0 ||
                    (x.location != null && _this.LocationArray.includes(x.location))) &&
                (_this.AccountNameArray.length === 0 ||
                    (x.accountName != null && _this.AccountNameArray.includes(x.accountName))) &&
                (_this.ProjectNameArray.length === 0 ||
                    (x.projectName != null && _this.ProjectNameArray.includes(x.projectName))) &&
                (_this.selectedEngagementType.length === 0 ||
                    (x.engagementType != null && _this.selectedEngagementType.includes(x.engagementType))) &&
                (_this.selectedGender.length === 0 ||
                    (x.gender != null && _this.selectedGender.includes(x.gender))) &&
                (_this.selectedCompetencies.length === 0 ||
                    (x.competency != null && _this.selectedCompetencies.includes(x.competency))) &&
                (_this.selectedSRStatus.length === 0 ||
                    (x.srStatus != null && _this.selectedSRStatus.includes(x.srStatus))) &&
                (_this.selectedOnOffshore.length === 0 ||
                    (x.onOffShore != null && _this.selectedOnOffshore.includes(x.onOffShore))) &&
                (_this.DeliveryManagerArray.length === 0 ||
                    (x.deliveryManager != null && _this.DeliveryManagerArray.includes(x.deliveryManager))) &&
                (_this.DeliveryPartnerArray.length === 0 ||
                    (x.deliveryPartner != null && _this.DeliveryPartnerArray.includes(x.deliveryPartner)))) {
                return true;
            }
        });
        this.totalRecords = data.length;
        this.capacityData = data;
        this.isFetch = false;
        if (data.length === 0) {
            this.haveRows = false;
        }
        else {
            this.haveRows = true;
        }
        this.SearchKey = ' ';
        this.getFilterOptions(data);
    };
    // to update filter options
    CapacityReportComponent.prototype.getFilterOptions = function (data) {
        var _this = this;
        this.isSavePrefOn = false;
        this.StartFilterLoading();
        if (this.DeliveryManagerArray.length === 0) {
            var deliveryManagerOptions = data.map(function (x) {
                return x.deliveryManager;
            });
            this.DeliveryManagerS = [];
            Array.from(new Set(deliveryManagerOptions)).forEach(function (item, index) {
                if (item != null) {
                    _this.DeliveryManagerS.push({
                        label: item,
                        value: item
                    });
                }
            });
        }
        if (this.BillableArray.length === 0) {
            var billablestatusOptions = data.map(function (x) {
                return x.billableStatus;
            });
            this.BillableS = [];
            Array.from(new Set(billablestatusOptions)).forEach(function (item, index) {
                if (item != null) {
                    _this.BillableS.push({
                        label: item,
                        value: item
                    });
                }
            });
        }
        if (this.selectedSRStatus.length === 0) {
            var srStatusOptions = data.map(function (x) {
                return x.srStatus;
            });
            this.srStatusArray = [];
            Array.from(new Set(srStatusOptions)).forEach(function (item, index) {
                if (item != null) {
                    _this.srStatusArray.push({
                        label: item,
                        value: item
                    });
                }
            });
        }
        if (this.ProfitCenterNameArray.length === 0) {
            var profitCentreOptions = data.map(function (x) {
                return x.profitCenter;
            });
            this.ProfitCenterNameS = [];
            Array.from(new Set(profitCentreOptions)).forEach(function (item, index) {
                if (item != null) {
                    _this.ProfitCenterNameS.push({
                        label: item,
                        value: item
                    });
                }
            });
        }
        if (this.LocationArray.length === 0) {
            var locationOptions = data.map(function (x) {
                return x.location;
            });
            this.LocationS = [];
            Array.from(new Set(locationOptions)).forEach(function (item, index) {
                if (item != null) {
                    _this.LocationS.push({
                        label: item,
                        value: item
                    });
                }
            });
        }
        if (this.AccountNameArray.length === 0) {
            var accountNameOptions = data.map(function (x) {
                return x.accountName;
            });
            this.AccountNameS = [];
            Array.from(new Set(accountNameOptions)).forEach(function (item, index) {
                if (item != null) {
                    _this.AccountNameS.push({
                        label: item,
                        value: item
                    });
                }
            });
        }
        if (this.selectedEngagementType.length === 0) {
            var engagementTypeOptions = data.map(function (x) {
                return x.engagementType;
            });
            this.engagementTypeArray = [];
            Array.from(new Set(engagementTypeOptions)).forEach(function (item, index) {
                if (item != null) {
                    _this.engagementTypeArray.push({
                        label: item,
                        value: item
                    });
                }
            });
        }
        if (this.ProjectNameArray.length === 0) {
            var projectNameOptions = data.map(function (x) {
                return x.projectName;
            });
            this.ProjectNameS = [];
            Array.from(new Set(projectNameOptions)).forEach(function (item, index) {
                if (item != null) {
                    _this.ProjectNameS.push({
                        label: item,
                        value: item
                    });
                }
            });
        }
        if (this.PracticeArray.length === 0) {
            var practiceOptions = data.map(function (x) {
                return x.practice;
            });
            this.PracticeS = [];
            Array.from(new Set(practiceOptions)).forEach(function (item, index) {
                if (item != null) {
                    _this.PracticeS.push({
                        label: item,
                        value: item
                    });
                }
            });
        }
        if (this.DeliveryPartnerArray.length === 0) {
            var deliveryPartnerOptions = data.map(function (x) {
                return x.deliveryPartner;
            });
            this.DeliveryPartnerS = [];
            Array.from(new Set(deliveryPartnerOptions)).forEach(function (item, index) {
                if (item != null) {
                    _this.DeliveryPartnerS.push({
                        label: item,
                        value: item
                    });
                }
            });
        }
        if (this.AccountNameArray.length === 0) {
            var accountNameArrayOptions = data.map(function (x) {
                return x.accountName;
            });
            this.AccountNameS = [];
            Array.from(new Set(accountNameArrayOptions)).forEach(function (item, index) {
                if (item != null) {
                    _this.AccountNameS.push({
                        label: item,
                        value: item
                    });
                }
            });
        }
        if (this.selectedCompetencies.length === 0) {
            var competencyArrayOptions = data.map(function (x) {
                return x.competency;
            });
            this.CompetencyArray = [];
            Array.from(new Set(competencyArrayOptions)).forEach(function (item, index) {
                if (item != null) {
                    _this.CompetencyArray.push({
                        label: item,
                        value: item
                    });
                }
            });
        }
        if (this.selectedOnOffshore.length === 0) {
            var onOffshoreArrayOptions = data.map(function (x) {
                return x.onOffShore;
            });
            this.onOffshoreArray = [];
            Array.from(new Set(onOffshoreArrayOptions)).forEach(function (item, index) {
                if (item != null) {
                    _this.onOffshoreArray.push({
                        label: item,
                        value: item
                    });
                }
            });
        }
        if (this.selectedGender.length === 0) {
            var genderArrayOptions = data.map(function (x) {
                return x.gender;
            });
            this.genderArray = [];
            Array.from(new Set(genderArrayOptions)).forEach(function (item, index) {
                if (item != null) {
                    _this.genderArray.push({
                        label: item,
                        value: item
                    });
                }
            });
        }
        this.StopFilterLoading();
        this.pageLoading = false;
        if (this.prefFlag !== true) {
            this.isDefaultPref = false;
        }
        this.prefFlag = false;
        this.loading = false;
    };
    /// Show dialog box
    CapacityReportComponent.prototype.showDialog = function () {
        this.displaydialog = true;
    };
    /// close dialog box
    CapacityReportComponent.prototype.closeDialog = function () {
        this.displaydialog = false;
    };
    /// on search change function
    CapacityReportComponent.prototype.onSearchChange = function (searchValue) {
        if (searchValue.length === 0) {
            this.isPrefName = true;
            this.isEmpty = true;
        }
        else {
            this.isPrefName = false;
        }
    };
    // Function to save preference information
    CapacityReportComponent.prototype.savePref = function () {
        var _this = this;
        this.preferenceName = this.confirmationForm.controls.prefName.value;
        this.defaultvalue = this.confirmationForm.controls.def.value;
        var filterModelObj = new src_app_Models_FilterModel__WEBPACK_IMPORTED_MODULE_2__["Filter"]({
            deliveryPartner: this.DeliveryPartnerArray,
            billableStatus: this.BillableArray,
            onOffshore: this.selectedOnOffshore,
            gender: this.selectedGender,
            sRStatus: this.selectedSRStatus,
            profitCenter: this.ProfitCenterNameArray,
            location: this.LocationArray,
            accountGroup: this.AccountNameArray,
            deliveryManager: this.DeliveryManagerArray,
            competencyCode: this.selectedCompetencies,
            engagementCode: this.selectedEngagementType,
            projectName: this.ProjectNameArray,
            practice: this.PracticeArray,
        });
        var date = new Date();
        var defa;
        if (this.defaultvalue[0] === 'Yes') {
            defa = true;
        }
        else {
            defa = false;
        }
        var savePref = new src_app_Models_PreferenceModel__WEBPACK_IMPORTED_MODULE_7__["PreferenceData"]({
            mid: this.mid,
            // tslint:disable-next-line:max-line-length
            filterName: this.preferenceName,
            preferences: filterModelObj,
            uploadedDate: date,
            uploadedby: this.mid,
            DefaultPreference: defa
        });
        this.show = false;
        // API call to save preference data
        this.data
            .savePreference('/api/Preferences/SavePreference', savePref)
            .subscribe(function (data) {
            // this.capacityData = data;
            // this.totalRecords = data.length;
            _this.isDefaultPref = true;
            _this.selPref = _this.preferenceName;
            // Hide the dialog and display success message
            _this.displaydialog = false;
            _this.msgdialog = true;
            _this.show = true;
            _this.msgsuccess = [];
            _this.submitted = false;
            _this.msgsuccess.push({
                severity: 'success',
                summary: 'You have successfully saved your preference!!'
            });
            _this.confirmationForm.reset();
        }, function (error) {
            _this.confirmationForm.reset();
            _this.displaydialog = false;
            _this.msgdialog = true;
            _this.show = true;
            _this.msgsuccess = [];
            _this.submitted = false;
            // tslint:disable-next-line:max-line-length
            _this.msgsuccess.push({
                severity: 'error',
                summary: _this.preferenceName +
                    ' is already used, give a different name to your preference!!'
            });
        });
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])('mySelect'),
        __metadata("design:type", Object)
    ], CapacityReportComponent.prototype, "mySelect", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])('msg'),
        __metadata("design:type", Object)
    ], CapacityReportComponent.prototype, "messageBox", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])('dt'),
        __metadata("design:type", primeng_components_table_table__WEBPACK_IMPORTED_MODULE_9__["Table"])
    ], CapacityReportComponent.prototype, "tableComponent", void 0);
    CapacityReportComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-capacity-report',
            template: __webpack_require__(/*! ./capacity-report.component.html */ "./src/app/Modules/FetaureModules/capacity-insights/capacity-report/capacity-report.component.html"),
            styles: [__webpack_require__(/*! ./capacity-report.component.css */ "./src/app/Modules/FetaureModules/capacity-insights/capacity-report/capacity-report.component.css")],
            providers: [primeng_components_common_messageservice__WEBPACK_IMPORTED_MODULE_4__["MessageService"]]
        }),
        __metadata("design:paramtypes", [_angular_forms__WEBPACK_IMPORTED_MODULE_6__["FormBuilder"],
            src_app_Services_httprequest_service__WEBPACK_IMPORTED_MODULE_3__["HTTPRequestService"],
            _Services_capacity_service_service__WEBPACK_IMPORTED_MODULE_1__["CapacityServiceService"],
            src_app_Services_sharedservice_service__WEBPACK_IMPORTED_MODULE_5__["SharedserviceService"],
            _angular_router__WEBPACK_IMPORTED_MODULE_10__["Router"],
            primeng_components_common_messageservice__WEBPACK_IMPORTED_MODULE_4__["MessageService"]])
    ], CapacityReportComponent);
    return CapacityReportComponent;
}());

// this is used to update column selections
function compareFn(v1, v2) {
    return v1 && v2 ? v1.field === v2.field : v1 === v2;
}


/***/ }),

/***/ "./src/app/Modules/FetaureModules/capacity-insights/filter/filter.component.css":
/*!**************************************************************************************!*\
  !*** ./src/app/Modules/FetaureModules/capacity-insights/filter/filter.component.css ***!
  \**************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".Card1 {\r\n    border-left: 5px solid #d81b60;\r\n    border-radius: 2px;\r\n    margin-top: 15px;\r\n    height: 35px;\r\n    font-size: 14px;\r\n    padding-top: 7px;\r\n    color: #e65100;\r\n    background-color: #f5f5f5;\r\n}\r\n.button-save-pref {\r\n    height: 30px;\r\n    width: 140px;\r\n    padding-top: 4px;\r\n    font-size: 13px;\r\n    padding-left: 8px;\r\n    margin-left: 0px;\r\n    margin-right: 0px;\r\n}\r\n.Card2 {\r\n    border-top: 8px solid #4a148c;\r\n}\r\nmat-slide-toggle.toggle-filter {\r\n    display: block;\r\n    margin-bottom: 10px;\r\n    font-size: 13px;\r\n    line-height: 14px;\r\n}\r\nspan.mat-slide-toggle-content {\r\n    padding-left: 10px;\r\n}\r\n.CardHeader {\r\n    font-weight: bold;\r\n}\r\n.Card3 {\r\n    border-top: 8px solid #e65100;\r\n}\r\n.Card4 {\r\n    border-top: 8px solid #3e2723;\r\n}\r\nspan.pi-times {\r\n    width: 30px;\r\n    height: 30px;\r\n    padding-top: 4px;\r\n    display: inline-block;\r\n    /* position: absolute; */\r\n    top: 0px;\r\n    right: 0px;\r\n    transition: ease 0.25s all;\r\n    -webkit-transform: translate(50%, -50%);\r\n    transform: translate(50%, -50%);\r\n    border-radius: 1000px;\r\n    background: #e74c3c;\r\n    font-size: 20px;\r\n    text-align: center;\r\n    line-height: 100%;\r\n    color: white;\r\n    cursor: pointer;\r\n    font-weight: lighter;\r\n    margin-top: 17px;\r\n    margin-right: 14px;\r\n}\r\n.ui-sidebar-right.ui-sidebar-active.filter {\r\n    overflow: scroll;\r\n}\r\n/* width */\r\n::-webkit-scrollbar {\r\n    width: 10px;\r\n}\r\n/* Track */\r\n::-webkit-scrollbar-track {\r\n    border-radius: 10px;\r\n}\r\n/* Handle */\r\n::-webkit-scrollbar-thumb {\r\n    background: #1a237e;\r\n    border-radius: 10px;\r\n}\r\n/* Handle on hover */\r\n::-webkit-scrollbar-thumb:hover {\r\n    background: #1a237e;\r\n}\r\n.filterPannelHeader {\r\n    display: block;\r\n    padding: 10px;\r\n    color: #01579b;\r\n    background-color: #ffffff;\r\n    -webkit-text-decoration: solid;\r\n            text-decoration: solid;\r\n    margin-left: -10px;\r\n    margin-top: 10px;\r\n}\r\n.mat-slide-toggle-label-before .mat-slide-toggle-bar,\r\n[dir=\"rtl\"] .mat-slide-toggle-bar {\r\n    margin-left: 70px;\r\n    margin-right: 0;\r\n}\r\n.display-inline-block {\r\n    display: inline-block;\r\n}\r\n.filterBtn {\r\n    border-radius: 27px;\r\n    font-size: 13px;\r\n    background-color: #a8b3a8;\r\n    border-color: #a0bfa2;\r\n    margin-right: 2px;\r\n    display: inline;\r\n    float: right;\r\n}\r\n\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvTW9kdWxlcy9GZXRhdXJlTW9kdWxlcy9jYXBhY2l0eS1pbnNpZ2h0cy9maWx0ZXIvZmlsdGVyLmNvbXBvbmVudC5jc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7SUFDSSwrQkFBK0I7SUFDL0IsbUJBQW1CO0lBQ25CLGlCQUFpQjtJQUNqQixhQUFhO0lBQ2IsZ0JBQWdCO0lBQ2hCLGlCQUFpQjtJQUNqQixlQUFlO0lBQ2YsMEJBQTBCO0NBQzdCO0FBQ0Q7SUFDSSxhQUFhO0lBQ2IsYUFBYTtJQUNiLGlCQUFpQjtJQUNqQixnQkFBZ0I7SUFDaEIsa0JBQWtCO0lBQ2xCLGlCQUFpQjtJQUNqQixrQkFBa0I7Q0FDckI7QUFDRDtJQUNJLDhCQUE4QjtDQUNqQztBQUVEO0lBQ0ksZUFBZTtJQUNmLG9CQUFvQjtJQUNwQixnQkFBZ0I7SUFDaEIsa0JBQWtCO0NBQ3JCO0FBQ0Q7SUFDSSxtQkFBbUI7Q0FDdEI7QUFDRDtJQUNJLGtCQUFrQjtDQUNyQjtBQUNEO0lBQ0ksOEJBQThCO0NBQ2pDO0FBQ0Q7SUFDSSw4QkFBOEI7Q0FDakM7QUFDRDtJQUNJLFlBQVk7SUFDWixhQUFhO0lBQ2IsaUJBQWlCO0lBQ2pCLHNCQUFzQjtJQUN0Qix5QkFBeUI7SUFDekIsU0FBUztJQUNULFdBQVc7SUFDWCwyQkFBMkI7SUFDM0Isd0NBQXdDO0lBQ3hDLGdDQUFnQztJQUNoQyxzQkFBc0I7SUFDdEIsb0JBQW9CO0lBQ3BCLGdCQUFnQjtJQUNoQixtQkFBbUI7SUFDbkIsa0JBQWtCO0lBQ2xCLGFBQWE7SUFDYixnQkFBZ0I7SUFDaEIscUJBQXFCO0lBQ3JCLGlCQUFpQjtJQUNqQixtQkFBbUI7Q0FDdEI7QUFDRDtJQUNJLGlCQUFpQjtDQUNwQjtBQUNELFdBQVc7QUFDWDtJQUNJLFlBQVk7Q0FDZjtBQUVELFdBQVc7QUFDWDtJQUNJLG9CQUFvQjtDQUN2QjtBQUVELFlBQVk7QUFDWjtJQUNJLG9CQUFvQjtJQUNwQixvQkFBb0I7Q0FDdkI7QUFFRCxxQkFBcUI7QUFDckI7SUFDSSxvQkFBb0I7Q0FDdkI7QUFDRDtJQUNJLGVBQWU7SUFDZixjQUFjO0lBQ2QsZUFBZTtJQUNmLDBCQUEwQjtJQUMxQiwrQkFBdUI7WUFBdkIsdUJBQXVCO0lBQ3ZCLG1CQUFtQjtJQUNuQixpQkFBaUI7Q0FDcEI7QUFDRDs7SUFFSSxrQkFBa0I7SUFDbEIsZ0JBQWdCO0NBQ25CO0FBQ0Q7SUFDSSxzQkFBc0I7Q0FDekI7QUFFRDtJQUNJLG9CQUFvQjtJQUNwQixnQkFBZ0I7SUFDaEIsMEJBQTBCO0lBQzFCLHNCQUFzQjtJQUN0QixrQkFBa0I7SUFDbEIsZ0JBQWdCO0lBQ2hCLGFBQWE7Q0FDaEIiLCJmaWxlIjoic3JjL2FwcC9Nb2R1bGVzL0ZldGF1cmVNb2R1bGVzL2NhcGFjaXR5LWluc2lnaHRzL2ZpbHRlci9maWx0ZXIuY29tcG9uZW50LmNzcyIsInNvdXJjZXNDb250ZW50IjpbIi5DYXJkMSB7XHJcbiAgICBib3JkZXItbGVmdDogNXB4IHNvbGlkICNkODFiNjA7XHJcbiAgICBib3JkZXItcmFkaXVzOiAycHg7XHJcbiAgICBtYXJnaW4tdG9wOiAxNXB4O1xyXG4gICAgaGVpZ2h0OiAzNXB4O1xyXG4gICAgZm9udC1zaXplOiAxNHB4O1xyXG4gICAgcGFkZGluZy10b3A6IDdweDtcclxuICAgIGNvbG9yOiAjZTY1MTAwO1xyXG4gICAgYmFja2dyb3VuZC1jb2xvcjogI2Y1ZjVmNTtcclxufVxyXG4uYnV0dG9uLXNhdmUtcHJlZiB7XHJcbiAgICBoZWlnaHQ6IDMwcHg7XHJcbiAgICB3aWR0aDogMTQwcHg7XHJcbiAgICBwYWRkaW5nLXRvcDogNHB4O1xyXG4gICAgZm9udC1zaXplOiAxM3B4O1xyXG4gICAgcGFkZGluZy1sZWZ0OiA4cHg7XHJcbiAgICBtYXJnaW4tbGVmdDogMHB4O1xyXG4gICAgbWFyZ2luLXJpZ2h0OiAwcHg7XHJcbn1cclxuLkNhcmQyIHtcclxuICAgIGJvcmRlci10b3A6IDhweCBzb2xpZCAjNGExNDhjO1xyXG59XHJcblxyXG5tYXQtc2xpZGUtdG9nZ2xlLnRvZ2dsZS1maWx0ZXIge1xyXG4gICAgZGlzcGxheTogYmxvY2s7XHJcbiAgICBtYXJnaW4tYm90dG9tOiAxMHB4O1xyXG4gICAgZm9udC1zaXplOiAxM3B4O1xyXG4gICAgbGluZS1oZWlnaHQ6IDE0cHg7XHJcbn1cclxuc3Bhbi5tYXQtc2xpZGUtdG9nZ2xlLWNvbnRlbnQge1xyXG4gICAgcGFkZGluZy1sZWZ0OiAxMHB4O1xyXG59XHJcbi5DYXJkSGVhZGVyIHtcclxuICAgIGZvbnQtd2VpZ2h0OiBib2xkO1xyXG59XHJcbi5DYXJkMyB7XHJcbiAgICBib3JkZXItdG9wOiA4cHggc29saWQgI2U2NTEwMDtcclxufVxyXG4uQ2FyZDQge1xyXG4gICAgYm9yZGVyLXRvcDogOHB4IHNvbGlkICMzZTI3MjM7XHJcbn1cclxuc3Bhbi5waS10aW1lcyB7XHJcbiAgICB3aWR0aDogMzBweDtcclxuICAgIGhlaWdodDogMzBweDtcclxuICAgIHBhZGRpbmctdG9wOiA0cHg7XHJcbiAgICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XHJcbiAgICAvKiBwb3NpdGlvbjogYWJzb2x1dGU7ICovXHJcbiAgICB0b3A6IDBweDtcclxuICAgIHJpZ2h0OiAwcHg7XHJcbiAgICB0cmFuc2l0aW9uOiBlYXNlIDAuMjVzIGFsbDtcclxuICAgIC13ZWJraXQtdHJhbnNmb3JtOiB0cmFuc2xhdGUoNTAlLCAtNTAlKTtcclxuICAgIHRyYW5zZm9ybTogdHJhbnNsYXRlKDUwJSwgLTUwJSk7XHJcbiAgICBib3JkZXItcmFkaXVzOiAxMDAwcHg7XHJcbiAgICBiYWNrZ3JvdW5kOiAjZTc0YzNjO1xyXG4gICAgZm9udC1zaXplOiAyMHB4O1xyXG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gICAgbGluZS1oZWlnaHQ6IDEwMCU7XHJcbiAgICBjb2xvcjogd2hpdGU7XHJcbiAgICBjdXJzb3I6IHBvaW50ZXI7XHJcbiAgICBmb250LXdlaWdodDogbGlnaHRlcjtcclxuICAgIG1hcmdpbi10b3A6IDE3cHg7XHJcbiAgICBtYXJnaW4tcmlnaHQ6IDE0cHg7XHJcbn1cclxuLnVpLXNpZGViYXItcmlnaHQudWktc2lkZWJhci1hY3RpdmUuZmlsdGVyIHtcclxuICAgIG92ZXJmbG93OiBzY3JvbGw7XHJcbn1cclxuLyogd2lkdGggKi9cclxuOjotd2Via2l0LXNjcm9sbGJhciB7XHJcbiAgICB3aWR0aDogMTBweDtcclxufVxyXG5cclxuLyogVHJhY2sgKi9cclxuOjotd2Via2l0LXNjcm9sbGJhci10cmFjayB7XHJcbiAgICBib3JkZXItcmFkaXVzOiAxMHB4O1xyXG59XHJcblxyXG4vKiBIYW5kbGUgKi9cclxuOjotd2Via2l0LXNjcm9sbGJhci10aHVtYiB7XHJcbiAgICBiYWNrZ3JvdW5kOiAjMWEyMzdlO1xyXG4gICAgYm9yZGVyLXJhZGl1czogMTBweDtcclxufVxyXG5cclxuLyogSGFuZGxlIG9uIGhvdmVyICovXHJcbjo6LXdlYmtpdC1zY3JvbGxiYXItdGh1bWI6aG92ZXIge1xyXG4gICAgYmFja2dyb3VuZDogIzFhMjM3ZTtcclxufVxyXG4uZmlsdGVyUGFubmVsSGVhZGVyIHtcclxuICAgIGRpc3BsYXk6IGJsb2NrO1xyXG4gICAgcGFkZGluZzogMTBweDtcclxuICAgIGNvbG9yOiAjMDE1NzliO1xyXG4gICAgYmFja2dyb3VuZC1jb2xvcjogI2ZmZmZmZjtcclxuICAgIHRleHQtZGVjb3JhdGlvbjogc29saWQ7XHJcbiAgICBtYXJnaW4tbGVmdDogLTEwcHg7XHJcbiAgICBtYXJnaW4tdG9wOiAxMHB4O1xyXG59XHJcbi5tYXQtc2xpZGUtdG9nZ2xlLWxhYmVsLWJlZm9yZSAubWF0LXNsaWRlLXRvZ2dsZS1iYXIsXHJcbltkaXI9XCJydGxcIl0gLm1hdC1zbGlkZS10b2dnbGUtYmFyIHtcclxuICAgIG1hcmdpbi1sZWZ0OiA3MHB4O1xyXG4gICAgbWFyZ2luLXJpZ2h0OiAwO1xyXG59XHJcbi5kaXNwbGF5LWlubGluZS1ibG9jayB7XHJcbiAgICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XHJcbn1cclxuXHJcbi5maWx0ZXJCdG4ge1xyXG4gICAgYm9yZGVyLXJhZGl1czogMjdweDtcclxuICAgIGZvbnQtc2l6ZTogMTNweDtcclxuICAgIGJhY2tncm91bmQtY29sb3I6ICNhOGIzYTg7XHJcbiAgICBib3JkZXItY29sb3I6ICNhMGJmYTI7XHJcbiAgICBtYXJnaW4tcmlnaHQ6IDJweDtcclxuICAgIGRpc3BsYXk6IGlubGluZTtcclxuICAgIGZsb2F0OiByaWdodDtcclxufVxyXG4iXX0= */"

/***/ }),

/***/ "./src/app/Modules/FetaureModules/capacity-insights/filter/filter.component.html":
/*!***************************************************************************************!*\
  !*** ./src/app/Modules/FetaureModules/capacity-insights/filter/filter.component.html ***!
  \***************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<p-sidebar\n    [(visible)]=\"display\"\n    (onHide)=\"close()\"\n    position=\"right\"\n    class=\"filter\"\n>\n    <form [formGroup]=\"filterForm\">\n        <mat-toolbar class=\"filterPannelHeader\">\n            <span>Filters</span>\n            <span class=\"buton-filter\">\n                <button (click)=\"selectAllFilter()\" class=\"btn button-fetch\">\n                    Select All\n                </button>\n                <!--select all Filter-->\n                <button (click)=\"resetAllFilter()\" class=\"btn button-reset\">\n                    Reset</button\n                ><!--reset all Filter-->\n            </span>\n        </mat-toolbar>\n        <mat-card class=\"example-card Card1\">\n            <p class=\"display-inline-block\">Profit Center</p>\n            <div class=\"display-inline-block pull-right\">\n                <mat-slide-toggle\n                    class=\"toggle-filter\"\n                    formControlName=\"ProfitCenter\"\n                ></mat-slide-toggle>\n            </div>\n        </mat-card>\n        <mat-card class=\"example-card Card1\">\n            <p class=\"display-inline-block\">Delivery Partner</p>\n            <div class=\"display-inline-block pull-right\">\n                <mat-slide-toggle\n                    class=\"toggle-filter\"\n                    formControlName=\"DeliveryPlatform\"\n                ></mat-slide-toggle>\n            </div>\n        </mat-card>\n        <mat-card class=\"example-card Card1\">\n            <p class=\"display-inline-block\">Delivery Manager</p>\n            <div class=\"display-inline-block pull-right\">\n                <mat-slide-toggle\n                    class=\"toggle-filter\"\n                    formControlName=\"DeliveryManager\"\n                ></mat-slide-toggle>\n            </div>\n        </mat-card>\n        <mat-card class=\"example-card Card1\">\n            <p class=\"display-inline-block\">Account Group</p>\n            <div class=\"display-inline-block pull-right\">\n                <mat-slide-toggle\n                    class=\"toggle-filter\"\n                    formControlName=\"AccountGroup\"\n                ></mat-slide-toggle>\n            </div>\n        </mat-card>\n        <!-- <mat-card class=\"example-card Card1\">\n        <p class=\"display-inline-block\">Competency</p>\n        <div class=\"display-inline-block pull-right\">\n            <mat-slide-toggle class=\"toggle-filter\" formControlName=\"Competency\"></mat-slide-toggle>\n        </div>\n      </mat-card>\n  -->\n\n        <!-- <mat-card class=\"example-card Card1\">\n        <p class=\"display-inline-block\">Gender</p>\n        <div class=\"display-inline-block pull-right\">\n            <mat-slide-toggle class=\"toggle-filter\" formControlName=\"Gender\"></mat-slide-toggle>\n        </div>\n      </mat-card> -->\n        <mat-card class=\"example-card Card1\">\n            <p class=\"display-inline-block\">Project Name</p>\n            <div class=\"display-inline-block pull-right\">\n                <mat-slide-toggle\n                    class=\"toggle-filter\"\n                    formControlName=\"ProjectName\"\n                ></mat-slide-toggle>\n            </div>\n        </mat-card>\n        <mat-card class=\"example-card Card1\">\n            <p class=\"display-inline-block\">Practice</p>\n            <div class=\"display-inline-block pull-right\">\n                <mat-slide-toggle\n                    class=\"toggle-filter\"\n                    formControlName=\"Practice\"\n                ></mat-slide-toggle>\n            </div>\n        </mat-card>\n        <mat-card class=\"example-card Card1\">\n            <p class=\"display-inline-block\">Billable Status</p>\n            <div class=\"display-inline-block pull-right\">\n                <mat-slide-toggle\n                    class=\"toggle-filter\"\n                    formControlName=\"BillableStatus\"\n                ></mat-slide-toggle>\n            </div>\n        </mat-card>\n\n        <mat-card class=\"example-card Card1\">\n            <p class=\"display-inline-block\">Location</p>\n            <div class=\"display-inline-block pull-right\">\n                <mat-slide-toggle\n                    class=\"toggle-filter\"\n                    formControlName=\"Location\"\n                ></mat-slide-toggle>\n            </div>\n        </mat-card>\n        <!-- <mat-card class=\"example-card Card1\">\n        <p class=\"display-inline-block\">On Off Shore</p>\n        <div class=\"display-inline-block pull-right\">\n            <mat-slide-toggle class=\"toggle-filter\" formControlName=\"OnOffShore\"></mat-slide-toggle>\n        </div>\n      </mat-card> -->\n\n        <mat-card class=\"example-card Card1\">\n            <p class=\"display-inline-block\">SR Status</p>\n            <div class=\"display-inline-block pull-right\">\n                <mat-slide-toggle\n                    class=\"toggle-filter\"\n                    formControlName=\"SRStatus\"\n                ></mat-slide-toggle>\n            </div>\n        </mat-card>\n\n        <mat-card class=\"example-card Card1\">\n            <p class=\"display-inline-block\">Engagement Type</p>\n            <div class=\"display-inline-block pull-right\">\n                <mat-slide-toggle\n                    class=\"toggle-filter\"\n                    formControlName=\"EngagementType\"\n                ></mat-slide-toggle>\n            </div>\n        </mat-card>\n        <br />\n        <div class=\"\">\n            <button\n                type=\"button\"\n                class=\"btn btn-primary button-save-pref float-right\"\n                (click)=\"close()\"\n            >\n                Apply Filters\n            </button>\n        </div>\n    </form>\n</p-sidebar>\n"

/***/ }),

/***/ "./src/app/Modules/FetaureModules/capacity-insights/filter/filter.component.ts":
/*!*************************************************************************************!*\
  !*** ./src/app/Modules/FetaureModules/capacity-insights/filter/filter.component.ts ***!
  \*************************************************************************************/
/*! exports provided: FilterComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FilterComponent", function() { return FilterComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _Services_capacity_service_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../Services/capacity-service.service */ "./src/app/Modules/FetaureModules/capacity-insights/Services/capacity-service.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var FilterComponent = /** @class */ (function () {
    function FilterComponent(route, formBuilder, data) {
        this.route = route;
        this.formBuilder = formBuilder;
        this.data = data;
        this.display = true;
        this.cities = [];
        this.filterForm = this.formBuilder.group({
            DeliveryPlatform: [''],
            Competency: [''],
            BillableStatus: [''],
            Location: [''],
            Gender: [''],
            Practice: [''],
            OnOffShore: [''],
            SRStatus: [''],
            AccountGroup: [''],
            DeliveryManager: [''],
            ProjectName: [''],
            EngagementType: [''],
            ProfitCenter: ['']
        });
        this.filterapplied = this.filterForm.value;
        this.cities = [];
        this.cities.push({ label: 'Male', value: 'Male' });
        this.cities.push({ label: 'Female', value: 'Female' });
    }
    // Page load function
    FilterComponent.prototype.ngOnInit = function () {
        this._filtersApplied = this.data.getFiltersApplied();
        if (this._filtersApplied) {
            this.filterForm.patchValue(this._filtersApplied);
        }
    };
    //  Function to close side panel
    FilterComponent.prototype.close = function () {
        this.filterapplied = this.filterForm.value;
        this.data.setFiltersApplied(this.filterapplied);
        this.route.navigate(['/CapacityInsights']);
    };
    // select all filter on click of this btn//assign true will select all 13 filters
    FilterComponent.prototype.selectAllFilter = function () {
        this.filterForm = this.formBuilder.group({
            DeliveryPlatform: true,
            Competency: true,
            BillableStatus: true,
            Location: true,
            Gender: true,
            Practice: true,
            OnOffShore: true,
            SRStatus: true,
            AccountGroup: true,
            DeliveryManager: true,
            ProjectName: true,
            EngagementType: true,
            ProfitCenter: true
        });
    };
    // function to  select all filter on click of this btn
    // assign false will select all 13 filters
    // Reset all filters
    FilterComponent.prototype.resetAllFilter = function () {
        this.filterForm = this.formBuilder.group({
            DeliveryPlatform: false,
            Competency: false,
            BillableStatus: false,
            Location: false,
            Gender: false,
            Practice: false,
            OnOffShore: false,
            SRStatus: false,
            AccountGroup: false,
            DeliveryManager: false,
            ProjectName: false,
            EngagementType: false,
            ProfitCenter: false
        });
    };
    FilterComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-filter',
            template: __webpack_require__(/*! ./filter.component.html */ "./src/app/Modules/FetaureModules/capacity-insights/filter/filter.component.html"),
            styles: [__webpack_require__(/*! ./filter.component.css */ "./src/app/Modules/FetaureModules/capacity-insights/filter/filter.component.css")]
        }),
        __metadata("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_1__["Router"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormBuilder"],
            _Services_capacity_service_service__WEBPACK_IMPORTED_MODULE_3__["CapacityServiceService"]])
    ], FilterComponent);
    return FilterComponent;
}());



/***/ }),

/***/ "./src/app/Modules/FetaureModules/capacity-insights/preferences/preferences.component.css":
/*!************************************************************************************************!*\
  !*** ./src/app/Modules/FetaureModules/capacity-insights/preferences/preferences.component.css ***!
  \************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".filterPannelHeader {\r\n    display: inline;\r\n    padding: 10px;\r\n    color: #01579b;\r\n    background-color: #ffffff;\r\n    -webkit-text-decoration: solid;\r\n            text-decoration: solid;\r\n    margin-left: -10px;\r\n\r\n    font-size: 17px;\r\n}\r\n.listBox {\r\n    border-radius: 2px;\r\n    margin-top: 15px;\r\n    height: 35px;\r\n    font-size: 20px;\r\n    background-color: #2980b9;\r\n}\r\n:host >>> .ui-listbox {\r\n    background-color: #2980b9;\r\n    background-image: none;\r\n    border: none;\r\n    color: white;\r\n}\r\n.iconColor {\r\n    color: black;\r\n}\r\n.defaultprefClass {\r\n    margin-top: -20px;\r\n}\r\n.alert {\r\n    padding: 6px;\r\n    background-color: blue;\r\n    color: white;\r\n    text-align: center;\r\n}\r\n.message {\r\n    margin-top: 100px;\r\n}\r\n@-webkit-keyframes ui-progress-spinner-color {\r\n    100%,\r\n    0% {\r\n        stroke: #d62d20;\r\n    }\r\n    40% {\r\n        stroke: #0057e7;\r\n    }\r\n    66% {\r\n        stroke: #008744;\r\n    }\r\n    80%,\r\n    90% {\r\n        stroke: #ffa700;\r\n    }\r\n}\r\n@keyframes ui-progress-spinner-color {\r\n    100%,\r\n    0% {\r\n        stroke: #d62d20;\r\n    }\r\n    40% {\r\n        stroke: #0057e7;\r\n    }\r\n    66% {\r\n        stroke: #008744;\r\n    }\r\n    80%,\r\n    90% {\r\n        stroke: #ffa700;\r\n    }\r\n}\r\n.ui-progress-spinner-svg {\r\n    -webkit-animation-duration: 0.5s;\r\n            animation-duration: 0.5s;\r\n    /* margin-right: 84px; */\r\n    position: absolute;\r\n    top: -263px;\r\n    left: -184px;\r\n}\r\n.font14 {\r\n    font-size: 12px;\r\n    margin-top: 5px;\r\n    padding-left: 5px;\r\n}\r\n::ng-deep .pref-button ul *{\r\n    margin: 0;\r\n    padding: 0.5em 1em !important;\r\n    border-radius: 0;\r\n    font-size: 12px;\r\n    max-width: 25em;\r\n}\r\n::ng-deep.ui-listbox .ui-listbox-item {\r\n    border-bottom: 5px solid white !important;\r\n}\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvTW9kdWxlcy9GZXRhdXJlTW9kdWxlcy9jYXBhY2l0eS1pbnNpZ2h0cy9wcmVmZXJlbmNlcy9wcmVmZXJlbmNlcy5jb21wb25lbnQuY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0lBQ0ksZ0JBQWdCO0lBQ2hCLGNBQWM7SUFDZCxlQUFlO0lBQ2YsMEJBQTBCO0lBQzFCLCtCQUF1QjtZQUF2Qix1QkFBdUI7SUFDdkIsbUJBQW1COztJQUVuQixnQkFBZ0I7Q0FDbkI7QUFDRDtJQUNJLG1CQUFtQjtJQUNuQixpQkFBaUI7SUFDakIsYUFBYTtJQUNiLGdCQUFnQjtJQUNoQiwwQkFBMEI7Q0FDN0I7QUFFRDtJQUNJLDBCQUEwQjtJQUMxQix1QkFBdUI7SUFDdkIsYUFBYTtJQUNiLGFBQWE7Q0FDaEI7QUFFRDtJQUNJLGFBQWE7Q0FDaEI7QUFFRDtJQUNJLGtCQUFrQjtDQUNyQjtBQUNEO0lBQ0ksYUFBYTtJQUNiLHVCQUF1QjtJQUN2QixhQUFhO0lBQ2IsbUJBQW1CO0NBQ3RCO0FBQ0Q7SUFDSSxrQkFBa0I7Q0FDckI7QUFDRDtJQUNJOztRQUVJLGdCQUFnQjtLQUNuQjtJQUNEO1FBQ0ksZ0JBQWdCO0tBQ25CO0lBQ0Q7UUFDSSxnQkFBZ0I7S0FDbkI7SUFDRDs7UUFFSSxnQkFBZ0I7S0FDbkI7Q0FDSjtBQWZEO0lBQ0k7O1FBRUksZ0JBQWdCO0tBQ25CO0lBQ0Q7UUFDSSxnQkFBZ0I7S0FDbkI7SUFDRDtRQUNJLGdCQUFnQjtLQUNuQjtJQUNEOztRQUVJLGdCQUFnQjtLQUNuQjtDQUNKO0FBQ0Q7SUFDSSxpQ0FBeUI7WUFBekIseUJBQXlCO0lBQ3pCLHlCQUF5QjtJQUN6QixtQkFBbUI7SUFDbkIsWUFBWTtJQUNaLGFBQWE7Q0FDaEI7QUFDRDtJQUNJLGdCQUFnQjtJQUNoQixnQkFBZ0I7SUFDaEIsa0JBQWtCO0NBQ3JCO0FBQ0Q7SUFDSSxVQUFVO0lBQ1YsOEJBQThCO0lBRzlCLGlCQUFpQjtJQUNqQixnQkFBZ0I7SUFDaEIsZ0JBQWdCO0NBQ25CO0FBQ0Q7SUFDSSwwQ0FBMEM7Q0FDN0MiLCJmaWxlIjoic3JjL2FwcC9Nb2R1bGVzL0ZldGF1cmVNb2R1bGVzL2NhcGFjaXR5LWluc2lnaHRzL3ByZWZlcmVuY2VzL3ByZWZlcmVuY2VzLmNvbXBvbmVudC5jc3MiLCJzb3VyY2VzQ29udGVudCI6WyIuZmlsdGVyUGFubmVsSGVhZGVyIHtcclxuICAgIGRpc3BsYXk6IGlubGluZTtcclxuICAgIHBhZGRpbmc6IDEwcHg7XHJcbiAgICBjb2xvcjogIzAxNTc5YjtcclxuICAgIGJhY2tncm91bmQtY29sb3I6ICNmZmZmZmY7XHJcbiAgICB0ZXh0LWRlY29yYXRpb246IHNvbGlkO1xyXG4gICAgbWFyZ2luLWxlZnQ6IC0xMHB4O1xyXG5cclxuICAgIGZvbnQtc2l6ZTogMTdweDtcclxufVxyXG4ubGlzdEJveCB7XHJcbiAgICBib3JkZXItcmFkaXVzOiAycHg7XHJcbiAgICBtYXJnaW4tdG9wOiAxNXB4O1xyXG4gICAgaGVpZ2h0OiAzNXB4O1xyXG4gICAgZm9udC1zaXplOiAyMHB4O1xyXG4gICAgYmFja2dyb3VuZC1jb2xvcjogIzI5ODBiOTtcclxufVxyXG5cclxuOmhvc3QgPj4+IC51aS1saXN0Ym94IHtcclxuICAgIGJhY2tncm91bmQtY29sb3I6ICMyOTgwYjk7XHJcbiAgICBiYWNrZ3JvdW5kLWltYWdlOiBub25lO1xyXG4gICAgYm9yZGVyOiBub25lO1xyXG4gICAgY29sb3I6IHdoaXRlO1xyXG59XHJcblxyXG4uaWNvbkNvbG9yIHtcclxuICAgIGNvbG9yOiBibGFjaztcclxufVxyXG5cclxuLmRlZmF1bHRwcmVmQ2xhc3Mge1xyXG4gICAgbWFyZ2luLXRvcDogLTIwcHg7XHJcbn1cclxuLmFsZXJ0IHtcclxuICAgIHBhZGRpbmc6IDZweDtcclxuICAgIGJhY2tncm91bmQtY29sb3I6IGJsdWU7XHJcbiAgICBjb2xvcjogd2hpdGU7XHJcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbn1cclxuLm1lc3NhZ2Uge1xyXG4gICAgbWFyZ2luLXRvcDogMTAwcHg7XHJcbn1cclxuQGtleWZyYW1lcyB1aS1wcm9ncmVzcy1zcGlubmVyLWNvbG9yIHtcclxuICAgIDEwMCUsXHJcbiAgICAwJSB7XHJcbiAgICAgICAgc3Ryb2tlOiAjZDYyZDIwO1xyXG4gICAgfVxyXG4gICAgNDAlIHtcclxuICAgICAgICBzdHJva2U6ICMwMDU3ZTc7XHJcbiAgICB9XHJcbiAgICA2NiUge1xyXG4gICAgICAgIHN0cm9rZTogIzAwODc0NDtcclxuICAgIH1cclxuICAgIDgwJSxcclxuICAgIDkwJSB7XHJcbiAgICAgICAgc3Ryb2tlOiAjZmZhNzAwO1xyXG4gICAgfVxyXG59XHJcbi51aS1wcm9ncmVzcy1zcGlubmVyLXN2ZyB7XHJcbiAgICBhbmltYXRpb24tZHVyYXRpb246IDAuNXM7XHJcbiAgICAvKiBtYXJnaW4tcmlnaHQ6IDg0cHg7ICovXHJcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbiAgICB0b3A6IC0yNjNweDtcclxuICAgIGxlZnQ6IC0xODRweDtcclxufVxyXG4uZm9udDE0IHtcclxuICAgIGZvbnQtc2l6ZTogMTJweDtcclxuICAgIG1hcmdpbi10b3A6IDVweDtcclxuICAgIHBhZGRpbmctbGVmdDogNXB4O1xyXG59XHJcbjo6bmctZGVlcCAucHJlZi1idXR0b24gdWwgKntcclxuICAgIG1hcmdpbjogMDtcclxuICAgIHBhZGRpbmc6IDAuNWVtIDFlbSAhaW1wb3J0YW50O1xyXG4gICAgLW1vei1ib3JkZXItcmFkaXVzOiAwO1xyXG4gICAgLXdlYmtpdC1ib3JkZXItcmFkaXVzOiAwO1xyXG4gICAgYm9yZGVyLXJhZGl1czogMDtcclxuICAgIGZvbnQtc2l6ZTogMTJweDtcclxuICAgIG1heC13aWR0aDogMjVlbTtcclxufVxyXG46Om5nLWRlZXAudWktbGlzdGJveCAudWktbGlzdGJveC1pdGVtIHtcclxuICAgIGJvcmRlci1ib3R0b206IDVweCBzb2xpZCB3aGl0ZSAhaW1wb3J0YW50O1xyXG59Il19 */"

/***/ }),

/***/ "./src/app/Modules/FetaureModules/capacity-insights/preferences/preferences.component.html":
/*!*************************************************************************************************!*\
  !*** ./src/app/Modules/FetaureModules/capacity-insights/preferences/preferences.component.html ***!
  \*************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<p-sidebar [(visible)]=\"display\" position=\"right\" (onHide)=\"close()\">\n    <div *ngIf=\"ShowPref\">\n        <mat-toolbar class=\"filterPannelHeader\">\n            <span>Select your filter preference</span>\n        </mat-toolbar>\n        <div class=\"ui-g-12 ui-md-12 ui-lg-12\">\n            <p-listbox\n                [options]=\"preferenceDetails\"\n                (onChange)=\"Validate()\"\n                [(ngModel)]=\"selectedPreference\"\n                optionLabel=\"filterName\"\n                [listStyle]=\"{ 'max-height': '250px' }\"\n                class=\"pref-button\"\n            ></p-listbox>\n            <div class=\"ui-g-12 ui-md-12 ui-lg-1\">\n                <p-checkbox\n                    [(ngModel)]=\"MakePrefrenceDefault\"\n                    binary=\"true\"\n                ></p-checkbox>\n            </div>\n            <div class=\"ui-g-12 ui-md-12 ui-lg-11\">\n                <p class=\"font14\">Make this selection as a defult preference</p>\n            </div>\n            <button\n            class=\"btn button-fetch float-right border-radius-xs\"\n            pTooltip=\"Clear selected preference\"\n            (click)=\"FetchAllCapacityData()\"\n        >\n            Clear\n        </button>\n            <button\n                class=\"btn button-fetch float-right border-radius-xs\"\n                pTooltip=\"Update default and fetch data\"\n                (click)=\"FetchFilteredCapacityData()\"\n                [disabled]=\"isNoPreferenceSet\"\n            >\n                Fetch\n            </button>\n            <button\n                *ngIf=\"MakePrefrenceDefault\"\n                pTooltip=\"Update default preference\"\n                class=\"btn button-save float-left border-radius-xs\"\n                (click)=\"SaveDefaultPreference()\"\n                [disabled]=\"isNoPreferenceSet\"\n            >\n                Save\n            </button>\n        </div>\n        <div>\n            <div\n                class=\"display-flex float-left margin-top-bottom\"\n                *ngIf=\"hideshowdef\"\n            >\n                <span class=\"count-table defaultprefClass\"\n                    ><span class=\"count-bubblePref\" pTooltip=\"\">{{\n                        defPrefname.filterName\n                    }}</span></span\n                ><span style=\"color: rgb(51, 51, 95);font-size: 14px;\" class=\"\">\n                    is your default preference.</span\n                >\n            </div>\n        </div>\n    </div>\n\n    <div *ngIf=\"ShowMessage\" class=\"message\">\n        <div class=\"alert\">\n            <strong class=\"font14\">No Saved Preferences Found</strong>\n        </div>\n    </div>\n\n    <div *ngIf=\"loading\">\n        <p-progressSpinner\n            [style]=\"{ width: '50px', height: '50px' }\"\n            strokeWidth=\"8\"\n            fill=\"#EEEEEE\"\n            animationDuration=\".5s\"\n        ></p-progressSpinner>\n    </div>\n</p-sidebar>\n<p-dialog header=\"Alert\" [(visible)]=\"ConfirmationDisplay\" (onHide)=\"close()\">\n    Your default preference has been changed.\n</p-dialog>\n<p-dialog\n    header=\"Alert\"\n    [(visible)]=\"SavedConfirmationDisplay\"\n    (onHide)=\"RefreshPreferences()\"\n>\n    Your default preference has been updated.\n</p-dialog>\n"

/***/ }),

/***/ "./src/app/Modules/FetaureModules/capacity-insights/preferences/preferences.component.ts":
/*!***********************************************************************************************!*\
  !*** ./src/app/Modules/FetaureModules/capacity-insights/preferences/preferences.component.ts ***!
  \***********************************************************************************************/
/*! exports provided: PreferencesComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PreferencesComponent", function() { return PreferencesComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _Services_capacity_service_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../Services/capacity-service.service */ "./src/app/Modules/FetaureModules/capacity-insights/Services/capacity-service.service.ts");
/* harmony import */ var src_app_Services_httprequest_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/Services/httprequest.service */ "./src/app/Services/httprequest.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var src_app_Models_PreferenceModel__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! src/app/Models/PreferenceModel */ "./src/app/Models/PreferenceModel.ts");
/* harmony import */ var src_app_Services_sharedservice_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! src/app/Services/sharedservice.service */ "./src/app/Services/sharedservice.service.ts");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







var PreferencesComponent = /** @class */ (function () {
    // tslint:disable-next-line:max-line-length
    function PreferencesComponent(route, data, service, sharedData) {
        var _this = this;
        this.route = route;
        this.data = data;
        this.service = service;
        this.sharedData = sharedData;
        this.DeliveryPartnerArray = [];
        this.OnOffshoreArray = [];
        this.GenderArray = [];
        this.BillableArray = [];
        this.SRStatusArray = [];
        this.ProfitCenterNameArray = [];
        this.LocationArray = [];
        this.AccountNameArray = [];
        this.DeliveryManagerArray = [];
        this.CompetencyArray = [];
        this.EngagementTypeArray = [];
        this.ProjectNameArray = [];
        this.PracticeArray = [];
        this.isDefaultPrefClass = false;
        this.defPrefname = new src_app_Models_PreferenceModel__WEBPACK_IMPORTED_MODULE_4__["PreferenceData"]();
        this.hideshowdef = false;
        // Fetch button is enabled by default
        this.isNoPreferenceSet = true;
        this.display = true;
        this.ShowPref = false;
        this.ShowMessage = false;
        this.loading = false;
        this.ConfirmationDisplay = false;
        this.mid = this.sharedData.midOfLoggedInUser;
        this.loading = true;
        // API call to get prefernces saved by a particular user
        // tslint:disable-next-line:max-line-length
        // tslint:disable-next-line:no-shadowed-variable
        this.data
            .getPreferenceData('/api/Preferences/GetProfilePreference?MID=' + this.mid)
            .subscribe(
        // tslint:disable-next-line:no-shadowed-variable
        function (data) {
            // Check if there are any prefernces set by the user. If yes display preference.
            if (data.length > 0) {
                _this.loading = false;
                _this.ShowPref = true;
                _this.preferenceDetails = data;
            }
            else {
                _this.loading = false;
                _this.ShowMessage = true;
            }
        }, function (error) {
            _this.route.navigate(['/core/InternalServerError']);
            if (error.status === 0) {
                _this.route.navigate(['/core/ServerDown']);
            }
            else if (error.status === 401) {
                _this.route.navigate(['/core/AccessDenied']);
            }
            else {
                _this.route.navigate(['/core/InternalServerError']);
            }
        });
        // Function to fetch default preference to show in side bar
        this.data
            .getAPI('/api/preferences/GetDefaultPreferenceForDisplay?MID=' +
            this.mid)
            .subscribe(function (resp) {
            if (resp == null) {
                _this.defPrefname.filterName = null;
                _this.defPrefname.preferences = null;
                _this.hideshowdef = false;
            }
            else {
                _this.defPrefname.filterName = resp.filterName;
                _this.defPrefname.preferences = resp.preferences;
                _this.hideshowdef = true; // This is used to hide and show the default preference
            }
        }, function (error) {
            _this.route.navigate(['/core/InternalServerError']);
            if (error.status === 0) {
                _this.route.navigate(['/core/ServerDown']);
            }
            else if (error.status === 401) {
                _this.route.navigate(['/core/AccessDenied']);
            }
            else {
                _this.route.navigate(['/core/InternalServerError']);
            }
        });
    }
    PreferencesComponent.prototype.showDialog = function () {
        this.ConfirmationDisplay = true;
    };
    PreferencesComponent.prototype.showSavedConfirmationDialog = function () {
        this.SavedConfirmationDisplay = true;
    };
    // Page load function
    PreferencesComponent.prototype.ngOnInit = function () { };
    //Reset selected preference and select all data
    PreferencesComponent.prototype.FetchAllCapacityData = function () {
        var selectedPref = null;
        var filterModelObj = null;
        this.service.clearSetSelectedPref(selectedPref, filterModelObj);
        this.close();
    };
    // Fetch button click function to capture details of selected preference
    PreferencesComponent.prototype.FetchFilteredCapacityDataAndUpdateDefault = function () {
        var _this = this;
        var selectedPref = this.selectedPreference.filterName;
        this.service.setSelectedPref(selectedPref, this.selectedPreference.preferences);
        // this.service.setSavedPrefrence();
        var token = this.sharedData.tokenIDOfLoggedUser;
        // tslint:disable-next-line:max-line-length
        var header = new _angular_common_http__WEBPACK_IMPORTED_MODULE_6__["HttpHeaders"]()
            .set('Content-Type', 'application/json')
            .set('Access-Control-Allow-Origin', '*')
            .set('Authorization', 'Bearer ' + token);
        this.data
            .post('/api/Preferences/ChangeDefaultPreference', JSON.stringify(this.selectedPreference))
            .subscribe(function (success) {
            _this.showDialog();
            setTimeout(function () {
                _this.route.navigate([
                    '/CapacityInsights'
                ]);
            }, 3000);
        }, function (error) {
            if (error.status === 0) {
                _this.route.navigate(['/core/ServerDown']);
            }
            else if (error.status === 401) {
                _this.route.navigate(['/core/AccessDenied']);
            }
            else {
                _this.route.navigate(['/core/InternalServerError']);
            }
        });
    };
    // this method is used to fetch filtered capacity data on preference selection if checkbox is checked it will make it default preference
    PreferencesComponent.prototype.FetchFilteredCapacityData = function () {
        if (this.MakePrefrenceDefault === true) {
            this.FetchFilteredCapacityDataAndUpdateDefault();
        }
        else {
            var selectedPref = this.selectedPreference.filterName;
            var filterModelObj = this.selectedPreference.preferences;
            this.service.setSelectedPref(selectedPref, filterModelObj);
            this.close();
        }
    };
    // Close preference side tab
    PreferencesComponent.prototype.close = function () {
        this.route.navigate(['/CapacityInsights']);
    };
    PreferencesComponent.prototype.Validate = function () {
        if (this.selectedPreference != null) {
            this.isNoPreferenceSet = false;
        }
    };
    PreferencesComponent.prototype.SaveDefaultPreference = function () {
        var _this = this;
        this.data
            .post('/api/Preferences/ChangeDefaultPreference', JSON.stringify(this.selectedPreference))
            .subscribe(function (success) {
            _this.showSavedConfirmationDialog();
        }, function (error) {
            _this.route.navigate(['/core/InternalServerError']);
            if (error.status === 0) {
                _this.route.navigate(['/core/ServerDown']);
            }
            else if (error.status === 401) {
                _this.route.navigate(['/core/AccessDenied']);
            }
            else {
                _this.route.navigate(['/core/InternalServerError']);
            }
        });
    };
    // this method refereshes preference after saving a default preference.
    PreferencesComponent.prototype.RefreshPreferences = function () {
        var _this = this;
        this.selectedPreference = null;
        this.MakePrefrenceDefault = false;
        this.isNoPreferenceSet = true;
        this.data
            .getAPI('/api/preferences/GetDefaultPreferenceForDisplay?MID=' +
            this.mid)
            .subscribe(function (resp) {
            if (resp == null) {
                _this.defPrefname.filterName = null;
                _this.defPrefname.preferences = null;
                _this.hideshowdef = false;
            }
            else {
                _this.defPrefname.filterName = resp.filterName;
                _this.defPrefname.preferences = resp.preferences;
                _this.hideshowdef = true; // This is used to hide and show the default preference
            }
        }, function (error) {
            _this.route.navigate(['/core/InternalServerError']);
            if (error.status === 0) {
                _this.route.navigate(['/core/ServerDown']);
            }
            else if (error.status === 401) {
                _this.route.navigate(['/core/AccessDenied']);
            }
            else {
                _this.route.navigate(['/core/InternalServerError']);
            }
        });
    };
    PreferencesComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-preferences',
            template: __webpack_require__(/*! ./preferences.component.html */ "./src/app/Modules/FetaureModules/capacity-insights/preferences/preferences.component.html"),
            styles: [__webpack_require__(/*! ./preferences.component.css */ "./src/app/Modules/FetaureModules/capacity-insights/preferences/preferences.component.css")]
        }),
        __metadata("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"],
            src_app_Services_httprequest_service__WEBPACK_IMPORTED_MODULE_2__["HTTPRequestService"],
            _Services_capacity_service_service__WEBPACK_IMPORTED_MODULE_1__["CapacityServiceService"],
            src_app_Services_sharedservice_service__WEBPACK_IMPORTED_MODULE_5__["SharedserviceService"]])
    ], PreferencesComponent);
    return PreferencesComponent;
}());



/***/ })

}]);
//# sourceMappingURL=Modules-FetaureModules-capacity-insights-capacity-insights-module.js.map