(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["Modules-core-core-module"],{

/***/ "./src/app/Modules/core/access-denied/access-denied.component.css":
/*!************************************************************************!*\
  !*** ./src/app/Modules/core/access-denied/access-denied.component.css ***!
  \************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".error\r\n{\r\n    text-align: center;\r\n    font-size: 40px;\r\n}\r\n.full{\r\n    width:100%;\r\n    height: 100%;\r\n}\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvTW9kdWxlcy9jb3JlL2FjY2Vzcy1kZW5pZWQvYWNjZXNzLWRlbmllZC5jb21wb25lbnQuY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBOztJQUVJLG1CQUFtQjtJQUNuQixnQkFBZ0I7Q0FDbkI7QUFDRDtJQUNJLFdBQVc7SUFDWCxhQUFhO0NBQ2hCIiwiZmlsZSI6InNyYy9hcHAvTW9kdWxlcy9jb3JlL2FjY2Vzcy1kZW5pZWQvYWNjZXNzLWRlbmllZC5jb21wb25lbnQuY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLmVycm9yXHJcbntcclxuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICAgIGZvbnQtc2l6ZTogNDBweDtcclxufVxyXG4uZnVsbHtcclxuICAgIHdpZHRoOjEwMCU7XHJcbiAgICBoZWlnaHQ6IDEwMCU7XHJcbn0iXX0= */"

/***/ }),

/***/ "./src/app/Modules/core/access-denied/access-denied.component.html":
/*!*************************************************************************!*\
  !*** ./src/app/Modules/core/access-denied/access-denied.component.html ***!
  \*************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"row\">\n  <div class=\"col-sm-12\">\n    <img src=\"../assets/layout/images/SessionExpired.png\" class=\"img-fluid img-notauth\"/>\n  </div>\n</div>\n<div class=\"row margin-top-sm\">\n  <div class=\"col-sm-12 margin-top-sm\">\n      <p class=\" error\">Your session is expired.</p> \n      <p class=\"text-center notauth-text\">Please try to close the browser and try again.</p>\n  </div>\n</div>\n\n"

/***/ }),

/***/ "./src/app/Modules/core/access-denied/access-denied.component.ts":
/*!***********************************************************************!*\
  !*** ./src/app/Modules/core/access-denied/access-denied.component.ts ***!
  \***********************************************************************/
/*! exports provided: AccessDeniedComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AccessDeniedComponent", function() { return AccessDeniedComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var AccessDeniedComponent = /** @class */ (function () {
    function AccessDeniedComponent() {
    }
    AccessDeniedComponent.prototype.ngOnInit = function () {
    };
    AccessDeniedComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-access-denied',
            template: __webpack_require__(/*! ./access-denied.component.html */ "./src/app/Modules/core/access-denied/access-denied.component.html"),
            styles: [__webpack_require__(/*! ./access-denied.component.css */ "./src/app/Modules/core/access-denied/access-denied.component.css")]
        }),
        __metadata("design:paramtypes", [])
    ], AccessDeniedComponent);
    return AccessDeniedComponent;
}());



/***/ }),

/***/ "./src/app/Modules/core/core-routing.module.ts":
/*!*****************************************************!*\
  !*** ./src/app/Modules/core/core-routing.module.ts ***!
  \*****************************************************/
/*! exports provided: CoreRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CoreRoutingModule", function() { return CoreRoutingModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _unauthorized_unauthorized_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./unauthorized/unauthorized.component */ "./src/app/Modules/core/unauthorized/unauthorized.component.ts");
/* harmony import */ var _access_denied_access_denied_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./access-denied/access-denied.component */ "./src/app/Modules/core/access-denied/access-denied.component.ts");
/* harmony import */ var _internal_server_error_internal_server_error_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./internal-server-error/internal-server-error.component */ "./src/app/Modules/core/internal-server-error/internal-server-error.component.ts");
/* harmony import */ var _server_down_server_down_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./server-down/server-down.component */ "./src/app/Modules/core/server-down/server-down.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};






// the routes array is feeded with routes within the components in core module
var routes = [
    { path: 'unauthorized', component: _unauthorized_unauthorized_component__WEBPACK_IMPORTED_MODULE_2__["UnauthorizedComponent"] },
    { path: 'InternalServerError', component: _internal_server_error_internal_server_error_component__WEBPACK_IMPORTED_MODULE_4__["InternalServerErrorComponent"] },
    { path: 'ServerDown', component: _server_down_server_down_component__WEBPACK_IMPORTED_MODULE_5__["ServerDownComponent"] },
    { path: 'AccessDenied', component: _access_denied_access_denied_component__WEBPACK_IMPORTED_MODULE_3__["AccessDeniedComponent"] }
    // {path:'addadmin',component:AddAdminComponent},
    // {path:'viewadmin',component:ViewAdminsComponent}
];
var CoreRoutingModule = /** @class */ (function () {
    function CoreRoutingModule() {
    }
    CoreRoutingModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"].forChild(routes)],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"]]
        })
    ], CoreRoutingModule);
    return CoreRoutingModule;
}());



/***/ }),

/***/ "./src/app/Modules/core/core.module.ts":
/*!*********************************************!*\
  !*** ./src/app/Modules/core/core.module.ts ***!
  \*********************************************/
/*! exports provided: CoreModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CoreModule", function() { return CoreModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _core_routing_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./core-routing.module */ "./src/app/Modules/core/core-routing.module.ts");
/* harmony import */ var _access_denied_access_denied_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./access-denied/access-denied.component */ "./src/app/Modules/core/access-denied/access-denied.component.ts");
/* harmony import */ var _unauthorized_unauthorized_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./unauthorized/unauthorized.component */ "./src/app/Modules/core/unauthorized/unauthorized.component.ts");
/* harmony import */ var _underconstruction_underconstruction_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./underconstruction/underconstruction.component */ "./src/app/Modules/core/underconstruction/underconstruction.component.ts");
/* harmony import */ var _internal_server_error_internal_server_error_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./internal-server-error/internal-server-error.component */ "./src/app/Modules/core/internal-server-error/internal-server-error.component.ts");
/* harmony import */ var _server_down_server_down_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./server-down/server-down.component */ "./src/app/Modules/core/server-down/server-down.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};




// import { UnAuthorizedPageComponent } from './un-authorized-page/un-authorized-page.component';




var CoreModule = /** @class */ (function () {
    function CoreModule() {
    }
    CoreModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            declarations: [_access_denied_access_denied_component__WEBPACK_IMPORTED_MODULE_3__["AccessDeniedComponent"], _unauthorized_unauthorized_component__WEBPACK_IMPORTED_MODULE_4__["UnauthorizedComponent"], _underconstruction_underconstruction_component__WEBPACK_IMPORTED_MODULE_5__["UnderconstructionComponent"],
                _internal_server_error_internal_server_error_component__WEBPACK_IMPORTED_MODULE_6__["InternalServerErrorComponent"], _server_down_server_down_component__WEBPACK_IMPORTED_MODULE_7__["ServerDownComponent"]],
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"],
                _core_routing_module__WEBPACK_IMPORTED_MODULE_2__["CoreRoutingModule"]
            ]
        })
    ], CoreModule);
    return CoreModule;
}());



/***/ }),

/***/ "./src/app/Modules/core/internal-server-error/internal-server-error.component.css":
/*!****************************************************************************************!*\
  !*** ./src/app/Modules/core/internal-server-error/internal-server-error.component.css ***!
  \****************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".error\r\n{\r\n    text-align: center;\r\n    font-size: 40px;\r\n}\r\n.full{\r\n    width:100%;\r\n    height: 100%;\r\n}\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvTW9kdWxlcy9jb3JlL2ludGVybmFsLXNlcnZlci1lcnJvci9pbnRlcm5hbC1zZXJ2ZXItZXJyb3IuY29tcG9uZW50LmNzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTs7SUFFSSxtQkFBbUI7SUFDbkIsZ0JBQWdCO0NBQ25CO0FBQ0Q7SUFDSSxXQUFXO0lBQ1gsYUFBYTtDQUNoQiIsImZpbGUiOiJzcmMvYXBwL01vZHVsZXMvY29yZS9pbnRlcm5hbC1zZXJ2ZXItZXJyb3IvaW50ZXJuYWwtc2VydmVyLWVycm9yLmNvbXBvbmVudC5jc3MiLCJzb3VyY2VzQ29udGVudCI6WyIuZXJyb3Jcclxue1xyXG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gICAgZm9udC1zaXplOiA0MHB4O1xyXG59XHJcbi5mdWxse1xyXG4gICAgd2lkdGg6MTAwJTtcclxuICAgIGhlaWdodDogMTAwJTtcclxufSJdfQ== */"

/***/ }),

/***/ "./src/app/Modules/core/internal-server-error/internal-server-error.component.html":
/*!*****************************************************************************************!*\
  !*** ./src/app/Modules/core/internal-server-error/internal-server-error.component.html ***!
  \*****************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n<div class=\"row\">\n    <div class=\"col-sm-12\">\n      <img src=\"../assets/layout/images/someting went wrong.png\" class=\"img-fluid img-notauth\"/>\n    </div>\n  </div>\n  <div class=\"row margin-top-sm\">\n    <div class=\"col-sm-12 margin-top-sm\">\n        <p class=\" error\">Oop's Something went wrong!!</p> \n        <p class=\"text-center notauth-text\">Please try to close the browser and try again.</p>\n    </div>\n  </div>\n\n\n\n\n"

/***/ }),

/***/ "./src/app/Modules/core/internal-server-error/internal-server-error.component.ts":
/*!***************************************************************************************!*\
  !*** ./src/app/Modules/core/internal-server-error/internal-server-error.component.ts ***!
  \***************************************************************************************/
/*! exports provided: InternalServerErrorComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "InternalServerErrorComponent", function() { return InternalServerErrorComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var InternalServerErrorComponent = /** @class */ (function () {
    function InternalServerErrorComponent() {
    }
    InternalServerErrorComponent.prototype.ngOnInit = function () {
    };
    InternalServerErrorComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-internal-server-error',
            template: __webpack_require__(/*! ./internal-server-error.component.html */ "./src/app/Modules/core/internal-server-error/internal-server-error.component.html"),
            styles: [__webpack_require__(/*! ./internal-server-error.component.css */ "./src/app/Modules/core/internal-server-error/internal-server-error.component.css")]
        }),
        __metadata("design:paramtypes", [])
    ], InternalServerErrorComponent);
    return InternalServerErrorComponent;
}());



/***/ }),

/***/ "./src/app/Modules/core/server-down/server-down.component.css":
/*!********************************************************************!*\
  !*** ./src/app/Modules/core/server-down/server-down.component.css ***!
  \********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".error\r\n{\r\n    text-align: center;\r\n    font-size: 40px;\r\n}\r\n.full{\r\n    width:100%;\r\n    height: 100%;\r\n}\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvTW9kdWxlcy9jb3JlL3NlcnZlci1kb3duL3NlcnZlci1kb3duLmNvbXBvbmVudC5jc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7O0lBRUksbUJBQW1CO0lBQ25CLGdCQUFnQjtDQUNuQjtBQUNEO0lBQ0ksV0FBVztJQUNYLGFBQWE7Q0FDaEIiLCJmaWxlIjoic3JjL2FwcC9Nb2R1bGVzL2NvcmUvc2VydmVyLWRvd24vc2VydmVyLWRvd24uY29tcG9uZW50LmNzcyIsInNvdXJjZXNDb250ZW50IjpbIi5lcnJvclxyXG57XHJcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgICBmb250LXNpemU6IDQwcHg7XHJcbn1cclxuLmZ1bGx7XHJcbiAgICB3aWR0aDoxMDAlO1xyXG4gICAgaGVpZ2h0OiAxMDAlO1xyXG59Il19 */"

/***/ }),

/***/ "./src/app/Modules/core/server-down/server-down.component.html":
/*!*********************************************************************!*\
  !*** ./src/app/Modules/core/server-down/server-down.component.html ***!
  \*********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"row\">\n    <div class=\"col-sm-12\">\n      <img src=\"../assets/layout/images/network-down.png\" class=\"img-fluid img-notauth\"/>\n    </div>\n  </div>\n  <div class=\"row margin-top-sm\">\n    <div class=\"col-sm-12 margin-top-sm\">\n        <p class=\" error\">Server is down temporarily!!</p> \n        <p class=\"text-center notauth-text\">Please try again after some time.</p>\n    </div>\n  </div>"

/***/ }),

/***/ "./src/app/Modules/core/server-down/server-down.component.ts":
/*!*******************************************************************!*\
  !*** ./src/app/Modules/core/server-down/server-down.component.ts ***!
  \*******************************************************************/
/*! exports provided: ServerDownComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ServerDownComponent", function() { return ServerDownComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var ServerDownComponent = /** @class */ (function () {
    function ServerDownComponent() {
    }
    ServerDownComponent.prototype.ngOnInit = function () {
    };
    ServerDownComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-server-down',
            template: __webpack_require__(/*! ./server-down.component.html */ "./src/app/Modules/core/server-down/server-down.component.html"),
            styles: [__webpack_require__(/*! ./server-down.component.css */ "./src/app/Modules/core/server-down/server-down.component.css")]
        }),
        __metadata("design:paramtypes", [])
    ], ServerDownComponent);
    return ServerDownComponent;
}());



/***/ }),

/***/ "./src/app/Modules/core/unauthorized/unauthorized.component.css":
/*!**********************************************************************!*\
  !*** ./src/app/Modules/core/unauthorized/unauthorized.component.css ***!
  \**********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".center {\r\n    display: block;\r\n    margin-left: auto;\r\n    margin-right: auto;\r\n    width: 50%;\r\n    }\r\n    .text-center{\r\n    margin-left: auto;\r\n    margin-right: auto;\r\n    font-family: sans-serif;\r\n    }\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvTW9kdWxlcy9jb3JlL3VuYXV0aG9yaXplZC91bmF1dGhvcml6ZWQuY29tcG9uZW50LmNzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtJQUNJLGVBQWU7SUFDZixrQkFBa0I7SUFDbEIsbUJBQW1CO0lBQ25CLFdBQVc7S0FDVjtJQUNEO0lBQ0Esa0JBQWtCO0lBQ2xCLG1CQUFtQjtJQUNuQix3QkFBd0I7S0FDdkIiLCJmaWxlIjoic3JjL2FwcC9Nb2R1bGVzL2NvcmUvdW5hdXRob3JpemVkL3VuYXV0aG9yaXplZC5jb21wb25lbnQuY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLmNlbnRlciB7XHJcbiAgICBkaXNwbGF5OiBibG9jaztcclxuICAgIG1hcmdpbi1sZWZ0OiBhdXRvO1xyXG4gICAgbWFyZ2luLXJpZ2h0OiBhdXRvO1xyXG4gICAgd2lkdGg6IDUwJTtcclxuICAgIH1cclxuICAgIC50ZXh0LWNlbnRlcntcclxuICAgIG1hcmdpbi1sZWZ0OiBhdXRvO1xyXG4gICAgbWFyZ2luLXJpZ2h0OiBhdXRvO1xyXG4gICAgZm9udC1mYW1pbHk6IHNhbnMtc2VyaWY7XHJcbiAgICB9Il19 */"

/***/ }),

/***/ "./src/app/Modules/core/unauthorized/unauthorized.component.html":
/*!***********************************************************************!*\
  !*** ./src/app/Modules/core/unauthorized/unauthorized.component.html ***!
  \***********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"row\">\n  <div class=\"col-sm-12\">\n    <img src=\"../assets/layout/images/403forbiddenerror.jpg\" class=\"img-fluid img-notauth\"/>\n  </div>\n</div>\n<div class=\"row margin-top-sm\">\n  <div class=\"col-sm-12 margin-top-sm\">\n    <p class=\"text-center notauth-text\">You are not authorized to view the requested page. You can click on your back button or the Dashboard menu to go back to the home page.\n    </p>\n  </div>\n</div>\n"

/***/ }),

/***/ "./src/app/Modules/core/unauthorized/unauthorized.component.ts":
/*!*********************************************************************!*\
  !*** ./src/app/Modules/core/unauthorized/unauthorized.component.ts ***!
  \*********************************************************************/
/*! exports provided: UnauthorizedComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UnauthorizedComponent", function() { return UnauthorizedComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var UnauthorizedComponent = /** @class */ (function () {
    function UnauthorizedComponent() {
    }
    UnauthorizedComponent.prototype.ngOnInit = function () {
    };
    UnauthorizedComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-unauthorized',
            template: __webpack_require__(/*! ./unauthorized.component.html */ "./src/app/Modules/core/unauthorized/unauthorized.component.html"),
            styles: [__webpack_require__(/*! ./unauthorized.component.css */ "./src/app/Modules/core/unauthorized/unauthorized.component.css")]
        }),
        __metadata("design:paramtypes", [])
    ], UnauthorizedComponent);
    return UnauthorizedComponent;
}());



/***/ }),

/***/ "./src/app/Modules/core/underconstruction/underconstruction.component.css":
/*!********************************************************************************!*\
  !*** ./src/app/Modules/core/underconstruction/underconstruction.component.css ***!
  \********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL01vZHVsZXMvY29yZS91bmRlcmNvbnN0cnVjdGlvbi91bmRlcmNvbnN0cnVjdGlvbi5jb21wb25lbnQuY3NzIn0= */"

/***/ }),

/***/ "./src/app/Modules/core/underconstruction/underconstruction.component.html":
/*!*********************************************************************************!*\
  !*** ./src/app/Modules/core/underconstruction/underconstruction.component.html ***!
  \*********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"row\">\n  <div class=\"col-sm-12\">\n    <img src=\"../assets/layout/images/Webpage-under-construction.jpeg\" class=\"img-fluid img-notauth\"/>\n  </div>\n</div>\n<div class=\"row margin-top-sm\">\n  <div class=\"col-sm-12 margin-top-sm\">\n    <p class=\"text-center notauth-text\">The webpage is under construction for the time being. You can click on your back button to go back to the home page.\n    </p>\n  </div>\n</div>"

/***/ }),

/***/ "./src/app/Modules/core/underconstruction/underconstruction.component.ts":
/*!*******************************************************************************!*\
  !*** ./src/app/Modules/core/underconstruction/underconstruction.component.ts ***!
  \*******************************************************************************/
/*! exports provided: UnderconstructionComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UnderconstructionComponent", function() { return UnderconstructionComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var UnderconstructionComponent = /** @class */ (function () {
    function UnderconstructionComponent() {
    }
    UnderconstructionComponent.prototype.ngOnInit = function () {
    };
    UnderconstructionComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-underconstruction',
            template: __webpack_require__(/*! ./underconstruction.component.html */ "./src/app/Modules/core/underconstruction/underconstruction.component.html"),
            styles: [__webpack_require__(/*! ./underconstruction.component.css */ "./src/app/Modules/core/underconstruction/underconstruction.component.css")]
        }),
        __metadata("design:paramtypes", [])
    ], UnderconstructionComponent);
    return UnderconstructionComponent;
}());



/***/ })

}]);
//# sourceMappingURL=Modules-core-core-module.js.map