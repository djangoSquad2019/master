(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["main"],{

/***/ "./src/$$_lazy_route_resource lazy recursive":
/*!**********************************************************!*\
  !*** ./src/$$_lazy_route_resource lazy namespace object ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var map = {
	"./Modules/FetaureModules/admin/admin.module": [
		"./src/app/Modules/FetaureModules/admin/admin.module.ts",
		"Modules-FetaureModules-admin-admin-module"
	],
	"./Modules/FetaureModules/capacity-insights/capacity-insights.module": [
		"./src/app/Modules/FetaureModules/capacity-insights/capacity-insights.module.ts",
		"Modules-FetaureModules-capacity-insights-capacity-insights-module"
	],
	"./Modules/core/core.module": [
		"./src/app/Modules/core/core.module.ts",
		"Modules-core-core-module"
	]
};
function webpackAsyncContext(req) {
	var ids = map[req];
	if(!ids) {
		return Promise.resolve().then(function() {
			var e = new Error("Cannot find module '" + req + "'");
			e.code = 'MODULE_NOT_FOUND';
			throw e;
		});
	}
	return __webpack_require__.e(ids[1]).then(function() {
		var id = ids[0];
		return __webpack_require__(id);
	});
}
webpackAsyncContext.keys = function webpackAsyncContextKeys() {
	return Object.keys(map);
};
webpackAsyncContext.id = "./src/$$_lazy_route_resource lazy recursive";
module.exports = webpackAsyncContext;

/***/ }),

/***/ "./src/app/Models/RoleModel.ts":
/*!*************************************!*\
  !*** ./src/app/Models/RoleModel.ts ***!
  \*************************************/
/*! exports provided: Role */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Role", function() { return Role; });
/* Model to store user role */
var Role = /** @class */ (function () {
    function Role() {
    }
    return Role;
}());



/***/ }),

/***/ "./src/app/Modules/FetaureModules/admin/Services/admin-service.service.ts":
/*!********************************************************************************!*\
  !*** ./src/app/Modules/FetaureModules/admin/Services/admin-service.service.ts ***!
  \********************************************************************************/
/*! exports provided: AdminServiceService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AdminServiceService", function() { return AdminServiceService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var rxjs_add_operator_map__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! rxjs/add/operator/map */ "./node_modules/rxjs-compat/_esm5/add/operator/map.js");
/* harmony import */ var rxjs_add_operator_catch__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! rxjs/add/operator/catch */ "./node_modules/rxjs-compat/_esm5/add/operator/catch.js");
/* harmony import */ var rxjs_add_observable_throw__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! rxjs/add/observable/throw */ "./node_modules/rxjs-compat/_esm5/add/observable/throw.js");
/* harmony import */ var rxjs_Rx__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! rxjs/Rx */ "./node_modules/rxjs-compat/_esm5/Rx.js");
/* harmony import */ var src_app_Models_RoleModel__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! src/app/Models/RoleModel */ "./src/app/Models/RoleModel.ts");
/* harmony import */ var src_environments_environment__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! src/environments/environment */ "./src/environments/environment.ts");
/* harmony import */ var _Services_sharedservice_service__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../../../../Services/sharedservice.service */ "./src/app/Services/sharedservice.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





// tslint:disable-next-line:import-blacklist




var AdminServiceService = /** @class */ (function () {
    function AdminServiceService(http, sharedData) {
        this.http = http;
        this.sharedData = sharedData;
        // Get base URL based on the environment of the run
        this.baseURL = src_environments_environment__WEBPACK_IMPORTED_MODULE_7__["environment"].baseUrl;
    }
    // function to view all admins.
    AdminServiceService.prototype.getUsers = function () {
        var token = this.sharedData.tokenIDOfLoggedUser;
        // tslint:disable-next-line:max-line-length
        var header = new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpHeaders"]()
            .set('Content-Type', 'application/json')
            .set('Access-Control-Allow-Origin', '*')
            .set('Authorization', 'Bearer ' + token);
        return this.http.get(this.baseURL + '/api/Role/GetAllAdmins', {
            headers: header
        });
    };
    // Funtion to get the roleID after getting the mid of logged in user
    // getRole(Mid: string): Observable<any> {
    //     const token = this.sharedData.tokenIDOfLoggedUser;
    //     const encrypted = this.sharedData.encryptingData(Mid);
    //     const header: HttpHeaders = new HttpHeaders()
    //     .set('Authorization', 'Bearer ' + token)
    //     .set('Access-Control-Allow-Origin', '*')
    //     .set('Strict-Transport-Security', 'max-age=31536000; includeSubDomains; preload')
    //     .set('X-Frame-Options', 'DENY');
    //     return this.http.get(
    //         this.baseURL + '/api/Role/GetUserRole?EncryptedMID=' +  btoa(encrypted),
    //         { headers: header }
    //     );
    // }
    // Function to get the details of user after getting mid from textbox
    AdminServiceService.prototype.getUserDetails = function (midOfUser) {
        var header = new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpHeaders"]()
            .set('Strict-Transport-Security', 'max-age=31536000; includeSubDomains; preload')
            .set('Access-Control-Allow-Origin', '*')
            .set('X-Frame-Options', 'DENY');
        var encrypted = this.sharedData.encryptingData(midOfUser);
        return this.http.get(this.baseURL +
            '/api/MicrosoftGraphAPI/GetUserDetails?EncryptedMID=' +
            btoa(encrypted));
    };
    // Function to create add a Mindtree mind as admin
    AdminServiceService.prototype.saveRole = function (value1, value2) {
        var obj = new src_app_Models_RoleModel__WEBPACK_IMPORTED_MODULE_6__["Role"]();
        var encryptedMid = this.sharedData.encryptingData(this.sharedData.midOfLoggedInUser);
        var encryptedMIDOfNewAdmin = this.sharedData.encryptingData(value1);
        var encryptedRoleOfNewAdmin = this.sharedData.encryptingData(value2);
        obj.MID = encryptedMIDOfNewAdmin;
        obj.Role = encryptedRoleOfNewAdmin;
        obj.MidOfLoggedInUser = encryptedMid;
        var jsonObj = JSON.stringify(obj);
        var token = this.sharedData.tokenIDOfLoggedUser;
        // tslint:disable-next-line:max-line-length
        var header = new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpHeaders"]()
            .set('Content-Type', 'application/json')
            .set('Access-Control-Allow-Origin', '*')
            .set('Authorization', 'Bearer ' + token);
        return this.http.post(this.baseURL + '/api/Role/CreateRole', jsonObj, { headers: header });
    };
    AdminServiceService.prototype.DeleteRole = function (user) {
        var jsonObj = JSON.stringify(user);
        var token = this.sharedData.tokenIDOfLoggedUser;
        // tslint:disable-next-line:max-line-length
        var header = new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpHeaders"]()
            .set('Content-Type', 'application/json')
            .set('Access-Control-Allow-Origin', '*')
            .set('Authorization', 'Bearer ' + token);
        return this.http.post(this.baseURL + '/api/Role/DeleteAdmin', jsonObj, { headers: header });
    };
    AdminServiceService = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])({
            providedIn: 'root'
        }),
        __metadata("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"],
            _Services_sharedservice_service__WEBPACK_IMPORTED_MODULE_8__["SharedserviceService"]])
    ], AdminServiceService);
    return AdminServiceService;
}());



/***/ }),

/***/ "./src/app/Modules/FetaureModules/dash-board/dash-board/dashboard.component.html":
/*!***************************************************************************************!*\
  !*** ./src/app/Modules/FetaureModules/dash-board/dash-board/dashboard.component.html ***!
  \***************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<p class=\"divHeader\">\n    <i class=\"fa fa-bar-chartfa fa-table PaddingRight5\" aria-hidden=\"true\"></i>\n    DashBoard\n</p>\n<!-- <div class=\"spinner-center\" *ngIf=\"DisplayBoards\">\n    <i class=\"fa fa-spinner fa-spin fa-3x\"></i>\n</div> -->\n<div class=\"spinner-center\" *ngIf=\"DisplayBoards\">\n    <p>\n        Please wait while the data is loading ...\n        <span class=\"fa fa-spinner fa-spin fa-2x\"></span>\n    </p>\n</div>\n<div class=\"ui-g dashboard\" *ngIf=\"!DisplayBoards\">\n    <div class=\"ui-g-12 ui-md-6 ui-lg-3 \" style=\"height:140px\">\n        <div class=\"ui-g card overview-box overview-box-1 DashboardCard1\">\n            <div class=\"ui-g-4 overview-box-icon\">\n                <img src=\"assets/layout/images/icon-paper.png\" />\n            </div>\n            <div class=\"ui-g-8\">\n                <span class=\"overview-box-count\">{{ MindtreemindsCount }}</span>\n                <span class=\"overview-box-name\">Total Minds</span>\n            </div>\n        </div>\n    </div>\n    <div class=\"ui-g-12 ui-md-6 ui-lg-3 \" style=\"height:140px\">\n        <div class=\"ui-g card overview-box2 overview-box-2 DashboardCard2\">\n            <div class=\"ui-g-4 overview-box-icon\">\n                <img src=\"assets/layout/images/icon-mail.png\" />\n            </div>\n            <div class=\"ui-g-8\">\n                <span class=\"overview-box-count\">{{ UnBilledMindsCount }}</span>\n                <span class=\"overview-box-name\">Unbilled Minds</span>\n            </div>\n        </div>\n    </div>\n    <div class=\"ui-g-12 ui-md-6 ui-lg-3 \" style=\"height:140px\">\n        <div class=\"ui-g card overview-box3 overview-box-3 DashboardCard3\">\n            <div class=\"ui-g-4 overview-box-icon \">\n                <img src=\"assets/layout/images/icon-location.png\" />\n            </div>\n            <div class=\"ui-g-8\">\n                <span class=\"overview-box-count\">{{ DeployableCount }}</span>\n                <span class=\"overview-box-name\">Deployable Minds</span>\n            </div>\n        </div>\n    </div>\n    <div class=\"ui-g-12 ui-md-6 ui-lg-3\" style=\"height:140px\">\n        <div class=\"ui-g card overview-box4 overview-box-4  DashboardCard4\">\n            <div class=\"ui-g-4 overview-box-icon\">\n                <img src=\"assets/layout/images/icon-orders.png\" />\n            </div>\n            <div class=\"ui-g-8\">\n                <span class=\"overview-box-count\">{{ BilledMindsCount }}</span>\n                <span class=\"overview-box-name\">Billed Minds</span>\n            </div>\n        </div>\n    </div>\n\n    <!-- <div class=\"ui-g-12 ui-md-6 ui-lg-4 task-list\">\n        <p-panel header=\"TASKS\" [style]=\"{minHeight: '360px'}\">\n          \n        </p-panel>\n    </div>\n    <div class=\"ui-g-12 ui-md-6 ui-lg-4 ui-fluid contact-form\">\n        <p-panel header=\"CONTACT US\" [style]=\"{minHeight: '360px'}\">\n           \n        </p-panel>\n    </div>\n    <div class=\"ui-g-12 ui-lg-4 contacts\">\n        <p-panel header=\"CONTACTS\" [style]=\"{minHeight: '360px'}\">\n            \n        </p-panel>\n    </div> -->\n\n    <div class=\"ui-g-12 ui-md-6 ui-lg-4\">\n        <div class=\"ui-g card \">\n            <p class=\"headerStyle\">Availability of Minds onshore/offshore</p>\n            <div class=\"spinner-center loaderStyle\" *ngIf=\"DisplayPieChartOnOffShore\">\n                <i class=\"fa fa-spinner fa-spin fa-3x\"></i>\n            </div>\n            <p-chart type=\"pie\" [data]=\"pieChartDataCompetency\"></p-chart>\n        </div>\n    </div>\n\n    <div class=\"ui-g-12 ui-md-6 ui-lg-4\">\n        <div class=\"ui-g card  \">\n            <p class=\"headerStyle\">Male and female minds ratio</p>\n            <div class=\"spinner-center loaderStyle\" *ngIf=\"DisplayPieChartGender\">\n                <i class=\"fa fa-spinner fa-spin fa-3x\"></i>\n            </div>\n            <p-chart type=\"pie\" [data]=\"pieChartMaleFemale\"></p-chart>\n        </div>\n    </div>\n\n    <div class=\"ui-g-12 ui-md-6 ui-lg-4\">\n        <div class=\"ui-g card \">\n            <p class=\"headerStyle\">Engagement type</p>\n            <div class=\"spinner-center loaderStyle\" *ngIf=\"DisplayPieChartEngagement\">\n                <i class=\"fa fa-spinner fa-spin fa-3x\"></i>\n            </div>\n            <p-chart type=\"pie\" [data]=\"pieChartEngagementType\"></p-chart>\n        </div>\n    </div>\n    <!-- <div class=\"ui-g-6 ui-md-6 ui-lg-12\">\n         <div class=\"ui-g card \">\n              <p-chart type=\"bar\" [data]=\"data\"></p-chart> \n          </div> -->\n</div>\n"

/***/ }),

/***/ "./src/app/Modules/FetaureModules/dash-board/dash-board/dashboarddemo.component.ts":
/*!*****************************************************************************************!*\
  !*** ./src/app/Modules/FetaureModules/dash-board/dash-board/dashboarddemo.component.ts ***!
  \*****************************************************************************************/
/*! exports provided: DashboardDemoComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DashboardDemoComponent", function() { return DashboardDemoComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _Services_httprequest_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../Services/httprequest.service */ "./src/app/Services/httprequest.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var DashboardDemoComponent = /** @class */ (function () {
    function DashboardDemoComponent(resp, router) {
        this.resp = resp;
        this.router = router;
        this.DisplayPieChartOnOffShore = true;
        this.DisplayPieChartGender = true;
        this.DisplayPieChartEngagement = true;
        this.DisplayBoards = true;
    }
    // Page load function
    DashboardDemoComponent.prototype.ngOnInit = function () {
        var _this = this;
        // API call to get total employee count
        this.resp.getAPI('/api/DashBoard/GetMindtreemindsCount').subscribe(function (resp) {
            _this.DisplayBoards = false;
            _this.MindtreemindsCount = resp;
        }, function (error) {
            if (error.status === 0) {
                _this.router.navigate(['/core/ServerDown']);
            }
            else if (error.status === 401) {
                _this.router.navigate(['/core/AccessDenied']);
            }
            else {
                _this.router.navigate(['/core/InternalServerError']);
            }
        });
        // API call to get total blocked employee count
        this.resp.getAPI('/api/DashBoard/GetUnBilledMindsCount').subscribe(function (resp) {
            _this.DisplayBoards = false;
            _this.UnBilledMindsCount = resp;
        }, function (error) {
            if (error.status === 0) {
                _this.router.navigate(['/core/ServerDown']);
            }
            else if (error.status === 401) {
                _this.router.navigate(['/core/AccessDenied']);
            }
            else {
                _this.router.navigate(['/core/InternalServerError']);
            }
        });
        // API call to get total deployable employee count
        this.resp.getAPI('/api/DashBoard/GetDeployableCount').subscribe(function (resp) {
            _this.DisplayBoards = false;
            _this.DeployableCount = resp;
        }, function (error) {
            if (error.status === 0) {
                _this.router.navigate(['/core/ServerDown']);
            }
            else if (error.status === 401) {
                _this.router.navigate(['/core/AccessDenied']);
            }
            else {
                _this.router.navigate(['/core/InternalServerError']);
            }
        });
        // API call to get total Competency building employee count
        this.resp.getAPI('/api/DashBoard/GetBilledMindsCount').subscribe(function (resp) {
            _this.DisplayBoards = false;
            _this.BilledMindsCount = resp;
        }, function (error) {
            if (error.status === 0) {
                _this.router.navigate(['/core/ServerDown']);
            }
            else if (error.status === 401) {
                _this.router.navigate(['/core/AccessDenied']);
            }
            else {
                _this.router.navigate(['/core/InternalServerError']);
            }
        });
        // API call to get total Onsite Offshore ratio
        this.resp.getAPI('/api/DashBoard/GetOnOffShoreCount').subscribe(function (data) {
            // this.DisplayPieChart = false;
            _this.assignValue(data);
        }, function (error) {
            if (error.status === 0) {
                _this.router.navigate(['/core/ServerDown']);
            }
            else if (error.status === 401) {
                _this.router.navigate(['/core/AccessDenied']);
            }
            else {
                _this.router.navigate(['/core/InternalServerError']);
            }
        });
        this.resp.getAPI('/api/DashBoard/GetMaleFemaleCount').subscribe(function (data) {
            // this.DisplayPieChart = false;
            _this.assignMaleFemaleValue(data);
        }, function (error) {
            if (error.status === 0) {
                _this.router.navigate(['/core/ServerDown']);
            }
            else if (error.status === 401) {
                _this.router.navigate(['/core/AccessDenied']);
            }
            else {
                _this.router.navigate(['/core/InternalServerError']);
            }
        });
        this.resp.getAPI('/api/DashBoard/GetEngagementTypeCount').subscribe(function (data) {
            // this.DisplayPieChart = false;
            _this.assignEngagementTypeValue(data);
        }, function (error) {
            if (error.status === 0) {
                _this.router.navigate(['/core/ServerDown']);
            }
            else if (error.status === 401) {
                _this.router.navigate(['/core/AccessDenied']);
            }
            else {
                _this.router.navigate(['/core/InternalServerError']);
            }
        });
    };
    // Pie chart to represent onsite offshore employee ratio
    DashboardDemoComponent.prototype.assignValue = function (data) {
        this.onsite = data.onsite;
        this.offshore = data.offshore;
        this.not_listed = data.not_listed;
        // hardcoded value for demo
        this.DisplayPieChartOnOffShore = false;
        this.pieChartDataCompetency = {
            labels: ['Offshore', 'Onsite', 'Not listed'],
            datasets: [
                {
                    data: [this.offshore, this.onsite, this.not_listed],
                    backgroundColor: ['#FF6384', '#36A2EB', '#FFCE56'],
                    hoverBackgroundColor: ['#FF6384', '#36A2EB', '#FFCE56']
                }
            ]
        };
    };
    DashboardDemoComponent.prototype.assignMaleFemaleValue = function (data) {
        this.male = data.male;
        this.female = data.female;
        // hardcoded value for demo
        this.DisplayPieChartGender = false;
        this.pieChartMaleFemale = {
            labels: ['Male', 'Female'],
            datasets: [
                {
                    data: [this.male, this.female],
                    backgroundColor: [
                        '#FF6384',
                        '#36A2EB'
                        // "#FFCE56",
                    ],
                    hoverBackgroundColor: [
                        '#FF6384',
                        '#36A2EB'
                        // "#FFCE56",
                    ]
                }
            ]
        };
    };
    DashboardDemoComponent.prototype.assignEngagementTypeValue = function (data) {
        this.notListed = data.notListed;
        this.HYB = data.hyb;
        this.TNM = data.tnm;
        this.FPC = data.fpc;
        this.FMC = data.fmc;
        // hardcoded value for demo
        this.DisplayPieChartEngagement = false;
        this.pieChartEngagementType = {
            labels: ['Not Listed', 'HYB', 'TNM', 'FPC', 'FMC'],
            datasets: [
                {
                    data: [
                        this.notListed,
                        this.HYB,
                        this.TNM,
                        this.FPC,
                        this.FMC
                    ],
                    backgroundColor: [
                        '#FF6384',
                        '#36A2EB',
                        '#FFCE56',
                        '#1E77A3',
                        '#E01D32'
                    ],
                    hoverBackgroundColor: [
                        '#FF6384',
                        '#36A2EB',
                        '#FFCE56',
                        '#1E77A3',
                        '#E01D32'
                    ]
                }
            ]
        };
    };
    DashboardDemoComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            template: __webpack_require__(/*! ./dashboard.component.html */ "./src/app/Modules/FetaureModules/dash-board/dash-board/dashboard.component.html")
        }),
        __metadata("design:paramtypes", [_Services_httprequest_service__WEBPACK_IMPORTED_MODULE_2__["HTTPRequestService"], _angular_router__WEBPACK_IMPORTED_MODULE_1__["Router"]])
    ], DashboardDemoComponent);
    return DashboardDemoComponent;
}());



/***/ }),

/***/ "./src/app/Modules/shared/GenericComponents/app-footer/app.footer.component.html":
/*!***************************************************************************************!*\
  !*** ./src/app/Modules/shared/GenericComponents/app-footer/app.footer.component.html ***!
  \***************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<footer class=\"margin-top-sm\">\n    <span class=\"footer-text\">&copy; 2018 - Capacity Insights</span>\n    <ul class=\"footer-block\">\n        <li>Imprint</li>\n        <li>Legal Disclaimer</li>\n        <li>Privacy Policy</li>\n        <li>Cookies</li>\n    </ul>\n</footer>\n"

/***/ }),

/***/ "./src/app/Modules/shared/GenericComponents/app-footer/app.footer.component.ts":
/*!*************************************************************************************!*\
  !*** ./src/app/Modules/shared/GenericComponents/app-footer/app.footer.component.ts ***!
  \*************************************************************************************/
/*! exports provided: AppFooterComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppFooterComponent", function() { return AppFooterComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

var AppFooterComponent = /** @class */ (function () {
    function AppFooterComponent() {
    }
    AppFooterComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-footer',
            template: __webpack_require__(/*! ./app.footer.component.html */ "./src/app/Modules/shared/GenericComponents/app-footer/app.footer.component.html")
        })
    ], AppFooterComponent);
    return AppFooterComponent;
}());



/***/ }),

/***/ "./src/app/Modules/shared/shared-routing.module.ts":
/*!*********************************************************!*\
  !*** ./src/app/Modules/shared/shared-routing.module.ts ***!
  \*********************************************************/
/*! exports provided: SharedRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SharedRoutingModule", function() { return SharedRoutingModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};


var routes = [];
var SharedRoutingModule = /** @class */ (function () {
    function SharedRoutingModule() {
    }
    SharedRoutingModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"].forChild(routes)],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"]]
        })
    ], SharedRoutingModule);
    return SharedRoutingModule;
}());



/***/ }),

/***/ "./src/app/Modules/shared/shared.module.ts":
/*!*************************************************!*\
  !*** ./src/app/Modules/shared/shared.module.ts ***!
  \*************************************************/
/*! exports provided: SharedModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SharedModule", function() { return SharedModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var primeng_accordion__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! primeng/accordion */ "./node_modules/primeng/accordion.js");
/* harmony import */ var primeng_accordion__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(primeng_accordion__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _shared_routing_module__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./shared-routing.module */ "./src/app/Modules/shared/shared-routing.module.ts");
/* harmony import */ var primeng_splitbutton__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! primeng/splitbutton */ "./node_modules/primeng/splitbutton.js");
/* harmony import */ var primeng_splitbutton__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(primeng_splitbutton__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var _angular_material_slide_toggle__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/material/slide-toggle */ "./node_modules/@angular/material/esm5/slide-toggle.es5.js");
/* harmony import */ var _angular_material_expansion__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/material/expansion */ "./node_modules/@angular/material/esm5/expansion.es5.js");
/* harmony import */ var primeng_autocomplete__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! primeng/autocomplete */ "./node_modules/primeng/autocomplete.js");
/* harmony import */ var primeng_autocomplete__WEBPACK_IMPORTED_MODULE_7___default = /*#__PURE__*/__webpack_require__.n(primeng_autocomplete__WEBPACK_IMPORTED_MODULE_7__);
/* harmony import */ var primeng_breadcrumb__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! primeng/breadcrumb */ "./node_modules/primeng/breadcrumb.js");
/* harmony import */ var primeng_breadcrumb__WEBPACK_IMPORTED_MODULE_8___default = /*#__PURE__*/__webpack_require__.n(primeng_breadcrumb__WEBPACK_IMPORTED_MODULE_8__);
/* harmony import */ var primeng_button__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! primeng/button */ "./node_modules/primeng/button.js");
/* harmony import */ var primeng_button__WEBPACK_IMPORTED_MODULE_9___default = /*#__PURE__*/__webpack_require__.n(primeng_button__WEBPACK_IMPORTED_MODULE_9__);
/* harmony import */ var primeng_calendar__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! primeng/calendar */ "./node_modules/primeng/calendar.js");
/* harmony import */ var primeng_calendar__WEBPACK_IMPORTED_MODULE_10___default = /*#__PURE__*/__webpack_require__.n(primeng_calendar__WEBPACK_IMPORTED_MODULE_10__);
/* harmony import */ var primeng_card__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! primeng/card */ "./node_modules/primeng/card.js");
/* harmony import */ var primeng_card__WEBPACK_IMPORTED_MODULE_11___default = /*#__PURE__*/__webpack_require__.n(primeng_card__WEBPACK_IMPORTED_MODULE_11__);
/* harmony import */ var primeng_carousel__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! primeng/carousel */ "./node_modules/primeng/carousel.js");
/* harmony import */ var primeng_carousel__WEBPACK_IMPORTED_MODULE_12___default = /*#__PURE__*/__webpack_require__.n(primeng_carousel__WEBPACK_IMPORTED_MODULE_12__);
/* harmony import */ var primeng_chart__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! primeng/chart */ "./node_modules/primeng/chart.js");
/* harmony import */ var primeng_chart__WEBPACK_IMPORTED_MODULE_13___default = /*#__PURE__*/__webpack_require__.n(primeng_chart__WEBPACK_IMPORTED_MODULE_13__);
/* harmony import */ var primeng_checkbox__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! primeng/checkbox */ "./node_modules/primeng/checkbox.js");
/* harmony import */ var primeng_checkbox__WEBPACK_IMPORTED_MODULE_14___default = /*#__PURE__*/__webpack_require__.n(primeng_checkbox__WEBPACK_IMPORTED_MODULE_14__);
/* harmony import */ var primeng_chips__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! primeng/chips */ "./node_modules/primeng/chips.js");
/* harmony import */ var primeng_chips__WEBPACK_IMPORTED_MODULE_15___default = /*#__PURE__*/__webpack_require__.n(primeng_chips__WEBPACK_IMPORTED_MODULE_15__);
/* harmony import */ var primeng_codehighlighter__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! primeng/codehighlighter */ "./node_modules/primeng/codehighlighter.js");
/* harmony import */ var primeng_codehighlighter__WEBPACK_IMPORTED_MODULE_16___default = /*#__PURE__*/__webpack_require__.n(primeng_codehighlighter__WEBPACK_IMPORTED_MODULE_16__);
/* harmony import */ var primeng_confirmdialog__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(/*! primeng/confirmdialog */ "./node_modules/primeng/confirmdialog.js");
/* harmony import */ var primeng_confirmdialog__WEBPACK_IMPORTED_MODULE_17___default = /*#__PURE__*/__webpack_require__.n(primeng_confirmdialog__WEBPACK_IMPORTED_MODULE_17__);
/* harmony import */ var primeng_colorpicker__WEBPACK_IMPORTED_MODULE_18__ = __webpack_require__(/*! primeng/colorpicker */ "./node_modules/primeng/colorpicker.js");
/* harmony import */ var primeng_colorpicker__WEBPACK_IMPORTED_MODULE_18___default = /*#__PURE__*/__webpack_require__.n(primeng_colorpicker__WEBPACK_IMPORTED_MODULE_18__);
/* harmony import */ var primeng_contextmenu__WEBPACK_IMPORTED_MODULE_19__ = __webpack_require__(/*! primeng/contextmenu */ "./node_modules/primeng/contextmenu.js");
/* harmony import */ var primeng_contextmenu__WEBPACK_IMPORTED_MODULE_19___default = /*#__PURE__*/__webpack_require__.n(primeng_contextmenu__WEBPACK_IMPORTED_MODULE_19__);
/* harmony import */ var primeng_dataview__WEBPACK_IMPORTED_MODULE_20__ = __webpack_require__(/*! primeng/dataview */ "./node_modules/primeng/dataview.js");
/* harmony import */ var primeng_dataview__WEBPACK_IMPORTED_MODULE_20___default = /*#__PURE__*/__webpack_require__.n(primeng_dataview__WEBPACK_IMPORTED_MODULE_20__);
/* harmony import */ var primeng_dialog__WEBPACK_IMPORTED_MODULE_21__ = __webpack_require__(/*! primeng/dialog */ "./node_modules/primeng/dialog.js");
/* harmony import */ var primeng_dialog__WEBPACK_IMPORTED_MODULE_21___default = /*#__PURE__*/__webpack_require__.n(primeng_dialog__WEBPACK_IMPORTED_MODULE_21__);
/* harmony import */ var primeng_dropdown__WEBPACK_IMPORTED_MODULE_22__ = __webpack_require__(/*! primeng/dropdown */ "./node_modules/primeng/dropdown.js");
/* harmony import */ var primeng_dropdown__WEBPACK_IMPORTED_MODULE_22___default = /*#__PURE__*/__webpack_require__.n(primeng_dropdown__WEBPACK_IMPORTED_MODULE_22__);
/* harmony import */ var primeng_editor__WEBPACK_IMPORTED_MODULE_23__ = __webpack_require__(/*! primeng/editor */ "./node_modules/primeng/editor.js");
/* harmony import */ var primeng_editor__WEBPACK_IMPORTED_MODULE_23___default = /*#__PURE__*/__webpack_require__.n(primeng_editor__WEBPACK_IMPORTED_MODULE_23__);
/* harmony import */ var primeng_fieldset__WEBPACK_IMPORTED_MODULE_24__ = __webpack_require__(/*! primeng/fieldset */ "./node_modules/primeng/fieldset.js");
/* harmony import */ var primeng_fieldset__WEBPACK_IMPORTED_MODULE_24___default = /*#__PURE__*/__webpack_require__.n(primeng_fieldset__WEBPACK_IMPORTED_MODULE_24__);
/* harmony import */ var primeng_fileupload__WEBPACK_IMPORTED_MODULE_25__ = __webpack_require__(/*! primeng/fileupload */ "./node_modules/primeng/fileupload.js");
/* harmony import */ var primeng_fileupload__WEBPACK_IMPORTED_MODULE_25___default = /*#__PURE__*/__webpack_require__.n(primeng_fileupload__WEBPACK_IMPORTED_MODULE_25__);
/* harmony import */ var primeng_galleria__WEBPACK_IMPORTED_MODULE_26__ = __webpack_require__(/*! primeng/galleria */ "./node_modules/primeng/galleria.js");
/* harmony import */ var primeng_galleria__WEBPACK_IMPORTED_MODULE_26___default = /*#__PURE__*/__webpack_require__.n(primeng_galleria__WEBPACK_IMPORTED_MODULE_26__);
/* harmony import */ var primeng_growl__WEBPACK_IMPORTED_MODULE_27__ = __webpack_require__(/*! primeng/growl */ "./node_modules/primeng/growl.js");
/* harmony import */ var primeng_growl__WEBPACK_IMPORTED_MODULE_27___default = /*#__PURE__*/__webpack_require__.n(primeng_growl__WEBPACK_IMPORTED_MODULE_27__);
/* harmony import */ var primeng_inplace__WEBPACK_IMPORTED_MODULE_28__ = __webpack_require__(/*! primeng/inplace */ "./node_modules/primeng/inplace.js");
/* harmony import */ var primeng_inplace__WEBPACK_IMPORTED_MODULE_28___default = /*#__PURE__*/__webpack_require__.n(primeng_inplace__WEBPACK_IMPORTED_MODULE_28__);
/* harmony import */ var primeng_inputmask__WEBPACK_IMPORTED_MODULE_29__ = __webpack_require__(/*! primeng/inputmask */ "./node_modules/primeng/inputmask.js");
/* harmony import */ var primeng_inputmask__WEBPACK_IMPORTED_MODULE_29___default = /*#__PURE__*/__webpack_require__.n(primeng_inputmask__WEBPACK_IMPORTED_MODULE_29__);
/* harmony import */ var primeng_inputswitch__WEBPACK_IMPORTED_MODULE_30__ = __webpack_require__(/*! primeng/inputswitch */ "./node_modules/primeng/inputswitch.js");
/* harmony import */ var primeng_inputswitch__WEBPACK_IMPORTED_MODULE_30___default = /*#__PURE__*/__webpack_require__.n(primeng_inputswitch__WEBPACK_IMPORTED_MODULE_30__);
/* harmony import */ var primeng_inputtext__WEBPACK_IMPORTED_MODULE_31__ = __webpack_require__(/*! primeng/inputtext */ "./node_modules/primeng/inputtext.js");
/* harmony import */ var primeng_inputtext__WEBPACK_IMPORTED_MODULE_31___default = /*#__PURE__*/__webpack_require__.n(primeng_inputtext__WEBPACK_IMPORTED_MODULE_31__);
/* harmony import */ var primeng_inputtextarea__WEBPACK_IMPORTED_MODULE_32__ = __webpack_require__(/*! primeng/inputtextarea */ "./node_modules/primeng/inputtextarea.js");
/* harmony import */ var primeng_inputtextarea__WEBPACK_IMPORTED_MODULE_32___default = /*#__PURE__*/__webpack_require__.n(primeng_inputtextarea__WEBPACK_IMPORTED_MODULE_32__);
/* harmony import */ var primeng_lightbox__WEBPACK_IMPORTED_MODULE_33__ = __webpack_require__(/*! primeng/lightbox */ "./node_modules/primeng/lightbox.js");
/* harmony import */ var primeng_lightbox__WEBPACK_IMPORTED_MODULE_33___default = /*#__PURE__*/__webpack_require__.n(primeng_lightbox__WEBPACK_IMPORTED_MODULE_33__);
/* harmony import */ var primeng_listbox__WEBPACK_IMPORTED_MODULE_34__ = __webpack_require__(/*! primeng/listbox */ "./node_modules/primeng/listbox.js");
/* harmony import */ var primeng_listbox__WEBPACK_IMPORTED_MODULE_34___default = /*#__PURE__*/__webpack_require__.n(primeng_listbox__WEBPACK_IMPORTED_MODULE_34__);
/* harmony import */ var primeng_megamenu__WEBPACK_IMPORTED_MODULE_35__ = __webpack_require__(/*! primeng/megamenu */ "./node_modules/primeng/megamenu.js");
/* harmony import */ var primeng_megamenu__WEBPACK_IMPORTED_MODULE_35___default = /*#__PURE__*/__webpack_require__.n(primeng_megamenu__WEBPACK_IMPORTED_MODULE_35__);
/* harmony import */ var primeng_menu__WEBPACK_IMPORTED_MODULE_36__ = __webpack_require__(/*! primeng/menu */ "./node_modules/primeng/menu.js");
/* harmony import */ var primeng_menu__WEBPACK_IMPORTED_MODULE_36___default = /*#__PURE__*/__webpack_require__.n(primeng_menu__WEBPACK_IMPORTED_MODULE_36__);
/* harmony import */ var primeng_menubar__WEBPACK_IMPORTED_MODULE_37__ = __webpack_require__(/*! primeng/menubar */ "./node_modules/primeng/menubar.js");
/* harmony import */ var primeng_menubar__WEBPACK_IMPORTED_MODULE_37___default = /*#__PURE__*/__webpack_require__.n(primeng_menubar__WEBPACK_IMPORTED_MODULE_37__);
/* harmony import */ var primeng_messages__WEBPACK_IMPORTED_MODULE_38__ = __webpack_require__(/*! primeng/messages */ "./node_modules/primeng/messages.js");
/* harmony import */ var primeng_messages__WEBPACK_IMPORTED_MODULE_38___default = /*#__PURE__*/__webpack_require__.n(primeng_messages__WEBPACK_IMPORTED_MODULE_38__);
/* harmony import */ var primeng_message__WEBPACK_IMPORTED_MODULE_39__ = __webpack_require__(/*! primeng/message */ "./node_modules/primeng/message.js");
/* harmony import */ var primeng_message__WEBPACK_IMPORTED_MODULE_39___default = /*#__PURE__*/__webpack_require__.n(primeng_message__WEBPACK_IMPORTED_MODULE_39__);
/* harmony import */ var primeng_multiselect__WEBPACK_IMPORTED_MODULE_40__ = __webpack_require__(/*! primeng/multiselect */ "./node_modules/primeng/multiselect.js");
/* harmony import */ var primeng_multiselect__WEBPACK_IMPORTED_MODULE_40___default = /*#__PURE__*/__webpack_require__.n(primeng_multiselect__WEBPACK_IMPORTED_MODULE_40__);
/* harmony import */ var primeng_orderlist__WEBPACK_IMPORTED_MODULE_41__ = __webpack_require__(/*! primeng/orderlist */ "./node_modules/primeng/orderlist.js");
/* harmony import */ var primeng_orderlist__WEBPACK_IMPORTED_MODULE_41___default = /*#__PURE__*/__webpack_require__.n(primeng_orderlist__WEBPACK_IMPORTED_MODULE_41__);
/* harmony import */ var primeng_organizationchart__WEBPACK_IMPORTED_MODULE_42__ = __webpack_require__(/*! primeng/organizationchart */ "./node_modules/primeng/organizationchart.js");
/* harmony import */ var primeng_organizationchart__WEBPACK_IMPORTED_MODULE_42___default = /*#__PURE__*/__webpack_require__.n(primeng_organizationchart__WEBPACK_IMPORTED_MODULE_42__);
/* harmony import */ var primeng_overlaypanel__WEBPACK_IMPORTED_MODULE_43__ = __webpack_require__(/*! primeng/overlaypanel */ "./node_modules/primeng/overlaypanel.js");
/* harmony import */ var primeng_overlaypanel__WEBPACK_IMPORTED_MODULE_43___default = /*#__PURE__*/__webpack_require__.n(primeng_overlaypanel__WEBPACK_IMPORTED_MODULE_43__);
/* harmony import */ var primeng_paginator__WEBPACK_IMPORTED_MODULE_44__ = __webpack_require__(/*! primeng/paginator */ "./node_modules/primeng/paginator.js");
/* harmony import */ var primeng_paginator__WEBPACK_IMPORTED_MODULE_44___default = /*#__PURE__*/__webpack_require__.n(primeng_paginator__WEBPACK_IMPORTED_MODULE_44__);
/* harmony import */ var primeng_panel__WEBPACK_IMPORTED_MODULE_45__ = __webpack_require__(/*! primeng/panel */ "./node_modules/primeng/panel.js");
/* harmony import */ var primeng_panel__WEBPACK_IMPORTED_MODULE_45___default = /*#__PURE__*/__webpack_require__.n(primeng_panel__WEBPACK_IMPORTED_MODULE_45__);
/* harmony import */ var primeng_panelmenu__WEBPACK_IMPORTED_MODULE_46__ = __webpack_require__(/*! primeng/panelmenu */ "./node_modules/primeng/panelmenu.js");
/* harmony import */ var primeng_panelmenu__WEBPACK_IMPORTED_MODULE_46___default = /*#__PURE__*/__webpack_require__.n(primeng_panelmenu__WEBPACK_IMPORTED_MODULE_46__);
/* harmony import */ var primeng_password__WEBPACK_IMPORTED_MODULE_47__ = __webpack_require__(/*! primeng/password */ "./node_modules/primeng/password.js");
/* harmony import */ var primeng_password__WEBPACK_IMPORTED_MODULE_47___default = /*#__PURE__*/__webpack_require__.n(primeng_password__WEBPACK_IMPORTED_MODULE_47__);
/* harmony import */ var primeng_picklist__WEBPACK_IMPORTED_MODULE_48__ = __webpack_require__(/*! primeng/picklist */ "./node_modules/primeng/picklist.js");
/* harmony import */ var primeng_picklist__WEBPACK_IMPORTED_MODULE_48___default = /*#__PURE__*/__webpack_require__.n(primeng_picklist__WEBPACK_IMPORTED_MODULE_48__);
/* harmony import */ var primeng_progressbar__WEBPACK_IMPORTED_MODULE_49__ = __webpack_require__(/*! primeng/progressbar */ "./node_modules/primeng/progressbar.js");
/* harmony import */ var primeng_progressbar__WEBPACK_IMPORTED_MODULE_49___default = /*#__PURE__*/__webpack_require__.n(primeng_progressbar__WEBPACK_IMPORTED_MODULE_49__);
/* harmony import */ var primeng_radiobutton__WEBPACK_IMPORTED_MODULE_50__ = __webpack_require__(/*! primeng/radiobutton */ "./node_modules/primeng/radiobutton.js");
/* harmony import */ var primeng_radiobutton__WEBPACK_IMPORTED_MODULE_50___default = /*#__PURE__*/__webpack_require__.n(primeng_radiobutton__WEBPACK_IMPORTED_MODULE_50__);
/* harmony import */ var primeng_rating__WEBPACK_IMPORTED_MODULE_51__ = __webpack_require__(/*! primeng/rating */ "./node_modules/primeng/rating.js");
/* harmony import */ var primeng_rating__WEBPACK_IMPORTED_MODULE_51___default = /*#__PURE__*/__webpack_require__.n(primeng_rating__WEBPACK_IMPORTED_MODULE_51__);
/* harmony import */ var primeng_schedule__WEBPACK_IMPORTED_MODULE_52__ = __webpack_require__(/*! primeng/schedule */ "./node_modules/primeng/schedule.js");
/* harmony import */ var primeng_schedule__WEBPACK_IMPORTED_MODULE_52___default = /*#__PURE__*/__webpack_require__.n(primeng_schedule__WEBPACK_IMPORTED_MODULE_52__);
/* harmony import */ var primeng_scrollpanel__WEBPACK_IMPORTED_MODULE_53__ = __webpack_require__(/*! primeng/scrollpanel */ "./node_modules/primeng/scrollpanel.js");
/* harmony import */ var primeng_scrollpanel__WEBPACK_IMPORTED_MODULE_53___default = /*#__PURE__*/__webpack_require__.n(primeng_scrollpanel__WEBPACK_IMPORTED_MODULE_53__);
/* harmony import */ var primeng_selectbutton__WEBPACK_IMPORTED_MODULE_54__ = __webpack_require__(/*! primeng/selectbutton */ "./node_modules/primeng/selectbutton.js");
/* harmony import */ var primeng_selectbutton__WEBPACK_IMPORTED_MODULE_54___default = /*#__PURE__*/__webpack_require__.n(primeng_selectbutton__WEBPACK_IMPORTED_MODULE_54__);
/* harmony import */ var primeng_slidemenu__WEBPACK_IMPORTED_MODULE_55__ = __webpack_require__(/*! primeng/slidemenu */ "./node_modules/primeng/slidemenu.js");
/* harmony import */ var primeng_slidemenu__WEBPACK_IMPORTED_MODULE_55___default = /*#__PURE__*/__webpack_require__.n(primeng_slidemenu__WEBPACK_IMPORTED_MODULE_55__);
/* harmony import */ var primeng_slider__WEBPACK_IMPORTED_MODULE_56__ = __webpack_require__(/*! primeng/slider */ "./node_modules/primeng/slider.js");
/* harmony import */ var primeng_slider__WEBPACK_IMPORTED_MODULE_56___default = /*#__PURE__*/__webpack_require__.n(primeng_slider__WEBPACK_IMPORTED_MODULE_56__);
/* harmony import */ var primeng_spinner__WEBPACK_IMPORTED_MODULE_57__ = __webpack_require__(/*! primeng/spinner */ "./node_modules/primeng/spinner.js");
/* harmony import */ var primeng_spinner__WEBPACK_IMPORTED_MODULE_57___default = /*#__PURE__*/__webpack_require__.n(primeng_spinner__WEBPACK_IMPORTED_MODULE_57__);
/* harmony import */ var primeng_steps__WEBPACK_IMPORTED_MODULE_58__ = __webpack_require__(/*! primeng/steps */ "./node_modules/primeng/steps.js");
/* harmony import */ var primeng_steps__WEBPACK_IMPORTED_MODULE_58___default = /*#__PURE__*/__webpack_require__.n(primeng_steps__WEBPACK_IMPORTED_MODULE_58__);
/* harmony import */ var primeng_tabmenu__WEBPACK_IMPORTED_MODULE_59__ = __webpack_require__(/*! primeng/tabmenu */ "./node_modules/primeng/tabmenu.js");
/* harmony import */ var primeng_tabmenu__WEBPACK_IMPORTED_MODULE_59___default = /*#__PURE__*/__webpack_require__.n(primeng_tabmenu__WEBPACK_IMPORTED_MODULE_59__);
/* harmony import */ var primeng_table__WEBPACK_IMPORTED_MODULE_60__ = __webpack_require__(/*! primeng/table */ "./node_modules/primeng/table.js");
/* harmony import */ var primeng_table__WEBPACK_IMPORTED_MODULE_60___default = /*#__PURE__*/__webpack_require__.n(primeng_table__WEBPACK_IMPORTED_MODULE_60__);
/* harmony import */ var primeng_tabview__WEBPACK_IMPORTED_MODULE_61__ = __webpack_require__(/*! primeng/tabview */ "./node_modules/primeng/tabview.js");
/* harmony import */ var primeng_tabview__WEBPACK_IMPORTED_MODULE_61___default = /*#__PURE__*/__webpack_require__.n(primeng_tabview__WEBPACK_IMPORTED_MODULE_61__);
/* harmony import */ var primeng_terminal__WEBPACK_IMPORTED_MODULE_62__ = __webpack_require__(/*! primeng/terminal */ "./node_modules/primeng/terminal.js");
/* harmony import */ var primeng_terminal__WEBPACK_IMPORTED_MODULE_62___default = /*#__PURE__*/__webpack_require__.n(primeng_terminal__WEBPACK_IMPORTED_MODULE_62__);
/* harmony import */ var primeng_tieredmenu__WEBPACK_IMPORTED_MODULE_63__ = __webpack_require__(/*! primeng/tieredmenu */ "./node_modules/primeng/tieredmenu.js");
/* harmony import */ var primeng_tieredmenu__WEBPACK_IMPORTED_MODULE_63___default = /*#__PURE__*/__webpack_require__.n(primeng_tieredmenu__WEBPACK_IMPORTED_MODULE_63__);
/* harmony import */ var primeng_toast__WEBPACK_IMPORTED_MODULE_64__ = __webpack_require__(/*! primeng/toast */ "./node_modules/primeng/toast.js");
/* harmony import */ var primeng_toast__WEBPACK_IMPORTED_MODULE_64___default = /*#__PURE__*/__webpack_require__.n(primeng_toast__WEBPACK_IMPORTED_MODULE_64__);
/* harmony import */ var primeng_togglebutton__WEBPACK_IMPORTED_MODULE_65__ = __webpack_require__(/*! primeng/togglebutton */ "./node_modules/primeng/togglebutton.js");
/* harmony import */ var primeng_togglebutton__WEBPACK_IMPORTED_MODULE_65___default = /*#__PURE__*/__webpack_require__.n(primeng_togglebutton__WEBPACK_IMPORTED_MODULE_65__);
/* harmony import */ var primeng_toolbar__WEBPACK_IMPORTED_MODULE_66__ = __webpack_require__(/*! primeng/toolbar */ "./node_modules/primeng/toolbar.js");
/* harmony import */ var primeng_toolbar__WEBPACK_IMPORTED_MODULE_66___default = /*#__PURE__*/__webpack_require__.n(primeng_toolbar__WEBPACK_IMPORTED_MODULE_66__);
/* harmony import */ var primeng_tooltip__WEBPACK_IMPORTED_MODULE_67__ = __webpack_require__(/*! primeng/tooltip */ "./node_modules/primeng/tooltip.js");
/* harmony import */ var primeng_tooltip__WEBPACK_IMPORTED_MODULE_67___default = /*#__PURE__*/__webpack_require__.n(primeng_tooltip__WEBPACK_IMPORTED_MODULE_67__);
/* harmony import */ var primeng_tree__WEBPACK_IMPORTED_MODULE_68__ = __webpack_require__(/*! primeng/tree */ "./node_modules/primeng/tree.js");
/* harmony import */ var primeng_tree__WEBPACK_IMPORTED_MODULE_68___default = /*#__PURE__*/__webpack_require__.n(primeng_tree__WEBPACK_IMPORTED_MODULE_68__);
/* harmony import */ var primeng_treetable__WEBPACK_IMPORTED_MODULE_69__ = __webpack_require__(/*! primeng/treetable */ "./node_modules/primeng/treetable.js");
/* harmony import */ var primeng_treetable__WEBPACK_IMPORTED_MODULE_69___default = /*#__PURE__*/__webpack_require__.n(primeng_treetable__WEBPACK_IMPORTED_MODULE_69__);
/* harmony import */ var primeng_primeng__WEBPACK_IMPORTED_MODULE_70__ = __webpack_require__(/*! primeng/primeng */ "./node_modules/primeng/primeng.js");
/* harmony import */ var primeng_primeng__WEBPACK_IMPORTED_MODULE_70___default = /*#__PURE__*/__webpack_require__.n(primeng_primeng__WEBPACK_IMPORTED_MODULE_70__);
/* harmony import */ var _GenericComponents_app_footer_app_footer_component__WEBPACK_IMPORTED_MODULE_71__ = __webpack_require__(/*! ./GenericComponents/app-footer/app.footer.component */ "./src/app/Modules/shared/GenericComponents/app-footer/app.footer.component.ts");
/* harmony import */ var _angular_material_card__WEBPACK_IMPORTED_MODULE_72__ = __webpack_require__(/*! @angular/material/card */ "./node_modules/@angular/material/esm5/card.es5.js");
/* harmony import */ var _angular_material_toolbar__WEBPACK_IMPORTED_MODULE_73__ = __webpack_require__(/*! @angular/material/toolbar */ "./node_modules/@angular/material/esm5/toolbar.es5.js");
/* harmony import */ var _angular_material_button__WEBPACK_IMPORTED_MODULE_74__ = __webpack_require__(/*! @angular/material/button */ "./node_modules/@angular/material/esm5/button.es5.js");
/* harmony import */ var primeng_datatable__WEBPACK_IMPORTED_MODULE_75__ = __webpack_require__(/*! primeng/datatable */ "./node_modules/primeng/datatable.js");
/* harmony import */ var primeng_datatable__WEBPACK_IMPORTED_MODULE_75___default = /*#__PURE__*/__webpack_require__.n(primeng_datatable__WEBPACK_IMPORTED_MODULE_75__);
/* harmony import */ var _angular_material_checkbox__WEBPACK_IMPORTED_MODULE_76__ = __webpack_require__(/*! @angular/material/checkbox */ "./node_modules/@angular/material/esm5/checkbox.es5.js");
/* harmony import */ var _angular_material_grid_list__WEBPACK_IMPORTED_MODULE_77__ = __webpack_require__(/*! @angular/material/grid-list */ "./node_modules/@angular/material/esm5/grid-list.es5.js");
/* harmony import */ var _angular_material_select__WEBPACK_IMPORTED_MODULE_78__ = __webpack_require__(/*! @angular/material/select */ "./node_modules/@angular/material/esm5/select.es5.js");
/* harmony import */ var _angular_material_core__WEBPACK_IMPORTED_MODULE_79__ = __webpack_require__(/*! @angular/material/core */ "./node_modules/@angular/material/esm5/core.es5.js");
/* harmony import */ var _angular_material_icon__WEBPACK_IMPORTED_MODULE_80__ = __webpack_require__(/*! @angular/material/icon */ "./node_modules/@angular/material/esm5/icon.es5.js");
/* harmony import */ var primeng_progressspinner__WEBPACK_IMPORTED_MODULE_81__ = __webpack_require__(/*! primeng/progressspinner */ "./node_modules/primeng/progressspinner.js");
/* harmony import */ var primeng_progressspinner__WEBPACK_IMPORTED_MODULE_81___default = /*#__PURE__*/__webpack_require__.n(primeng_progressspinner__WEBPACK_IMPORTED_MODULE_81__);
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};


















































































var SharedModule = /** @class */ (function () {
    function SharedModule() {
    }
    SharedModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            declarations: [_GenericComponents_app_footer_app_footer_component__WEBPACK_IMPORTED_MODULE_71__["AppFooterComponent"]],
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"],
                _angular_material_icon__WEBPACK_IMPORTED_MODULE_80__["MatIconModule"],
                _angular_material_select__WEBPACK_IMPORTED_MODULE_78__["MatSelectModule"],
                _angular_material_core__WEBPACK_IMPORTED_MODULE_79__["MatOptionModule"],
                _shared_routing_module__WEBPACK_IMPORTED_MODULE_3__["SharedRoutingModule"],
                primeng_accordion__WEBPACK_IMPORTED_MODULE_2__["AccordionModule"],
                primeng_autocomplete__WEBPACK_IMPORTED_MODULE_7__["AutoCompleteModule"],
                primeng_breadcrumb__WEBPACK_IMPORTED_MODULE_8__["BreadcrumbModule"],
                primeng_button__WEBPACK_IMPORTED_MODULE_9__["ButtonModule"],
                primeng_calendar__WEBPACK_IMPORTED_MODULE_10__["CalendarModule"],
                primeng_card__WEBPACK_IMPORTED_MODULE_11__["CardModule"],
                primeng_carousel__WEBPACK_IMPORTED_MODULE_12__["CarouselModule"],
                primeng_chart__WEBPACK_IMPORTED_MODULE_13__["ChartModule"],
                primeng_checkbox__WEBPACK_IMPORTED_MODULE_14__["CheckboxModule"],
                primeng_chips__WEBPACK_IMPORTED_MODULE_15__["ChipsModule"],
                primeng_codehighlighter__WEBPACK_IMPORTED_MODULE_16__["CodeHighlighterModule"],
                primeng_confirmdialog__WEBPACK_IMPORTED_MODULE_17__["ConfirmDialogModule"],
                primeng_colorpicker__WEBPACK_IMPORTED_MODULE_18__["ColorPickerModule"],
                primeng_contextmenu__WEBPACK_IMPORTED_MODULE_19__["ContextMenuModule"],
                primeng_dataview__WEBPACK_IMPORTED_MODULE_20__["DataViewModule"],
                primeng_dialog__WEBPACK_IMPORTED_MODULE_21__["DialogModule"],
                primeng_dropdown__WEBPACK_IMPORTED_MODULE_22__["DropdownModule"],
                primeng_editor__WEBPACK_IMPORTED_MODULE_23__["EditorModule"],
                primeng_fieldset__WEBPACK_IMPORTED_MODULE_24__["FieldsetModule"],
                primeng_fileupload__WEBPACK_IMPORTED_MODULE_25__["FileUploadModule"],
                primeng_galleria__WEBPACK_IMPORTED_MODULE_26__["GalleriaModule"],
                primeng_growl__WEBPACK_IMPORTED_MODULE_27__["GrowlModule"],
                primeng_inplace__WEBPACK_IMPORTED_MODULE_28__["InplaceModule"],
                primeng_inputmask__WEBPACK_IMPORTED_MODULE_29__["InputMaskModule"],
                primeng_inputswitch__WEBPACK_IMPORTED_MODULE_30__["InputSwitchModule"],
                primeng_inputtext__WEBPACK_IMPORTED_MODULE_31__["InputTextModule"],
                primeng_inputtextarea__WEBPACK_IMPORTED_MODULE_32__["InputTextareaModule"],
                primeng_lightbox__WEBPACK_IMPORTED_MODULE_33__["LightboxModule"],
                primeng_listbox__WEBPACK_IMPORTED_MODULE_34__["ListboxModule"],
                primeng_megamenu__WEBPACK_IMPORTED_MODULE_35__["MegaMenuModule"],
                primeng_menu__WEBPACK_IMPORTED_MODULE_36__["MenuModule"],
                primeng_menubar__WEBPACK_IMPORTED_MODULE_37__["MenubarModule"],
                primeng_message__WEBPACK_IMPORTED_MODULE_39__["MessageModule"],
                primeng_messages__WEBPACK_IMPORTED_MODULE_38__["MessagesModule"],
                primeng_multiselect__WEBPACK_IMPORTED_MODULE_40__["MultiSelectModule"],
                primeng_orderlist__WEBPACK_IMPORTED_MODULE_41__["OrderListModule"],
                primeng_organizationchart__WEBPACK_IMPORTED_MODULE_42__["OrganizationChartModule"],
                primeng_overlaypanel__WEBPACK_IMPORTED_MODULE_43__["OverlayPanelModule"],
                primeng_paginator__WEBPACK_IMPORTED_MODULE_44__["PaginatorModule"],
                primeng_panel__WEBPACK_IMPORTED_MODULE_45__["PanelModule"],
                primeng_panelmenu__WEBPACK_IMPORTED_MODULE_46__["PanelMenuModule"],
                primeng_password__WEBPACK_IMPORTED_MODULE_47__["PasswordModule"],
                primeng_picklist__WEBPACK_IMPORTED_MODULE_48__["PickListModule"],
                primeng_progressbar__WEBPACK_IMPORTED_MODULE_49__["ProgressBarModule"],
                primeng_radiobutton__WEBPACK_IMPORTED_MODULE_50__["RadioButtonModule"],
                primeng_rating__WEBPACK_IMPORTED_MODULE_51__["RatingModule"],
                primeng_schedule__WEBPACK_IMPORTED_MODULE_52__["ScheduleModule"],
                primeng_scrollpanel__WEBPACK_IMPORTED_MODULE_53__["ScrollPanelModule"],
                primeng_selectbutton__WEBPACK_IMPORTED_MODULE_54__["SelectButtonModule"],
                primeng_slidemenu__WEBPACK_IMPORTED_MODULE_55__["SlideMenuModule"],
                primeng_slider__WEBPACK_IMPORTED_MODULE_56__["SliderModule"],
                primeng_spinner__WEBPACK_IMPORTED_MODULE_57__["SpinnerModule"],
                primeng_splitbutton__WEBPACK_IMPORTED_MODULE_4__["SplitButtonModule"],
                primeng_steps__WEBPACK_IMPORTED_MODULE_58__["StepsModule"],
                primeng_table__WEBPACK_IMPORTED_MODULE_60__["TableModule"],
                primeng_tabmenu__WEBPACK_IMPORTED_MODULE_59__["TabMenuModule"],
                primeng_tabview__WEBPACK_IMPORTED_MODULE_61__["TabViewModule"],
                primeng_terminal__WEBPACK_IMPORTED_MODULE_62__["TerminalModule"],
                primeng_tieredmenu__WEBPACK_IMPORTED_MODULE_63__["TieredMenuModule"],
                primeng_toast__WEBPACK_IMPORTED_MODULE_64__["ToastModule"],
                primeng_togglebutton__WEBPACK_IMPORTED_MODULE_65__["ToggleButtonModule"],
                primeng_toolbar__WEBPACK_IMPORTED_MODULE_66__["ToolbarModule"],
                primeng_tooltip__WEBPACK_IMPORTED_MODULE_67__["TooltipModule"],
                primeng_tree__WEBPACK_IMPORTED_MODULE_68__["TreeModule"],
                primeng_treetable__WEBPACK_IMPORTED_MODULE_69__["TreeTableModule"],
                _angular_material_slide_toggle__WEBPACK_IMPORTED_MODULE_5__["MatSlideToggleModule"],
                _angular_material_card__WEBPACK_IMPORTED_MODULE_72__["MatCardModule"],
                _angular_material_toolbar__WEBPACK_IMPORTED_MODULE_73__["MatToolbarModule"],
                _angular_material_expansion__WEBPACK_IMPORTED_MODULE_6__["MatExpansionModule"],
                _angular_material_button__WEBPACK_IMPORTED_MODULE_74__["MatButtonModule"],
                primeng_datatable__WEBPACK_IMPORTED_MODULE_75__["DataTableModule"],
                _angular_material_card__WEBPACK_IMPORTED_MODULE_72__["MatCardModule"],
                _angular_material_checkbox__WEBPACK_IMPORTED_MODULE_76__["MatCheckboxModule"],
                _angular_material_grid_list__WEBPACK_IMPORTED_MODULE_77__["MatGridListModule"],
                primeng_progressspinner__WEBPACK_IMPORTED_MODULE_81__["ProgressSpinnerModule"]
            ],
            exports: [
                _GenericComponents_app_footer_app_footer_component__WEBPACK_IMPORTED_MODULE_71__["AppFooterComponent"],
                _angular_material_core__WEBPACK_IMPORTED_MODULE_79__["MatOptionModule"],
                _angular_material_icon__WEBPACK_IMPORTED_MODULE_80__["MatIconModule"],
                _angular_material_select__WEBPACK_IMPORTED_MODULE_78__["MatSelectModule"],
                primeng_accordion__WEBPACK_IMPORTED_MODULE_2__["AccordionModule"],
                primeng_autocomplete__WEBPACK_IMPORTED_MODULE_7__["AutoCompleteModule"],
                primeng_breadcrumb__WEBPACK_IMPORTED_MODULE_8__["BreadcrumbModule"],
                primeng_button__WEBPACK_IMPORTED_MODULE_9__["ButtonModule"],
                primeng_calendar__WEBPACK_IMPORTED_MODULE_10__["CalendarModule"],
                primeng_card__WEBPACK_IMPORTED_MODULE_11__["CardModule"],
                primeng_carousel__WEBPACK_IMPORTED_MODULE_12__["CarouselModule"],
                primeng_chart__WEBPACK_IMPORTED_MODULE_13__["ChartModule"],
                primeng_checkbox__WEBPACK_IMPORTED_MODULE_14__["CheckboxModule"],
                primeng_chips__WEBPACK_IMPORTED_MODULE_15__["ChipsModule"],
                primeng_codehighlighter__WEBPACK_IMPORTED_MODULE_16__["CodeHighlighterModule"],
                primeng_confirmdialog__WEBPACK_IMPORTED_MODULE_17__["ConfirmDialogModule"],
                primeng_colorpicker__WEBPACK_IMPORTED_MODULE_18__["ColorPickerModule"],
                primeng_contextmenu__WEBPACK_IMPORTED_MODULE_19__["ContextMenuModule"],
                primeng_dataview__WEBPACK_IMPORTED_MODULE_20__["DataViewModule"],
                primeng_dialog__WEBPACK_IMPORTED_MODULE_21__["DialogModule"],
                primeng_dropdown__WEBPACK_IMPORTED_MODULE_22__["DropdownModule"],
                primeng_editor__WEBPACK_IMPORTED_MODULE_23__["EditorModule"],
                primeng_fieldset__WEBPACK_IMPORTED_MODULE_24__["FieldsetModule"],
                primeng_fileupload__WEBPACK_IMPORTED_MODULE_25__["FileUploadModule"],
                primeng_galleria__WEBPACK_IMPORTED_MODULE_26__["GalleriaModule"],
                primeng_growl__WEBPACK_IMPORTED_MODULE_27__["GrowlModule"],
                primeng_inplace__WEBPACK_IMPORTED_MODULE_28__["InplaceModule"],
                primeng_inputmask__WEBPACK_IMPORTED_MODULE_29__["InputMaskModule"],
                primeng_inputswitch__WEBPACK_IMPORTED_MODULE_30__["InputSwitchModule"],
                primeng_inputtext__WEBPACK_IMPORTED_MODULE_31__["InputTextModule"],
                primeng_inputtextarea__WEBPACK_IMPORTED_MODULE_32__["InputTextareaModule"],
                primeng_lightbox__WEBPACK_IMPORTED_MODULE_33__["LightboxModule"],
                primeng_listbox__WEBPACK_IMPORTED_MODULE_34__["ListboxModule"],
                primeng_megamenu__WEBPACK_IMPORTED_MODULE_35__["MegaMenuModule"],
                primeng_menu__WEBPACK_IMPORTED_MODULE_36__["MenuModule"],
                primeng_menubar__WEBPACK_IMPORTED_MODULE_37__["MenubarModule"],
                primeng_message__WEBPACK_IMPORTED_MODULE_39__["MessageModule"],
                primeng_messages__WEBPACK_IMPORTED_MODULE_38__["MessagesModule"],
                primeng_multiselect__WEBPACK_IMPORTED_MODULE_40__["MultiSelectModule"],
                primeng_orderlist__WEBPACK_IMPORTED_MODULE_41__["OrderListModule"],
                primeng_organizationchart__WEBPACK_IMPORTED_MODULE_42__["OrganizationChartModule"],
                primeng_overlaypanel__WEBPACK_IMPORTED_MODULE_43__["OverlayPanelModule"],
                primeng_paginator__WEBPACK_IMPORTED_MODULE_44__["PaginatorModule"],
                primeng_panel__WEBPACK_IMPORTED_MODULE_45__["PanelModule"],
                primeng_panelmenu__WEBPACK_IMPORTED_MODULE_46__["PanelMenuModule"],
                primeng_password__WEBPACK_IMPORTED_MODULE_47__["PasswordModule"],
                primeng_picklist__WEBPACK_IMPORTED_MODULE_48__["PickListModule"],
                primeng_progressbar__WEBPACK_IMPORTED_MODULE_49__["ProgressBarModule"],
                primeng_radiobutton__WEBPACK_IMPORTED_MODULE_50__["RadioButtonModule"],
                primeng_rating__WEBPACK_IMPORTED_MODULE_51__["RatingModule"],
                primeng_schedule__WEBPACK_IMPORTED_MODULE_52__["ScheduleModule"],
                primeng_scrollpanel__WEBPACK_IMPORTED_MODULE_53__["ScrollPanelModule"],
                primeng_selectbutton__WEBPACK_IMPORTED_MODULE_54__["SelectButtonModule"],
                primeng_slidemenu__WEBPACK_IMPORTED_MODULE_55__["SlideMenuModule"],
                primeng_slider__WEBPACK_IMPORTED_MODULE_56__["SliderModule"],
                primeng_spinner__WEBPACK_IMPORTED_MODULE_57__["SpinnerModule"],
                primeng_splitbutton__WEBPACK_IMPORTED_MODULE_4__["SplitButtonModule"],
                primeng_steps__WEBPACK_IMPORTED_MODULE_58__["StepsModule"],
                primeng_table__WEBPACK_IMPORTED_MODULE_60__["TableModule"],
                primeng_tabmenu__WEBPACK_IMPORTED_MODULE_59__["TabMenuModule"],
                primeng_tabview__WEBPACK_IMPORTED_MODULE_61__["TabViewModule"],
                primeng_terminal__WEBPACK_IMPORTED_MODULE_62__["TerminalModule"],
                primeng_tieredmenu__WEBPACK_IMPORTED_MODULE_63__["TieredMenuModule"],
                primeng_toast__WEBPACK_IMPORTED_MODULE_64__["ToastModule"],
                primeng_togglebutton__WEBPACK_IMPORTED_MODULE_65__["ToggleButtonModule"],
                primeng_toolbar__WEBPACK_IMPORTED_MODULE_66__["ToolbarModule"],
                primeng_tooltip__WEBPACK_IMPORTED_MODULE_67__["TooltipModule"],
                primeng_tree__WEBPACK_IMPORTED_MODULE_68__["TreeModule"],
                primeng_treetable__WEBPACK_IMPORTED_MODULE_69__["TreeTableModule"],
                primeng_primeng__WEBPACK_IMPORTED_MODULE_70__["SidebarModule"],
                _angular_material_slide_toggle__WEBPACK_IMPORTED_MODULE_5__["MatSlideToggleModule"],
                _angular_material_card__WEBPACK_IMPORTED_MODULE_72__["MatCardModule"],
                _angular_material_toolbar__WEBPACK_IMPORTED_MODULE_73__["MatToolbarModule"],
                _angular_material_expansion__WEBPACK_IMPORTED_MODULE_6__["MatExpansionModule"],
                _angular_material_button__WEBPACK_IMPORTED_MODULE_74__["MatButtonModule"],
                primeng_datatable__WEBPACK_IMPORTED_MODULE_75__["DataTableModule"],
                _angular_material_card__WEBPACK_IMPORTED_MODULE_72__["MatCardModule"],
                _angular_material_checkbox__WEBPACK_IMPORTED_MODULE_76__["MatCheckboxModule"],
                _angular_material_grid_list__WEBPACK_IMPORTED_MODULE_77__["MatGridListModule"],
                primeng_progressspinner__WEBPACK_IMPORTED_MODULE_81__["ProgressSpinnerModule"]
            ]
        })
    ], SharedModule);
    return SharedModule;
}());



/***/ }),

/***/ "./src/app/Services/httprequest.service.ts":
/*!*************************************************!*\
  !*** ./src/app/Services/httprequest.service.ts ***!
  \*************************************************/
/*! exports provided: HTTPRequestService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HTTPRequestService", function() { return HTTPRequestService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm5/index.js");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../environments/environment */ "./src/environments/environment.ts");
/* harmony import */ var _sharedservice_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./sharedservice.service */ "./src/app/Services/sharedservice.service.ts");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var HTTPRequestService = /** @class */ (function () {
    function HTTPRequestService(http, sharedData) {
        this.http = http;
        this.sharedData = sharedData;
        this.urlHost = _environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].baseUrl;
    }
    HTTPRequestService.prototype.storeCpacityData = function (data) {
        this.allCapacityData = data;
    };
    HTTPRequestService.prototype.GetCapacitydata = function () {
        return this.allCapacityData;
    };
    // Generic post Method to call external API
    HTTPRequestService.prototype.post = function (url, data) {
        var token = this.sharedData.tokenIDOfLoggedUser;
        // tslint:disable-next-line:max-line-length
        var header = new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpHeaders"]()
            .set('Content-Type', 'application/json')
            .set('Access-Control-Allow-Origin', '*')
            .set('Authorization', 'Bearer ' + token);
        var apiEndpoint = "" + this.urlHost + url;
        return this.http.post(apiEndpoint, data, {
            headers: header
        });
    };
    // Generic post Method to call external API
    HTTPRequestService.prototype.postFile = function (url, data, httpHeader) {
        var apiEndpoint = "" + this.urlHost + url;
        return this.http.post(apiEndpoint, data, { headers: httpHeader });
    };
    // Generic get Method to call external API
    HTTPRequestService.prototype.get = function (url, httpHeader) {
        return this.http.get("" + this.urlHost + url, {
            headers: httpHeader
        });
    };
    // Generic post Method to call external API
    HTTPRequestService.prototype.getAPI = function (url) {
        var token = this.sharedData.tokenIDOfLoggedUser;
        // tslint:disable-next-line:max-line-length
        var header = new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpHeaders"]()
            .set('Content-Type', 'application/json')
            .set('Access-Control-Allow-Origin', '*')
            .set('Authorization', 'Bearer ' + token);
        return this.http.get(this.urlHost + url, { headers: header });
    };
    HTTPRequestService.prototype.put = function (url, data, httpHeader) {
        return this.http.put("" + this.urlHost + url, data, {
            headers: httpHeader
        });
    };
    HTTPRequestService.prototype.putFile = function (url, data, httpHeader) {
        return this.http.put("" + this.urlHost + url, data, {
            headers: httpHeader
        });
    };
    HTTPRequestService.prototype.delete = function (url, httpHeader) {
        return this.http.delete("" + this.urlHost + url, {
            headers: httpHeader
        });
    };
    // public getCapacityData<T>(url: string): Observable<T> {
    //     const token = this.sharedData.tokenIDOfLoggedUser;
    //     const header: HttpHeaders = new HttpHeaders().set(
    //         'Authorization',
    //         'Bearer ' + token
    //     );
    //     return this.http.get<T>(this.urlHost + url, { headers: header });
    // }
    HTTPRequestService.prototype.getCapacityData = function (url) {
        var _this = this;
        if (!this.capacityDatacache$) {
            var timer$ = Object(rxjs__WEBPACK_IMPORTED_MODULE_2__["timer"])(0, 900000);
            this.capacityDatacache$ = timer$.pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_5__["switchMap"])(function (_) { return _this.GetCapacityDataFromAPI(url); }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_5__["shareReplay"])(1));
            return this.capacityDatacache$;
        }
        return this.capacityDatacache$;
    };
    HTTPRequestService.prototype.GetCapacityDataFromAPI = function (url) {
        var token = this.sharedData.tokenIDOfLoggedUser;
        var header = new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpHeaders"]().set('Authorization', 'Bearer ' + token);
        return this.http.get(this.urlHost + url, { headers: header }).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_5__["tap"])());
    };
    HTTPRequestService.prototype.getPreferenceData = function (url) {
        var token = this.sharedData.tokenIDOfLoggedUser;
        var header = new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpHeaders"]().set('Authorization', 'Bearer ' + token);
        return this.http.get(this.urlHost + url, { headers: header });
    };
    // tslint:disable-next-line:no-shadowed-variable
    HTTPRequestService.prototype.getFilteredCapacityData = function (url, filterData) {
        var token = this.sharedData.tokenIDOfLoggedUser;
        // tslint:disable-next-line:max-line-length
        var header = new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpHeaders"]()
            .set('Content-Type', 'application/json')
            .set('Access-Control-Allow-Origin', '*')
            .set('Authorization', 'Bearer ' + token);
        return this.http.post(this.urlHost + url, filterData, {
            headers: header
        });
    };
    HTTPRequestService.prototype.getFilters = function (url) {
        var token = this.sharedData.tokenIDOfLoggedUser;
        var header = new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpHeaders"]().set('Authorization', 'Bearer ' + token);
        return this.http.get(this.urlHost + url, { headers: header });
    };
    HTTPRequestService.prototype.getFiltersOnSelectedFilters = function (url, filter) {
        var token = this.sharedData.tokenIDOfLoggedUser;
        // tslint:disable-next-line:max-line-length
        var header = new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpHeaders"]()
            .set('Content-Type', 'application/json')
            .set('Access-Control-Allow-Origin', '*')
            .set('Authorization', 'Bearer ' + token);
        return this.http.post(this.urlHost + url, filter, {
            headers: header
        });
    };
    // tslint:disable-next-line:no-shadowed-variable
    HTTPRequestService.prototype.savePreference = function (url, savePref) {
        var token = this.sharedData.tokenIDOfLoggedUser;
        // tslint:disable-next-line:max-line-length
        var header = new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpHeaders"]()
            .set('Content-Type', 'application/json')
            .set('Access-Control-Allow-Origin', '*')
            .set('Authorization', 'Bearer ' + token);
        return this.http.post(this.urlHost + url, savePref, {
            headers: header
        });
    };
    HTTPRequestService = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])({
            providedIn: 'root'
        }),
        __metadata("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"],
            _sharedservice_service__WEBPACK_IMPORTED_MODULE_4__["SharedserviceService"]])
    ], HTTPRequestService);
    return HTTPRequestService;
}());



/***/ }),

/***/ "./src/app/Services/sharedservice.service.ts":
/*!***************************************************!*\
  !*** ./src/app/Services/sharedservice.service.ts ***!
  \***************************************************/
/*! exports provided: SharedserviceService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SharedserviceService", function() { return SharedserviceService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm5/index.js");
/* harmony import */ var crypto_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! crypto-js */ "./node_modules/crypto-js/index.js");
/* harmony import */ var crypto_js__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(crypto_js__WEBPACK_IMPORTED_MODULE_3__);
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var SharedserviceService = /** @class */ (function () {
    function SharedserviceService(http) {
        this.http = http;
        // Declaring Behavior Subject//
        this.name = new rxjs__WEBPACK_IMPORTED_MODULE_2__["BehaviorSubject"](null);
        this.competency = new rxjs__WEBPACK_IMPORTED_MODULE_2__["BehaviorSubject"](null);
        this.mid = new rxjs__WEBPACK_IMPORTED_MODULE_2__["BehaviorSubject"](null);
        this.roleID = new rxjs__WEBPACK_IMPORTED_MODULE_2__["BehaviorSubject"](null);
        this.token = new rxjs__WEBPACK_IMPORTED_MODULE_2__["BehaviorSubject"](null);
        this.secretKey = 'dsqwertuujexn4t34veem8ijtffd4vyn';
        this.parsedSecretKey = crypto_js__WEBPACK_IMPORTED_MODULE_3__["enc"].Utf8.parse(this.secretKey);
        this.IV = '45dm3pufjr2fdmwk';
        this.parsedIV = crypto_js__WEBPACK_IMPORTED_MODULE_3__["enc"].Utf8.parse(this.IV);
    }
    // Function to get the logged in user details
    SharedserviceService.prototype.showUserDetails = function (data) {
        this.competency.next(data[0]);
        this.mid.next(data[1]);
        this.name.next(data[2]);
        this.token.next(data[3]);
        this.roleID.next(data[4]);
    };
    // // Function to get the role ID of user
    // showRoleName(data) {
    //     this.roleID.next(data);
    // }
    SharedserviceService.prototype.encryptingData = function (data) {
        var encryptedData = crypto_js__WEBPACK_IMPORTED_MODULE_3__["AES"].encrypt(data, this.parsedSecretKey, {
            keySize: 256 / 8,
            iv: this.parsedIV,
            mode: crypto_js__WEBPACK_IMPORTED_MODULE_3__["mode"].CBC,
            padding: crypto_js__WEBPACK_IMPORTED_MODULE_3__["pad"].Pkcs7
        }).toString();
        return encryptedData;
    };
    SharedserviceService.prototype.decryptingData = function (data) {
        var decryptedData = crypto_js__WEBPACK_IMPORTED_MODULE_3__["AES"].decrypt(data, this.parsedSecretKey, {
            keySize: 256 / 8,
            iv: this.parsedIV,
            mode: crypto_js__WEBPACK_IMPORTED_MODULE_3__["mode"].CBC,
            padding: crypto_js__WEBPACK_IMPORTED_MODULE_3__["pad"].Pkcs7
        }).toString(crypto_js__WEBPACK_IMPORTED_MODULE_3__["enc"].Utf8);
        return decryptedData;
    };
    SharedserviceService = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])({
            providedIn: 'root'
        }),
        __metadata("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"]])
    ], SharedserviceService);
    return SharedserviceService;
}());



/***/ }),

/***/ "./src/app/app-routing.module.ts":
/*!***************************************!*\
  !*** ./src/app/app-routing.module.ts ***!
  \***************************************/
/*! exports provided: AppRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppRoutingModule", function() { return AppRoutingModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _Modules_FetaureModules_dash_board_dash_board_dashboarddemo_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./Modules/FetaureModules/dash-board/dash-board/dashboarddemo.component */ "./src/app/Modules/FetaureModules/dash-board/dash-board/dashboarddemo.component.ts");
/* harmony import */ var _guards_auth_guard__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../guards/auth.guard */ "./src/guards/auth.guard.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};




// In this array we register all the routes for the application
var routes = [
    // {path:'',canActivate:[AuthGuard],component:DashboardDemoComponent},
    { path: 'dashboard', component: _Modules_FetaureModules_dash_board_dash_board_dashboarddemo_component__WEBPACK_IMPORTED_MODULE_2__["DashboardDemoComponent"] },
    // tslint:disable-next-line:max-line-length
    {
        path: 'CapacityInsights',
        canActivate: [_guards_auth_guard__WEBPACK_IMPORTED_MODULE_3__["AuthGuard"]],
        loadChildren: './Modules/FetaureModules/capacity-insights/capacity-insights.module#CapacityInsightsModule'
    },
    {
        path: 'admin',
        canActivate: [_guards_auth_guard__WEBPACK_IMPORTED_MODULE_3__["AuthGuard"]],
        loadChildren: './Modules/FetaureModules/admin/admin.module#AdminModule'
    },
    { path: 'core', loadChildren: './Modules/core/core.module#CoreModule' }
    // ,  {path:'http://localhost:4200/:id_token', component:AppComponent}
];
var AppRoutingModule = /** @class */ (function () {
    function AppRoutingModule() {
    }
    AppRoutingModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"].forRoot(routes)],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"]]
        })
    ], AppRoutingModule);
    return AppRoutingModule;
}());



/***/ }),

/***/ "./src/app/app.component.html":
/*!************************************!*\
  !*** ./src/app/app.component.html ***!
  \************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div *ngIf=\"ShowCapacityInsights\">\n    <!-- ABCD UNBRELLA HOME PAGE BLOCK-->\n    <div class=\"container-fluid\">\n        <div class=\"navbar navbar-inverse fixed-top header\">\n            <div class=\"navbar-header\">\n                <img\n                    src=\"../assets/layout/images/logo_1.png\"\n                    class=\"img-responsive logo\"\n                />\n                <p class=\"logo-text\">Umbrella</p>\n                <sub class=\"logo-subtext\">Innovation Evolution</sub>\n                <div *ngIf=\"showMessage\">\n                    <p>{{ message }}</p>\n                </div>\n            </div>\n            <!-- <ul class=\"nav navbar-nav navbar-right\">\n                        <li class=\"dropdown\">\n                            <p class=\"user-name\">Welcome <span>{{name1}}</span></p>\n                            <img src=\"../assets/layout/images/user profile.jpg\" class=\"img-responsive img-user dropdown-toggle\" data-toggle=\"dropdown\" role=\"button\" aria-haspopup=\"true\" aria-expanded=\"false\"/>\n                            \n                            <ul class=\"dropdown-menu dropdown-profile\">\n                                <li><a>Neupa Sinha</a></li>\n                                <li><a class=\"font-size-xs\">(Admin)</a></li>\n                                <li role=\"separator\" class=\"divider\"></li>\n                                <li><a href=\"#\" class=\"check-profile\">View Profile</a></li>\n                            </ul>\n                        </li>\n                    </ul> -->\n        </div>\n\n        <div class=\"row margin-top-sm\">\n            <div class=\"col-lg-6 col-sm-12 margin-top-sm\">\n                <div class=\"p-relative\">\n                    <img\n                        src=\"../assets/layout/images/promo10.jpg\"\n                        class=\"img-responsive img-height-main\"\n                    />\n                    <div class=\"details-block\">\n                        <p>\n                            ABCD gives you the possibility to explore on NextGen\n                            technologies and focus on multidimensional learning\n                            path with a 360 vision.\n                        </p>\n                        <button class=\"btn button-details\">View Details</button>\n                    </div>\n                </div>\n            </div>\n            <div class=\"col-lg-6 col-sm-12\">\n                <div class=\"row\">\n                    <div class=\"col-lg-6  col-sm-12 margin-top-sm img-height\">\n                        <img\n                            src=\"../assets/layout/images/promo6.jpg\"\n                            class=\"img-responsive img-height boxShadow\"\n                        />\n                        <div class=\"promo-sub-blocks\">\n                            <p class=\"font-size-md margin-bottom-null\">\n                                Capacity Insights\n                            </p>\n                            <input\n                                type=\"text\"\n                                [(ngModel)]=\"midOfUser\"\n                                placeholder=\"Enter your MID\"\n                                class=\"input-field\"\n                            />\n\n                            <button\n                                class=\"btn button-details button-pages margin-top-xs-more\"\n                                (click)=\"ShowCI()\"\n                            >\n                                Launch\n                            </button>\n                        </div>\n                    </div>\n                    <div class=\"col-lg-6 col-sm-12 margin-top-sm img-height \">\n                        <img\n                            src=\"../assets/layout/images/promo7.jpg\"\n                            class=\"img-responsive img-height boxShadow\"\n                        />\n                        <div class=\"promo-sub-blocks\">\n                            <p class=\"font-size-md\">GM simulator</p>\n                            <button\n                                (click)=\"ShowUnderConstruction()\"\n                                class=\"btn button-details button-pages\"\n                            >\n                                Launch\n                            </button>\n                        </div>\n                    </div>\n                </div>\n                <div class=\"row\">\n                    <div class=\"col-lg-6 col-sm-12 margin-top-sm img-height\">\n                        <img\n                            src=\"../assets/layout/images/promo8.jpg\"\n                            class=\"img-responsive img-height boxShadow\"\n                        />\n                        <div class=\"promo-sub-blocks\">\n                            <p class=\"font-size-md\">Action Tracker</p>\n                            <a (click)=\"ShowUnderConstruction()\" target=\"blank\"\n                                ><button\n                                    class=\"btn button-details button-pages\"\n                                >\n                                    Launch\n                                </button></a\n                            >\n                        </div>\n                    </div>\n                    <div class=\"col-lg-6 col-sm-12 margin-top-sm img-height\">\n                        <img\n                            src=\"../assets/layout/images/promo9.jpg\"\n                            class=\"img-responsive img-height boxShadow \"\n                        />\n                        <div class=\"promo-sub-blocks\">\n                            <p class=\"font-size-md\">Interaction Suite</p>\n                            <button\n                                (click)=\"ShowUnderConstruction()\"\n                                class=\"btn button-details button-pages\"\n                            >\n                                Launch\n                            </button>\n                        </div>\n                    </div>\n                </div>\n            </div>\n        </div>\n\n        <div class=\"row margin-top-sm\">\n            <div class=\"col-sm-12 margin-top-sm\">\n                <h2 class=\"quote\">Learn..Grow..Excel</h2>\n            </div>\n        </div>\n\n        <div class=\"row margin-top-sm\">\n            <div class=\"col-md-3 col-sm-12\">\n                <div class=\"promo-div\">\n                    <div class=\"promo-img-block\">\n                        <img\n                            src=\"../assets/layout/images/promo1.jpg\"\n                            class=\"mx-auto d-block img-promo\"\n                        />\n                    </div>\n                    <div class=\"promo-block\">\n                        <h4 class=\"promo-title\">Application Evolution</h4>\n                        <p class=\"promo-text\">\n                            Maximize benefits from critical applications\n                        </p>\n                    </div>\n                </div>\n            </div>\n            <div class=\"col-md-3 col-sm-12\">\n                <div class=\"promo-div\">\n                    <div class=\"promo-img-block\">\n                        <img\n                            src=\"../assets/layout/images/promo2.png\"\n                            class=\"mx-auto d-block img-promo\"\n                        />\n                    </div>\n                    <div class=\"promo-block\">\n                        <h4 class=\"promo-title\">Integrated Services</h4>\n                        <p class=\"promo-text\">Re-imagine end-to-end IT</p>\n                    </div>\n                </div>\n            </div>\n            <div class=\"col-md-3 col-sm-12\">\n                <div class=\"promo-div\">\n                    <div class=\"promo-img-block\">\n                        <img\n                            src=\"../assets/layout/images/promo3.png\"\n                            class=\"mx-auto d-block img-promo\"\n                        />\n                    </div>\n                    <div class=\"promo-block\">\n                        <h4 class=\"promo-title\">Growth Optimization</h4>\n                        <p class=\"promo-text\">\n                            Sound strategy to sustain growth and profitability\n                        </p>\n                    </div>\n                </div>\n            </div>\n            <div class=\"col-md-3 col-sm-12\">\n                <div class=\"promo-div\">\n                    <div class=\"promo-img-block\">\n                        <img\n                            src=\"../assets/layout/images/promo4.png\"\n                            class=\"mx-auto d-block img-promo\"\n                        />\n                    </div>\n                    <div class=\"promo-block\">\n                        <h4 class=\"promo-title\">Artificial Inteligence</h4>\n                        <p class=\"promo-text\">Re-imagine end-to-end IT</p>\n                    </div>\n                </div>\n            </div>\n        </div>\n        <div class=\"row margin-top-sm\">\n            <div class=\"col-sm-12 margin-top-sm\">\n                <footer>\n                    <span class=\"footer-text\">&copy; 2018 - ABCD Umbrella</span>\n                    <ul class=\"footer-block\">\n                        <li>Imprint</li>\n                        <li>Legal Disclaimer</li>\n                        <li>Privacy Policy</li>\n                        <li>Cookies</li>\n                    </ul>\n                </footer>\n            </div>\n        </div>\n    </div>\n</div>\n<!-- CAPACITY INSIGHTS BLOCK-->\n<div *ngIf=\"CI\">\n    <div class=\"layout-wrapper layout-menu-horizontal\">\n        <!-- <div class=\"progressbar\">\n            <ng2-slim-loading-bar [color]=\"'#ffbc1c'\"></ng2-slim-loading-bar>\n    </div>  -->\n        <app-topbar></app-topbar>\n\n        <div class=\"layout-content\"><router-outlet></router-outlet></div>\n        <app-footer class=\"margin-top-sm\"></app-footer>\n    </div>\n</div>\n<!-- UNDER CONSTUCTION BLOCK-->\n<div *ngIf=\"UnderConstruction\">\n    <div class=\"row\">\n        <div class=\"col-sm-12\">\n            <img\n                src=\"../assets/layout/images/Webpage-under-construction.jpeg\"\n                class=\"img-fluid img-notauth\"\n            />\n        </div>\n    </div>\n    <div class=\"row margin-top-sm\">\n        <div class=\"col-sm-12 margin-top-sm\">\n            <p class=\"text-center notauth-text\">\n                The webpage is under construction for the time being. You can\n                click on your back button to go back to the home page.\n            </p>\n        </div>\n    </div>\n</div>\n<p-dialog\n    header=\"Access Denied\"\n    [(visible)]=\"displaydialog\"\n    [modal]=\"true\"\n    [responsive]=\"true\"\n    [style]=\"{ width: '700px' }\"\n    [minY]=\"70\"\n    [maximizable]=\"false\"\n    [baseZIndex]=\"10000\"\n>\n    <div style=\"margin-top:20px;margin-bottom:20px\">\n        <span style=\"font-size: 15px;margin:60px\" class=\"text-modal\"\n            >Please enter a valid MID.</span\n        >\n    </div>\n</p-dialog>\n\n<p-dialog\n    header=\"Access Denied\"\n    [(visible)]=\"dialogEnterMID\"\n    [modal]=\"true\"\n    [responsive]=\"true\"\n    [style]=\"{ width: '700px' }\"\n    [minY]=\"70\"\n    [maximizable]=\"false\"\n    [baseZIndex]=\"10000\"\n>\n    <div style=\"margin-top:20px;margin-bottom:20px\">\n        <span style=\"font-size: 15px;margin:60px\" class=\"text-modal\"\n            >Please enter MID</span\n        >\n    </div>\n</p-dialog>\n\n<p-dialog\n    header=\"Access Denied\"\n    [(visible)]=\"dialogValidMID\"\n    [modal]=\"true\"\n    [responsive]=\"true\"\n    [style]=\"{ width: '700px' }\"\n    [minY]=\"70\"\n    [maximizable]=\"false\"\n    [baseZIndex]=\"10000\"\n>\n    <div style=\"margin-top:20px;margin-bottom:20px\">\n        <span style=\"font-size: 15px;margin:60px\" class=\"text-modal\"\n            >This MID does not exist.</span\n        >\n    </div>\n</p-dialog>\n"

/***/ }),

/***/ "./src/app/app.component.ts":
/*!**********************************!*\
  !*** ./src/app/app.component.ts ***!
  \**********************************/
/*! exports provided: AppComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppComponent", function() { return AppComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _Modules_FetaureModules_admin_Services_admin_service_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./Modules/FetaureModules/admin/Services/admin-service.service */ "./src/app/Modules/FetaureModules/admin/Services/admin-service.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _Services_sharedservice_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./Services/sharedservice.service */ "./src/app/Services/sharedservice.service.ts");
/* harmony import */ var ng2_slim_loading_bar__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ng2-slim-loading-bar */ "./node_modules/ng2-slim-loading-bar/index.js");
/* harmony import */ var _Services_httprequest_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./Services/httprequest.service */ "./src/app/Services/httprequest.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var AppComponent = /** @class */ (function () {
    function AppComponent(data, router, slimLoadingBarService, sharedData, _http) {
        this.data = data;
        this.router = router;
        this.slimLoadingBarService = slimLoadingBarService;
        this.sharedData = sharedData;
        this._http = _http;
        this.ShowCapacityInsights = true;
        this.menuMode = 'horizontal';
        this.menuActive = true;
        this.topbarMenuActive = false;
        this.CI = false;
        this.UnderConstruction = false;
        this.showMessage = false;
        this.displaydialog = false;
        this.dialogEnterMID = false;
        this.dialogValidMID = false;
    }
    // Function called when Launch button is clicked
    AppComponent.prototype.ShowCI = function () {
        var _this = this;
        if (this.midOfUser != null && this.midOfUser !== '') {
            this.capsMID = this.midOfUser;
            this.capsMID = this.capsMID.toUpperCase();
            var startsWithDigit = new RegExp('^\\d{7}');
            var startsWithM = new RegExp('^[Mm]{1}\\d{7}');
            this.result =
                startsWithDigit.test(this.capsMID) ||
                    startsWithM.test(this.capsMID);
            if (this.result) {
                this.slimLoadingBarService.start();
                // API call to get user details
                this.data.getUserDetails(this.capsMID).subscribe(function (data) {
                    _this.ShowCapacityInsights = false;
                    _this.CI = true;
                    _this.sharedData.token.subscribe(function (x) {
                        _this.sharedData.tokenIDOfLoggedUser = x;
                    });
                    _this.sharedData.mid.subscribe(function (x) {
                        _this.sharedData.midOfLoggedInUser = x;
                    });
                    _this.sharedData.roleID.subscribe(function (x) {
                        _this.sharedData.roleIDOfLoggedInUser = x;
                    });
                    var decryptedData = _this.sharedData.decryptingData(data);
                    _this.splitString = decryptedData.split(',', 5);
                    // Saving user details in Sharedservcie
                    _this.sharedData.showUserDetails(_this.splitString);
                    // tslint:disable-next-line:no-shadowed-variable
                    // this.data.getRole(this.sharedData.midOfLoggedInUser).subscribe(
                    //     // tslint:disable-next-line:no-shadowed-variable
                    //     (data: any) => {
                    //         const decryptedRole = this.sharedData.decryptingData(data);
                    //         this.sharedData.showRoleName(decryptedRole); // Saving roleID in SharedService
                    // If roleID is 1( Admin) and competency>=C7, redirect to CapacityInsights page, else Dashboard page
                    var EligibleCompetency = [
                        'C7',
                        'C8',
                        'C9.1',
                        'C9.2',
                        'C10',
                        'C11',
                        'C12'
                    ];
                    // Redirect to Capacity report page
                    if (_this.sharedData.roleIDOfLoggedInUser ===
                        '1' ||
                        EligibleCompetency.includes(_this.sharedData.competencyOfLoggedInUser)) {
                        _this.slimLoadingBarService.complete();
                        _this.slimLoadingBarService.stop();
                        _this._http
                            .getCapacityData('/api/capacity/GetCapacity?MID=' + _this.sharedData.midOfLoggedInUser)
                            .subscribe(
                        // tslint:disable-next-line:no-shadowed-variable
                        function (data) {
                            _this._http.storeCpacityData(data);
                            _this.router.navigate(['/CapacityInsights']);
                        }, function (error) {
                            if (error.status === 0) {
                                _this.router.navigate(['/core/ServerDown']);
                            }
                            else if (error.status === 401) {
                                _this.router.navigate(['/core/AccessDenied']);
                            }
                            else {
                                _this.router.navigate(['/core/InternalServerError']);
                            }
                        });
                    }
                    else {
                        _this.slimLoadingBarService.complete();
                        _this.slimLoadingBarService.stop();
                        _this.router.navigate(['/dashboard']);
                    }
                }, function (error) {
                    if (error.status === 0) {
                        _this.ShowCapacityInsights = false;
                        _this.CI = true;
                        _this.router.navigate(['/core/ServerDown']);
                    }
                    else if (error.status === 401) {
                        _this.ShowCapacityInsights = false;
                        _this.CI = true;
                        _this.router.navigate(['/core/AccessDenied']);
                    }
                    else if (error.status === 404) {
                        _this.ShowCapacityInsights = false;
                        _this.CI = true;
                        _this.router.navigate(['/core/InternalServerError']);
                    }
                    else if (error.status === 501) {
                        _this.dialogValidMID = true;
                    }
                    else {
                        _this.router.navigate([
                            '/core/InternalServerError'
                        ]);
                    }
                });
                //     (error: any) => {
                //         if (error.status === 0) {
                //             this.ShowCapacityInsights = false;
                //             this.CI = true;
                //             this.router.navigate(['/core/ServerDown']);
                //         } else if (error.status === 404) {
                //             this.ShowCapacityInsights = false;
                //             this.CI = true;
                //             this.router.navigate(['/core/InternalServerError']);
                //         } else if (error.status === 401) {
                //                     this.ShowCapacityInsights = false;
                //                     this.CI = true;
                //                     this.router.navigate(['/core/AccessDenied']);
                //             }
                //         if (error.status === 501) {
                //             this.dialogValidMID = true;
                //         }
                //     }
                // );
            }
            else {
                this.displaydialog = true;
            }
        }
        else {
            this.dialogEnterMID = true;
        }
    };
    /// Show dialog box
    AppComponent.prototype.showDialog = function () {
        this.displaydialog = true;
    };
    AppComponent.prototype.ShowUnderConstruction = function () {
        this.ShowCapacityInsights = false;
        this.UnderConstruction = true;
    };
    // tslint:disable-next-line:use-life-cycle-interface
    AppComponent.prototype.ngOnInit = function () { };
    AppComponent.prototype.onMenuButtonClick = function (event) {
        this.menuButtonClick = true;
        this.menuActive = !this.menuActive;
        event.preventDefault();
    };
    AppComponent.prototype.onTopbarMenuButtonClick = function (event) {
        this.topbarMenuButtonClick = true;
        this.topbarMenuActive = !this.topbarMenuActive;
        event.preventDefault();
    };
    AppComponent.prototype.onTopbarItemClick = function (event, item) {
        this.topbarMenuButtonClick = true;
        if (this.activeTopbarItem === item) {
            this.activeTopbarItem = null;
        }
        else {
            this.activeTopbarItem = item;
        }
        event.preventDefault();
    };
    AppComponent.prototype.onTopbarSubItemClick = function (event) {
        event.preventDefault();
    };
    AppComponent.prototype.onLayoutClick = function () {
        if (!this.menuButtonClick && !this.menuClick) {
            if (this.menuMode === 'horizontal') {
                this.resetMenu = true;
            }
            if (this.isMobile() ||
                this.menuMode === 'overlay' ||
                this.menuMode === 'popup') {
                this.menuActive = false;
            }
            this.menuHoverActive = false;
        }
        if (!this.topbarMenuButtonClick) {
            this.activeTopbarItem = null;
            this.topbarMenuActive = false;
        }
        this.menuButtonClick = false;
        this.menuClick = false;
        this.topbarMenuButtonClick = false;
    };
    AppComponent.prototype.onMenuClick = function () {
        this.menuClick = true;
        this.resetMenu = false;
    };
    AppComponent.prototype.isMobile = function () {
        return window.innerWidth < 1025;
    };
    AppComponent.prototype.isHorizontal = function () {
        return this.menuMode === 'horizontal';
    };
    AppComponent.prototype.isTablet = function () {
        var width = window.innerWidth;
        return width <= 1024 && width > 640;
    };
    AppComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-root',
            template: __webpack_require__(/*! ./app.component.html */ "./src/app/app.component.html")
        }),
        __metadata("design:paramtypes", [_Modules_FetaureModules_admin_Services_admin_service_service__WEBPACK_IMPORTED_MODULE_1__["AdminServiceService"],
            _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"],
            ng2_slim_loading_bar__WEBPACK_IMPORTED_MODULE_4__["SlimLoadingBarService"],
            _Services_sharedservice_service__WEBPACK_IMPORTED_MODULE_3__["SharedserviceService"],
            _Services_httprequest_service__WEBPACK_IMPORTED_MODULE_5__["HTTPRequestService"]])
    ], AppComponent);
    return AppComponent;
}());



/***/ }),

/***/ "./src/app/app.menu.component.html":
/*!*****************************************!*\
  !*** ./src/app/app.menu.component.html ***!
  \*****************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div\n    class=\"layout-menu-wrapper\"\n    [ngClass]=\"{ 'layout-menu-wrapper-active': app.menuActive }\"\n    (click)=\"onMenuClick()\"\n>\n    <p-scrollPanel #layoutMenuScroller [style]=\"{ height: '100%' }\">\n        <div class=\"layout-menu-container\">\n            <ul\n                app-submenu\n                [item]=\"model\"\n                root=\"true\"\n                class=\"layout-menu\"\n                visible=\"true\"\n                [reset]=\"reset\"\n                parentActive=\"true\"\n            ></ul>\n        </div>\n    </p-scrollPanel>\n</div>\n"

/***/ }),

/***/ "./src/app/app.menu.component.ts":
/*!***************************************!*\
  !*** ./src/app/app.menu.component.ts ***!
  \***************************************/
/*! exports provided: AppMenuComponent, AppSubMenuComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppMenuComponent", function() { return AppMenuComponent; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppSubMenuComponent", function() { return AppSubMenuComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_animations__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/animations */ "./node_modules/@angular/animations/fesm5/animations.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var primeng_primeng__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! primeng/primeng */ "./node_modules/primeng/primeng.js");
/* harmony import */ var primeng_primeng__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(primeng_primeng__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var _app_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./app.component */ "./src/app/app.component.ts");
/* harmony import */ var _Services_sharedservice_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./Services/sharedservice.service */ "./src/app/Services/sharedservice.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







var AppMenuComponent = /** @class */ (function () {
    function AppMenuComponent(app, route, sharedData) {
        this.app = app;
        this.route = route;
        this.sharedData = sharedData;
    }
    AppMenuComponent.prototype.ngOnInit = function () {
        var _this = this;
        // Subscribing to get the roleID of User
        this.sharedData.roleID.subscribe(function (x) {
            _this.roleIDOfUser = x;
            // if (this.roleIDOfUser != null && this.competencyOfUser != null) {
            //     this.displayTabs();
            // }
        });
        this.sharedData.competency.subscribe(function (x) {
            _this.competencyOfUser = x;
        });
        if (this.roleIDOfUser != null && this.competencyOfUser != null) {
            this.displayTabs();
        }
    };
    AppMenuComponent.prototype.displayTabs = function () {
        var _this = this;
        var EligibleCompetency = [
            'C7',
            'C8',
            'C9.1',
            'C9.2',
            'C10',
            'C11',
            'C12'
        ];
        // If roleID=1 and competency>=C7  , then display 'CapacityReport' and 'Admin' tab else hide the tabs
        if (this.sharedData.roleIDOfLoggedInUser === '1' ||
            EligibleCompetency.includes(this.sharedData.competencyOfLoggedInUser)) {
            this.model = [
                {
                    label: 'Capacity Report',
                    icon: 'fa fa-fw fa-home',
                    command: function (event) { return _this.route.navigate(['/CapacityInsights']); }
                },
                {
                    label: 'Dashboard',
                    icon: 'fa fa-fw fa-pie-chart',
                    command: function (event) { return _this.route.navigate(['/dashboard']); }
                },
                {
                    label: 'Admin',
                    icon: 'fa fa-fw fa-lock',
                    // items: [
                    //     {label: 'Add an admin', icon: 'fa fa-fw fa-user-plus',
                    command: function (event) { return _this.route.navigate(['/admin']); }
                    //     },
                    // ]
                },
                {
                    label: 'Layout Colors',
                    icon: 'fa fa-fw fa-magic',
                    items: [
                        {
                            label: 'Flat',
                            icon: 'fa fa-fw fa-circle',
                            badge: 3,
                            badgeStyleClass: 'blue-badge',
                            items: [
                                {
                                    label: 'Dark',
                                    icon: 'fa fa-fw fa-paint-brush',
                                    command: function (event) {
                                        _this.changeLayout('dark');
                                    }
                                },
                                {
                                    label: 'Blue',
                                    icon: 'fa fa-fw fa-paint-brush',
                                    command: function (event) {
                                        _this.changeLayout('blue');
                                    }
                                },
                                {
                                    label: 'Blue Grey',
                                    icon: 'fa fa-fw fa-paint-brush',
                                    command: function (event) {
                                        _this.changeLayout('bluegrey');
                                    }
                                }
                            ]
                        },
                        {
                            label: 'Special',
                            icon: 'fa fa-fw fa-fire',
                            badge: 1,
                            badgeStyleClass: 'blue-badge',
                            items: [
                                {
                                    label: 'Cosmic',
                                    icon: 'fa fa-fw fa-paint-brush',
                                    command: function (event) {
                                        _this.changeLayout('cosmic');
                                    }
                                }
                            ]
                        }
                    ]
                }
            ];
        }
        else {
            this.model = [
                {
                    label: 'Dashboard',
                    icon: 'fa fa-fw fa-pie-chart',
                    command: function (event) { return _this.route.navigate(['/dashboard']); }
                },
                {
                    label: 'Layout Colors',
                    icon: 'fa fa-fw fa-magic',
                    items: [
                        {
                            label: 'Flat',
                            icon: 'fa fa-fw fa-circle',
                            badge: 3,
                            badgeStyleClass: 'blue-badge',
                            items: [
                                {
                                    label: 'Dark',
                                    icon: 'fa fa-fw fa-paint-brush',
                                    command: function (event) {
                                        _this.changeLayout('dark');
                                    }
                                },
                                {
                                    label: 'Blue',
                                    icon: 'fa fa-fw fa-paint-brush',
                                    command: function (event) {
                                        _this.changeLayout('blue');
                                    }
                                },
                                {
                                    label: 'Blue Grey',
                                    icon: 'fa fa-fw fa-paint-brush',
                                    command: function (event) {
                                        _this.changeLayout('bluegrey');
                                    }
                                }
                            ]
                        },
                        {
                            label: 'Special',
                            icon: 'fa fa-fw fa-fire',
                            badge: 1,
                            badgeStyleClass: 'blue-badge',
                            items: [
                                {
                                    label: 'Cosmic',
                                    icon: 'fa fa-fw fa-paint-brush',
                                    command: function (event) {
                                        _this.changeLayout('cosmic');
                                    }
                                }
                            ]
                        }
                    ]
                }
            ];
        }
    };
    AppMenuComponent.prototype.ngAfterViewInit = function () {
        var _this = this;
        setTimeout(function () {
            _this.layoutMenuScrollerViewChild.moveBar();
        }, 100);
    };
    AppMenuComponent.prototype.changeTheme = function (theme) {
        var themeLink = (document.getElementById('theme-css'));
        themeLink.href = 'assets/theme/theme-' + theme + '.css';
    };
    AppMenuComponent.prototype.changeLayout = function (layout) {
        var layoutLink = (document.getElementById('layout-css'));
        layoutLink.href = 'assets/layout/css/layout-' + layout + '.css';
    };
    AppMenuComponent.prototype.onMenuClick = function () {
        var _this = this;
        if (!this.app.isHorizontal()) {
            setTimeout(function () {
                _this.layoutMenuScrollerViewChild.moveBar();
            }, 450);
        }
        this.app.onMenuClick();
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata("design:type", Boolean)
    ], AppMenuComponent.prototype, "reset", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])('layoutMenuScroller'),
        __metadata("design:type", primeng_primeng__WEBPACK_IMPORTED_MODULE_4__["ScrollPanel"])
    ], AppMenuComponent.prototype, "layoutMenuScrollerViewChild", void 0);
    AppMenuComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-menu',
            template: __webpack_require__(/*! ./app.menu.component.html */ "./src/app/app.menu.component.html")
        }),
        __metadata("design:paramtypes", [_app_component__WEBPACK_IMPORTED_MODULE_5__["AppComponent"],
            _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"],
            _Services_sharedservice_service__WEBPACK_IMPORTED_MODULE_6__["SharedserviceService"]])
    ], AppMenuComponent);
    return AppMenuComponent;
}());

var AppSubMenuComponent = /** @class */ (function () {
    function AppSubMenuComponent(app, router, location, appMenu) {
        this.app = app;
        this.router = router;
        this.location = location;
        this.appMenu = appMenu;
    }
    AppSubMenuComponent.prototype.itemClick = function (event, item, index) {
        var _this = this;
        if (this.root) {
            this.app.menuHoverActive = !this.app.menuHoverActive;
        }
        // avoid processing disabled items
        if (item.disabled) {
            event.preventDefault();
            return true;
        }
        // activate current item and deactivate active sibling if any
        if (item.routerLink || item.items || item.command || item.url) {
            this.activeIndex = this.activeIndex === index ? null : index;
        }
        // execute command
        if (item.command) {
            item.command({ originalEvent: event, item: item });
        }
        // prevent hash change
        if (item.items || (!item.url && !item.routerLink)) {
            setTimeout(function () {
                _this.appMenu.layoutMenuScrollerViewChild.moveBar();
            }, 450);
            event.preventDefault();
        }
        // hide menu
        if (!item.items) {
            if (this.app.menuMode === 'horizontal') {
                this.app.resetMenu = true;
            }
            else {
                this.app.resetMenu = false;
            }
            if (this.app.isMobile() ||
                this.app.menuMode === 'overlay' ||
                this.app.menuMode === 'popup') {
                this.app.menuActive = false;
            }
            this.app.menuHoverActive = false;
        }
    };
    AppSubMenuComponent.prototype.onMouseEnter = function (index) {
        if (this.root &&
            this.app.menuHoverActive &&
            this.app.isHorizontal() &&
            !this.app.isMobile() &&
            !this.app.isTablet()) {
            this.activeIndex = index;
        }
    };
    AppSubMenuComponent.prototype.isActive = function (index) {
        return this.activeIndex === index;
    };
    Object.defineProperty(AppSubMenuComponent.prototype, "reset", {
        get: function () {
            return this._reset;
        },
        set: function (val) {
            this._reset = val;
            if (this._reset && this.app.menuMode === 'horizontal') {
                this.activeIndex = null;
            }
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(AppSubMenuComponent.prototype, "parentActive", {
        get: function () {
            return this._parentActive;
        },
        set: function (val) {
            this._parentActive = val;
            if (!this._parentActive) {
                this.activeIndex = null;
            }
        },
        enumerable: true,
        configurable: true
    });
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata("design:type", Object)
    ], AppSubMenuComponent.prototype, "item", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata("design:type", Boolean)
    ], AppSubMenuComponent.prototype, "root", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata("design:type", Boolean)
    ], AppSubMenuComponent.prototype, "visible", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata("design:type", Boolean),
        __metadata("design:paramtypes", [Boolean])
    ], AppSubMenuComponent.prototype, "reset", null);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata("design:type", Boolean),
        __metadata("design:paramtypes", [Boolean])
    ], AppSubMenuComponent.prototype, "parentActive", null);
    AppSubMenuComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            /* tslint:disable:component-selector */
            selector: '[app-submenu]',
            /* tslint:enable:component-selector */
            template: "\n        <ng-template\n            ngFor\n            let-child\n            let-i=\"index\"\n            [ngForOf]=\"root ? item : item.items\"\n        >\n            <li [ngClass]=\"{ 'active-menuitem': isActive(i) }\">\n                <a\n                    [href]=\"child.url || '#'\"\n                    (click)=\"itemClick($event, child, i)\"\n                    *ngIf=\"!child.routerLink\"\n                    [attr.tabindex]=\"!visible ? '-1' : null\"\n                    [attr.target]=\"child.target\"\n                    (mouseenter)=\"onMouseEnter(i)\"\n                >\n                    <i [ngClass]=\"child.icon\"></i>\n                    <span>{{ child.label }}</span>\n                    <i\n                        class=\"fa fa-fw fa-angle-down layout-submenu-toggler\"\n                        *ngIf=\"child.items\"\n                    ></i>\n                    <span\n                        class=\"menuitem-badge\"\n                        *ngIf=\"child.badge\"\n                        [ngClass]=\"child.badgeStyleClass\"\n                        >{{ child.badge }}</span\n                    >\n                </a>\n\n                <a\n                    (click)=\"itemClick($event, child, i)\"\n                    *ngIf=\"child.routerLink\"\n                    [routerLink]=\"child.routerLink\"\n                    routerLinkActive=\"active-menuitem-routerlink\"\n                    [routerLinkActiveOptions]=\"{ exact: true }\"\n                    [attr.tabindex]=\"!visible ? '-1' : null\"\n                    [attr.target]=\"child.target\"\n                    (mouseenter)=\"onMouseEnter(i)\"\n                >\n                    <i [ngClass]=\"child.icon\"></i>\n                    <span>{{ child.label }}</span>\n                    <i class=\"fa fa-fw fa-angle-down\" *ngIf=\"child.items\"></i>\n                    <span\n                        class=\"menuitem-badge\"\n                        *ngIf=\"child.badge\"\n                        [ngClass]=\"child.badgeStyleClass\"\n                        >{{ child.badge }}</span\n                    >\n                </a>\n                <ul\n                    app-submenu\n                    [item]=\"child\"\n                    *ngIf=\"child.items\"\n                    [visible]=\"isActive(i)\"\n                    [reset]=\"reset\"\n                    [parentActive]=\"isActive(i)\"\n                    [@children]=\"isActive(i) ? 'visible' : 'hidden'\"\n                ></ul>\n            </li>\n        </ng-template>\n    ",
            animations: [
                Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["trigger"])('children', [
                    Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["state"])('visible', Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["style"])({
                        height: '*'
                    })),
                    Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["state"])('hidden', Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["style"])({
                        height: '0px'
                    })),
                    Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["transition"])('visible => hidden', Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["animate"])('400ms cubic-bezier(0.86, 0, 0.07, 1)')),
                    Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["transition"])('hidden => visible', Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["animate"])('400ms cubic-bezier(0.86, 0, 0.07, 1)'))
                ])
            ]
        }),
        __metadata("design:paramtypes", [_app_component__WEBPACK_IMPORTED_MODULE_5__["AppComponent"],
            _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"],
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["Location"],
            AppMenuComponent])
    ], AppSubMenuComponent);
    return AppSubMenuComponent;
}());



/***/ }),

/***/ "./src/app/app.module.ts":
/*!*******************************!*\
  !*** ./src/app/app.module.ts ***!
  \*******************************/
/*! exports provided: AppModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppModule", function() { return AppModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/platform-browser */ "./node_modules/@angular/platform-browser/fesm5/platform-browser.js");
/* harmony import */ var _angular_platform_browser_animations__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/platform-browser/animations */ "./node_modules/@angular/platform-browser/fesm5/animations.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _app_routing_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./app-routing.module */ "./src/app/app-routing.module.ts");
/* harmony import */ var primeng_accordion__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! primeng/accordion */ "./node_modules/primeng/accordion.js");
/* harmony import */ var primeng_accordion__WEBPACK_IMPORTED_MODULE_8___default = /*#__PURE__*/__webpack_require__.n(primeng_accordion__WEBPACK_IMPORTED_MODULE_8__);
/* harmony import */ var primeng_autocomplete__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! primeng/autocomplete */ "./node_modules/primeng/autocomplete.js");
/* harmony import */ var primeng_autocomplete__WEBPACK_IMPORTED_MODULE_9___default = /*#__PURE__*/__webpack_require__.n(primeng_autocomplete__WEBPACK_IMPORTED_MODULE_9__);
/* harmony import */ var primeng_breadcrumb__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! primeng/breadcrumb */ "./node_modules/primeng/breadcrumb.js");
/* harmony import */ var primeng_breadcrumb__WEBPACK_IMPORTED_MODULE_10___default = /*#__PURE__*/__webpack_require__.n(primeng_breadcrumb__WEBPACK_IMPORTED_MODULE_10__);
/* harmony import */ var primeng_button__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! primeng/button */ "./node_modules/primeng/button.js");
/* harmony import */ var primeng_button__WEBPACK_IMPORTED_MODULE_11___default = /*#__PURE__*/__webpack_require__.n(primeng_button__WEBPACK_IMPORTED_MODULE_11__);
/* harmony import */ var primeng_calendar__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! primeng/calendar */ "./node_modules/primeng/calendar.js");
/* harmony import */ var primeng_calendar__WEBPACK_IMPORTED_MODULE_12___default = /*#__PURE__*/__webpack_require__.n(primeng_calendar__WEBPACK_IMPORTED_MODULE_12__);
/* harmony import */ var primeng_card__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! primeng/card */ "./node_modules/primeng/card.js");
/* harmony import */ var primeng_card__WEBPACK_IMPORTED_MODULE_13___default = /*#__PURE__*/__webpack_require__.n(primeng_card__WEBPACK_IMPORTED_MODULE_13__);
/* harmony import */ var primeng_carousel__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! primeng/carousel */ "./node_modules/primeng/carousel.js");
/* harmony import */ var primeng_carousel__WEBPACK_IMPORTED_MODULE_14___default = /*#__PURE__*/__webpack_require__.n(primeng_carousel__WEBPACK_IMPORTED_MODULE_14__);
/* harmony import */ var primeng_chart__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! primeng/chart */ "./node_modules/primeng/chart.js");
/* harmony import */ var primeng_chart__WEBPACK_IMPORTED_MODULE_15___default = /*#__PURE__*/__webpack_require__.n(primeng_chart__WEBPACK_IMPORTED_MODULE_15__);
/* harmony import */ var primeng_checkbox__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! primeng/checkbox */ "./node_modules/primeng/checkbox.js");
/* harmony import */ var primeng_checkbox__WEBPACK_IMPORTED_MODULE_16___default = /*#__PURE__*/__webpack_require__.n(primeng_checkbox__WEBPACK_IMPORTED_MODULE_16__);
/* harmony import */ var primeng_chips__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(/*! primeng/chips */ "./node_modules/primeng/chips.js");
/* harmony import */ var primeng_chips__WEBPACK_IMPORTED_MODULE_17___default = /*#__PURE__*/__webpack_require__.n(primeng_chips__WEBPACK_IMPORTED_MODULE_17__);
/* harmony import */ var primeng_codehighlighter__WEBPACK_IMPORTED_MODULE_18__ = __webpack_require__(/*! primeng/codehighlighter */ "./node_modules/primeng/codehighlighter.js");
/* harmony import */ var primeng_codehighlighter__WEBPACK_IMPORTED_MODULE_18___default = /*#__PURE__*/__webpack_require__.n(primeng_codehighlighter__WEBPACK_IMPORTED_MODULE_18__);
/* harmony import */ var primeng_confirmdialog__WEBPACK_IMPORTED_MODULE_19__ = __webpack_require__(/*! primeng/confirmdialog */ "./node_modules/primeng/confirmdialog.js");
/* harmony import */ var primeng_confirmdialog__WEBPACK_IMPORTED_MODULE_19___default = /*#__PURE__*/__webpack_require__.n(primeng_confirmdialog__WEBPACK_IMPORTED_MODULE_19__);
/* harmony import */ var primeng_colorpicker__WEBPACK_IMPORTED_MODULE_20__ = __webpack_require__(/*! primeng/colorpicker */ "./node_modules/primeng/colorpicker.js");
/* harmony import */ var primeng_colorpicker__WEBPACK_IMPORTED_MODULE_20___default = /*#__PURE__*/__webpack_require__.n(primeng_colorpicker__WEBPACK_IMPORTED_MODULE_20__);
/* harmony import */ var primeng_contextmenu__WEBPACK_IMPORTED_MODULE_21__ = __webpack_require__(/*! primeng/contextmenu */ "./node_modules/primeng/contextmenu.js");
/* harmony import */ var primeng_contextmenu__WEBPACK_IMPORTED_MODULE_21___default = /*#__PURE__*/__webpack_require__.n(primeng_contextmenu__WEBPACK_IMPORTED_MODULE_21__);
/* harmony import */ var primeng_dataview__WEBPACK_IMPORTED_MODULE_22__ = __webpack_require__(/*! primeng/dataview */ "./node_modules/primeng/dataview.js");
/* harmony import */ var primeng_dataview__WEBPACK_IMPORTED_MODULE_22___default = /*#__PURE__*/__webpack_require__.n(primeng_dataview__WEBPACK_IMPORTED_MODULE_22__);
/* harmony import */ var primeng_dialog__WEBPACK_IMPORTED_MODULE_23__ = __webpack_require__(/*! primeng/dialog */ "./node_modules/primeng/dialog.js");
/* harmony import */ var primeng_dialog__WEBPACK_IMPORTED_MODULE_23___default = /*#__PURE__*/__webpack_require__.n(primeng_dialog__WEBPACK_IMPORTED_MODULE_23__);
/* harmony import */ var primeng_dropdown__WEBPACK_IMPORTED_MODULE_24__ = __webpack_require__(/*! primeng/dropdown */ "./node_modules/primeng/dropdown.js");
/* harmony import */ var primeng_dropdown__WEBPACK_IMPORTED_MODULE_24___default = /*#__PURE__*/__webpack_require__.n(primeng_dropdown__WEBPACK_IMPORTED_MODULE_24__);
/* harmony import */ var primeng_editor__WEBPACK_IMPORTED_MODULE_25__ = __webpack_require__(/*! primeng/editor */ "./node_modules/primeng/editor.js");
/* harmony import */ var primeng_editor__WEBPACK_IMPORTED_MODULE_25___default = /*#__PURE__*/__webpack_require__.n(primeng_editor__WEBPACK_IMPORTED_MODULE_25__);
/* harmony import */ var primeng_fieldset__WEBPACK_IMPORTED_MODULE_26__ = __webpack_require__(/*! primeng/fieldset */ "./node_modules/primeng/fieldset.js");
/* harmony import */ var primeng_fieldset__WEBPACK_IMPORTED_MODULE_26___default = /*#__PURE__*/__webpack_require__.n(primeng_fieldset__WEBPACK_IMPORTED_MODULE_26__);
/* harmony import */ var primeng_fileupload__WEBPACK_IMPORTED_MODULE_27__ = __webpack_require__(/*! primeng/fileupload */ "./node_modules/primeng/fileupload.js");
/* harmony import */ var primeng_fileupload__WEBPACK_IMPORTED_MODULE_27___default = /*#__PURE__*/__webpack_require__.n(primeng_fileupload__WEBPACK_IMPORTED_MODULE_27__);
/* harmony import */ var primeng_galleria__WEBPACK_IMPORTED_MODULE_28__ = __webpack_require__(/*! primeng/galleria */ "./node_modules/primeng/galleria.js");
/* harmony import */ var primeng_galleria__WEBPACK_IMPORTED_MODULE_28___default = /*#__PURE__*/__webpack_require__.n(primeng_galleria__WEBPACK_IMPORTED_MODULE_28__);
/* harmony import */ var primeng_growl__WEBPACK_IMPORTED_MODULE_29__ = __webpack_require__(/*! primeng/growl */ "./node_modules/primeng/growl.js");
/* harmony import */ var primeng_growl__WEBPACK_IMPORTED_MODULE_29___default = /*#__PURE__*/__webpack_require__.n(primeng_growl__WEBPACK_IMPORTED_MODULE_29__);
/* harmony import */ var primeng_inplace__WEBPACK_IMPORTED_MODULE_30__ = __webpack_require__(/*! primeng/inplace */ "./node_modules/primeng/inplace.js");
/* harmony import */ var primeng_inplace__WEBPACK_IMPORTED_MODULE_30___default = /*#__PURE__*/__webpack_require__.n(primeng_inplace__WEBPACK_IMPORTED_MODULE_30__);
/* harmony import */ var primeng_inputmask__WEBPACK_IMPORTED_MODULE_31__ = __webpack_require__(/*! primeng/inputmask */ "./node_modules/primeng/inputmask.js");
/* harmony import */ var primeng_inputmask__WEBPACK_IMPORTED_MODULE_31___default = /*#__PURE__*/__webpack_require__.n(primeng_inputmask__WEBPACK_IMPORTED_MODULE_31__);
/* harmony import */ var primeng_inputswitch__WEBPACK_IMPORTED_MODULE_32__ = __webpack_require__(/*! primeng/inputswitch */ "./node_modules/primeng/inputswitch.js");
/* harmony import */ var primeng_inputswitch__WEBPACK_IMPORTED_MODULE_32___default = /*#__PURE__*/__webpack_require__.n(primeng_inputswitch__WEBPACK_IMPORTED_MODULE_32__);
/* harmony import */ var primeng_inputtext__WEBPACK_IMPORTED_MODULE_33__ = __webpack_require__(/*! primeng/inputtext */ "./node_modules/primeng/inputtext.js");
/* harmony import */ var primeng_inputtext__WEBPACK_IMPORTED_MODULE_33___default = /*#__PURE__*/__webpack_require__.n(primeng_inputtext__WEBPACK_IMPORTED_MODULE_33__);
/* harmony import */ var primeng_inputtextarea__WEBPACK_IMPORTED_MODULE_34__ = __webpack_require__(/*! primeng/inputtextarea */ "./node_modules/primeng/inputtextarea.js");
/* harmony import */ var primeng_inputtextarea__WEBPACK_IMPORTED_MODULE_34___default = /*#__PURE__*/__webpack_require__.n(primeng_inputtextarea__WEBPACK_IMPORTED_MODULE_34__);
/* harmony import */ var primeng_lightbox__WEBPACK_IMPORTED_MODULE_35__ = __webpack_require__(/*! primeng/lightbox */ "./node_modules/primeng/lightbox.js");
/* harmony import */ var primeng_lightbox__WEBPACK_IMPORTED_MODULE_35___default = /*#__PURE__*/__webpack_require__.n(primeng_lightbox__WEBPACK_IMPORTED_MODULE_35__);
/* harmony import */ var primeng_listbox__WEBPACK_IMPORTED_MODULE_36__ = __webpack_require__(/*! primeng/listbox */ "./node_modules/primeng/listbox.js");
/* harmony import */ var primeng_listbox__WEBPACK_IMPORTED_MODULE_36___default = /*#__PURE__*/__webpack_require__.n(primeng_listbox__WEBPACK_IMPORTED_MODULE_36__);
/* harmony import */ var primeng_megamenu__WEBPACK_IMPORTED_MODULE_37__ = __webpack_require__(/*! primeng/megamenu */ "./node_modules/primeng/megamenu.js");
/* harmony import */ var primeng_megamenu__WEBPACK_IMPORTED_MODULE_37___default = /*#__PURE__*/__webpack_require__.n(primeng_megamenu__WEBPACK_IMPORTED_MODULE_37__);
/* harmony import */ var primeng_menu__WEBPACK_IMPORTED_MODULE_38__ = __webpack_require__(/*! primeng/menu */ "./node_modules/primeng/menu.js");
/* harmony import */ var primeng_menu__WEBPACK_IMPORTED_MODULE_38___default = /*#__PURE__*/__webpack_require__.n(primeng_menu__WEBPACK_IMPORTED_MODULE_38__);
/* harmony import */ var primeng_menubar__WEBPACK_IMPORTED_MODULE_39__ = __webpack_require__(/*! primeng/menubar */ "./node_modules/primeng/menubar.js");
/* harmony import */ var primeng_menubar__WEBPACK_IMPORTED_MODULE_39___default = /*#__PURE__*/__webpack_require__.n(primeng_menubar__WEBPACK_IMPORTED_MODULE_39__);
/* harmony import */ var primeng_messages__WEBPACK_IMPORTED_MODULE_40__ = __webpack_require__(/*! primeng/messages */ "./node_modules/primeng/messages.js");
/* harmony import */ var primeng_messages__WEBPACK_IMPORTED_MODULE_40___default = /*#__PURE__*/__webpack_require__.n(primeng_messages__WEBPACK_IMPORTED_MODULE_40__);
/* harmony import */ var primeng_message__WEBPACK_IMPORTED_MODULE_41__ = __webpack_require__(/*! primeng/message */ "./node_modules/primeng/message.js");
/* harmony import */ var primeng_message__WEBPACK_IMPORTED_MODULE_41___default = /*#__PURE__*/__webpack_require__.n(primeng_message__WEBPACK_IMPORTED_MODULE_41__);
/* harmony import */ var primeng_multiselect__WEBPACK_IMPORTED_MODULE_42__ = __webpack_require__(/*! primeng/multiselect */ "./node_modules/primeng/multiselect.js");
/* harmony import */ var primeng_multiselect__WEBPACK_IMPORTED_MODULE_42___default = /*#__PURE__*/__webpack_require__.n(primeng_multiselect__WEBPACK_IMPORTED_MODULE_42__);
/* harmony import */ var primeng_orderlist__WEBPACK_IMPORTED_MODULE_43__ = __webpack_require__(/*! primeng/orderlist */ "./node_modules/primeng/orderlist.js");
/* harmony import */ var primeng_orderlist__WEBPACK_IMPORTED_MODULE_43___default = /*#__PURE__*/__webpack_require__.n(primeng_orderlist__WEBPACK_IMPORTED_MODULE_43__);
/* harmony import */ var primeng_organizationchart__WEBPACK_IMPORTED_MODULE_44__ = __webpack_require__(/*! primeng/organizationchart */ "./node_modules/primeng/organizationchart.js");
/* harmony import */ var primeng_organizationchart__WEBPACK_IMPORTED_MODULE_44___default = /*#__PURE__*/__webpack_require__.n(primeng_organizationchart__WEBPACK_IMPORTED_MODULE_44__);
/* harmony import */ var primeng_overlaypanel__WEBPACK_IMPORTED_MODULE_45__ = __webpack_require__(/*! primeng/overlaypanel */ "./node_modules/primeng/overlaypanel.js");
/* harmony import */ var primeng_overlaypanel__WEBPACK_IMPORTED_MODULE_45___default = /*#__PURE__*/__webpack_require__.n(primeng_overlaypanel__WEBPACK_IMPORTED_MODULE_45__);
/* harmony import */ var primeng_paginator__WEBPACK_IMPORTED_MODULE_46__ = __webpack_require__(/*! primeng/paginator */ "./node_modules/primeng/paginator.js");
/* harmony import */ var primeng_paginator__WEBPACK_IMPORTED_MODULE_46___default = /*#__PURE__*/__webpack_require__.n(primeng_paginator__WEBPACK_IMPORTED_MODULE_46__);
/* harmony import */ var primeng_panel__WEBPACK_IMPORTED_MODULE_47__ = __webpack_require__(/*! primeng/panel */ "./node_modules/primeng/panel.js");
/* harmony import */ var primeng_panel__WEBPACK_IMPORTED_MODULE_47___default = /*#__PURE__*/__webpack_require__.n(primeng_panel__WEBPACK_IMPORTED_MODULE_47__);
/* harmony import */ var primeng_panelmenu__WEBPACK_IMPORTED_MODULE_48__ = __webpack_require__(/*! primeng/panelmenu */ "./node_modules/primeng/panelmenu.js");
/* harmony import */ var primeng_panelmenu__WEBPACK_IMPORTED_MODULE_48___default = /*#__PURE__*/__webpack_require__.n(primeng_panelmenu__WEBPACK_IMPORTED_MODULE_48__);
/* harmony import */ var primeng_password__WEBPACK_IMPORTED_MODULE_49__ = __webpack_require__(/*! primeng/password */ "./node_modules/primeng/password.js");
/* harmony import */ var primeng_password__WEBPACK_IMPORTED_MODULE_49___default = /*#__PURE__*/__webpack_require__.n(primeng_password__WEBPACK_IMPORTED_MODULE_49__);
/* harmony import */ var primeng_picklist__WEBPACK_IMPORTED_MODULE_50__ = __webpack_require__(/*! primeng/picklist */ "./node_modules/primeng/picklist.js");
/* harmony import */ var primeng_picklist__WEBPACK_IMPORTED_MODULE_50___default = /*#__PURE__*/__webpack_require__.n(primeng_picklist__WEBPACK_IMPORTED_MODULE_50__);
/* harmony import */ var primeng_progressbar__WEBPACK_IMPORTED_MODULE_51__ = __webpack_require__(/*! primeng/progressbar */ "./node_modules/primeng/progressbar.js");
/* harmony import */ var primeng_progressbar__WEBPACK_IMPORTED_MODULE_51___default = /*#__PURE__*/__webpack_require__.n(primeng_progressbar__WEBPACK_IMPORTED_MODULE_51__);
/* harmony import */ var primeng_radiobutton__WEBPACK_IMPORTED_MODULE_52__ = __webpack_require__(/*! primeng/radiobutton */ "./node_modules/primeng/radiobutton.js");
/* harmony import */ var primeng_radiobutton__WEBPACK_IMPORTED_MODULE_52___default = /*#__PURE__*/__webpack_require__.n(primeng_radiobutton__WEBPACK_IMPORTED_MODULE_52__);
/* harmony import */ var primeng_rating__WEBPACK_IMPORTED_MODULE_53__ = __webpack_require__(/*! primeng/rating */ "./node_modules/primeng/rating.js");
/* harmony import */ var primeng_rating__WEBPACK_IMPORTED_MODULE_53___default = /*#__PURE__*/__webpack_require__.n(primeng_rating__WEBPACK_IMPORTED_MODULE_53__);
/* harmony import */ var primeng_schedule__WEBPACK_IMPORTED_MODULE_54__ = __webpack_require__(/*! primeng/schedule */ "./node_modules/primeng/schedule.js");
/* harmony import */ var primeng_schedule__WEBPACK_IMPORTED_MODULE_54___default = /*#__PURE__*/__webpack_require__.n(primeng_schedule__WEBPACK_IMPORTED_MODULE_54__);
/* harmony import */ var primeng_scrollpanel__WEBPACK_IMPORTED_MODULE_55__ = __webpack_require__(/*! primeng/scrollpanel */ "./node_modules/primeng/scrollpanel.js");
/* harmony import */ var primeng_scrollpanel__WEBPACK_IMPORTED_MODULE_55___default = /*#__PURE__*/__webpack_require__.n(primeng_scrollpanel__WEBPACK_IMPORTED_MODULE_55__);
/* harmony import */ var primeng_selectbutton__WEBPACK_IMPORTED_MODULE_56__ = __webpack_require__(/*! primeng/selectbutton */ "./node_modules/primeng/selectbutton.js");
/* harmony import */ var primeng_selectbutton__WEBPACK_IMPORTED_MODULE_56___default = /*#__PURE__*/__webpack_require__.n(primeng_selectbutton__WEBPACK_IMPORTED_MODULE_56__);
/* harmony import */ var primeng_slidemenu__WEBPACK_IMPORTED_MODULE_57__ = __webpack_require__(/*! primeng/slidemenu */ "./node_modules/primeng/slidemenu.js");
/* harmony import */ var primeng_slidemenu__WEBPACK_IMPORTED_MODULE_57___default = /*#__PURE__*/__webpack_require__.n(primeng_slidemenu__WEBPACK_IMPORTED_MODULE_57__);
/* harmony import */ var primeng_slider__WEBPACK_IMPORTED_MODULE_58__ = __webpack_require__(/*! primeng/slider */ "./node_modules/primeng/slider.js");
/* harmony import */ var primeng_slider__WEBPACK_IMPORTED_MODULE_58___default = /*#__PURE__*/__webpack_require__.n(primeng_slider__WEBPACK_IMPORTED_MODULE_58__);
/* harmony import */ var primeng_spinner__WEBPACK_IMPORTED_MODULE_59__ = __webpack_require__(/*! primeng/spinner */ "./node_modules/primeng/spinner.js");
/* harmony import */ var primeng_spinner__WEBPACK_IMPORTED_MODULE_59___default = /*#__PURE__*/__webpack_require__.n(primeng_spinner__WEBPACK_IMPORTED_MODULE_59__);
/* harmony import */ var primeng_splitbutton__WEBPACK_IMPORTED_MODULE_60__ = __webpack_require__(/*! primeng/splitbutton */ "./node_modules/primeng/splitbutton.js");
/* harmony import */ var primeng_splitbutton__WEBPACK_IMPORTED_MODULE_60___default = /*#__PURE__*/__webpack_require__.n(primeng_splitbutton__WEBPACK_IMPORTED_MODULE_60__);
/* harmony import */ var primeng_steps__WEBPACK_IMPORTED_MODULE_61__ = __webpack_require__(/*! primeng/steps */ "./node_modules/primeng/steps.js");
/* harmony import */ var primeng_steps__WEBPACK_IMPORTED_MODULE_61___default = /*#__PURE__*/__webpack_require__.n(primeng_steps__WEBPACK_IMPORTED_MODULE_61__);
/* harmony import */ var primeng_tabmenu__WEBPACK_IMPORTED_MODULE_62__ = __webpack_require__(/*! primeng/tabmenu */ "./node_modules/primeng/tabmenu.js");
/* harmony import */ var primeng_tabmenu__WEBPACK_IMPORTED_MODULE_62___default = /*#__PURE__*/__webpack_require__.n(primeng_tabmenu__WEBPACK_IMPORTED_MODULE_62__);
/* harmony import */ var primeng_table__WEBPACK_IMPORTED_MODULE_63__ = __webpack_require__(/*! primeng/table */ "./node_modules/primeng/table.js");
/* harmony import */ var primeng_table__WEBPACK_IMPORTED_MODULE_63___default = /*#__PURE__*/__webpack_require__.n(primeng_table__WEBPACK_IMPORTED_MODULE_63__);
/* harmony import */ var primeng_tabview__WEBPACK_IMPORTED_MODULE_64__ = __webpack_require__(/*! primeng/tabview */ "./node_modules/primeng/tabview.js");
/* harmony import */ var primeng_tabview__WEBPACK_IMPORTED_MODULE_64___default = /*#__PURE__*/__webpack_require__.n(primeng_tabview__WEBPACK_IMPORTED_MODULE_64__);
/* harmony import */ var primeng_terminal__WEBPACK_IMPORTED_MODULE_65__ = __webpack_require__(/*! primeng/terminal */ "./node_modules/primeng/terminal.js");
/* harmony import */ var primeng_terminal__WEBPACK_IMPORTED_MODULE_65___default = /*#__PURE__*/__webpack_require__.n(primeng_terminal__WEBPACK_IMPORTED_MODULE_65__);
/* harmony import */ var primeng_tieredmenu__WEBPACK_IMPORTED_MODULE_66__ = __webpack_require__(/*! primeng/tieredmenu */ "./node_modules/primeng/tieredmenu.js");
/* harmony import */ var primeng_tieredmenu__WEBPACK_IMPORTED_MODULE_66___default = /*#__PURE__*/__webpack_require__.n(primeng_tieredmenu__WEBPACK_IMPORTED_MODULE_66__);
/* harmony import */ var primeng_toast__WEBPACK_IMPORTED_MODULE_67__ = __webpack_require__(/*! primeng/toast */ "./node_modules/primeng/toast.js");
/* harmony import */ var primeng_toast__WEBPACK_IMPORTED_MODULE_67___default = /*#__PURE__*/__webpack_require__.n(primeng_toast__WEBPACK_IMPORTED_MODULE_67__);
/* harmony import */ var primeng_togglebutton__WEBPACK_IMPORTED_MODULE_68__ = __webpack_require__(/*! primeng/togglebutton */ "./node_modules/primeng/togglebutton.js");
/* harmony import */ var primeng_togglebutton__WEBPACK_IMPORTED_MODULE_68___default = /*#__PURE__*/__webpack_require__.n(primeng_togglebutton__WEBPACK_IMPORTED_MODULE_68__);
/* harmony import */ var primeng_toolbar__WEBPACK_IMPORTED_MODULE_69__ = __webpack_require__(/*! primeng/toolbar */ "./node_modules/primeng/toolbar.js");
/* harmony import */ var primeng_toolbar__WEBPACK_IMPORTED_MODULE_69___default = /*#__PURE__*/__webpack_require__.n(primeng_toolbar__WEBPACK_IMPORTED_MODULE_69__);
/* harmony import */ var primeng_tooltip__WEBPACK_IMPORTED_MODULE_70__ = __webpack_require__(/*! primeng/tooltip */ "./node_modules/primeng/tooltip.js");
/* harmony import */ var primeng_tooltip__WEBPACK_IMPORTED_MODULE_70___default = /*#__PURE__*/__webpack_require__.n(primeng_tooltip__WEBPACK_IMPORTED_MODULE_70__);
/* harmony import */ var primeng_tree__WEBPACK_IMPORTED_MODULE_71__ = __webpack_require__(/*! primeng/tree */ "./node_modules/primeng/tree.js");
/* harmony import */ var primeng_tree__WEBPACK_IMPORTED_MODULE_71___default = /*#__PURE__*/__webpack_require__.n(primeng_tree__WEBPACK_IMPORTED_MODULE_71__);
/* harmony import */ var primeng_treetable__WEBPACK_IMPORTED_MODULE_72__ = __webpack_require__(/*! primeng/treetable */ "./node_modules/primeng/treetable.js");
/* harmony import */ var primeng_treetable__WEBPACK_IMPORTED_MODULE_72___default = /*#__PURE__*/__webpack_require__.n(primeng_treetable__WEBPACK_IMPORTED_MODULE_72__);
/* harmony import */ var _app_component__WEBPACK_IMPORTED_MODULE_73__ = __webpack_require__(/*! ./app.component */ "./src/app/app.component.ts");
/* harmony import */ var _angular_material_button__WEBPACK_IMPORTED_MODULE_74__ = __webpack_require__(/*! @angular/material/button */ "./node_modules/@angular/material/esm5/button.es5.js");
/* harmony import */ var _Modules_FetaureModules_dash_board_dash_board_dashboarddemo_component__WEBPACK_IMPORTED_MODULE_75__ = __webpack_require__(/*! ./Modules/FetaureModules/dash-board/dash-board/dashboarddemo.component */ "./src/app/Modules/FetaureModules/dash-board/dash-board/dashboarddemo.component.ts");
/* harmony import */ var primeng_sidebar__WEBPACK_IMPORTED_MODULE_76__ = __webpack_require__(/*! primeng/sidebar */ "./node_modules/primeng/sidebar.js");
/* harmony import */ var primeng_sidebar__WEBPACK_IMPORTED_MODULE_76___default = /*#__PURE__*/__webpack_require__.n(primeng_sidebar__WEBPACK_IMPORTED_MODULE_76__);
/* harmony import */ var _Modules_shared_shared_module__WEBPACK_IMPORTED_MODULE_77__ = __webpack_require__(/*! ./Modules/shared/shared.module */ "./src/app/Modules/shared/shared.module.ts");
/* harmony import */ var _app_menu_component__WEBPACK_IMPORTED_MODULE_78__ = __webpack_require__(/*! ./app.menu.component */ "./src/app/app.menu.component.ts");
/* harmony import */ var _app_topbar_component__WEBPACK_IMPORTED_MODULE_79__ = __webpack_require__(/*! ./app.topbar.component */ "./src/app/app.topbar.component.ts");
/* harmony import */ var _Modules_FetaureModules_admin_Services_admin_service_service__WEBPACK_IMPORTED_MODULE_80__ = __webpack_require__(/*! ./Modules/FetaureModules/admin/Services/admin-service.service */ "./src/app/Modules/FetaureModules/admin/Services/admin-service.service.ts");
/* harmony import */ var ng2_slim_loading_bar__WEBPACK_IMPORTED_MODULE_81__ = __webpack_require__(/*! ng2-slim-loading-bar */ "./node_modules/ng2-slim-loading-bar/index.js");
/* harmony import */ var _Services_sharedservice_service__WEBPACK_IMPORTED_MODULE_82__ = __webpack_require__(/*! ./Services/sharedservice.service */ "./src/app/Services/sharedservice.service.ts");
/* harmony import */ var _guards_auth_guard__WEBPACK_IMPORTED_MODULE_83__ = __webpack_require__(/*! ../guards/auth.guard */ "./src/guards/auth.guard.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};




// import { HTTP_INTERCEPTORS } from '@angular/common/http';
// import { AdalInterceptor, AdalService } from 'adal-angular4';














































































// import { CapacityReportComponent } from './Modules/FetaureModules/capacity-insights/capacity-report/capacity-report.component'



var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [
                _angular_platform_browser__WEBPACK_IMPORTED_MODULE_4__["BrowserModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormsModule"],
                _app_routing_module__WEBPACK_IMPORTED_MODULE_7__["AppRoutingModule"],
                _angular_common_http__WEBPACK_IMPORTED_MODULE_3__["HttpClientModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"],
                _angular_platform_browser_animations__WEBPACK_IMPORTED_MODULE_5__["BrowserAnimationsModule"],
                primeng_accordion__WEBPACK_IMPORTED_MODULE_8__["AccordionModule"],
                primeng_autocomplete__WEBPACK_IMPORTED_MODULE_9__["AutoCompleteModule"],
                primeng_breadcrumb__WEBPACK_IMPORTED_MODULE_10__["BreadcrumbModule"],
                primeng_button__WEBPACK_IMPORTED_MODULE_11__["ButtonModule"],
                primeng_calendar__WEBPACK_IMPORTED_MODULE_12__["CalendarModule"],
                primeng_card__WEBPACK_IMPORTED_MODULE_13__["CardModule"],
                primeng_carousel__WEBPACK_IMPORTED_MODULE_14__["CarouselModule"],
                primeng_chart__WEBPACK_IMPORTED_MODULE_15__["ChartModule"],
                primeng_checkbox__WEBPACK_IMPORTED_MODULE_16__["CheckboxModule"],
                primeng_chips__WEBPACK_IMPORTED_MODULE_17__["ChipsModule"],
                primeng_codehighlighter__WEBPACK_IMPORTED_MODULE_18__["CodeHighlighterModule"],
                primeng_confirmdialog__WEBPACK_IMPORTED_MODULE_19__["ConfirmDialogModule"],
                primeng_colorpicker__WEBPACK_IMPORTED_MODULE_20__["ColorPickerModule"],
                primeng_contextmenu__WEBPACK_IMPORTED_MODULE_21__["ContextMenuModule"],
                primeng_dataview__WEBPACK_IMPORTED_MODULE_22__["DataViewModule"],
                primeng_dialog__WEBPACK_IMPORTED_MODULE_23__["DialogModule"],
                primeng_dropdown__WEBPACK_IMPORTED_MODULE_24__["DropdownModule"],
                primeng_editor__WEBPACK_IMPORTED_MODULE_25__["EditorModule"],
                primeng_fieldset__WEBPACK_IMPORTED_MODULE_26__["FieldsetModule"],
                primeng_fileupload__WEBPACK_IMPORTED_MODULE_27__["FileUploadModule"],
                primeng_galleria__WEBPACK_IMPORTED_MODULE_28__["GalleriaModule"],
                primeng_growl__WEBPACK_IMPORTED_MODULE_29__["GrowlModule"],
                primeng_inplace__WEBPACK_IMPORTED_MODULE_30__["InplaceModule"],
                primeng_inputmask__WEBPACK_IMPORTED_MODULE_31__["InputMaskModule"],
                primeng_inputswitch__WEBPACK_IMPORTED_MODULE_32__["InputSwitchModule"],
                primeng_inputtext__WEBPACK_IMPORTED_MODULE_33__["InputTextModule"],
                primeng_inputtextarea__WEBPACK_IMPORTED_MODULE_34__["InputTextareaModule"],
                primeng_lightbox__WEBPACK_IMPORTED_MODULE_35__["LightboxModule"],
                primeng_listbox__WEBPACK_IMPORTED_MODULE_36__["ListboxModule"],
                primeng_megamenu__WEBPACK_IMPORTED_MODULE_37__["MegaMenuModule"],
                primeng_menu__WEBPACK_IMPORTED_MODULE_38__["MenuModule"],
                primeng_menubar__WEBPACK_IMPORTED_MODULE_39__["MenubarModule"],
                primeng_message__WEBPACK_IMPORTED_MODULE_41__["MessageModule"],
                primeng_messages__WEBPACK_IMPORTED_MODULE_40__["MessagesModule"],
                primeng_multiselect__WEBPACK_IMPORTED_MODULE_42__["MultiSelectModule"],
                primeng_orderlist__WEBPACK_IMPORTED_MODULE_43__["OrderListModule"],
                primeng_organizationchart__WEBPACK_IMPORTED_MODULE_44__["OrganizationChartModule"],
                primeng_overlaypanel__WEBPACK_IMPORTED_MODULE_45__["OverlayPanelModule"],
                primeng_paginator__WEBPACK_IMPORTED_MODULE_46__["PaginatorModule"],
                primeng_panel__WEBPACK_IMPORTED_MODULE_47__["PanelModule"],
                primeng_panelmenu__WEBPACK_IMPORTED_MODULE_48__["PanelMenuModule"],
                primeng_password__WEBPACK_IMPORTED_MODULE_49__["PasswordModule"],
                primeng_picklist__WEBPACK_IMPORTED_MODULE_50__["PickListModule"],
                primeng_progressbar__WEBPACK_IMPORTED_MODULE_51__["ProgressBarModule"],
                primeng_radiobutton__WEBPACK_IMPORTED_MODULE_52__["RadioButtonModule"],
                primeng_rating__WEBPACK_IMPORTED_MODULE_53__["RatingModule"],
                primeng_schedule__WEBPACK_IMPORTED_MODULE_54__["ScheduleModule"],
                primeng_scrollpanel__WEBPACK_IMPORTED_MODULE_55__["ScrollPanelModule"],
                primeng_selectbutton__WEBPACK_IMPORTED_MODULE_56__["SelectButtonModule"],
                primeng_slidemenu__WEBPACK_IMPORTED_MODULE_57__["SlideMenuModule"],
                primeng_slider__WEBPACK_IMPORTED_MODULE_58__["SliderModule"],
                primeng_spinner__WEBPACK_IMPORTED_MODULE_59__["SpinnerModule"],
                primeng_splitbutton__WEBPACK_IMPORTED_MODULE_60__["SplitButtonModule"],
                primeng_steps__WEBPACK_IMPORTED_MODULE_61__["StepsModule"],
                primeng_table__WEBPACK_IMPORTED_MODULE_63__["TableModule"],
                primeng_tabmenu__WEBPACK_IMPORTED_MODULE_62__["TabMenuModule"],
                primeng_tabview__WEBPACK_IMPORTED_MODULE_64__["TabViewModule"],
                primeng_terminal__WEBPACK_IMPORTED_MODULE_65__["TerminalModule"],
                primeng_tieredmenu__WEBPACK_IMPORTED_MODULE_66__["TieredMenuModule"],
                primeng_toast__WEBPACK_IMPORTED_MODULE_67__["ToastModule"],
                primeng_togglebutton__WEBPACK_IMPORTED_MODULE_68__["ToggleButtonModule"],
                primeng_toolbar__WEBPACK_IMPORTED_MODULE_69__["ToolbarModule"],
                primeng_tooltip__WEBPACK_IMPORTED_MODULE_70__["TooltipModule"],
                primeng_tree__WEBPACK_IMPORTED_MODULE_71__["TreeModule"],
                primeng_treetable__WEBPACK_IMPORTED_MODULE_72__["TreeTableModule"],
                primeng_sidebar__WEBPACK_IMPORTED_MODULE_76__["SidebarModule"],
                _Modules_shared_shared_module__WEBPACK_IMPORTED_MODULE_77__["SharedModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_1__["ReactiveFormsModule"],
                _angular_material_button__WEBPACK_IMPORTED_MODULE_74__["MatButtonModule"],
                ng2_slim_loading_bar__WEBPACK_IMPORTED_MODULE_81__["SlimLoadingBarModule"].forRoot()
            ],
            declarations: [
                _app_topbar_component__WEBPACK_IMPORTED_MODULE_79__["AppTopBarComponent"],
                _app_component__WEBPACK_IMPORTED_MODULE_73__["AppComponent"],
                _app_menu_component__WEBPACK_IMPORTED_MODULE_78__["AppMenuComponent"],
                _app_menu_component__WEBPACK_IMPORTED_MODULE_78__["AppSubMenuComponent"],
                _Modules_FetaureModules_dash_board_dash_board_dashboarddemo_component__WEBPACK_IMPORTED_MODULE_75__["DashboardDemoComponent"]
            ],
            exports: [_app_component__WEBPACK_IMPORTED_MODULE_73__["AppComponent"], _app_menu_component__WEBPACK_IMPORTED_MODULE_78__["AppMenuComponent"], ng2_slim_loading_bar__WEBPACK_IMPORTED_MODULE_81__["SlimLoadingBarModule"]],
            providers: [
                { provide: _angular_common__WEBPACK_IMPORTED_MODULE_6__["LocationStrategy"], useClass: _angular_common__WEBPACK_IMPORTED_MODULE_6__["HashLocationStrategy"] },
                // {provide: HTTP_INTERCEPTORS,
                //     useClass: httpInterceptor,
                //     multi: true},
                _Modules_FetaureModules_admin_Services_admin_service_service__WEBPACK_IMPORTED_MODULE_80__["AdminServiceService"],
                _Services_sharedservice_service__WEBPACK_IMPORTED_MODULE_82__["SharedserviceService"],
                _guards_auth_guard__WEBPACK_IMPORTED_MODULE_83__["AuthGuard"]
                // ,AdalService , { provide: HTTP_INTERCEPTORS, useClass: AdalInterceptor, multi: true },
            ],
            bootstrap: [_app_component__WEBPACK_IMPORTED_MODULE_73__["AppComponent"]]
        })
    ], AppModule);
    return AppModule;
}());



/***/ }),

/***/ "./src/app/app.topbar.component.html":
/*!*******************************************!*\
  !*** ./src/app/app.topbar.component.html ***!
  \*******************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<!-- <mat-toolbar color=\"primary\">\r\n    <span>Capacity Insights</span>\r\n    \r\n    <a style=\"margin-left:1000px;color: white \" routerLink=\"/CapacityInsights/preference\">\r\n        <i class=\"fa fa-cog fa-2x\" aria-hidden=\"true\"></i>\r\n        </a>\r\n    <a  style=\"color: white \" routerLink=\"/CapacityInsights\">\r\n        <i class=\"fa fa-filter fa-2x\" aria-hidden=\"true\"></i>\r\n   </a>\r\n   <a href=\"#\" (click)=\"app.onTopbarMenuButtonClick($event)\">\r\n        <span>AVS\r\n        Campus Mind</span>\r\n        <img alt=\"main logo\" src=\"assets/layout/images/avatar.png\">\r\n    </a>      \r\n  \r\n</mat-toolbar> -->\r\n<div class=\"topbar\">\r\n    <div class=\"topbar-main\">\r\n        <a href=\"#\" id=\"menu-button\" (click)=\"app.onMenuButtonClick($event)\">\r\n            <span class=\"fa fa-bars \"></span>\r\n        </a>\r\n\r\n        <a href=\"/\">\r\n            <!-- <img alt=\"main logo\" src=\"assets/layout/images/logo.png\" class=\"logo\"> -->\r\n            <i class=\"fa fa-angle-right fa-5x logo\" aria-hidden=\"true\"></i>\r\n        </a>\r\n\r\n        <div class=\"app-name\">CapacityInsights</div>\r\n\r\n        <div class=\"ui-toolbar-group-right\">\r\n            <a\r\n                href=\"#\"\r\n                id=\"user-display\"\r\n                (click)=\"app.onTopbarMenuButtonClick($event)\"\r\n            >\r\n                <span class=\"username\">{{ nameOfUserTop }}</span>\r\n                <img\r\n                    class=\"ProfileImage\"\r\n                    src=\"{{ imgUrlHeader + midOfUserTop + imgUrlTail }}\"\r\n                    onError=\"this.src='../assets/images/ProfilePic.png';\"\r\n                />\r\n            </a>\r\n            <a routerLink=\"/CapacityInsights/filter\" id=\"user-display\">\r\n                <i\r\n                    *ngIf=\"DisplayFunnel\"\r\n                    class=\"fa fa-filter fa-2x float-right filter-icon\"\r\n                    aria-hidden=\"true\"\r\n                ></i>\r\n            </a>\r\n            <a routerLink=\"/CapacityInsights/preference\" id=\"user-display\">\r\n                <i\r\n                *ngIf=\"DisplayFunnel\"\r\n                class=\"fa fa-cogs fa-2x float-right filter-icon\"\r\n                aria-hidden=\"true\"\r\n                ></i>\r\n                </a> \r\n            \r\n         \r\n        </div>\r\n        <!-- <a id=\"user-display\">  \r\n         \r\n            <i class=\"fa fa-search fa-2x float-right filter-icon\"></i>\r\n        </a>-->\r\n        <!--<input type=\"text\" pInputText placeholder=\"Search\" />-->\r\n\r\n        <ul\r\n            id=\"topbar-menu\"\r\n            class=\"fadeInDown animated\"\r\n            [ngClass]=\"{ 'topbar-menu-visible': app.topbarMenuActive }\"\r\n        >\r\n            <li\r\n                #profile\r\n                [ngClass]=\"{\r\n                    'menuitem-active': app.activeTopbarItem === profile\r\n                }\"\r\n            >\r\n                <a href=\"#\" (click)=\"app.onTopbarItemClick($event, profile)\">\r\n                    <i class=\"topbar-icon fa fa-fw fa-user\"></i>\r\n                    <span class=\"topbar-item-name\">{{ nameOfUserTop }}</span>\r\n                </a>\r\n            </li>\r\n            <li role=\"menuitem\">\r\n                <a href=\"#\" (click)=\"app.onTopbarItemClick($event, profile)\">\r\n                    <i class=\"topbar-icon fa fa-fw fa-user\"></i>\r\n                    <span class=\"topbar-item-name\">{{ roleName }}</span>\r\n                </a>\r\n            </li>\r\n        </ul>\r\n    </div>\r\n\r\n    <app-menu [reset]=\"app.resetMenu\"></app-menu>\r\n</div>\r\n"

/***/ }),

/***/ "./src/app/app.topbar.component.ts":
/*!*****************************************!*\
  !*** ./src/app/app.topbar.component.ts ***!
  \*****************************************/
/*! exports provided: AppTopBarComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppTopBarComponent", function() { return AppTopBarComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _app_component__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./app.component */ "./src/app/app.component.ts");
/* harmony import */ var _Services_sharedservice_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./Services/sharedservice.service */ "./src/app/Services/sharedservice.service.ts");
/* harmony import */ var _Modules_FetaureModules_admin_Services_admin_service_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./Modules/FetaureModules/admin/Services/admin-service.service */ "./src/app/Modules/FetaureModules/admin/Services/admin-service.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var AppTopBarComponent = /** @class */ (function () {
    function AppTopBarComponent(app, sharedData, data) {
        this.app = app;
        this.sharedData = sharedData;
        this.data = data;
        this.DisplayFunnel = false; // Funnel and settings icon on the topbar
        this.nameOfUserTop = ''; // Name of user on the topbar
        this.midOfUserTop = ''; // Mid of user to get the image of user
        this.competencyOfUser = ''; // Competency of user
        this.imgUrlHeader = 'https://social.mindtree.com/User%20Photos/Profile%20Pictures/'; // Url for image of user
        this.imgUrlTail = '_LThumb.jpg';
    }
    // tslint:disable-next-line:use-life-cycle-interface
    AppTopBarComponent.prototype.ngOnInit = function () {
        var _this = this;
        var EligibleCompetency = [
            'C7',
            'C8',
            'C9.1',
            'C9.2',
            'C10',
            'C11',
            'C12'
        ];
        // Subscribing behavior Subject to get the name of user. Created 'name' as Behavior Subject in SharedserviceService.
        this.sharedData.name.subscribe(function (x) {
            _this.nameOfUserTop = x;
            _this.sharedData.nameOfLoggedInUser = _this.nameOfUserTop;
            // this.competencyOfLoggedInUser=x.competency;
            // this.user=x.mid;
        });
        // Subscribing behavior Subject to get the mid of user. Created 'mid' as Behavior Subject in SharedserviceService.
        this.sharedData.mid.subscribe(function (x) {
            _this.midOfUserTop = x;
            _this.sharedData.midOfLoggedInUser = _this.midOfUserTop;
        });
        // Subscribing behavior Subject to get the roleID of user. Created 'roleID' as Behavior Subject in SharedserviceService.
        this.sharedData.roleID.subscribe(function (x) {
            _this.roleIDOfUser = x;
            _this.sharedData.roleIDOfLoggedInUser = _this.roleIDOfUser;
            if (_this.roleIDOfUser === '1' ||
                EligibleCompetency.includes(_this.competencyOfUser)) {
                _this.roleName = 'Admin';
            }
            else {
                _this.roleName = 'User';
            }
            // if(this.roleIDOfUser!=null && this.competencyOfUser!=null)
            if (_this.roleIDOfUser != null && _this.competencyOfUser != null) {
                _this.displayOfFunnel();
            }
        });
        // Subscribing behavior Subject to get the competency of user. Created 'competency' as Behavior Subject in SharedserviceService.
        this.sharedData.competency.subscribe(function (x) {
            _this.competencyOfUser = x;
            _this.sharedData.competencyOfLoggedInUser = _this.competencyOfUser;
        });
    };
    // If roleIDOfUser and competencyOfUser is not null, then displayOfFunnel() is called.
    AppTopBarComponent.prototype.displayOfFunnel = function () {
        // if(this.roleIDOfLoggedInUser===1|| EligibleCompetency.includes(this.competencyOfLoggedInUser))
        // If roleID=1(Admin) and Competency >=C7, then Funnel icon(Select filters) and Settings icon(Save Preference) is visible.
        var EligibleCompetency = [
            'C7',
            'C8',
            'C9.1',
            'C9.2',
            'C10',
            'C11',
            'C12'
        ];
        if (this.roleIDOfUser === '1' ||
            EligibleCompetency.includes(this.competencyOfUser)) {
            this.DisplayFunnel = true;
        }
        else {
            this.DisplayFunnel = false;
        }
    };
    AppTopBarComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-topbar',
            template: __webpack_require__(/*! ./app.topbar.component.html */ "./src/app/app.topbar.component.html")
        }),
        __metadata("design:paramtypes", [_app_component__WEBPACK_IMPORTED_MODULE_1__["AppComponent"],
            _Services_sharedservice_service__WEBPACK_IMPORTED_MODULE_2__["SharedserviceService"],
            _Modules_FetaureModules_admin_Services_admin_service_service__WEBPACK_IMPORTED_MODULE_3__["AdminServiceService"]])
    ], AppTopBarComponent);
    return AppTopBarComponent;
}());



/***/ }),

/***/ "./src/environments/environment.ts":
/*!*****************************************!*\
  !*** ./src/environments/environment.ts ***!
  \*****************************************/
/*! exports provided: environment */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "environment", function() { return environment; });
// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.
var environment = {
    production: false,
    baseUrl: 'https://localhost:44323'
};


/***/ }),

/***/ "./src/guards/auth.guard.ts":
/*!**********************************!*\
  !*** ./src/guards/auth.guard.ts ***!
  \**********************************/
/*! exports provided: AuthGuard */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AuthGuard", function() { return AuthGuard; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var src_app_Services_sharedservice_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/Services/sharedservice.service */ "./src/app/Services/sharedservice.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var AuthGuard = /** @class */ (function () {
    function AuthGuard(router, sharedData) {
        this.router = router;
        this.sharedData = sharedData;
    }
    AuthGuard.prototype.canActivate = function (next, state) {
        var EligibleCompetency = [
            'C7',
            'C8',
            'C9.1',
            'C9.2',
            'C10',
            'C11',
            'C12'
        ];
        // tslint:disable-next-line:max-line-length
        if (this.sharedData.roleIDOfLoggedInUser === '1' ||
            EligibleCompetency.includes(this.sharedData.competencyOfLoggedInUser)) {
            return true;
        }
        else {
            this.router.navigate(['/dashboard']);
            return false;
        }
    };
    AuthGuard = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])({
            providedIn: 'root'
        }),
        __metadata("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_1__["Router"],
            src_app_Services_sharedservice_service__WEBPACK_IMPORTED_MODULE_2__["SharedserviceService"]])
    ], AuthGuard);
    return AuthGuard;
}());



/***/ }),

/***/ "./src/main.ts":
/*!*********************!*\
  !*** ./src/main.ts ***!
  \*********************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/platform-browser-dynamic */ "./node_modules/@angular/platform-browser-dynamic/fesm5/platform-browser-dynamic.js");
/* harmony import */ var _app_app_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./app/app.module */ "./src/app/app.module.ts");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./environments/environment */ "./src/environments/environment.ts");




if (_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].production) {
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["enableProdMode"])();
}
Object(_angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_1__["platformBrowserDynamic"])().bootstrapModule(_app_app_module__WEBPACK_IMPORTED_MODULE_2__["AppModule"])
    .catch();


/***/ }),

/***/ 0:
/*!***************************!*\
  !*** multi ./src/main.ts ***!
  \***************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! D:\Ozone TFS\CapacityInsights\Mindtree.CapacityInsights.UI\src\main.ts */"./src/main.ts");


/***/ })

},[[0,"runtime","vendor"]]]);
//# sourceMappingURL=main.js.map