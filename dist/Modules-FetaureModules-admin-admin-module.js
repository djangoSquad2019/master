(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["Modules-FetaureModules-admin-admin-module"],{

/***/ "./src/app/Modules/FetaureModules/admin/add-admin/add-admin.component.css":
/*!********************************************************************************!*\
  !*** ./src/app/Modules/FetaureModules/admin/add-admin/add-admin.component.css ***!
  \********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "strong {\r\n    display: block;\r\n}\r\nspan {\r\n    margin-bottom: 20px;\r\n    display: block;\r\n}\r\n.floatAdmin {\r\n    float: left;\r\n}\r\n.mainDiv {\r\n    border-radius: 2px;\r\n    background-color: white;\r\n    padding: 10px;\r\n    box-shadow: 0 1px 3px 0 rgba(0, 0, 0, 0.2), 0 1px 1px 0 rgba(0, 0, 0, 0.14),\r\n        0 2px 1px -1px rgba(0, 0, 0, 0.12);\r\n}\r\n.AdminHeader {\r\n    font-size: 20px;\r\n    display: inline-block;\r\n    border-bottom: 2px solid;\r\n    color: #1a237e;\r\n}\r\n.labelColor {\r\n    color: black;\r\n}\r\n.btnSubmit {\r\n    margin: 2px;\r\n    margin-left: 74px;\r\n}\r\n.btnCancel {\r\n    margin: 2px;\r\n}\r\n.divHeader {\r\n    font-size: 18px;\r\n}\r\n.PaddingRight5 {\r\n    padding-right: 5px;\r\n}\r\n.error {\r\n    color: red;\r\n}\r\n\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvTW9kdWxlcy9GZXRhdXJlTW9kdWxlcy9hZG1pbi9hZGQtYWRtaW4vYWRkLWFkbWluLmNvbXBvbmVudC5jc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7SUFDSSxlQUFlO0NBQ2xCO0FBQ0Q7SUFDSSxvQkFBb0I7SUFDcEIsZUFBZTtDQUNsQjtBQUVEO0lBQ0ksWUFBWTtDQUNmO0FBQ0Q7SUFDSSxtQkFBbUI7SUFDbkIsd0JBQXdCO0lBQ3hCLGNBQWM7SUFDZDsyQ0FDdUM7Q0FDMUM7QUFDRDtJQUNJLGdCQUFnQjtJQUNoQixzQkFBc0I7SUFDdEIseUJBQXlCO0lBQ3pCLGVBQWU7Q0FDbEI7QUFDRDtJQUNJLGFBQWE7Q0FDaEI7QUFDRDtJQUNJLFlBQVk7SUFDWixrQkFBa0I7Q0FDckI7QUFDRDtJQUNJLFlBQVk7Q0FDZjtBQUVEO0lBQ0ksZ0JBQWdCO0NBQ25CO0FBRUQ7SUFDSSxtQkFBbUI7Q0FDdEI7QUFFRDtJQUNJLFdBQVc7Q0FDZCIsImZpbGUiOiJzcmMvYXBwL01vZHVsZXMvRmV0YXVyZU1vZHVsZXMvYWRtaW4vYWRkLWFkbWluL2FkZC1hZG1pbi5jb21wb25lbnQuY3NzIiwic291cmNlc0NvbnRlbnQiOlsic3Ryb25nIHtcclxuICAgIGRpc3BsYXk6IGJsb2NrO1xyXG59XHJcbnNwYW4ge1xyXG4gICAgbWFyZ2luLWJvdHRvbTogMjBweDtcclxuICAgIGRpc3BsYXk6IGJsb2NrO1xyXG59XHJcblxyXG4uZmxvYXRBZG1pbiB7XHJcbiAgICBmbG9hdDogbGVmdDtcclxufVxyXG4ubWFpbkRpdiB7XHJcbiAgICBib3JkZXItcmFkaXVzOiAycHg7XHJcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiB3aGl0ZTtcclxuICAgIHBhZGRpbmc6IDEwcHg7XHJcbiAgICBib3gtc2hhZG93OiAwIDFweCAzcHggMCByZ2JhKDAsIDAsIDAsIDAuMiksIDAgMXB4IDFweCAwIHJnYmEoMCwgMCwgMCwgMC4xNCksXHJcbiAgICAgICAgMCAycHggMXB4IC0xcHggcmdiYSgwLCAwLCAwLCAwLjEyKTtcclxufVxyXG4uQWRtaW5IZWFkZXIge1xyXG4gICAgZm9udC1zaXplOiAyMHB4O1xyXG4gICAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xyXG4gICAgYm9yZGVyLWJvdHRvbTogMnB4IHNvbGlkO1xyXG4gICAgY29sb3I6ICMxYTIzN2U7XHJcbn1cclxuLmxhYmVsQ29sb3Ige1xyXG4gICAgY29sb3I6IGJsYWNrO1xyXG59XHJcbi5idG5TdWJtaXQge1xyXG4gICAgbWFyZ2luOiAycHg7XHJcbiAgICBtYXJnaW4tbGVmdDogNzRweDtcclxufVxyXG4uYnRuQ2FuY2VsIHtcclxuICAgIG1hcmdpbjogMnB4O1xyXG59XHJcblxyXG4uZGl2SGVhZGVyIHtcclxuICAgIGZvbnQtc2l6ZTogMThweDtcclxufVxyXG5cclxuLlBhZGRpbmdSaWdodDUge1xyXG4gICAgcGFkZGluZy1yaWdodDogNXB4O1xyXG59XHJcblxyXG4uZXJyb3Ige1xyXG4gICAgY29sb3I6IHJlZDtcclxufVxyXG4iXX0= */"

/***/ }),

/***/ "./src/app/Modules/FetaureModules/admin/add-admin/add-admin.component.html":
/*!*********************************************************************************!*\
  !*** ./src/app/Modules/FetaureModules/admin/add-admin/add-admin.component.html ***!
  \*********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<p class=\"divHeader\">\n    <i class=\"fa fa-user-plus PaddingRight5\" aria-hidden=\"true\"></i> Add Admin\n</p>\n<div class=\"row mSpacer5\"></div>\n<div class=\"mainDiv\">\n    <form [formGroup]=\"adminForm\" (ngSubmit)=\"submit()\" class=\"p-3 bg-faded\">\n        <div class=\"form-group ui-g\">\n            <div class=\"ui-lg-20\">\n                <label for=\"MID\" class=\"col-sm-3 col-form-label labelColor\"\n                    ><strong>MID</strong></label\n                >\n            </div>\n            <div class=\"ui-lg-6\">\n                <input\n                    id=\"MID\"\n                    type=\"text\"\n                    class=\"form-control\"\n                    formControlName=\"MID\"\n                />\n                <div\n                    *ngIf=\"submitted && adminForm.controls.MID.errors\"\n                    class=\"error\"\n                >\n               \n                    <div *ngIf=\"adminForm.controls.MID.errors.required\">\n                        Your MID is required\n                    </div>\n                    <div *ngIf=\"adminForm.controls.MID.errors.pattern\">\n                        MID not valid. Please enter a valid MID\n                    </div>\n                    </div>\n           \n            </div>\n        </div>\n        <div class=\"form-group  ui-g\">\n            <div class=\"ui-lg-20\">\n                <label for=\"Role\" class=\"col-sm-3 col-form-label labelColor\"\n                    ><strong>Role</strong></label\n                >\n            </div>\n            <div class=\"ui-lg-6\">\n                <select\n                    id=\"Role\"\n                    formControlName=\"Role\"\n                    name=\"Role\"\n                    class=\"ui-lg-6\"\n                >\n                    <option *ngFor=\"let r of roles\" [ngValue]=\"r\">{{\n                        r\n                    }}</option>\n                </select>\n                <div\n                    *ngIf=\"submitted && adminForm.controls.Role.errors\"\n                    class=\"error\"\n                >\n                    <div *ngIf=\"adminForm.controls.Role.errors.required\">\n                        A role is required\n                    </div>\n                </div>\n            </div>\n        </div>\n        <div class=\"ui-lg-6\">\n            <button\n                pButton\n                type=\"submit\"\n                label=\"Save\"\n                class=\"ui-md-2 ui btn button-fetch\"\n            ></button>\n            <button\n                pButton\n                type=\"button\"\n                class=\"ui-md-2 btn button-cancel\"\n                label=\"Cancel\"\n                (click)=\"cancel()\"\n            ></button>\n        </div>\n    </form>\n</div>\n<div>\n    <div *ngIf=\"submitted\" class=\"labelColor\">\n        <!-- <strong>MID:</strong>\n    <span>{{ adminForm.controls.MID.value }}</span>\n    \n    <strong>Role:</strong>\n    <span>{{ adminForm.controls.Role.value }}</span>\n    </div> -->\n    </div>\n\n    <!-- Success message for addition of admin -->\n    <p-dialog\n        header=\"Confirmation\"\n        [(visible)]=\"msgDialog\"\n        [modal]=\"true\"\n        (onHide)=\"cancel()\"\n        [responsive]=\"true\"\n        [width]=\"700\"\n        [minY]=\"70\"\n        [baseZIndex]=\"10000\"\n    >\n     <div style=\"margin-top:20px;margin-bottom:20px\">\n        <span style=\"font-size: 15px;margin:60px\" class=\"text-modal\"\n        >You have successfully added an admin!! </span>\n        </div>   \n        \n    </p-dialog>\n\n    <!-- Warning message for adding duplicate admin -->\n    <p-dialog\n        header=\"Error occured\"\n        [(visible)]=\"msgWarningDialog\"\n        [modal]=\"true\"\n        (onHide)=\"cancel()\"\n        [responsive]=\"true\"\n        [width]=\"700\"\n        [minY]=\"70\"\n        [baseZIndex]=\"10000\"\n    >\n        <div style=\"margin-top:20px;margin-bottom:20px\">\n        <span style=\"font-size: 15px;margin:60px\" class=\"text-modal\"\n        >{{displayMessage}}</span>\n        </div> \n    </p-dialog>\n</div>\n"

/***/ }),

/***/ "./src/app/Modules/FetaureModules/admin/add-admin/add-admin.component.ts":
/*!*******************************************************************************!*\
  !*** ./src/app/Modules/FetaureModules/admin/add-admin/add-admin.component.ts ***!
  \*******************************************************************************/
/*! exports provided: AddAdminComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AddAdminComponent", function() { return AddAdminComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _Services_admin_service_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../Services/admin-service.service */ "./src/app/Modules/FetaureModules/admin/Services/admin-service.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var AddAdminComponent = /** @class */ (function () {
    function AddAdminComponent(formBuilder, route, data) {
        this.formBuilder = formBuilder;
        this.route = route;
        this.data = data;
        // @Output() submitForm = new EventEmitter<AddAdminComponent>();
        this.cancelForm = new _angular_core__WEBPACK_IMPORTED_MODULE_0__["EventEmitter"]();
        this.roles = ['Admin'];
        this.default = 'Admin'; // default value for Role dropdown
        this.submitted = false;
        this.success = false;
        this.show = true;
        this.roleadded = [];
        this.msg = null;
        this.msgs = [];
        this.msgDialog = false;
        this.empnamePattern = '^[Mm]{1}\\d{7}';
        this.msgWarningDialog = false;
        this.errorMsgs = [];
        this.displayMessage = ' ';
    }
    AddAdminComponent.prototype.ngOnInit = function () {
        // check the values of form controls and validations
        this.adminForm = this.formBuilder.group({
            MID: [
                '',
                [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].pattern(this.empnamePattern)]
            ],
            Role: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
            roles: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](null)
        });
        // set the values Role dropdown
        this.adminForm.controls['Role'].setValue(this.default, {
            onlySelf: true
        });
    };
    // function for submitting the form.
    AddAdminComponent.prototype.submit = function () {
        var _this = this;
        this.submitted = true;
        if (!this.adminForm.valid) {
            return;
        }
        this.data
            .saveRole(this.adminForm.controls.MID.value, this.adminForm.controls.Role.value)
            .subscribe(function (data) {
            _this.msgDialog = true;
            _this.msgs = [];
            _this.show = true;
        }, function (error) {
            if (error.status === 400) {
                _this.submitted = true;
                _this.msgWarningDialog = true;
                _this.errorMsgs = [];
                _this.show = true;
                _this.displayMessage = 'This MID is already registered';
            }
            else if (error.status === 501) {
                // Status code 501 (Not Implemented)
                _this.submitted = true;
                _this.msgWarningDialog = true;
                _this.errorMsgs = [];
                _this.show = true;
                _this.displayMessage = 'This MID does not exist';
            }
            else if (error.status === 401) {
                _this.route.navigate(['/core/AccessDenied']);
            }
            else if (error.status === 0) {
                _this.route.navigate(['/core/ServerDown']);
            }
            else {
                _this.route.navigate(['/core/InternalServerError']);
            }
        });
    };
    // function to cancel the form.Navigate to admin page
    AddAdminComponent.prototype.cancel = function () {
        this.submitted = false;
        this.adminForm.patchValue({ 'MID': ' ' });
        this.msgDialog = false;
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Output"])(),
        __metadata("design:type", Object)
    ], AddAdminComponent.prototype, "cancelForm", void 0);
    AddAdminComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-add-admin',
            template: __webpack_require__(/*! ./add-admin.component.html */ "./src/app/Modules/FetaureModules/admin/add-admin/add-admin.component.html"),
            styles: [__webpack_require__(/*! ./add-admin.component.css */ "./src/app/Modules/FetaureModules/admin/add-admin/add-admin.component.css")]
        }),
        __metadata("design:paramtypes", [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormBuilder"],
            _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"],
            _Services_admin_service_service__WEBPACK_IMPORTED_MODULE_3__["AdminServiceService"]])
    ], AddAdminComponent);
    return AddAdminComponent;
}());



/***/ }),

/***/ "./src/app/Modules/FetaureModules/admin/admin-routing.module.ts":
/*!**********************************************************************!*\
  !*** ./src/app/Modules/FetaureModules/admin/admin-routing.module.ts ***!
  \**********************************************************************/
/*! exports provided: AdminRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AdminRoutingModule", function() { return AdminRoutingModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _admin_admin_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./admin/admin.component */ "./src/app/Modules/FetaureModules/admin/admin/admin.component.ts");
/* harmony import */ var _add_admin_add_admin_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./add-admin/add-admin.component */ "./src/app/Modules/FetaureModules/admin/add-admin/add-admin.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};




// the routes array is feeded with routes within the components in Admin module
var routes = [
    { path: '', component: _admin_admin_component__WEBPACK_IMPORTED_MODULE_2__["AdminComponent"] },
    { path: 'addadmin', component: _add_admin_add_admin_component__WEBPACK_IMPORTED_MODULE_3__["AddAdminComponent"] }
];
var AdminRoutingModule = /** @class */ (function () {
    function AdminRoutingModule() {
    }
    AdminRoutingModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"].forChild(routes)],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"]]
        })
    ], AdminRoutingModule);
    return AdminRoutingModule;
}());



/***/ }),

/***/ "./src/app/Modules/FetaureModules/admin/admin.module.ts":
/*!**************************************************************!*\
  !*** ./src/app/Modules/FetaureModules/admin/admin.module.ts ***!
  \**************************************************************/
/*! exports provided: AdminModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AdminModule", function() { return AdminModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _admin_routing_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./admin-routing.module */ "./src/app/Modules/FetaureModules/admin/admin-routing.module.ts");
/* harmony import */ var _admin_admin_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./admin/admin.component */ "./src/app/Modules/FetaureModules/admin/admin/admin.component.ts");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _shared_shared_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../shared/shared.module */ "./src/app/Modules/shared/shared.module.ts");
/* harmony import */ var _add_admin_add_admin_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./add-admin/add-admin.component */ "./src/app/Modules/FetaureModules/admin/add-admin/add-admin.component.ts");
/* harmony import */ var _Services_admin_service_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./Services/admin-service.service */ "./src/app/Modules/FetaureModules/admin/Services/admin-service.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};









var AdminModule = /** @class */ (function () {
    function AdminModule() {
    }
    AdminModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            declarations: [_admin_admin_component__WEBPACK_IMPORTED_MODULE_3__["AdminComponent"], _add_admin_add_admin_component__WEBPACK_IMPORTED_MODULE_6__["AddAdminComponent"]],
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"],
                _admin_routing_module__WEBPACK_IMPORTED_MODULE_2__["AdminRoutingModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormsModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_4__["ReactiveFormsModule"],
                _shared_shared_module__WEBPACK_IMPORTED_MODULE_5__["SharedModule"]
            ],
            providers: [_Services_admin_service_service__WEBPACK_IMPORTED_MODULE_7__["AdminServiceService"]]
        })
    ], AdminModule);
    return AdminModule;
}());



/***/ }),

/***/ "./src/app/Modules/FetaureModules/admin/admin/admin.component.css":
/*!************************************************************************!*\
  !*** ./src/app/Modules/FetaureModules/admin/admin/admin.component.css ***!
  \************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".PaddingRight5 {\r\n    padding-right: 5px;\r\n}\r\n\r\nimg {\r\n    height: 100px;\r\n    width: 100px;\r\n}\r\n\r\n.adminColor {\r\n    color: black;\r\n    font-size: 14px;\r\n    font-weight: 500;\r\n    margin-bottom: 4px;\r\n    text-align: center;\r\n    white-space: nowrap;\r\n    width: auto;\r\n    overflow: hidden;\r\n    text-overflow: ellipsis;\r\n    margin-bottom: 5px;\r\n}\r\n\r\n.divHeader {\r\n    font-size: 18px;\r\n    display: inline-block;\r\n}\r\n\r\n.PaddingRight5 {\r\n    padding-right: 5px;\r\n}\r\n\r\n.tabmenu {\r\n    float: right;\r\n}\r\n\r\n.block-admin {\r\n    height: 230px;\r\n    width: 180px;\r\n    display: inline-block;\r\n    margin: 10px;\r\n    background: white;\r\n    padding: 10px;\r\n}\r\n\r\n.delete-admin{\r\n    margin-bottom: 0px;\r\n    float: right;\r\n    color: #E74C3C;\r\n    font-size: 20px;\r\n    margin-top: -10px;\r\n    cursor: pointer;\r\n}\r\n\r\n.button-active-inactive{\r\n    margin-top: -5px;\r\n    height: 30px;\r\n    width: 100px;\r\n    font-size: 14px;\r\n    line-height: 14px;\r\n}\r\n\r\n.confirmation{\r\n    color: blue;\r\n    font-weight: 400;\r\n    text-align: center;\r\n    white-space: nowrap;\r\n    vertical-align: middle;\r\n}\r\n\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvTW9kdWxlcy9GZXRhdXJlTW9kdWxlcy9hZG1pbi9hZG1pbi9hZG1pbi5jb21wb25lbnQuY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0lBQ0ksbUJBQW1CO0NBQ3RCOztBQUVEO0lBQ0ksY0FBYztJQUNkLGFBQWE7Q0FDaEI7O0FBRUQ7SUFDSSxhQUFhO0lBQ2IsZ0JBQWdCO0lBQ2hCLGlCQUFpQjtJQUNqQixtQkFBbUI7SUFDbkIsbUJBQW1CO0lBQ25CLG9CQUFvQjtJQUNwQixZQUFZO0lBQ1osaUJBQWlCO0lBQ2pCLHdCQUF3QjtJQUN4QixtQkFBbUI7Q0FDdEI7O0FBRUQ7SUFDSSxnQkFBZ0I7SUFDaEIsc0JBQXNCO0NBQ3pCOztBQUNEO0lBQ0ksbUJBQW1CO0NBQ3RCOztBQUNEO0lBQ0ksYUFBYTtDQUNoQjs7QUFDRDtJQUNJLGNBQWM7SUFDZCxhQUFhO0lBQ2Isc0JBQXNCO0lBQ3RCLGFBQWE7SUFDYixrQkFBa0I7SUFDbEIsY0FBYztDQUNqQjs7QUFDRDtJQUNJLG1CQUFtQjtJQUNuQixhQUFhO0lBQ2IsZUFBZTtJQUNmLGdCQUFnQjtJQUNoQixrQkFBa0I7SUFDbEIsZ0JBQWdCO0NBQ25COztBQUNEO0lBQ0ksaUJBQWlCO0lBQ2pCLGFBQWE7SUFDYixhQUFhO0lBQ2IsZ0JBQWdCO0lBQ2hCLGtCQUFrQjtDQUNyQjs7QUFDRDtJQUNJLFlBQVk7SUFDWixpQkFBaUI7SUFDakIsbUJBQW1CO0lBQ25CLG9CQUFvQjtJQUNwQix1QkFBdUI7Q0FDMUIiLCJmaWxlIjoic3JjL2FwcC9Nb2R1bGVzL0ZldGF1cmVNb2R1bGVzL2FkbWluL2FkbWluL2FkbWluLmNvbXBvbmVudC5jc3MiLCJzb3VyY2VzQ29udGVudCI6WyIuUGFkZGluZ1JpZ2h0NSB7XHJcbiAgICBwYWRkaW5nLXJpZ2h0OiA1cHg7XHJcbn1cclxuXHJcbmltZyB7XHJcbiAgICBoZWlnaHQ6IDEwMHB4O1xyXG4gICAgd2lkdGg6IDEwMHB4O1xyXG59XHJcblxyXG4uYWRtaW5Db2xvciB7XHJcbiAgICBjb2xvcjogYmxhY2s7XHJcbiAgICBmb250LXNpemU6IDE0cHg7XHJcbiAgICBmb250LXdlaWdodDogNTAwO1xyXG4gICAgbWFyZ2luLWJvdHRvbTogNHB4O1xyXG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gICAgd2hpdGUtc3BhY2U6IG5vd3JhcDtcclxuICAgIHdpZHRoOiBhdXRvO1xyXG4gICAgb3ZlcmZsb3c6IGhpZGRlbjtcclxuICAgIHRleHQtb3ZlcmZsb3c6IGVsbGlwc2lzO1xyXG4gICAgbWFyZ2luLWJvdHRvbTogNXB4O1xyXG59XHJcblxyXG4uZGl2SGVhZGVyIHtcclxuICAgIGZvbnQtc2l6ZTogMThweDtcclxuICAgIGRpc3BsYXk6IGlubGluZS1ibG9jaztcclxufVxyXG4uUGFkZGluZ1JpZ2h0NSB7XHJcbiAgICBwYWRkaW5nLXJpZ2h0OiA1cHg7XHJcbn1cclxuLnRhYm1lbnUge1xyXG4gICAgZmxvYXQ6IHJpZ2h0O1xyXG59XHJcbi5ibG9jay1hZG1pbiB7XHJcbiAgICBoZWlnaHQ6IDIzMHB4O1xyXG4gICAgd2lkdGg6IDE4MHB4O1xyXG4gICAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xyXG4gICAgbWFyZ2luOiAxMHB4O1xyXG4gICAgYmFja2dyb3VuZDogd2hpdGU7XHJcbiAgICBwYWRkaW5nOiAxMHB4O1xyXG59XHJcbi5kZWxldGUtYWRtaW57XHJcbiAgICBtYXJnaW4tYm90dG9tOiAwcHg7XHJcbiAgICBmbG9hdDogcmlnaHQ7XHJcbiAgICBjb2xvcjogI0U3NEMzQztcclxuICAgIGZvbnQtc2l6ZTogMjBweDtcclxuICAgIG1hcmdpbi10b3A6IC0xMHB4O1xyXG4gICAgY3Vyc29yOiBwb2ludGVyO1xyXG59XHJcbi5idXR0b24tYWN0aXZlLWluYWN0aXZle1xyXG4gICAgbWFyZ2luLXRvcDogLTVweDtcclxuICAgIGhlaWdodDogMzBweDtcclxuICAgIHdpZHRoOiAxMDBweDtcclxuICAgIGZvbnQtc2l6ZTogMTRweDtcclxuICAgIGxpbmUtaGVpZ2h0OiAxNHB4O1xyXG59XHJcbi5jb25maXJtYXRpb257XHJcbiAgICBjb2xvcjogYmx1ZTtcclxuICAgIGZvbnQtd2VpZ2h0OiA0MDA7XHJcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgICB3aGl0ZS1zcGFjZTogbm93cmFwO1xyXG4gICAgdmVydGljYWwtYWxpZ246IG1pZGRsZTtcclxufVxyXG4iXX0= */"

/***/ }),

/***/ "./src/app/Modules/FetaureModules/admin/admin/admin.component.html":
/*!*************************************************************************!*\
  !*** ./src/app/Modules/FetaureModules/admin/admin/admin.component.html ***!
  \*************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<p class=\"divHeader\">\n    <i class=\"fa fa-user-circle-o PaddingRight5\" aria-hidden=\"true\"></i> Admin\n</p>\n\n<p-tabMenu [model]=\"items\" class=\"tabmenu\"></p-tabMenu>\n\n<div class=\"spinner-center\" *ngIf=\"isViewAdmin\">\n    <p>\n        Please wait while the data is loaded ...\n        <span class=\"fa fa-spinner fa-spin fa-2x\"></span>\n    </p>\n</div>\n<div *ngIf=\"users\">\n    <div class=\"block-admin\" *ngFor=\"let user of users\">\n            <img src=\"{{ imgUrlHeader +'M'+ user.employeeID + imgUrlTail }}\" class=\"mx-auto d-block\" />\n            <p class=\"adminColor\">{{ user.employeeID }}</p>\n            <p class=\"adminColor\" pTooltip=\"{{ user.name }}\" tooltipPosition=\"top\">{{ user.name }}</p>\n            <p class=\"adminColor\">{{user.competency}}</p>\n            <hr/>\n            <i class=\"fa fa-trash delete-admin\" (click)=\"showConfirmDialog(user)\" ></i> \n           <!-- <button class=\"btn btn-success button-active-inactive\">Active</button>-->\n    </div>\n</div>\n<p-dialog\n        header=\"Confirmation\"\n        [(visible)]=\"msgDialog\"\n        [modal]=\"true\"\n        [responsive]=\"true\"\n        [width]=\"700\"\n        [minY]=\"70\"\n        [baseZIndex]=\"10000\"\n    >\n    <div style=\"margin-top:20px;margin-bottom:20px\">\n        <span style=\"font-size: 15px;margin:60px\" class=\"text-modal\"\n        >You have successfully deleted an admin!! </span>\n        </div>        \n    </p-dialog>\n\n    <p-dialog \nheader=\"Confirmation\" \n[(visible)]=\"displayConfirm\" \n[modal]=\"true\" \n[responsive]=\"true\" \n[style]=\"{width: '700px'}\" \n[minY]=\"70\" \n[maximizable]=\"false\" \n[baseZIndex]=\"10000\">\n<div style=\"margin-top:20px;margin-bottom:20px\">\n<span style=\"font-size: 15px;margin:60px\" class=\"text-modal\"\n>Are you sure you want to delete ? </span>\n</div>\n<p-footer>\n<button  class=\"btn button-fetch  border-radius-xs\" (click)=\"deleteadmin()\" >Yes</button>\n<button (click)=\"displayConfirm=false\"  class=\"btn button-save  border-radius-xs\">NO</button>\n</p-footer>\n</p-dialog>\n\n\n"

/***/ }),

/***/ "./src/app/Modules/FetaureModules/admin/admin/admin.component.ts":
/*!***********************************************************************!*\
  !*** ./src/app/Modules/FetaureModules/admin/admin/admin.component.ts ***!
  \***********************************************************************/
/*! exports provided: AdminComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AdminComponent", function() { return AdminComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _Services_admin_service_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../Services/admin-service.service */ "./src/app/Modules/FetaureModules/admin/Services/admin-service.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var AdminComponent = /** @class */ (function () {
    function AdminComponent(route, data) {
        this.route = route;
        this.data = data;
        this.show = false;
        this.roleadded = [];
        this.msg = null;
        this.msgs = [];
        this.submitted = false;
        this.msgDialog = false;
        this.imgUrlHeader = 'https://social.mindtree.com/User%20Photos/Profile%20Pictures/';
        this.imgUrlTail = '_LThumb.jpg';
        this.isViewAdmin = true;
        this.displayConfirm = false;
    }
    // tab menu function
    AdminComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.items = [
            {
                label: 'Add Admin',
                icon: 'fa fa-user',
                command: function (event) { return _this.route.navigate(['/admin/addadmin']); }
            }
        ];
        this.GetAdmins();
    };
    // function to fetch all admins
    AdminComponent.prototype.GetAdmins = function () {
        var _this = this;
        this.data.getUsers().subscribe(function (data) {
            _this.isViewAdmin = false;
            _this.users = data;
        }, function (error) {
            if (error.status === 0) {
                _this.route.navigate(['/core/ServerDown']);
            }
            else if (error.status === 401) {
                _this.route.navigate(['/core/AccessDenied']);
            }
            else {
                _this.route.navigate(['/core/InternalServerError']);
            }
        });
    };
    AdminComponent.prototype.deleteadmin = function () {
        var _this = this;
        this.displayConfirm = false;
        this.data.DeleteRole(this.saveDeleteData).subscribe(function (data) {
            _this.submitted = true;
            _this.msgDialog = true;
            _this.msgs = [];
            _this.show = true;
            _this.GetAdmins();
        });
    };
    AdminComponent.prototype.showConfirmDialog = function (user) {
        this.saveDeleteData = user;
        this.displayConfirm = true;
    };
    AdminComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-admin',
            template: __webpack_require__(/*! ./admin.component.html */ "./src/app/Modules/FetaureModules/admin/admin/admin.component.html"),
            styles: [__webpack_require__(/*! ./admin.component.css */ "./src/app/Modules/FetaureModules/admin/admin/admin.component.css")]
        }),
        __metadata("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_1__["Router"], _Services_admin_service_service__WEBPACK_IMPORTED_MODULE_2__["AdminServiceService"]])
    ], AdminComponent);
    return AdminComponent;
}());



/***/ })

}]);
//# sourceMappingURL=Modules-FetaureModules-admin-admin-module.js.map